# SpiceIM Client端（Android版本）

#### 介绍
SpiceIM Client端Android版是与SpiceIM Server配套系统，SpiceIM Server是一个基于NIO（V1）、AIO（V2）的客户、服务器端编程框架及IM基础服务能力开发平台，使用SpiceIM Server可以确保你快速和简单的开发出一个网络应用（包括推送服务，IM服务，业务插件服务）。SpiceIM Server相当简化和流程化了TCP的socket网络应用的编程开发过程。通过利用云端与客户端之间建立稳定、可靠的长连接来为开发者提供客户端应用与平台交互通道，开发者只需关注业务开发，无须关注底层网络交互。SpiceIM客户端包括Android及WebIM版本。同时征集ios app以及h5志愿开发人员共同完善SpiceIM其他接入端。欢迎开发人员加入：QQ群 218162018 QQ 25897555 微信 allenlei2010 另外欢迎提供公网服务器资源供平台演示环境部署。
PS：目前tio框架很火，笔者的android版本客户端同时也兼容接入tio。
欢迎star，欢迎捐赠！！！捐赠将用于平台演示环境资源租赁部署。
![输入图片说明](https://images.gitee.com/uploads/images/2020/0730/085310_09f2b03a_409461.jpeg "微信图片_20200730085141.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0730/085337_c5053cb8_409461.jpeg "微信图片_20200730085150.jpg")
#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
