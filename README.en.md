# SpiceIM Client端（Android版本）

#### Description
SpiceIM Client端Android版是与SpiceIM Server配套系统，SpiceIM Server是一个基于NIO（V1）、AIO（V2）的客户、服务器端编程框架及IM基础服务能力开发平台，使用SpiceIM Server可以确保你快速和简单的开发出一个网络应用（包括推送服务，IM服务，业务插件服务）。SpiceIM Server相当简化和流程化了TCP的socket网络应用的编程开发过程。通过利用云端与客户端之间建立稳定、可靠的长连接来为开发者提供客户端应用与平台交互通道，开发者只需关注业务开发，无须关注底层网络交互。SpiceIM客户端包括Android及WebIM版本。同时征集ios app以及h5志愿开发人员共同完善SpiceIM其他接入端。欢迎开发人员加入：QQ群 218162018 QQ 25897555 微信 allenlei2010 另外欢迎提供公网服务器资源供平台演示环境部署。

PS：目前tio框架很火，笔者的android版本客户端同时也兼容接入tio。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
