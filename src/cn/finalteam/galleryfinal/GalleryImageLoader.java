/*
 * Copyright (C) 2014 pengjianbo(pengjianbosoft@gmail.com), Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package cn.finalteam.galleryfinal;

import android.graphics.Bitmap;

import android.widget.ImageView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.spice.im.R;

/**
 * Desction:
 * Author:pengjianbo
 * Date:15/10/10 下午5:52
 */
public class GalleryImageLoader implements cn.finalteam.galleryfinal.ImageLoader {
	private com.nostra13.universalimageloader.core.ImageLoader imageLoader;
	@Override
	public com.nostra13.universalimageloader.core.ImageLoader getImageLoader(){
		imageLoader = ImageLoader.getInstance();
		return imageLoader;
	}
    @Override
    public void displayImage(final ImageView imageView, String url) {
    	DisplayImageOptions options = new DisplayImageOptions.Builder()
		.showStubImage(R.drawable.ic_gf_default_photo)			// 设置图片下载期间显示的图片
		.showImageForEmptyUri(R.drawable.ic_gf_default_photo)	// 设置图片Uri为空或是错误的时候显示的图片
		.showImageOnFail(R.drawable.ic_gf_default_photo)		// 设置图片加载或解码过程中发生错误显示的图片	
		.cacheInMemory(false)						// 设置下载的图片是否缓存在内存中
		.cacheOnDisc(true)							// 设置下载的图片是否缓存在SD卡中
//		.cacheOnDisk(true)
		.displayer(new RoundedBitmapDisplayer(20))	// 设置成圆角图片
		.bitmapConfig(Bitmap.Config.RGB_565)//配置bitmapConfig为Bitmap.Config.RGB_565，因为默认是ARGB_8888， 使用RGB_565会比使用ARGB_8888少消耗2倍的内存
		.imageScaleType(ImageScaleType.IN_SAMPLE_INT)//设置.imageScaleType(ImageScaleType.IN_SAMPLE_INT)或者imageScaleType(ImageScaleType.EXACTLY)
//		.imageScaleType(ImageScaleType.NONE)
		.delayBeforeLoading(100)//载入图片前稍做延时可以提高整体滑动的流畅度
		.build();									// 创建配置过得DisplayImageOption对象
    	try{
//    		imageLoader = ImageLoader.getInstance();
//    		ImageLoader.getInstance().displayImage(url, imageView, options);
    		imageLoader.displayImage(url, imageView, options);
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    }
}
