package com.dodowaterfall.widget;

import java.util.ArrayList;







import java.util.Iterator;




import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

//import org.jivesoftware.smack.util.StringUtils;



//import com.allen.ui.profile.OtherProfileManagermentActivity;
//import com.stb.isharemessage.R;
//import com.stb.isharemessage.service.aidl.IRoster;
//import com.stb.isharemessage.ui.ChatActivity;
//import com.stb.isharemessage.ui.ContactListWithPop;
//import com.stb.isharemessage.ui.list.ManageFriendPopupWindow;
import com.example.android.bitmapfun.util.AsyncTask;
import com.spice.im.OtherProfileActivity;
import com.spice.im.SimpleListDialog;
import com.spice.im.SimpleListDialog.onSimpleListItemClickListener;
//import com.stb.isharemessage.service.Contact;
//import com.stb.isharemessage.ui.AdvancedSearchResPullRefListActivity;
//import com.stb.isharemessage.ui.ContactFrameActivity;
//import com.stb.isharemessage.ui.GpsSearchResPullRefListActivity;
//import com.stb.isharemessage.ui.dialogs.builders.BlackListContact;
//import com.stb.isharemessage.ui.dialogs.builders.DeleteContact;
//import com.stb.isharemessage.ui.dialogs.builders.DeleteMessages;
//import com.stb.isharemessage.ui.dialogs.builders.InviteContact;
//import com.stb.isharemessage.ui.list.SelectPicPopupWindow;
//import com.stb.isharemessage.ui.list.BasePopupWindow.onSubmitClickListener;
//import com.stb.isharemessage.ui.profile.GroupProfileActivity;
//import com.stb.isharemessage.ui.profile.OtherProfileActivity;
//import com.stb.isharemessage.ui.register.BaseDialog;
//import com.stb.isharemessage.ui.register.FlippingLoadingDialog;
//import com.stb.isharemessage.ui.register.SimpleListDialog;
//import com.stb.isharemessage.ui.register.SimpleListDialogAdapter;
//import com.stb.isharemessage.ui.register.SimpleListDialog.onSimpleListItemClickListener;
//import com.speed.im.SimpleListDialog;
//import com.speed.im.SimpleListDialog.onSimpleListItemClickListener;
//import com.speed.im.utils.StringUtils;
//import com.stb.isharemessage.utils.FeedComment;
//import com.geniuseoe2012.popwindow.SingleChoicePopWindow;

import com.spice.im.group.GroupDetailsActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.PopupWindow.OnDismissListener;

public class FlowView extends ImageView implements View.OnClickListener, View.OnLongClickListener {
	//searchkey 对应username OtherProfileActivity
	//groupId对应tagid,groupName对应tagname GroupDetailsActivity

    private Context context;
    public Bitmap bitmap;
    private int columnIndex;// 图片属于第几列
    private String fileName;
    private int ItemWidth;
    private Handler viewHandler;
    private String _url;
    private String _roomname;
    private String _nickname;
    private String name;
    private String headimg;
    private String myheadimg;
    private String myusername;
    
    
    
//    private String sex = "";
//	private String age = "";
//	private String sign = "";
    private String mGender = "";
    
    
    private String username;
    private String tagid;
    private String tagname;
    private String tagimg;
    public void setUsername(String username){
    	this.username = username;
    }
    public String getUsername(){
    	return this.username;
    }
    public void setTagid(String tagid){
    	this.tagid = tagid;
    }
    public String getTagid(){
    	return this.tagid;
    }
    public void setTagname(String tagname){
    	this.tagname = tagname;
    }
    public String getTagname(){
    	return this.tagname;
    }
    public void setTagimg(String tagimg){
    	this.tagimg = tagimg;
    }
    public String getTagimg(){
    	return this.tagimg;
    }
    
    public void setGender(String gender){
    	mGender = gender;
    }
    public String getGender(){
    	return mGender;
    }
    
    private String mBirthday = "";
    public void setBirthday(String birthday){
    	mBirthday = birthday;
    }
    public String getBirthday(){
    	return mBirthday;
    }
    private String mSign = "";
    public void setSign(String sign){
    	mSign = sign;
    }
    public String getSign(){
    	return mSign;
    }
    
//    private ContactListWithPop mActivity = null;
    private Activity mActivity = null;
//    public void setMActivity(ContactListWithPop Activity){
    public void setMActivity(Activity Activity1){
    	mActivity = Activity1;
    	initPopupWindow(context,Activity1,get_url());
    }
//    private IRoster mRoster;
//    public void 

    public FlowView(Context c, AttributeSet attrs, int defStyle) {
        super(c, attrs, defStyle);
        this.context = c;
        Init();
    }

    public FlowView(Context c, AttributeSet attrs) {
        super(c, attrs);
        this.context = c;
        Init();
    }

    public FlowView(Context c) {
        super(c);
        this.context = c;
        Init();
    }

    private void Init() {
//    	mLoadingDialog = new FlippingLoadingDialog(this.context, "请求提交中");
        setOnClickListener(this);
        setOnLongClickListener(this);
        setAdjustViewBounds(true);
        
    }

//    private SingleChoicePopWindow mSingleChoicePopWindow;
    private List<String> mSingleDataList = null;
    private String[] pickphoto = null;
//    private ManageFriendPopupWindow mPopupWindow;
    
    private SimpleListDialog mDialog = null;
//    public void initPopupWindow(Context context,ContactListWithPop Activity,String mContactname) {
	public void initPopupWindow(Context context,Activity activity,String mContactname) {
//		mPopupWindow = new ManageFriendPopupWindow(context,activity,mContactname);
//		mPopupWindow.setOnSubmitClickListener(new onSubmitClickListener() {
//
//			@Override
//			public void onClick() {
////				mPeopleFragment.onManualRefresh();
//			}
//		});
//		mPopupWindow.setOnDismissListener(new OnDismissListener() {
//
//			@Override
//			public void onDismiss() {
////				mHeaderSpinner.initSpinnerState(false);
//			}
//		});
		
////		String[] codes = new String[] { "修改备注名", "拉黑屏蔽此人消息", "删除好友", "重新发送邀请" };
//		if(mActivity instanceof ContactFrameActivity){
//			String[] codes = new String[] { "拉黑屏蔽此人消息", "删除好友", "重新发送好友邀请","删除聊天记录" };
//			if(mContactname.indexOf("@conference.")!=-1)
//				codes = new String[] { "拉黑屏蔽此群消息","删除聊天记录"};
//			mDialog = new SimpleListDialog(context);
//			mDialog.setTitle("提示");
//			mDialog.setTitleLineVisibility(View.GONE);
//			mDialog.setAdapter(new SimpleListDialogAdapter(context, codes));
//			mDialog.setOnSimpleListItemClickListener(new OnReplyDialogItemClickListener(
//					));
//		}else if((mActivity instanceof AdvancedSearchResPullRefListActivity)
//				|| (mActivity instanceof GpsSearchResPullRefListActivity)){
//			String[] codes = new String[] { "发送好友邀请" };
//			mDialog = new SimpleListDialog(context);
//			mDialog.setTitle("提示");
//			mDialog.setTitleLineVisibility(View.GONE);
//			mDialog.setAdapter(new SimpleListDialogAdapter(context, codes));
//			mDialog.setOnSimpleListItemClickListener(new OnReplyDialogItemClickListener(
//					));
//		}
	}
    @Override
    public boolean onLongClick(View v) {
        Log.d("FlowView", "LongClick");
//        Toast.makeText(context,"长按：" + getId(), Toast.LENGTH_SHORT).show();
//        Log.w("FlowView", "20130814createPickPhotoDialog() (mActivity!=null) = "+(mActivity!=null)+";mActivity.findViewById(R.id.list)="+mActivity.findViewById(R.id.ptrlvHeadLineNews)+";mActivity.getLocalClassName()="+mActivity.getLocalClassName());
//        if(mActivity!=null && mDialog!=null && !(mActivity instanceof GroupProfileActivity))
////		mPopupWindow
////		.showViewTopCenter(mActivity.findViewById(R.id.ptrlvHeadLineNews));
//        	mDialog.show();
        
        return true;
    }

    @Override
    public void onClick(View v) {
        Log.d("FlowView", "Click");
//        Toast.makeText(context, "单击：" + getId()+";getTagid()="+getTagid()+";getUsername()="+getUsername(), Toast.LENGTH_SHORT).show();

        	if(getTagid()!=null
        			&& getTagid().length()!=0
        			&& !getTagid().equalsIgnoreCase("null")
        			&& (getUsername()==null||getUsername().equalsIgnoreCase("null")||getUsername().length()==0)){
            	Intent intent = new Intent(this.context, GroupDetailsActivity.class);//GroupProfileActivity.class multiuserlistmain.class
//            	Toast.makeText(context, "单击roomname：" + StringUtils.parseName(get_url()), Toast.LENGTH_SHORT).show();
            	intent.putExtra("groupId", getTagid());
            	intent.putExtra("groupName",getTagname());
            	intent.putExtra("tagimg", getTagimg());
            	this.context.startActivity(intent);
        	}else if(get_url()!=null
        			&& get_url().equalsIgnoreCase("newgroup")){
        		
        	}else if(getUsername()!=null
        			&& getUsername().length()!=0
        			&& !getUsername().equalsIgnoreCase("null")){
	        	Intent intent = new Intent(this.context, OtherProfileActivity.class);
	        	intent.putExtra("searchkey",getUsername());
	            this.context.startActivity(intent);
        	}
//            this.context.finish();
        
    }

    /**
     * 回收内存
     */
    public void recycle() {
    	//销毁ImageView的bitmap
    	BitmapDrawable drawable = (BitmapDrawable)getDrawable();
    	Bitmap bmp = drawable.getBitmap();
    	if (null != bmp && !bmp.isRecycled()){
    		bmp.recycle();
    		bmp = null;
    	}
        setImageBitmap(null);
        
        if ((this.bitmap == null) || (this.bitmap.isRecycled()))
            return;
        this.bitmap.recycle();
        this.bitmap = null;
    }

    public int getColumnIndex() {
        return columnIndex;
    }

    public void setColumnIndex(int columnIndex) {
        this.columnIndex = columnIndex;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getItemWidth() {
        return ItemWidth;
    }

    public void setItemWidth(int itemWidth) {
        ItemWidth = itemWidth;
    }

    public Handler getViewHandler() {
        return viewHandler;
    }

    public FlowView setViewHandler(Handler viewHandler) {
        this.viewHandler = viewHandler;
        return this;
    }

    public String get_url() {
        return _url;
    }

    public void set_url(String _url) {
        this._url = _url;
    }
    public String get_roomname() {
        return _roomname;
    }

    public void set_roomname(String _roomname) {
        this._roomname = _roomname;
    }
    public String get_nickname() {
        return _nickname;
    }

    public void set_nickname(String _nickname) {
        this._nickname = _nickname;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getHeadimg() {
        return headimg;
    }

    public void setHeadimg(String headimg) {
        this.headimg = headimg;
    }
    public String getMyheadimg() {
        return myheadimg;
    }

    public void setMyheadimg(String myheadimg) {
        this.myheadimg = myheadimg;
    }
    public String getMyusername() {
        return myusername;
    }

    public void setMyusername(String myusername) {
        this.myusername = myusername;
    }
    
    
//    private DeleteContact delete = null;
//    private BlackListContact black = null;
//    private InviteContact invite = null;
//    private DeleteMessages deleteMessages = null;
	private class OnReplyDialogItemClickListener implements
	onSimpleListItemClickListener {

//		private FeedComment mComment;
		
		public OnReplyDialogItemClickListener() {//FeedComment comment
//			mComment = comment;
		}
		
		@Override
		public void onItemClick(int position) {
			//"修改备注名", "拉黑屏蔽此人消息", "删除好友", "重新发送邀请"
		    //"拉黑屏蔽此人消息", "删除好友", "重新发送邀请"
			switch (position) {
//				case 0:
//	//				mEtvEditerTitle.setVisibility(View.VISIBLE);
//	//				mEtvEditerTitle.setText("回复" + mComment.getName() + " :");
//	//				mEetEditer.requestFocus();
//	//				showKeyBoard();
//					break;
			
				case 0:
	//				String text = mComment.getContent();
	//				copy(text);
					
//					mBaseDialog = BaseDialog.getDialog(context, "提示", "你确认拉黑屏蔽'"+getName()+"'的消息吗?",
//					"确认", new DialogInterface.OnClickListener() {
//				
//								@Override
//								public void onClick(DialogInterface dialog, int which) {
//									dialog.dismiss();
////									startDelete();
//								}
//							},"取消", new DialogInterface.OnClickListener() {
//				
//								@Override
//								public void onClick(DialogInterface dialog, int which) {
//									dialog.dismiss();
//								}
//							});
//					mBaseDialog.show();
//					if(mActivity instanceof ContactFrameActivity){
//						Contact contact = new Contact(get_url());
//						contact.setName(getName());
//						black = new BlackListContact(mActivity, ((ContactFrameActivity)mActivity).geIXmppFacade(), contact);
////						delete.show();
//					}else if((mActivity instanceof AdvancedSearchResPullRefListActivity)
//							|| (mActivity instanceof GpsSearchResPullRefListActivity)){
//						Contact contact = new Contact(get_url());
//						contact.setName(getName());
//						if(mActivity instanceof AdvancedSearchResPullRefListActivity)
//						invite = new InviteContact(mActivity, ((AdvancedSearchResPullRefListActivity)mActivity).geIXmppFacade(), contact,
//								getHeadimg(),
//								getGender(),
//								getBirthday(),
//								getSign());
//						if(mActivity instanceof GpsSearchResPullRefListActivity)
//						invite = new InviteContact(mActivity, ((GpsSearchResPullRefListActivity)mActivity).geIXmppFacade(), contact,
//								getHeadimg(),
//								getGender(),
//								getBirthday(),
//								getSign());
//					}
					break;
			
				case 1:
//	//				report();
//					if(mActivity instanceof ContactFrameActivity){
//						
//						Contact contact = new Contact(get_url());
//						contact.setName(getName());
//						if(get_url().indexOf("@conference.")==-1)
//							delete = new DeleteContact(mActivity, ((ContactFrameActivity)mActivity).getMRoster(), contact);
//						else
//							deleteMessages = new DeleteMessages(mActivity, contact);
////						delete.show();
//					}
					break;
				case 2:
//					if(mActivity instanceof ContactFrameActivity){
//						Contact contact = new Contact(get_url());
//						contact.setName(getName());
//						invite = new InviteContact(mActivity, ((ContactFrameActivity)mActivity).geIXmppFacade(), contact,
//								getHeadimg(),
//								getGender(),
//								getBirthday(),
//								getSign());
////						delete.show();
//					}
					break;
				case 3:
//					if(mActivity instanceof ContactFrameActivity){
//						Contact contact = new Contact(get_url());
//						contact.setName(getName());
//						deleteMessages = new DeleteMessages(mActivity, contact);
//					}
					break;
			}
		}

	}
	
	
//	private static ExecutorService LIMITED_TASK_EXECUTOR;
//    static {
//    	LIMITED_TASK_EXECUTOR = (ExecutorService) Executors.newFixedThreadPool(7); 
//    }; 
//	protected List<AsyncTask<Void, Void, Boolean>> mAsyncTasks = new ArrayList<AsyncTask<Void, Void, Boolean>>();
//	protected FlippingLoadingDialog mLoadingDialog;
//	protected void putAsyncTask(AsyncTask<Void, Void, Boolean> asyncTask) {
////		mAsyncTasks.add(asyncTask.execute());
//		mAsyncTasks.add(asyncTask.executeOnExecutor(LIMITED_TASK_EXECUTOR, null));
//	}
//
//	protected void clearAsyncTask() {
//		Iterator<AsyncTask<Void, Void, Boolean>> iterator = mAsyncTasks
//				.iterator();
//		while (iterator.hasNext()) {
//			AsyncTask<Void, Void, Boolean> asyncTask = iterator.next();
//			if (asyncTask != null && !asyncTask.isCancelled()) {
//				asyncTask.cancel(true);
//			}
//		}
//		mAsyncTasks.clear();
//	}
//
//	protected void showLoadingDialog(String text) {
//		if (text != null) {
//			mLoadingDialog.setText(text);
//		}
//		mLoadingDialog.show();
//	}
//
//	protected void dismissLoadingDialog() {
//		if (mLoadingDialog.isShowing()) {
//			mLoadingDialog.dismiss();
//		}
//	}
//	private BaseDialog mBaseDialog;
	
}
