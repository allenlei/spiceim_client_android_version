/*
 * Copyright (C) 2010 Moduad Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.beem.push.utils;

import java.io.File;


import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

//import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.util.StringUtils;

import com.dodola.model.GreetInfo;
import com.dodola.model.GreetInfoAdapter;
import com.stb.core.connect.PacketListener;
import com.stb.isharemessage.BeemService;
import com.stb.isharemessage.utils.FileUtils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;

/** 
 * This class notifies the receiver of incoming notifcation packets asynchronously.  
 *
 * @author Sehwan Noh (devnoh@gmail.com)
 */
public class NotificationPacketListener implements PacketListener {
    //00000000000000-000000000-0000
	private String defaultNotificationApiKey_superAdmin = "00000000000000-000000000-0000";//超级管理员推送
	//99999999999999-999999999-9999
	private String defaultNotificationApiKey_IShareMessageAdmin = "99999999999999-999999999-9999";//IShareMessage管理员推送
	private String defaultNotificationApiKey_ISharePushPartnerAdmin = "";//推送企业管理员
//    private static final String LOGTAG = LogUtil
//            .makeLogTag(NotificationPacketListener.class);

//    private final XmppManager xmppManager;
	private BeemService mService;

//    public NotificationPacketListener(XmppManager xmppManager) {
//        this.xmppManager = xmppManager;
//    }
    public NotificationPacketListener(BeemService mService) {
        this.mService = mService;
    }

    @Override
    public void processPacket(Packet packet) {
//        Log.d(LOGTAG, "NotificationPacketListener.processPacket()...");
//        Log.d(LOGTAG, "packet.toXML()=" + packet.toXML());

        if (packet instanceof NotificationIQ) {
            NotificationIQ notification = (NotificationIQ) packet;

            if (notification.getChildElementXML().contains(
                    "androidpn:iq:notification")) {
                String notificationId = notification.getId();
                String notificationApiKey = notification.getApiKey();
                
                String notificationTitle = notification.getTitle();
                String notificationMessage = notification.getMessage();
                //                String notificationTicker = notification.getTicker();
                String notificationUri = notification.getUri();
                
                if(notificationApiKey.equals(mService.getPartnerid())
                		|| notificationApiKey.equals(defaultNotificationApiKey_superAdmin)){
                
                String newFilePath = saveFileToSDCard(mService,notificationMessage);

                Intent intent = new Intent(Constants.ACTION_SHOW_NOTIFICATION);
                intent.putExtra(Constants.NOTIFICATION_ID, notificationId);
                intent.putExtra(Constants.NOTIFICATION_API_KEY,
                        notificationApiKey);
                intent
                        .putExtra(Constants.NOTIFICATION_TITLE,
                                notificationTitle);
                intent.putExtra(Constants.NOTIFICATION_MESSAGE,
                		newFilePath);//notificationMessage
                intent.putExtra(Constants.NOTIFICATION_URI, notificationUri);
                //                intent.setData(Uri.parse((new StringBuilder(
                //                        "notif://notification.androidpn.org/")).append(
                //                        notificationApiKey).append("/").append(
                //                        System.currentTimeMillis()).toString()));

//                xmppManager.getContext().sendBroadcast(intent);
//                String to = StringUtils.parseName(mService.getXmppConnectionAdapter().getUserInfo().getJid());
                GreetInfo greetInfo = new GreetInfo(300,
	    				"admin",
//	    				to,
	    				"",
			    		null,
			    		null,
			    		notificationId,
			    		notificationApiKey,
			    		notificationTitle,
			    		newFilePath,//notificationMessage,
			    		notificationUri,
			    		null);
			    GreetInfoAdapter.getInstance(mService).addGreetInfo(greetInfo);
                
                mService.sendBroadcast(intent);
                }
//                mService.
            }
        }

    }
    
    
    
///////////////////////////////////20130608 sd or cachefile save path start by allen
	private static final String PHOTOPATH = "beemphoto";
    /** 
     * 检验SDcard状态 
     * @return boolean 
     */  
    public static boolean checkSDCard()  
    {  
        if(android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))  
        {  
            return true;  
        }else{  
            return false;  
        }  
    }
    /** 
     * 保存文件文件到目录 
     * @param context 
     * @return  文件保存的目录 
     */  
    public static String setMkdir(Context context,String myrelativefilepath)  
    {  
        String filePath;  
        if(checkSDCard())  
        {  
//            filePath = Environment.getExternalStorageDirectory()+File.separator+"myfilepath"; 
            filePath = Environment.getExternalStorageDirectory()+File.separator+myrelativefilepath+File.separator;
        }else{  
//            filePath = context.getCacheDir().getAbsolutePath()+File.separator+"myfilepath";  
            filePath = context.getCacheDir().getAbsolutePath()+File.separator+myrelativefilepath+File.separator; 
//            getActivity().getParent().getCacheDir();
        }  
        File file = new File(filePath);  
        if(!file.exists())  
        {  
            boolean b = file.mkdirs();  
//            Log.e("file", "文件不存在  创建文件    "+b);  
        }else{  
//            Log.e("file", "文件存在");  
        }  
        return filePath;  
    } 
    
    
	public static String saveFileToSDCard(Context context,String message) {
//		if (!FileUtils.isSdcardExist()) {
//			return null;
//		}
		FileOutputStream fileOutputStream = null;
		byte [] bytes = null;
//		FileUtils.createDirFile(IMAGE_PATH);
		FileUtils.createDirFile(setMkdir(context,PHOTOPATH));

		String fileName = UUID.randomUUID().toString() + ".txt";
//		String newFilePath = IMAGE_PATH + fileName;
		String newFilePath = setMkdir(context,PHOTOPATH) + fileName;
		File file = FileUtils.createNewFile(newFilePath);
		if (file == null) {
			return null;
		}
		try {
			 fileOutputStream = new FileOutputStream(newFilePath);
			 bytes = message.getBytes();   
			 fileOutputStream.write(bytes);   
			 fileOutputStream.close();
			 fileOutputStream = null;
		} catch (FileNotFoundException e1) {
			return null;
		} catch(IOException e2){
			return null;
		} catch(Exception e3){
			return null;
		}finally {
			if(fileOutputStream != null)
			try {
				fileOutputStream.flush();
				fileOutputStream.close();
				fileOutputStream = null;
			} catch (IOException e) {
			} catch(Exception e){
			}
		}
		return newFilePath;
	}
	///////////////////////////////////20130608 sd or cachefile save path end by allen

}
