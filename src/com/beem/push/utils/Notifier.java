/*
 * Copyright (C) 2010 Moduad Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.beem.push.utils;

import java.util.Random;


//import com.stb.isharepush.R;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

/** 
 * This class is to notify the user of messages with NotificationManager.
 *
 * @author Sehwan Noh (devnoh@gmail.com)
 */
public class Notifier {

//    private static final String LOGTAG = LogUtil.makeLogTag(Notifier.class);

    private static final Random random = new Random(System.currentTimeMillis());

    private Context context;

    private SharedPreferences sharedPrefs;

    private NotificationManager notificationManager;
    String mDetailActivityPackageName = "";
    String mDetailActivityClassName = "";
    
    public Notifier(Context context,String detailActivityPackageName,String detailActivityClassName) {
        this.context = context;
//        this.sharedPrefs = context.getSharedPreferences(
//                Constants.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        this.sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        this.notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        mDetailActivityPackageName = detailActivityPackageName;
    	mDetailActivityClassName = detailActivityClassName;
    }

    public void notify(String notificationId, String apiKey, String title,
            String message, String uri) {

            // Notification
            Notification notification = new Notification();
//            notification.icon = getNotificationIcon();
            if(getNotificationIcon()!=-1)
            	notification.icon = getNotificationIcon();
            else
            	notification.icon =  android.R.drawable.ic_dialog_email;//R.drawable.sysmsg;
//            Toast.makeText(context, "notification.icon="+notification.icon, Toast.LENGTH_LONG).show();
            notification.defaults = Notification.DEFAULT_LIGHTS;
            if (isNotificationSoundEnabled()) {
                notification.defaults |= Notification.DEFAULT_SOUND;
            }
            if (isNotificationVibrateEnabled()) {
                notification.defaults |= Notification.DEFAULT_VIBRATE;
            }
            notification.flags |= Notification.FLAG_AUTO_CANCEL;
            notification.when = System.currentTimeMillis();
//            notification.tickerText = message;
            notification.tickerText = title;


//            Intent intent = new Intent(context,
//                    NotificationDetailsActivity.class);
            
//            Intent intent =  new Intent().setClassName(
//            		mDetailActivityPackageName,
//            		mDetailActivityClassName);
            
            Log.e("================2015 Notifier.java ================", "++++++++++++++mDetailActivityPackageName="+mDetailActivityPackageName);
            Log.e("================2015 Notifier.java ================", "++++++++++++++mDetailActivityClassName="+mDetailActivityClassName);
            Intent intent =  new Intent();
            try{
            	intent.setClass(context, Class.forName(mDetailActivityPackageName+"."+mDetailActivityClassName));
            }catch(Exception e){
            	e.printStackTrace();
            }
            intent.putExtra(Constants.NOTIFICATION_ID, notificationId);
            intent.putExtra(Constants.NOTIFICATION_API_KEY, apiKey);
            intent.putExtra(Constants.NOTIFICATION_TITLE, title);
            intent.putExtra(Constants.NOTIFICATION_MESSAGE, message);
            intent.putExtra(Constants.NOTIFICATION_URI, uri);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            intent.setFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);//baoliu
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
                    intent, PendingIntent.FLAG_UPDATE_CURRENT);

//            notification.setLatestEventInfo(context, title, message,
//                    contentIntent);//
            notification.setLatestEventInfo(context, title, "",
                    contentIntent);//
//            notificationManager.notify(random.nextInt(), notification);
//            notificationManager.notify(Integer.valueOf(notificationId, 16).intValue(), notification);
            
            notificationManager.notify(Integer.parseInt(notificationId), notification);
    }

    private int getNotificationIcon() {
        return sharedPrefs.getInt(Constants.NOTIFICATION_ICON, -1);//0
    }
//	public void setNotificationIcon(int iconId) {
//	  Editor editor = sharedPrefs.edit();
//	  editor.putInt(Constants.NOTIFICATION_ICON, iconId);
//	  editor.commit();
//	}

    private boolean isNotificationEnabled() {
        return sharedPrefs.getBoolean(Constants.SETTINGS_NOTIFICATION_ENABLED,
                true);
    }

    private boolean isNotificationSoundEnabled() {
        return sharedPrefs.getBoolean(Constants.SETTINGS_SOUND_ENABLED, true);
    }

    private boolean isNotificationVibrateEnabled() {
        return sharedPrefs.getBoolean(Constants.SETTINGS_VIBRATE_ENABLED, true);
    }

    private boolean isNotificationToastEnabled() {
        return sharedPrefs.getBoolean(Constants.SETTINGS_TOAST_ENABLED, false);
    }

}
