/*
 * Copyright (C) 2010 Moduad Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.beem.push.utils;

import android.R;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

/** 
 * Broadcast receiver that handles push notification messages from the server.
 * This should be registered as receiver in AndroidManifest.xml. 
 * 
 * @author Sehwan Noh (devnoh@gmail.com)
 */
public final class NotificationReceiver extends BroadcastReceiver {
    //00000000000000-000000000-0000
	private String defaultNotificationApiKey_superAdmin = "00000000000000-000000000-0000";//超级管理员推送
	//99999999999999-999999999-9999
	private String defaultNotificationApiKey_IShareMessageAdmin = "99999999999999-999999999-9999";//IShareMessage管理员推送
	private String defaultNotificationApiKey_ISharePushPartnerAdmin = "";//推送企业管理员
	
    String mDetailActivityPackageName = "";
    String mDetailActivityClassName = "";
    String mPartnerid = "";
    public NotificationReceiver(String detailActivityPackageName,String detailActivityClassName,String partnerid) {
    	mDetailActivityPackageName = detailActivityPackageName;
    	mDetailActivityClassName = detailActivityClassName;
    	mPartnerid = partnerid;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
//        Log.d(LOGTAG, "NotificationReceiver.onReceive()...");
        String action = intent.getAction();

        if (Constants.ACTION_SHOW_NOTIFICATION.equals(action)) {
            String notificationId = intent
                    .getStringExtra(Constants.NOTIFICATION_ID);
            String notificationApiKey = intent
                    .getStringExtra(Constants.NOTIFICATION_API_KEY);
            String notificationTitle = intent
                    .getStringExtra(Constants.NOTIFICATION_TITLE);
            String notificationMessage = intent
                    .getStringExtra(Constants.NOTIFICATION_MESSAGE);
            String notificationUri = intent
                    .getStringExtra(Constants.NOTIFICATION_URI);
            
            if(notificationApiKey.equals(mPartnerid)
            		|| notificationApiKey.equals(defaultNotificationApiKey_superAdmin)){
            Notifier notifier = new Notifier(context,mDetailActivityPackageName,mDetailActivityClassName);
//            notifier.setNotificationIcon(R.drawable.ic_notification_overlay);
            notifier.notify(notificationId, notificationApiKey,
                    notificationTitle, notificationMessage, notificationUri);
            }
        }

    }

}
