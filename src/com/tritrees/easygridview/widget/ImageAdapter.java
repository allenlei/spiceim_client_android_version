package com.tritrees.easygridview.widget;

import java.util.HashMap;



import java.util.List;

//import com.stb.isharemessage.R;
import com.dodowaterfall.widget.FlowView;
import com.spice.im.R;
import com.spice.im.utils.ImageFetcher;
//import com.example.android.bitmapfun.util.ImageFetcher;
//import com.speed.im.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

//import com.tritrees.easygridview.R;

public class ImageAdapter extends BaseAdapter
{

	private static final String TAG = "ImageAdapter";
	
	private ImageFetcher mImageFetcher;

	/** �Զ���ͼƬ�Ŀ�� */
	protected float imageWidth, imageHeight;

	public float getImageWidth()
	{
		return imageWidth;
	}

	public void setImageWidth(float imageWidth)
	{
		this.imageWidth = imageWidth;
	}

	public float getImageHeight()
	{
		return imageHeight;
	}

	public void setImageHeight(float imageHeight)
	{
		this.imageHeight = imageHeight;
	}

	protected List<HashMap<String, Object>> data;

	public void setData(List<HashMap<String, Object>> data)
	{
		this.data = data;
	}

	private Context context;
	protected int blankItemCount;// �հ�����ĸ���
	protected boolean isRemoved = false;// �Ƿ�����ɾ��ͼ��

	private boolean isWithText = false;// ͼƬ�����Ƿ������

	/**
	 * ���ÿհ�����ĸ���
	 * 
	 * @param blankItemCount
	 */
	public void setBlankItemCount(int blankItemCount)
	{
		this.blankItemCount = blankItemCount;
	}

	/**
	 * �����Ƿ�Ϊɾ��״̬
	 * 
	 * @param isRemoved
	 */
	public void setRemoved(boolean isRemoved)
	{
		this.isRemoved = isRemoved;
	}

	public ImageAdapter(Context context)
	{
		this(context, false);
	}

	public ImageAdapter(Context context, boolean isWithText)
	{
		this.isWithText = isWithText;
		this.context = context;
		
        mImageFetcher = new ImageFetcher(this.context, 77);//240
        mImageFetcher.setUListype(0);//uListype
//        mImageFetcher.setLoadingImage(R.drawable.empty_photo3);
        mImageFetcher.setExitTasksEarly(false);
	}

	@Override
	public int getCount()
	{
		return data.size();
	}

	@Override
	public Object getItem(int position)
	{
		return data.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(final int position, View convertView,
			final ViewGroup parent)
	{
		ViewHolder viewHolder = null;
		if (convertView == null)
		{
			viewHolder = new ViewHolder();
			convertView = View.inflate(context, R.layout.imageadapter_item,
					null);
			viewHolder.iv_image = (ImageView) convertView
					.findViewById(R.id.iv_image);
//			viewHolder.iv_image = (FlowView) convertView
//			.findViewById(R.id.iv_image);
			if (imageWidth != 0 && imageHeight != 0)
			{
				LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) viewHolder.iv_image
						.getLayoutParams();
				Log.e(TAG, "params.width======>" + params.width
						+ "params.height=====>" + params.height);
				params.width = (int) imageWidth;
				params.height = (int) imageHeight;
				Log.e(TAG, "params.width2======>" + params.width
						+ "params.height2=====>" + params.height);
				viewHolder.iv_image.setLayoutParams(params);
			}

			viewHolder.tv_text = (TextView) convertView
					.findViewById(R.id.tv_text);
			viewHolder.badgeView = new BadgeView(context, viewHolder.iv_image);
			convertView.setTag(viewHolder);
		} else
		{
			viewHolder = (ViewHolder) convertView.getTag();
		}
//
//		viewHolder.iv_image.setImageBitmap((Bitmap) data.get(position).get(
//				EasyGridView.IMAGE));
		if(data.get(position).get(EasyGridView.IMAGE) instanceof Bitmap){

			viewHolder.iv_image.setImageBitmap((Bitmap) data.get(position).get(
					EasyGridView.IMAGE));
		}else if(data.get(position).get(EasyGridView.IMAGE) instanceof HeadImg){
//			mImageFetcher.setLoadingImage(R.drawable.empty_photo3);
			mImageFetcher.loadNormalImage(((HeadImg)data.get(position).get(EasyGridView.IMAGE)).getImgsrc(), viewHolder.iv_image,0,0,0,null);
		}
		viewHolder.tv_text.setVisibility(View.VISIBLE);
		if (isWithText)
		{
			
			String name = (String) data.get(position).get(EasyGridView.TEXT);
			if (name != null && !"".equals(name))
			{
				viewHolder.tv_text.setText(name);
			}else{
				viewHolder.tv_text.setVisibility(View.GONE);
			}
		} else
		{
			viewHolder.tv_text.setVisibility(View.GONE);
		}
		


		if (isRemoved && position < data.size() - blankItemCount)
		{

			viewHolder.badgeView.setText("ɾ��");//- ����ɾ���ͷ��
			if (!viewHolder.badgeView.isShown())
			{
				viewHolder.badgeView.setBadgeMargin(0, 0);
				viewHolder.badgeView
						.setBadgePosition(BadgeView.POSITION_TOP_LEFT);
				viewHolder.badgeView.show();
			}
		} else if (!isRemoved && position < data.size() - blankItemCount)
		{
			if(position!=0){
			if (viewHolder.badgeView.isShown())
			{
				viewHolder.badgeView.hide();
			}
			}else if(position == 0){//20140818 added
				if(this.isWithText){
				viewHolder.badgeView.setText("ͷ��");//- ����ͷ���ͷ��
				if (!viewHolder.badgeView.isShown())
				{
					viewHolder.badgeView.setBadgeMargin(0, 0);
					viewHolder.badgeView
							.setBadgePosition(BadgeView.POSITION_TOP_LEFT);
					viewHolder.badgeView.show();
				}
				}else{
					if (viewHolder.badgeView.isShown())
					{
						viewHolder.badgeView.hide();
					}
				}
			}
		}
		// �հ�����badgeview�Ŀ���
		if (position!=0 && position < data.size() && position >= data.size() - blankItemCount)
		{
			if (viewHolder.badgeView.isShown())
			{
				viewHolder.badgeView.hide();
			}
		}

		return convertView;

	}

	class ViewHolder
	{
		ImageView iv_image;
//		FlowView iv_image;
		TextView tv_text;
		BadgeView badgeView;
	}
}

