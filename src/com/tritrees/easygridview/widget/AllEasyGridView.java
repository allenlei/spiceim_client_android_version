package com.tritrees.easygridview.widget;

import java.util.HashMap;

import android.content.Context;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

//import com.tritrees.easygridview.R;

public class AllEasyGridView extends EasyGridView
{
	private boolean emptyed = false;//true表示当没有图片数据时，此时界面只显示“+”号

	public AllEasyGridView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	@Override
	protected void initAdapterData()
	{
		int num = 0;
		HashMap<String, Object> map;
		dataList.clear();
		if (data.size() == 0 || data == null)
		{//没有图片数据时，此时界面只显示“+”号
			emptyed = true;

			map = new HashMap<String, Object>();
			map.put(EasyGridView.IMAGE, bmp_add);
			dataList.add(map);
		} else
		{
			emptyed = false;
			for (int i = 0; i < data.size(); i++)
			{
				dataList.add(data.get(i));
			}
			if (!isRemoved)
			{

				map = new HashMap<String, Object>();
				map.put(EasyGridView.IMAGE, bmp_add);
				dataList.add(map);


				map = new HashMap<String, Object>();
				map.put(EasyGridView.IMAGE, bmp_del);
				dataList.add(map);
			}
		}
		num = dataList.size();
		addBlankItem(num);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id)
	{
		if (emptyed)
		{
			if (position == 0)
			{// 添加按钮
				if (isChildItem)
				{
					if (handleGridViewWithChild != null)
					{
						handleGridViewWithChild.addImage(childPosition);
					}
				} else
				{
					if (handleGridView != null)
					{
						handleGridView.addImage();
					}
				}
				updateAdapter();
			}
		} else
		{
			if (!isRemoved && position == dataList.size() - blankItemCount - 2)
			{// 不是在删除状态下的添加按钮
				if (isChildItem)
				{
					if (handleGridViewWithChild != null)
					{
						handleGridViewWithChild.addImage(childPosition);
					}
				} else
				{
					if (handleGridView != null)
					{
						handleGridView.addImage();
					}
				}
				updateAdapter();
			} else if (!isRemoved
					&& position == dataList.size() - blankItemCount - 1)
			{// 不是在删除状态下的删除按钮
				isRemoved = true;
				adapter.setRemoved(isRemoved);
				updateAdapter();
			} else if (isRemoved
					&& position >= dataList.size() - blankItemCount)
			{// 当为删除状态时，单击空白区域，回到正常状态
				isRemoved = false;
				adapter.setRemoved(false);
				updateAdapter();
			} else if(position < dataList.size() - blankItemCount)
			{// 头像图标，在正常状态下，单击查询用户信息
				if (!isRemoved)
				{
					if (isChildItem)
					{
						if (handleGridViewWithChild != null)
						{
							handleGridViewWithChild.queryImage(childPosition,
									position);
						}
					} else
					{
						if (handleGridView != null)
						{
							handleGridView.queryImage(position);
						}
					}
				}
				// 在删除状态下，单击删除用户
				if (isRemoved)
				{
					if (isChildItem)
					{
						if (handleGridViewWithChild != null)
						{
							handleGridViewWithChild.delImage(childPosition,
									position);
						}
					} else
					{
						if (handleGridView != null)
						{
							handleGridView.delImage(position);
						}
					}
					
					if (data.size() == 0 || data == null)
					{//当删除完最后一张图片后，此时没有图片数据，将不再是删除状态
						isRemoved = false;
						adapter.setRemoved(false);
					}
					updateAdapter();
				}
			}
		}

	}
//	@Override
//    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id){
//		Toast.makeText(context, "长按：id=" + id+";position="+position, Toast.LENGTH_SHORT).show();
//		Log.e("====&&&&&&&******20140814******&&&&&&&=====", "====&&&&&&&******20140814******&&&&&&&====="+"长按：id=" + id+";position="+position);
//		return true;
//	}

}
