package com.tritrees.easygridview.widget;

import java.util.ArrayList;




import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import com.spice.im.R;

//import com.speed.im.R;

//import com.stb.isharemessage.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

//import com.tritrees.easygridview.R;

public class EasyGridView extends MyGridView implements
		android.widget.AdapterView.OnItemClickListener
		
{

	public static final String IMAGE = "icon";
	public static final String TEXT = "text";
	
	/** "+"��ͼƬ */
	protected int addResourceID;
	protected Bitmap bmp_add;
	public void setAddImage(int addResourceID)
	{
		this.addResourceID = addResourceID;
	}
	/** "-"��ͼƬ */
	protected int delResourceID;
	protected Bitmap bmp_del;
	public void setDelImage(int delResourceID)
	{
		this.delResourceID = delResourceID;
	}
	
	
	/** �ṩ��һ��gridview�ؼ��õĻص��ӿ� */
	public interface IHandleGridView
	{
		void addImage();

		void delImage(int position);

		void queryImage(int position);
	}
	
	/** �ṩ��ʹ�ö��gridview�ؼ��õĻص��ӿ�,������Ϊlistview������ʱ */
	public interface IHandleGridViewWithChild
	{
		void addImage(int childPosition);

		void delImage(int childPosition, int position);

		void queryImage(int childPosition, int position);
	}

	protected IHandleGridView handleGridView;

	public void setHandleGridView(IHandleGridView handleGridView)
	{
		this.handleGridView = handleGridView;
	}
	
	/** ����Ϊ����ʱ���ڸ��ؼ������Ӧ��λ�� */
	protected int childPosition;
	protected IHandleGridViewWithChild handleGridViewWithChild;

	public void setHandleGridViewWithChild(int childPosition,
			IHandleGridViewWithChild handleGridViewWithChild)
	{
		this.childPosition = childPosition;
		this.handleGridViewWithChild = handleGridViewWithChild;
	}

	protected boolean isRemoved = false;// �Ƿ�Ϊɾ��״̬
	protected boolean isChildItem = false;// �Ƿ���Ϊ����ؼ�������
	protected int blankItemCount = 0;
	protected int itemCountInRow = 4;// mygridviewÿ�е���������,Ĭ��Ϊ4
	protected ImageAdapter adapter;
	protected Context context;
	
	protected float imageWidth,imageHeight;//ͼƬ�Ŀ��

	protected List<HashMap<String, Object>> dataList;// ������Ŷ�̬��ݵ��������
	protected ArrayList<HashMap<String, Object>> data;// ���������Դ

	public ArrayList<HashMap<String, Object>> getData()
	{
		return data;
	}

	public void setData(ArrayList<HashMap<String, Object>> data)
	{
		this.data = data;
	}

	public EasyGridView(Context context)
	{
		super(context);
	}

	public EasyGridView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		this.context = context;
		
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.EasyGridView);
		this.imageWidth = a.getDimension(
				R.styleable.EasyGridView_imagewidth, 100);
		this.imageHeight = a.getDimension(
				R.styleable.EasyGridView_imageheight, 100);
		//2.2�汾�� ���������������xml����������ã��ڴ������Ȼ�Ҳ�����̬��ȡ�ķ����������Һ��޽⣬ֻ���Զ���һ��������������
		//���Ե�xml���numcolumnֵ�޸��ˣ�columncountֵҲҪ����Ϊһ��
		this.itemCountInRow = a.getInteger(
				R.styleable.EasyGridView_columncount, 4);
		a.recycle();
		
		if(addResourceID != 0)
		{
			bmp_add = BitmapFactory.decodeResource(getResources(),
					addResourceID);
		}else
		{
			bmp_add = BitmapFactory.decodeResource(getResources(),
					R.drawable.ic_add);
		}
		
		if(delResourceID != 0)
		{
			bmp_del = BitmapFactory.decodeResource(getResources(),
					delResourceID);
		}else
		{
			bmp_del = BitmapFactory.decodeResource(getResources(),
					R.drawable.ic_del);
		}
	}

	public EasyGridView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
	}

	/**
	 * ��ʼ��ͼƬ���������
	 * 
	 * @param isChildItem �Ƿ���Ϊ����ؼ�������
	 * @param data �Զ������ݽṹ��������ArrayList<HashMap<String, Object>>������ݽṹ
	 * @param adapter �Զ������������ ��Ҫ�����Լ��Ĳ����ļ������Լ̳����ImageAdapter,����getview����
	 */
	public void initData(boolean isChildItem,
			ArrayList<HashMap<String, Object>> data,ImageAdapter adapter)
	{
		this.isChildItem = isChildItem;
		this.data = data;
		this.adapter = adapter;

		dataList = new ArrayList<HashMap<String, Object>>();
		initAdapterData();

		adapter.setData(dataList);
		adapter.setBlankItemCount(blankItemCount);
		adapter.setImageWidth(imageWidth);
		adapter.setImageHeight(imageHeight);

		setOnItemClickListener(this);
		setSelector(new ColorDrawable(Color.TRANSPARENT));
		setAdapter(adapter);
	}

	/**
	 * �������ʱ�ؼ�
	 */
	protected void initAdapterData()
	{
		dataList = data;
	}

	/**
	 * ����հ�����ĸ���
	 * 
	 * @param num
	 */
	protected void addBlankItem(int num)
	{
		Bitmap bmp_blank = BitmapFactory.decodeResource(getResources(),
				R.drawable.ic_blank);
		HashMap<String, Object> map;
		// ��ɾ��״̬��ʱ�������һ��ͼ���Ǹ��е����һ��������Ҫ��һ�еĿհ�����
		if (isRemoved)
		{
			blankItemCount = (num / itemCountInRow + 1) * itemCountInRow - num;
		} else
		{
			blankItemCount = (num % itemCountInRow == 0 ? num / itemCountInRow
					: (num / itemCountInRow + 1)) * itemCountInRow - num;
		}
		for (int i = 0; i < blankItemCount; i++)
		{
			map = new HashMap<String, Object>();
			map.put(IMAGE, bmp_blank);
			dataList.add(map);
		}
	}

	/**
	 * ����������
	 */
	public void updateAdapter()
	{
		initAdapterData();
		adapter.setData(dataList);//20140819
		adapter.setBlankItemCount(blankItemCount);
		adapter.notifyDataSetChanged();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id)
	{
		if (isChildItem)
		{
			if (handleGridViewWithChild != null)
			{
				handleGridViewWithChild.queryImage(childPosition,
						position);
			}
		} else
		{
			if (handleGridView != null)
			{
				handleGridView.queryImage(position);
			}
		}
	}


}
