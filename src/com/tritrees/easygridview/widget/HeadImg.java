package com.tritrees.easygridview.widget;

public class HeadImg {
	public String imgsrc;
	public int sortid = 1;
	public HeadImg(String _imgsrc) {
		this.imgsrc = _imgsrc;
	}
	public HeadImg(String _imgsrc,int _sortid) {
		this.imgsrc = _imgsrc;
		this.sortid = _sortid;
	}
	public void setImgsrc(String imgsrc){
		this.imgsrc = imgsrc;
	}
	public String getImgsrc(){
		return imgsrc;
	}
	public void setSortid(int sortid){
		this.sortid = sortid;
	}
	public int getSortid(){
		return sortid;
	}
}
