package com.spiceim.db;



import com.lidroid.xutils.db.annotation.Table;
import com.lidroid.xutils.db.annotation.Column;
import com.stb.core.test.msg.Base64;

//package com.speed.im.login;
@Table(name = "TContactGroup")  // 建议加上注解， 混淆后表名不受影响
public class TContactGroup{
	public TContactGroup(){}
	//extends EntityBase
	@Column(column = "id")
	private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    @Column(column = "owner")
    private String owner;//增加字段owner即写入此记录的属主（即谁是此条记录的所有者），防止同一个手机上多个账号登录时混淆
    public String getOwner(){
    	return owner;
    }
    public void setOwner(String owner){
    	this.owner = owner;
    }
    
    @Column(column = "bareJID")
    private String bareJID;
    public String getBareJID(){
    	return bareJID;
    }
    public void setBareJID(String mBareJID){
    	this.bareJID = mBareJID;
    }
    
    @Column(column = "type")
	private int type;//0 contact,1 group
	
	//个人帐户
    @Column(column = "uid")
	private String uid;//帐户对应id号
    @Column(column = "uuid")
	private String uuid;//终端id号
    @Column(column = "username")
	private String username;//登录用户名
    @Column(column = "name")
	private String name;//真实姓名
    @Column(column = "sex")
	private int sex;//0 默认未知，1男，2女
    @Column(column = "blood")
	private String blood;//A,B,O,AB
    @Column(column = "marry")
	private int marry;//婚姻状态：0默认未知，1单身，2已婚
    @Column(column = "avatar")
	private String avatar;//注意avator值的值域：0默认未上传头像，1已上传头像
    @Column(column = "avatarPath")
	private String avatarPath;
    @Column(column = "email")
	private String email;
    @Column(column = "mobile")
	private String mobile;
    @Column(column = "qq")
	private String qq;
    @Column(column = "birthyear")
	private String birthyear;
    @Column(column = "birthmonth")
	private String birthmonth;
    @Column(column = "birthday")
	private String birthday;
    @Column(column = "birthprovince")
	private String birthprovince;//家乡省份
    @Column(column = "birthcity")
	private String birthcity;//家乡城市
    @Column(column = "resideprovince")
	private String resideprovince;//居住省份
    @Column(column = "residecity")
	private String residecity;//居住城市
    @Column(column = "note")
	private String note;//代表用户更新的状态，可以让好友知道他在做什么
	
	public void setType(int mType){
		type = mType;
	}
	public int getType(){
		return type;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String mUid) {
		uid = mUid;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String mUuid) {
		uuid = mUuid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String mUsername) {
		username = mUsername;
	}
	public String getName() {
		return name;
	}
	public void setName(String mName) {
		name = mName;
	}
	public int getSex() {
		return sex;
	}
	public void setSex(int mSex) {
		sex = mSex;
	}
	public String getBlood() {
		return blood;
	}
	public void setBlood(String mBlood) {
		blood = mBlood;
	}
	public int getMarry() {
		return marry;
	}
	public void setMarry(int mMarry) {
		marry = mMarry;
	}
    public void setAvatar(String mAvatar) {
    	avatar = mAvatar;
    }
    public String getAvatar(){
    	return avatar;
    }
    public void setAvatarPath(String mAvatarPath) {
    	avatarPath = mAvatarPath;
    }
    public String getAvatarPath(){
    	return avatarPath;
    }
    public void setEmail(String mEmail) {
    	email = mEmail;
    }
    public String getEmail(){
    	return email;
    }
    public void setMobile(String mMobile) {
    	mobile = mMobile;
    }
    public String getMobile(){
    	return mobile;
    }
    public void setQq(String mQq) {
    	qq = mQq;
    }
    public String getQq(){
    	return qq;
    }
    public void setBirthyear(String mBirthyear) {
    	birthyear = mBirthyear;
    }
    public String getBirthyear(){
    	return birthyear;
    }
    public void setBirthmonth(String mBirthmonth) {
    	birthmonth = mBirthmonth;
    }
    public String getBirthmonth(){
    	return birthmonth;
    }
    public void setBirthday(String mBirthday) {
    	birthday = mBirthday;
    }
    public String getBirthday(){
    	return birthday;
    }
    public void setBirthprovince(String mBirthprovince) {
    	birthprovince = mBirthprovince;
    }
    public String getBirthprovince(){
    	return birthprovince;
    }
    public void setBirthcity(String mBirthcity) {
    	birthcity = mBirthcity;
    }
    public String getBirthcity(){
    	return birthcity;
    }
    public void setResideprovince(String mResideprovince) {
    	resideprovince = mResideprovince;
    }
    public String getResideprovince(){
    	return resideprovince;
    }
    public void setResidecity(String mResidecity) {
    	residecity = mResidecity;
    }
    public String getResidecity(){
    	return residecity;
    }
    public void setNote(String mNote) {
    	note = mNote;
    }
    public String getNote(){
    	return note;
    }
    
    
	//群组帐户
    @Column(column = "tagid")
	private String tagid;//群组id号
    @Column(column = "tagname")
	private String tagname;//群组名称
    @Column(column = "fieldid")
	private String fieldid;//群组所属类别id号
    @Column(column = "title")
	private String title;//群组所属类别名称
    @Column(column = "membernum")
	private String membernum;//群组成员数量
    @Column(column = "threadnum")
	private String threadnum;//群组在线成员数量
    @Column(column = "pic")
	private String pic;//群组头像pic
    @Column(column = "tid")
	private String tid;//群组最新话题id号
    @Column(column = "subject")
	private String subject;//群组最新话题标题
    @Column(column = "tuid")
	private String tuid;//群组最新话题作者用户uid
    @Column(column = "tuuid")
	private String tuuid;//群组最新话题作者用户uuid
    @Column(column = "tusername")
	private String tusername;//群组最新话题作者用户username
    @Column(column = "lastauthorid")
	private String lastauthorid;//群组最新话题最后更新用户id
    @Column(column = "lastauthor")
	private String lastauthor;//群组最新话题最后更新用户名
    @Column(column = "viewnum")
	private String viewnum;//群组最新话题被阅读次数
    @Column(column = "replynum")
	private String replynum;//群组最新话题被回应（回复）次数
    @Column(column = "dateline")
	private String dateline;//群组最新话题发布时间
    @Column(column = "lastpost")
	private String lastpost;//群组最新话题最后更新时间
    @Column(column = "itemType")
	private String itemType = "0";//none 0,remove 1
	
	public String getTagid() {
		return tagid;
	}
	public void setTagid(String mTagid) {
		tagid = mTagid;
	}
	public String getTagname() {
		return tagname;
	}
	public void setTagname(String mTagname) {
		tagname = mTagname;
	}
	public String getFieldid() {
		return fieldid;
	}
	public void setFieldid(String mFieldid) {
		fieldid = mFieldid;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String mTitle) {
		title = mTitle;
	}
	public String getMembernum() {
		return membernum;
	}
	public void setMembernum(String mMembernum) {
		membernum = mMembernum;
	}
	public String getThreadnum() {
		return threadnum;
	}
	public void setThreadnum(String mThreadnum) {
		threadnum = mThreadnum;
	}
	public String getPic() {
		return pic;
	}
	public void setPic(String mPic) {
		pic = mPic;
	}
	public String getTid() {
		return tid;
	}
	public void setTid(String mTid) {
		tid = mTid;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String mSubject) {
		subject = mSubject;
	}
	
	public String getTuid() {
		return tuid;
	}
	public void setTuid(String mTuid) {
		tuid = mTuid;
	}
	public String getTuuid() {
		return tuuid;
	}
	public void setTuuid(String mTuuid) {
		tuuid = mTuuid;
	}
	public String getTusername() {
		return tusername;
	}
	public void setTusername(String mTusername) {
		tusername = mTusername;
	}
	public String getLastauthorid() {
		return lastauthorid;
	}
	public void setLastauthorid(String mLastauthorid) {
		lastauthorid = mLastauthorid;
	}
	public String getLastauthor() {
		return lastauthor;
	}
	public void setLastauthor(String mLastauthor) {
		lastauthor = mLastauthor;
	}
	public String getViewnum() {
		return viewnum;
	}
	public void setViewnum(String mViewnum) {
		viewnum = mViewnum;
	}
	public String getReplynum() {
		return replynum;
	}
	public void setReplynum(String mReplynum) {
		replynum = mReplynum;
	}
	public String getDateline() {
		return dateline;
	}
	public void setDateline(String mDateline) {
		dateline = mDateline;
	}
	public String getLastpost() {
		return lastpost;
	}
	public void setLastpost(String mLastpost) {
		lastpost = mLastpost;
	}
	
	public String getItemType() {
		return itemType;
	}
	public void setItemType(String mItemType) {
		itemType = mItemType;
	}
//	/**
//     * Constructor.
//     * @param jid JID of the contact
//     */
//    public ContactGroup(final String jid) {//uid或tagid@type/username或（tagname转base64）
//    	String uid_type = StringUtils.parseBareAddress(jid);
////    	uid = StringUtils.parseBareAddress2(uid_type);
////    	mName = mJID;
////    	mStatus = Status.CONTACT_STATUS_DISCONNECT;
////    	mMsgState = null;
////    	mRes = new ArrayList<String>();
//    	String type_temp = StringUtils.parseResource2(uid_type);
//    	try{
//    		type = Integer.parseInt(type_temp);
//    	}catch(Exception e){
//    		type = 0;
//    	}
//    	switch(type){
//    		case 0:
//    			uid = StringUtils.parseBareAddress2(uid_type);
//    			username = StringUtils.parseResource(jid);
//    			break;
//    		case 1:
//    			tagid = StringUtils.parseBareAddress2(uid_type);
//    			try{
//    				tagname = new String(Base64.decode(StringUtils.parseResource(jid), Base64.NO_WRAP));
//    			}catch(Exception e){
//    				tagname = "";
//    			}
//    			break;
//    	}
//    }
    public String getJID(){
    	String jid = "";
    	switch(getType()){
    		case 0:
    			jid = getUid()+"@0/"+getUsername();
    			break;
    		case 1:
    			jid = getTagid()+"@1/"+Base64.encodeToString(getTagname().getBytes(), Base64.NO_WRAP);
    			break;
    		default:
    			break;
    	}
    	return jid;
    }
	
    
    
    
    public String toXML() {
    	StringBuilder buf = new StringBuilder();
    	if(getType()==0)
        	buf.append("<cgroup type=\""+getType()+"\" "
        			+"uid=\""+getUid()+"\" "
        			+"uuid=\""+getUuid()+"\" "
        			+"username=\""+getUsername()+"\" "
        			+"name=\""+getName()+"\" "
        			+"sex=\""+getSex()+"\" "
        			+"blood=\""+getBlood()+"\" "
        			+"marry=\""+getMarry()+"\" "
        			+"avatar=\""+getAvatar()+"\" "
        			+"avatarPath=\""+getAvatarPath()+"\" "
        			+"email=\""+getEmail()+"\" "
        			+"mobile=\""+getMobile()+"\" "
        			+"qq=\""+getQq()+"\" "
        			+"birthyear=\""+getBirthyear()+"\" "
        			+"birthmonth=\""+getBirthmonth()+"\" "
        			+"birthday=\""+getBirthday()+"\" "
        			+"birthprovince=\""+getBirthprovince()+"\" "
        			+"birthcity=\""+getBirthcity()+"\" "
        			+"resideprovince=\""+getResideprovince()+"\" "
        			+"residecity=\""+getResidecity()+"\" "
        			+"note=\""+getNote()+"\""
        			+"itemType=\""+getItemType()+"\""
        			+">")
        			.append(getUsername())
        			.append("</cgroup>");
    	else if(getType()==1)
        	buf.append("<cgroup type=\""+getType()+"\" "
        			+"tagid=\""+getTagid()+"\" "
        			+"tagname=\""+getTagname()+"\" "
        			+"fieldid=\""+getFieldid()+"\" "
        			+"title=\""+getTitle()+"\" "
        			+"membernum=\""+getMembernum()+"\" "
        			+"threadnum=\""+getThreadnum()+"\" "
        			+"pic=\""+getPic()+"\" "
        			+"tid=\""+getTid()+"\" "
        			+"subject=\""+getSubject()+"\" "
        			+"tuid=\""+getTuid()+"\" "
        			+"tuuid=\""+getTuuid()+"\" "
        			+"tusername=\""+getTusername()+"\" "
        			+"lastauthorid=\""+getLastauthorid()+"\" "
        			+"lastauthor=\""+getLastauthor()+"\" "
        			+"viewnum=\""+getViewnum()+"\" "
        			+"replynum=\""+getReplynum()+"\" "
        			+"dateline=\""+getDateline()+"\" "
        			+"lastpost=\""+getLastpost()+"\""
        			+"itemType=\""+getItemType()+"\""
        			+">")
        			.append(getTagname())
        			.append("</cgroup>");
    	return buf.toString();
    }
}


