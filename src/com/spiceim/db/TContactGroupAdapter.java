package com.spiceim.db;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;


import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.util.Log;

import com.lidroid.xutils.DbUtils;
import com.lidroid.xutils.db.sqlite.Selector;
import com.lidroid.xutils.db.sqlite.WhereBuilder;
import com.lidroid.xutils.exception.DbException;
import com.spice.im.utils.StringUtils;
import com.stb.core.chat.ContactGroup;
import com.stb.isharemessage.BeemApplication;

public class TContactGroupAdapter {
	private static final String TAG = "TContactGroupAdapter";
	private static TContactGroupAdapter instance = null;
	public static TContactGroupAdapter getInstance(Context context){
		if(instance==null)
			instance = new TContactGroupAdapter(context);
		return instance;
	}
	public TContactGroupAdapter(Context context){
		mContext = context;
		initialDB();	
	}
	
    private Context mContext;
    public void setContext(Context context){
    	mContext = context;
    }
    public Context getContext(){
    	return mContext;
    }
    private DbUtils db;
    public void initialDB(){
    	db = DbUtils.create(mContext);
        db.configAllowTransaction(true);
        db.configDebug(true);
    }
    
    public static String sqliteEscape(String keyWord){  
        keyWord = keyWord.replace("/", "//");  
        keyWord = keyWord.replace("'", "''");  
        keyWord = keyWord.replace("[", "/[");  
        keyWord = keyWord.replace("]", "/]");  
        keyWord = keyWord.replace("%", "/%");  
        keyWord = keyWord.replace("&","/&");  
        keyWord = keyWord.replace("_", "/_");  
        keyWord = keyWord.replace("(", "/(");  
        keyWord = keyWord.replace(")", "/)");  
        return keyWord;  
    } 
    
    public static String reverseSqliteEscape(String keyWord){  
        keyWord = keyWord.replace("//", "/");  
        keyWord = keyWord.replace("''", "'");  
        keyWord = keyWord.replace("/[", "[");  
        keyWord = keyWord.replace("/]", "]");  
        keyWord = keyWord.replace("/%", "%");  
        keyWord = keyWord.replace("/&","&");  
        keyWord = keyWord.replace("/_", "_");  
        keyWord = keyWord.replace("/(", "(");  
        keyWord = keyWord.replace("/)", ")");  
        return keyWord;  
    } 
    
    public String addTContactGroup(ContactGroup contactGroup,String owner) {//单个存储
    	Log.e(TAG,"20170811children size : contactGroup.getJID()=" + contactGroup.toXML());
    	String state = Environment.getExternalStorageState();
    	TContactGroup tContactGroup = new TContactGroup();
    	try{
    		
	        if(contactGroup.getJID()!=null 
	        		&& !contactGroup.getJID().equalsIgnoreCase("null")
	        		&& contactGroup.getJID().length()!=0
	        		&& contactGroup.getJID().indexOf("@")!=-1)
	        	tContactGroup.setBareJID(StringUtils.parseBareAddress(contactGroup.getJID()));
	        
	        tContactGroup.setType(contactGroup.getType());
	    	
	        if(contactGroup.getUid()!=null)
	        	tContactGroup.setUid(sqliteEscape(contactGroup.getUid()));
	        
	        if(contactGroup.getUuid()!=null)
	        	tContactGroup.setUuid(sqliteEscape(contactGroup.getUuid()));
	        
	        if(contactGroup.getUsername()!=null)
	        	tContactGroup.setUsername(sqliteEscape(contactGroup.getUsername()));
	        
	        if(contactGroup.getName()!=null)
	        	tContactGroup.setName(sqliteEscape(contactGroup.getName())) ;
	        
	    	tContactGroup.setSex(contactGroup.getSex());
	    	
	    	if(contactGroup.getBlood()!=null)
	    		tContactGroup.setBlood(sqliteEscape(contactGroup.getBlood()));
	    	
	    	
	    	tContactGroup.setMarry(contactGroup.getMarry());
	    	
	    	if(contactGroup.getAvatar()!=null)
	    		tContactGroup.setAvatar(sqliteEscape(contactGroup.getAvatar()));
	        
	    	if(contactGroup.getAvatarPath()!=null){
//	    		Log.e(TAG,"================20170811children size : contactGroup.getAvatarPath()=" + contactGroup.getAvatarPath());
//	    		URLEncoder.encode(String s, String charset)
//	    		URLEncoder.encode(StringUtils.encodeBase64(contactGroup.getAvatarPath()),"ISO-8859-1");
//	    		tContactGroup.setAvatarPath(sqliteEscape(contactGroup.getAvatarPath()));
	    		tContactGroup.setAvatarPath(URLEncoder.encode(StringUtils.encodeBase64(contactGroup.getAvatarPath()),"ISO-8859-1"));
//	    		Log.e(TAG,"================20170811children size : sqliteEscape(contactGroup.getAvatarPath())=" + sqliteEscape(contactGroup.getAvatarPath()));
	    	}
	    	
	    	if(contactGroup.getEmail()!=null)
	    		tContactGroup.setEmail(sqliteEscape(contactGroup.getEmail()));
	    	
	    	if(contactGroup.getMobile()!=null)
	    		tContactGroup.setMobile(sqliteEscape(contactGroup.getMobile()));
	    	
	    	if(contactGroup.getQq()!=null)
	    		tContactGroup.setQq(sqliteEscape(contactGroup.getQq()));
	    	
	    	if(contactGroup.getBirthyear()!=null)
	    		tContactGroup.setBirthyear(sqliteEscape(contactGroup.getBirthyear()));
	    	
	    	if(contactGroup.getBirthmonth()!=null)
	    		tContactGroup.setBirthmonth(sqliteEscape(contactGroup.getBirthmonth()));
	    	
	    	if(contactGroup.getBirthday()!=null)
	    		tContactGroup.setBirthday(sqliteEscape(contactGroup.getBirthday()));
	    	
	    	if(contactGroup.getBirthprovince()!=null)
	    		tContactGroup.setBirthprovince(sqliteEscape(contactGroup.getBirthprovince()));
	    	
	    	if(contactGroup.getBirthcity()!=null)
	    		tContactGroup.setBirthcity(sqliteEscape(contactGroup.getBirthcity()));
	    	
	    	if(contactGroup.getResideprovince()!=null)
	    		tContactGroup.setResideprovince(sqliteEscape(contactGroup.getResideprovince()));
	    	
	    	if(contactGroup.getResidecity()!=null)
	    		tContactGroup.setResidecity(sqliteEscape(contactGroup.getResidecity()));
	    	
	    	if(contactGroup.getNote()!=null)
	    		tContactGroup.setNote(sqliteEscape(contactGroup.getNote()));
	        
	        
	    	if(contactGroup.getTagid()!=null)//群组属性开始
	    		tContactGroup.setTagid(sqliteEscape(contactGroup.getTagid()));
	    	
	    	if(contactGroup.getTagname()!=null)
	    		tContactGroup.setTagname(sqliteEscape(contactGroup.getTagname()));
	    	
	    	if(contactGroup.getFieldid()!=null)
	    		tContactGroup.setFieldid(sqliteEscape(contactGroup.getFieldid()));
	    	
	    	if(contactGroup.getTitle()!=null)
	    		tContactGroup.setTitle(sqliteEscape(contactGroup.getTitle()));
	    	
	    	if(contactGroup.getMembernum()!=null)
	    		tContactGroup.setMembernum(sqliteEscape(contactGroup.getMembernum()));
	    	
	    	if(contactGroup.getThreadnum()!=null)
	    		tContactGroup.setThreadnum(sqliteEscape(contactGroup.getThreadnum()));
	    	
	    	if(contactGroup.getPic()!=null)
//	    		tContactGroup.setPic(sqliteEscape(contactGroup.getPic()));
	    	tContactGroup.setPic(URLEncoder.encode(StringUtils.encodeBase64(contactGroup.getPic()),"ISO-8859-1"));
	    	
	    	if(contactGroup.getTid()!=null)
	    		tContactGroup.setTid(sqliteEscape(contactGroup.getTid()));
	    	
	    	if(contactGroup.getSubject()!=null)
	    		tContactGroup.setSubject(sqliteEscape(contactGroup.getSubject()));
	    	
	    	if(contactGroup.getTuid()!=null)
	    		tContactGroup.setTuid(sqliteEscape(contactGroup.getTuid()));
	    	
	    	if(contactGroup.getTuuid()!=null)
	    		tContactGroup.setTuuid(sqliteEscape(contactGroup.getTuuid()));
	    	
	    	if(contactGroup.getTusername()!=null)
	    		tContactGroup.setTusername(sqliteEscape(contactGroup.getTusername()));
	    	
	    	if(contactGroup.getLastauthorid()!=null)
	    		tContactGroup.setLastauthorid(sqliteEscape(contactGroup.getLastauthorid()));
	    	
	    	if(contactGroup.getLastauthor()!=null)
	    		tContactGroup.setLastauthor(sqliteEscape(contactGroup.getLastauthor()));
	    	
	    	if(contactGroup.getViewnum()!=null)
	    		tContactGroup.setViewnum(sqliteEscape(contactGroup.getViewnum()));
	    	
	    	if(contactGroup.getReplynum()!=null)
	    		tContactGroup.setReplynum(sqliteEscape(contactGroup.getReplynum()));
	    	
	    	if(contactGroup.getDateline()!=null)
	    		tContactGroup.setDateline(sqliteEscape(contactGroup.getDateline()));
	    	
	    	if(contactGroup.getLastpost()!=null)
	    		tContactGroup.setLastpost(sqliteEscape(contactGroup.getLastpost()));
	    	
	    	if(contactGroup.getItemType()!=null)
	    		tContactGroup.setItemType(sqliteEscape(contactGroup.getItemType()));
	        
	    	tContactGroup.setOwner(owner);
//	        db.saveBindingId(duitangInfo);//保存对象关联数据库生成的id   保存一条记录
//	        db.saveOrUpdate(tContactGroup);//20200315 修改测试
	        db.replace(tContactGroup);
	        
//	        db.saveOrUpdateAll(entities);
	        
//	        List<TContactGroup> children = db.findAll(Selector.from(TContactGroup.class));
//	        Log.e(TAG,"children size:" + children.size());
//	        if (children.size() > 0) {
//	        	for(int i=0;i<children.size();i++)
//	        		Log.e(TAG,"last children单个存储结果:" + children.get(i).toXML());
//	        }
//	        children = null;
        
        } catch (DbException e) {
            e.printStackTrace();
            
        } catch (Exception e){
        	e.printStackTrace();
        }
        Log.e("※※※※※####20141014####※※※※※", "※※数据库addTContactGroup key※※:"+tContactGroup.getBareJID());
        return tContactGroup.getBareJID();
        
    }
    
    
    
    public int addTContactGroups(List<ContactGroup> contactGroups,String owner) {//批量存储
//    	Log.e(TAG,"children size : duitangInfo.getKey()=" + duitangInfo.getKey()+";duitangInfo.getSign()="+duitangInfo.getSign());
    	String state = Environment.getExternalStorageState();
    	List<TContactGroup> tContactGroups = new ArrayList<TContactGroup>();
    	TContactGroup tContactGroup = null;
    	ContactGroup contactGroup = null;
    	try{
    		for(int i=0;i<contactGroups.size();i++){
    			tContactGroup = new TContactGroup();
    			contactGroup = (ContactGroup)contactGroups.get(i);
    			Log.e(TAG,"addTContactGroups: contactGroup.getJID()=" + contactGroup.getJID());
    	        if(contactGroup.getJID()!=null 
    	        		&& !contactGroup.getJID().equalsIgnoreCase("null")
    	        		&& contactGroup.getJID().length()!=0
    	        		&& contactGroup.getJID().indexOf("@")!=-1)
    	        	tContactGroup.setBareJID(StringUtils.parseBareAddress(contactGroup.getJID()));
    	        
    	        tContactGroup.setType(contactGroup.getType());
    	    	
    	        if(contactGroup.getUid()!=null)
    	        	tContactGroup.setUid(sqliteEscape(contactGroup.getUid()));
    	        
    	        if(contactGroup.getUuid()!=null)
    	        	tContactGroup.setUuid(sqliteEscape(contactGroup.getUuid()));
    	        
    	        if(contactGroup.getUsername()!=null)
    	        	tContactGroup.setUsername(sqliteEscape(contactGroup.getUsername()));
    	        
    	        if(contactGroup.getName()!=null)
    	        	tContactGroup.setName(sqliteEscape(contactGroup.getName())) ;
    	        
    	    	tContactGroup.setSex(contactGroup.getSex());
    	    	
    	    	if(contactGroup.getBlood()!=null)
    	    		tContactGroup.setBlood(sqliteEscape(contactGroup.getBlood()));
    	    	
    	    	
    	    	tContactGroup.setMarry(contactGroup.getMarry());
    	    	
    	    	if(contactGroup.getAvatar()!=null)
    	    		tContactGroup.setAvatar(sqliteEscape(contactGroup.getAvatar()));
    	        
    	    	if(contactGroup.getAvatarPath()!=null)
//    	    		tContactGroup.setAvatarPath(sqliteEscape(contactGroup.getAvatarPath()));
    	    	tContactGroup.setAvatarPath(URLEncoder.encode(StringUtils.encodeBase64(contactGroup.getAvatarPath()),"ISO-8859-1"));
    	    	
    	    	if(contactGroup.getEmail()!=null)
    	    		tContactGroup.setEmail(sqliteEscape(contactGroup.getEmail()));
    	    	
    	    	if(contactGroup.getMobile()!=null)
    	    		tContactGroup.setMobile(sqliteEscape(contactGroup.getMobile()));
    	    	
    	    	if(contactGroup.getQq()!=null)
    	    		tContactGroup.setQq(sqliteEscape(contactGroup.getQq()));
    	    	
    	    	if(contactGroup.getBirthyear()!=null)
    	    		tContactGroup.setBirthyear(sqliteEscape(contactGroup.getBirthyear()));
    	    	
    	    	if(contactGroup.getBirthmonth()!=null)
    	    		tContactGroup.setBirthmonth(sqliteEscape(contactGroup.getBirthmonth()));
    	    	
    	    	if(contactGroup.getBirthday()!=null)
    	    		tContactGroup.setBirthday(sqliteEscape(contactGroup.getBirthday()));
    	    	
    	    	if(contactGroup.getBirthprovince()!=null)
    	    		tContactGroup.setBirthprovince(sqliteEscape(contactGroup.getBirthprovince()));
    	    	
    	    	if(contactGroup.getBirthcity()!=null)
    	    		tContactGroup.setBirthcity(sqliteEscape(contactGroup.getBirthcity()));
    	    	
    	    	if(contactGroup.getResideprovince()!=null)
    	    		tContactGroup.setResideprovince(sqliteEscape(contactGroup.getResideprovince()));
    	    	
    	    	if(contactGroup.getResidecity()!=null)
    	    		tContactGroup.setResidecity(sqliteEscape(contactGroup.getResidecity()));
    	    	
    	    	if(contactGroup.getNote()!=null)
    	    		tContactGroup.setNote(sqliteEscape(contactGroup.getNote()));
    	        
    	        
    	    	if(contactGroup.getTagid()!=null)//群组属性开始
    	    		tContactGroup.setTagid(sqliteEscape(contactGroup.getTagid()));
    	    	
    	    	if(contactGroup.getTagname()!=null)
    	    		tContactGroup.setTagname(sqliteEscape(contactGroup.getTagname()));
    	    	
    	    	if(contactGroup.getFieldid()!=null)
    	    		tContactGroup.setFieldid(sqliteEscape(contactGroup.getFieldid()));
    	    	
    	    	if(contactGroup.getTitle()!=null)
    	    		tContactGroup.setTitle(sqliteEscape(contactGroup.getTitle()));
    	    	
    	    	if(contactGroup.getMembernum()!=null)
    	    		tContactGroup.setMembernum(sqliteEscape(contactGroup.getMembernum()));
    	    	
    	    	if(contactGroup.getThreadnum()!=null)
    	    		tContactGroup.setThreadnum(sqliteEscape(contactGroup.getThreadnum()));
    	    	
    	    	if(contactGroup.getPic()!=null)
//    	    		tContactGroup.setPic(sqliteEscape(contactGroup.getPic()));
    	    	tContactGroup.setPic(URLEncoder.encode(StringUtils.encodeBase64(contactGroup.getPic()),"ISO-8859-1"));
    	    	
    	    	if(contactGroup.getTid()!=null)
    	    		tContactGroup.setTid(sqliteEscape(contactGroup.getTid()));
    	    	
    	    	if(contactGroup.getSubject()!=null)
    	    		tContactGroup.setSubject(sqliteEscape(contactGroup.getSubject()));
    	    	
    	    	if(contactGroup.getTuid()!=null)
    	    		tContactGroup.setTuid(sqliteEscape(contactGroup.getTuid()));
    	    	
    	    	if(contactGroup.getTuuid()!=null)
    	    		tContactGroup.setTuuid(sqliteEscape(contactGroup.getTuuid()));
    	    	
    	    	if(contactGroup.getTusername()!=null)
    	    		tContactGroup.setTusername(sqliteEscape(contactGroup.getTusername()));
    	    	
    	    	if(contactGroup.getLastauthorid()!=null)
    	    		tContactGroup.setLastauthorid(sqliteEscape(contactGroup.getLastauthorid()));
    	    	
    	    	if(contactGroup.getLastauthor()!=null)
    	    		tContactGroup.setLastauthor(sqliteEscape(contactGroup.getLastauthor()));
    	    	
    	    	if(contactGroup.getViewnum()!=null)
    	    		tContactGroup.setViewnum(sqliteEscape(contactGroup.getViewnum()));
    	    	
    	    	if(contactGroup.getReplynum()!=null)
    	    		tContactGroup.setReplynum(sqliteEscape(contactGroup.getReplynum()));
    	    	
    	    	if(contactGroup.getDateline()!=null)
    	    		tContactGroup.setDateline(sqliteEscape(contactGroup.getDateline()));
    	    	
    	    	if(contactGroup.getLastpost()!=null)
    	    		tContactGroup.setLastpost(sqliteEscape(contactGroup.getLastpost()));
    	    	
    	    	if(contactGroup.getItemType()!=null)
    	    		tContactGroup.setItemType(sqliteEscape(contactGroup.getItemType()));
    	    	tContactGroup.setOwner(owner);
    	    	tContactGroups.add(tContactGroup);
    		}
//	        db.saveBindingId(duitangInfo);//保存对象关联数据库生成的id   保存一条记录
//	        db.saveOrUpdate(duitangInfo);
//	        db.saveOrUpdateAll(tContactGroups);//20200315 修改测试
	        db.replaceAll(tContactGroups);
//	        Log.e("※※※※※####20141014####※※※※※", "※※数据库addDuitangInfo※※:" + duitangInfo.toString());
//	        
//	        List<TContactGroup> children = db.findAll(Selector.from(TContactGroup.class));
//	        Log.e(TAG,"children size:" + children.size());
//	        if (children.size() > 0) {
//	        	for(int i=0;i<children.size();i++)
//	        		Log.e(TAG,"last children批量存储结果:" + children.get(i).toXML());
//	        }
//	        children = null;
        
        } catch (DbException e) {
            e.printStackTrace();
            
        } catch (Exception e){
        	e.printStackTrace();
        }
//        Log.e("※※※※※####20141014####※※※※※", "※※数据库addDuitangInfo key※※:"+duitangInfo.getKey());
        return tContactGroups.size();
        
    }
    
    public TContactGroup getTContactGroup(String bareJID){
    	TContactGroup tContactGroup = null;
    	try{
//    		tContactGroup = db.findById(TContactGroup.class, bareJID);
    		tContactGroup = db.findFirst((Selector.from(TContactGroup.class)).where(WhereBuilder.b("bareJID","=",bareJID)));
    		if(tContactGroup!=null){
    			Log.e(TAG,"last children查询结果:" + tContactGroup.toXML());
    	        if(tContactGroup.getJID()!=null 
    	        		&& !tContactGroup.getJID().equalsIgnoreCase("null")
    	        		&& tContactGroup.getJID().length()!=0
    	        		&& tContactGroup.getJID().indexOf("@")!=-1)
    	        	tContactGroup.setBareJID(reverseSqliteEscape(tContactGroup.getBareJID()));
    	        
    	        tContactGroup.setType(tContactGroup.getType());
    	    	
    	        if(tContactGroup.getUid()!=null)
    	        	tContactGroup.setUid(reverseSqliteEscape(tContactGroup.getUid()));
    	        
    	        if(tContactGroup.getUuid()!=null)
    	        	tContactGroup.setUuid(reverseSqliteEscape(tContactGroup.getUuid()));
    	        
    	        if(tContactGroup.getUsername()!=null)
    	        	tContactGroup.setUsername(reverseSqliteEscape(tContactGroup.getUsername()));
    	        
    	        if(tContactGroup.getName()!=null)
//    	        	tContactGroup.setName(reverseSqliteEscape(tContactGroup.getName())) ;
    	        	tContactGroup.setName(tContactGroup.getName()) ;
    	        
    	    	tContactGroup.setSex(tContactGroup.getSex());
    	    	
    	    	if(tContactGroup.getBlood()!=null)
    	    		tContactGroup.setBlood(reverseSqliteEscape(tContactGroup.getBlood()));
    	    	
    	    	
    	    	tContactGroup.setMarry(tContactGroup.getMarry());
    	    	
    	    	if(tContactGroup.getAvatar()!=null)
    	    		tContactGroup.setAvatar(reverseSqliteEscape(tContactGroup.getAvatar()));
    	        
    	    	if(tContactGroup.getAvatarPath()!=null){
//    	    		Log.e(TAG,"================20171220 search children size : contactGroup.getAvatarPath()="+tContactGroup.getAvatarPath());
//    	    		tContactGroup.setAvatarPath(reverseSqliteEscape(tContactGroup.getAvatarPath()));
    	    		tContactGroup.setAvatarPath(new String(StringUtils.decodeBase64(URLDecoder.decode(tContactGroup.getAvatarPath())),"ISO-8859-1"));
//    	    		Log.e(TAG,"================20171220 search children size : contactGroup.getAvatarPath()="+reverseSqliteEscape(tContactGroup.getAvatarPath()));
    	    	}
    	    	if(tContactGroup.getEmail()!=null)
    	    		tContactGroup.setEmail(reverseSqliteEscape(tContactGroup.getEmail()));
    	    	
    	    	if(tContactGroup.getMobile()!=null)
    	    		tContactGroup.setMobile(reverseSqliteEscape(tContactGroup.getMobile()));
    	    	
    	    	if(tContactGroup.getQq()!=null)
    	    		tContactGroup.setQq(reverseSqliteEscape(tContactGroup.getQq()));
    	    	
    	    	if(tContactGroup.getBirthyear()!=null)
    	    		tContactGroup.setBirthyear(reverseSqliteEscape(tContactGroup.getBirthyear()));
    	    	
    	    	if(tContactGroup.getBirthmonth()!=null)
    	    		tContactGroup.setBirthmonth(reverseSqliteEscape(tContactGroup.getBirthmonth()));
    	    	
    	    	if(tContactGroup.getBirthday()!=null)
    	    		tContactGroup.setBirthday(reverseSqliteEscape(tContactGroup.getBirthday()));
    	    	
    	    	if(tContactGroup.getBirthprovince()!=null)
    	    		tContactGroup.setBirthprovince(reverseSqliteEscape(tContactGroup.getBirthprovince()));
    	    	
    	    	if(tContactGroup.getBirthcity()!=null)
    	    		tContactGroup.setBirthcity(reverseSqliteEscape(tContactGroup.getBirthcity()));
    	    	
    	    	if(tContactGroup.getResideprovince()!=null)
    	    		tContactGroup.setResideprovince(reverseSqliteEscape(tContactGroup.getResideprovince()));
    	    	
    	    	if(tContactGroup.getResidecity()!=null)
    	    		tContactGroup.setResidecity(reverseSqliteEscape(tContactGroup.getResidecity()));
    	    	
    	    	if(tContactGroup.getNote()!=null)
    	    		tContactGroup.setNote(reverseSqliteEscape(tContactGroup.getNote()));
    	        
    	        
    	    	if(tContactGroup.getTagid()!=null)//群组属性开始
    	    		tContactGroup.setTagid(reverseSqliteEscape(tContactGroup.getTagid()));
    	    	
    	    	if(tContactGroup.getTagname()!=null)
    	    		tContactGroup.setTagname(reverseSqliteEscape(tContactGroup.getTagname()));
    	    	
    	    	if(tContactGroup.getFieldid()!=null)
    	    		tContactGroup.setFieldid(reverseSqliteEscape(tContactGroup.getFieldid()));
    	    	
    	    	if(tContactGroup.getTitle()!=null)
    	    		tContactGroup.setTitle(reverseSqliteEscape(tContactGroup.getTitle()));
    	    	
    	    	if(tContactGroup.getMembernum()!=null)
    	    		tContactGroup.setMembernum(reverseSqliteEscape(tContactGroup.getMembernum()));
    	    	
    	    	if(tContactGroup.getThreadnum()!=null)
    	    		tContactGroup.setThreadnum(reverseSqliteEscape(tContactGroup.getThreadnum()));
    	    	
    	    	if(tContactGroup.getPic()!=null)
//    	    		tContactGroup.setPic(reverseSqliteEscape(tContactGroup.getPic()));
    	    		tContactGroup.setPic(new String(StringUtils.decodeBase64(URLDecoder.decode(tContactGroup.getPic())),"ISO-8859-1"));
    	    	
    	    	if(tContactGroup.getTid()!=null)
    	    		tContactGroup.setTid(reverseSqliteEscape(tContactGroup.getTid()));
    	    	
    	    	if(tContactGroup.getSubject()!=null)
    	    		tContactGroup.setSubject(reverseSqliteEscape(tContactGroup.getSubject()));
    	    	
    	    	if(tContactGroup.getTuid()!=null)
    	    		tContactGroup.setTuid(reverseSqliteEscape(tContactGroup.getTuid()));
    	    	
    	    	if(tContactGroup.getTuuid()!=null)
    	    		tContactGroup.setTuuid(reverseSqliteEscape(tContactGroup.getTuuid()));
    	    	
    	    	if(tContactGroup.getTusername()!=null)
    	    		tContactGroup.setTusername(reverseSqliteEscape(tContactGroup.getTusername()));
    	    	
    	    	if(tContactGroup.getLastauthorid()!=null)
    	    		tContactGroup.setLastauthorid(reverseSqliteEscape(tContactGroup.getLastauthorid()));
    	    	
    	    	if(tContactGroup.getLastauthor()!=null)
    	    		tContactGroup.setLastauthor(reverseSqliteEscape(tContactGroup.getLastauthor()));
    	    	
    	    	if(tContactGroup.getViewnum()!=null)
    	    		tContactGroup.setViewnum(reverseSqliteEscape(tContactGroup.getViewnum()));
    	    	
    	    	if(tContactGroup.getReplynum()!=null)
    	    		tContactGroup.setReplynum(reverseSqliteEscape(tContactGroup.getReplynum()));
    	    	
    	    	if(tContactGroup.getDateline()!=null)
    	    		tContactGroup.setDateline(reverseSqliteEscape(tContactGroup.getDateline()));
    	    	
    	    	if(tContactGroup.getLastpost()!=null)
    	    		tContactGroup.setLastpost(reverseSqliteEscape(tContactGroup.getLastpost()));
    	    	
    	    	if(tContactGroup.getItemType()!=null)
    	    		tContactGroup.setItemType(reverseSqliteEscape(tContactGroup.getItemType()));
	        
    			Log.e("※※※※※####20141014####※※※※※", "※※数据库getTContactGroup key※※:"+bareJID+"=="+tContactGroup.toString());
    		}
    	}catch (DbException e) {
            e.printStackTrace();
            
        } catch (Exception e){
        	e.printStackTrace();
        }
        return tContactGroup;
    }
    private List<TContactGroup> list = null;
    private TContactGroup tContactGroup = null;
    private List<ContactGroup> list_ContactGroup = null;
    private ContactGroup contactGroup = null;
    public List<ContactGroup> getContactGroupsByPageIndex(int pageSize,int pageNum,String username) throws RemoteException {
//    	SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(mContext);
//	    String login = settings.getString(BeemApplication.ACCOUNT_USERNAME_KEY, "");
    	try{
    		int pageIndex = pageNum;
    		list = db.findAll(Selector.from(TContactGroup.class)
    				.where(WhereBuilder.b("username","!=",username))
    				.and(WhereBuilder.b("owner","=",username))
    				.limit(pageSize)  //pageSize
		              .offset(pageSize * pageIndex)); //pageSize * pageIndex  
    		
    		if(list!=null && list.size()!=0){
    			list_ContactGroup = new ArrayList<ContactGroup>();
    			for(int i=0;i<list.size();i++){
    				tContactGroup = (TContactGroup)list.get(i);
//    				if(!tContactGroup.getUsername().equals(login)){
	    				contactGroup = toContactGroup(tContactGroup);
	    				if(contactGroup!=null)
	    					list_ContactGroup.add(contactGroup);
//    				}
    			}
    		}
    		return list_ContactGroup;
    	}catch(DbException e){
    		e.printStackTrace();
    	}
    	return null;
    }
    public ContactGroup toContactGroup(TContactGroup tContactGroup){
    	ContactGroup contactGroup = null;
    	try{
    		if(tContactGroup!=null){
    			contactGroup = new ContactGroup();
//    			Log.e(TAG,"last children查询结果:" + tContactGroup.toXML());
//    	        if(tContactGroup.getJID()!=null 
//    	        		&& !tContactGroup.getJID().equalsIgnoreCase("null")
//    	        		&& tContactGroup.getJID().length()!=0
//    	        		&& tContactGroup.getJID().indexOf("@")!=-1)
//    	        	contactGroup.setUid(reverseSqliteEscape(tContactGroup.getBareJID()));
    	        
    			contactGroup.setType(tContactGroup.getType());
    	    	
    	        if(tContactGroup.getUid()!=null)
    	        	contactGroup.setUid(reverseSqliteEscape(tContactGroup.getUid()));
    	        
    	        if(tContactGroup.getUuid()!=null)
    	        	contactGroup.setUuid(reverseSqliteEscape(tContactGroup.getUuid()));
    	        
    	        if(tContactGroup.getUsername()!=null)
    	        	contactGroup.setUsername(reverseSqliteEscape(tContactGroup.getUsername()));
    	        
    	        if(tContactGroup.getName()!=null)
//    	        	tContactGroup.setName(reverseSqliteEscape(tContactGroup.getName())) ;
    	        	contactGroup.setName(tContactGroup.getName()) ;
    	        
    	        contactGroup.setSex(tContactGroup.getSex());
    	    	
    	    	if(tContactGroup.getBlood()!=null)
    	    		contactGroup.setBlood(reverseSqliteEscape(tContactGroup.getBlood()));
    	    	
    	    	
    	    	contactGroup.setMarry(tContactGroup.getMarry());
    	    	
    	    	if(tContactGroup.getAvatar()!=null)
    	    		contactGroup.setAvatar(reverseSqliteEscape(tContactGroup.getAvatar()));
    	        
    	    	if(tContactGroup.getAvatarPath()!=null)
//    	    		contactGroup.setAvatarPath(reverseSqliteEscape(tContactGroup.getAvatarPath()));
    	    	contactGroup.setAvatarPath(new String(StringUtils.decodeBase64(URLDecoder.decode(tContactGroup.getAvatarPath())),"ISO-8859-1"));
    	    	
    	    	if(tContactGroup.getEmail()!=null)
    	    		contactGroup.setEmail(reverseSqliteEscape(tContactGroup.getEmail()));
    	    	
    	    	if(tContactGroup.getMobile()!=null)
    	    		contactGroup.setMobile(reverseSqliteEscape(tContactGroup.getMobile()));
    	    	
    	    	if(tContactGroup.getQq()!=null)
    	    		contactGroup.setQq(reverseSqliteEscape(tContactGroup.getQq()));
    	    	
    	    	if(tContactGroup.getBirthyear()!=null)
    	    		contactGroup.setBirthyear(reverseSqliteEscape(tContactGroup.getBirthyear()));
    	    	
    	    	if(tContactGroup.getBirthmonth()!=null)
    	    		contactGroup.setBirthmonth(reverseSqliteEscape(tContactGroup.getBirthmonth()));
    	    	
    	    	if(tContactGroup.getBirthday()!=null)
    	    		contactGroup.setBirthday(reverseSqliteEscape(tContactGroup.getBirthday()));
    	    	
    	    	if(tContactGroup.getBirthprovince()!=null)
    	    		contactGroup.setBirthprovince(reverseSqliteEscape(tContactGroup.getBirthprovince()));
    	    	
    	    	if(tContactGroup.getBirthcity()!=null)
    	    		contactGroup.setBirthcity(reverseSqliteEscape(tContactGroup.getBirthcity()));
    	    	
    	    	if(tContactGroup.getResideprovince()!=null)
    	    		contactGroup.setResideprovince(reverseSqliteEscape(tContactGroup.getResideprovince()));
    	    	
    	    	if(tContactGroup.getResidecity()!=null)
    	    		contactGroup.setResidecity(reverseSqliteEscape(tContactGroup.getResidecity()));
    	    	
    	    	if(tContactGroup.getNote()!=null)
    	    		contactGroup.setNote(reverseSqliteEscape(tContactGroup.getNote()));
    	        
    	        
    	    	if(tContactGroup.getTagid()!=null)//群组属性开始
    	    		contactGroup.setTagid(reverseSqliteEscape(tContactGroup.getTagid()));
    	    	
    	    	if(tContactGroup.getTagname()!=null)
    	    		contactGroup.setTagname(reverseSqliteEscape(tContactGroup.getTagname()));
    	    	
    	    	if(tContactGroup.getFieldid()!=null)
    	    		contactGroup.setFieldid(reverseSqliteEscape(tContactGroup.getFieldid()));
    	    	
    	    	if(tContactGroup.getTitle()!=null)
    	    		contactGroup.setTitle(reverseSqliteEscape(tContactGroup.getTitle()));
    	    	
    	    	if(tContactGroup.getMembernum()!=null)
    	    		contactGroup.setMembernum(reverseSqliteEscape(tContactGroup.getMembernum()));
    	    	
    	    	if(tContactGroup.getThreadnum()!=null)
    	    		contactGroup.setThreadnum(reverseSqliteEscape(tContactGroup.getThreadnum()));
    	    	
    	    	if(tContactGroup.getPic()!=null)
//    	    		contactGroup.setPic(reverseSqliteEscape(tContactGroup.getPic()));
    	    		contactGroup.setPic(new String(StringUtils.decodeBase64(URLDecoder.decode(tContactGroup.getPic())),"ISO-8859-1"));
    	    	
    	    	if(tContactGroup.getTid()!=null)
    	    		contactGroup.setTid(reverseSqliteEscape(tContactGroup.getTid()));
    	    	
    	    	if(tContactGroup.getSubject()!=null)
    	    		contactGroup.setSubject(reverseSqliteEscape(tContactGroup.getSubject()));
    	    	
    	    	if(tContactGroup.getTuid()!=null)
    	    		contactGroup.setTuid(reverseSqliteEscape(tContactGroup.getTuid()));
    	    	
    	    	if(tContactGroup.getTuuid()!=null)
    	    		contactGroup.setTuuid(reverseSqliteEscape(tContactGroup.getTuuid()));
    	    	
    	    	if(tContactGroup.getTusername()!=null)
    	    		contactGroup.setTusername(reverseSqliteEscape(tContactGroup.getTusername()));
    	    	
    	    	if(tContactGroup.getLastauthorid()!=null)
    	    		contactGroup.setLastauthorid(reverseSqliteEscape(tContactGroup.getLastauthorid()));
    	    	
    	    	if(tContactGroup.getLastauthor()!=null)
    	    		contactGroup.setLastauthor(reverseSqliteEscape(tContactGroup.getLastauthor()));
    	    	
    	    	if(tContactGroup.getViewnum()!=null)
    	    		contactGroup.setViewnum(reverseSqliteEscape(tContactGroup.getViewnum()));
    	    	
    	    	if(tContactGroup.getReplynum()!=null)
    	    		contactGroup.setReplynum(reverseSqliteEscape(tContactGroup.getReplynum()));
    	    	
    	    	if(tContactGroup.getDateline()!=null)
    	    		contactGroup.setDateline(reverseSqliteEscape(tContactGroup.getDateline()));
    	    	
    	    	if(tContactGroup.getLastpost()!=null)
    	    		contactGroup.setLastpost(reverseSqliteEscape(tContactGroup.getLastpost()));
    	    	
    	    	if(tContactGroup.getItemType()!=null)
    	    		contactGroup.setItemType(reverseSqliteEscape(tContactGroup.getItemType()));
	        
    		}
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	return contactGroup;
    }
}
