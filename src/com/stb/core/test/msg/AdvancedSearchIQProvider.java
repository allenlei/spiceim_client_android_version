/*
 * Copyright (C) 2010 Moduad Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.stb.core.test.msg;

import org.jivesoftware.smack.packet.IQ;


import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;

/** 
 * This class parses incoming IQ packets to NotificationIQ objects.
 *
 * @author Sehwan Noh (devnoh@gmail.com)
 */
public class AdvancedSearchIQProvider implements IQProvider {

    public AdvancedSearchIQProvider() {
    }

    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {

    	AdvancedSearchIQ advancedSearchIQ = new AdvancedSearchIQ();
        for (boolean done = false; !done;) {
            int eventType = parser.next();
            if (eventType == 2) {
                if ("id".equals(parser.getName())) {
                	advancedSearchIQ.setId(parser.nextText());
                }
                if ("apikey".equals(parser.getName())) {
                	advancedSearchIQ.setApikey(parser.nextText());
                }
                if ("searchkey".equals(parser.getName())) {
                	advancedSearchIQ.setSearchkey(parser.nextText());
                }
                if ("searchtype".equals(parser.getName())) {
                	advancedSearchIQ.setSearchtype(parser.nextText());
                }
                if ("ages".equals(parser.getName())) {
                	advancedSearchIQ.setAges(parser.nextText());
                }
                if ("genders".equals(parser.getName())) {
                	advancedSearchIQ.setGenders(parser.nextText());
                }
                if ("distances".equals(parser.getName())) {
                	advancedSearchIQ.setDistances(parser.nextText());
                }
                if ("online".equals(parser.getName())) {
                	advancedSearchIQ.setOnline(parser.nextText());
                }
                if ("startIndex".equals(parser.getName())) {
                	advancedSearchIQ.setStartIndex(parser.nextText());
                }
                if ("numResults".equals(parser.getName())) {
                	advancedSearchIQ.setNumResults(parser.nextText());
                }
            } else if (eventType == 3
                    && "advancedsearchiq".equals(parser.getName())) {
                done = true;
            }
        }

        return advancedSearchIQ;
    }

}
