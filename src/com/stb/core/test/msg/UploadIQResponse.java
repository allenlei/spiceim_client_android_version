/*
 * Copyright (C) 2010 Moduad Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.stb.core.test.msg;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.jivesoftware.smack.packet.IQ;

/** 
 * This class represents a notifcatin IQ packet.
 *
 * @author Sehwan Noh (devnoh@gmail.com)
 */
public class UploadIQResponse extends IQ {

    private String id;

    private String filename;

    private String minetype;

    private String status;//0 success,1 false

    public UploadIQResponse() {
    }

    @Override
    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append("uploadiq").append(" xmlns=\"").append(
                "com:stb:uploadiq").append("\">");
        if (id != null) {
            buf.append("<id>").append(id).append("</id>");
        }
        if (filename != null) {
            buf.append("<filename>").append(filename).append("</filename>");
        }
        if (minetype != null) {
            buf.append("<minetype>").append(minetype).append("</minetype>");
        }
        if(status!=null){
        	buf.append("<status>").append(status).append("</status>");
        }
        buf.append("</").append("uploadiq").append(">");
        return buf.toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getMinetype() {
		return minetype;
	}
	public void setMinetype(String minetype) {
		this.minetype = minetype;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

}
