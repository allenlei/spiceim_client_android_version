package com.stb.core.test.msg;

import org.jivesoftware.smack.packet.IQ;

import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;

public class BindingPartnerIQResponseProvider implements IQProvider {
//	HashMap parameters = new HashMap();
    public BindingPartnerIQResponseProvider() {
    }

    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {

//    	ThreePartPushIQ threePartPushIQ = new ThreePartPushIQ();
    	BindingPartnerIQResponse bindingPartnerIQResponse = new BindingPartnerIQResponse();
        for (boolean done = false; !done;) {
            int eventType = parser.next();
            if (eventType == 2) {
                if ("id".equals(parser.getName())) {
                	bindingPartnerIQResponse.setId(parser.nextText());
//                	threePartPushIQResponse.setId(threePartPushIQ.getId());
                }
                if ("uuid".equals(parser.getName())) {
                	bindingPartnerIQResponse.setUuid(parser.nextText());
                }
                if ("retcode".equals(parser.getName())) {
                	bindingPartnerIQResponse.setRetcode(parser.nextText());
                }
                if ("memo".equals(parser.getName())) {
                	bindingPartnerIQResponse.setMemo(parser.nextText());
                }
            } else if (eventType == 3
                    && "bindingpartneriq".equals(parser.getName())) {//elementName = bindingpartneriq
            	
                done = true;
            }
        }
        
        return bindingPartnerIQResponse;
        		
    }
  
}
