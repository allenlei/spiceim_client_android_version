package com.stb.core.test.msg;

import org.jivesoftware.smack.packet.IQ;

public class BindingPartnerIQ extends IQ {
    //elementName = bindingpartneriq
	//namespace = com:isharemessage:bindingpartneriq
    private String id;

    private String apikey;
    
    private String partnerid;

    private String partneruserid;
    
    private String uuid;
    
    private String hash;//partnerid+partneruserid+uuid 取sha-1摘要
    
    public BindingPartnerIQ() {
    }

    @Override
    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append("bindingpartneriq").append(" xmlns=\"").append(
                "com:isharemessage:bindingpartneriq").append("\">");
        if (id != null) {
            buf.append("<id>").append(id).append("</id>");
        }
        if (apikey != null) {
            buf.append("<apikey>").append(apikey).append("</apikey>");
        }
        if (partnerid != null) {
            buf.append("<partnerid>").append(partnerid).append("</partnerid>");
        }
        if (partneruserid != null) {
            buf.append("<partneruserid>").append(partneruserid).append("</partneruserid>");
        }
        if (uuid != null) {
            buf.append("<uuid>").append(uuid).append("</uuid>");
        }
        if (hash != null) {
            buf.append("<hash>").append(hash).append("</hash>");
        }
        buf.append("</").append("bindingpartneriq").append(">");
        return buf.toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

	public String getApikey() {
		return apikey;
	}
	public void setApikey(String apikey) {
		this.apikey = apikey;
	}
	public String getPartnerid() {
		return partnerid;
	}
	public void setPartnerid(String partnerid) {
		this.partnerid = partnerid;
	}
	public String getPartneruserid() {
		return partneruserid;
	}
	public void setPartneruserid(String partneruserid) {
		this.partneruserid = partneruserid;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getHash() {
		return hash;
	}
	public void setHash(String hash) {
		this.hash = hash;
	}	
}
