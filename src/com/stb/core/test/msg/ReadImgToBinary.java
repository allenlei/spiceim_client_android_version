package com.stb.core.test.msg;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

//import org.jivesoftware.smack.util.Base64;

//import com.example.android.bitmapfun.util.Base64;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class ReadImgToBinary {
	/**
	 * 
	 * @param imgPath
	 * @param bitmap
	 * @param imgFormat ͼƬ��ʽ
	 * @return
	 */
	public static String imgToBase64URLEncode(Bitmap bitmap,String imgFormat) {
//		if (imgPath !=null && imgPath.length() > 0) {
//			bitmap = readBitmap(imgPath);
//		}
		if(bitmap == null){
			//bitmap not found!!
			return null;
		}
		ByteArrayOutputStream out = null;
//		java.net.URLDecoder urlDecoder = null;
		try {
			out = new ByteArrayOutputStream();
			if(imgFormat.equals("jpg"))
				bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
			else if(imgFormat.equals("png"))
				bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
			out.flush();
			out.close();

			byte[] imgBytes = out.toByteArray();
//			urlDecoder = new java.net.URLDecoder(); 
			return URLEncoder.encode(Base64.encodeToString(imgBytes, Base64.NO_WRAP), "UTF-8");
//			return Base64.encodeToString(imgBytes, Base64.NO_WRAP);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return null;
		} finally {
			try {
				out.flush();
				out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private static Bitmap readBitmap(String imgPath) {
		try {
			return BitmapFactory.decodeFile(imgPath);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return null;
		}

	}

	/**
	 * 
	 * @param base64Data
	 * @param imgName
	 * @param imgFormat ͼƬ��ʽ
	 */
	public static void URLEncodebase64ToBitmap(String urlencodebase64Data,String filePath,String imgName,String imgFormat) {
//		java.net.URLDecoder urlDecoder = new java.net.URLDecoder();
		try{
			String base64Data = URLDecoder.decode(urlencodebase64Data,"UTF-8"); 
			byte[] bytes = Base64.decode(base64Data, Base64.NO_WRAP);
			Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
	
			File myCaptureFile = new File(filePath, imgName);//"/sdcard/"
			FileOutputStream fos = null;
			try {
				fos = new FileOutputStream(myCaptureFile);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			boolean isTu = false;
			if(imgFormat.equals("jpg"))
				isTu = bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
			else if(imgFormat.equals("png"))
				isTu = bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
			if (isTu) {
				// fos.notifyAll();
				try {
					fos.flush();
					fos.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				try {
					fos.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}catch(UnsupportedEncodingException e1){
			e1.printStackTrace();
		}catch(Exception e2){
			e2.printStackTrace();
		}
	}
}
