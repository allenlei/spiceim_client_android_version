/*
 * Copyright (C) 2010 Moduad Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.stb.core.test.msg;

import java.util.HashMap;


import org.jivesoftware.smack.packet.IQ;

import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;

/** 
 * This class parses incoming IQ packets to NotificationIQ objects.
 *
 * @author Sehwan Noh (devnoh@gmail.com)
 */
public class AdvancedSearchIQResponseProvider implements IQProvider {
//	HashMap hm = new HashMap();
	Person person = null;  
    public AdvancedSearchIQResponseProvider() {
    }

    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {

    	AdvancedSearchIQResponse advancedSearchIQResponse = new AdvancedSearchIQResponse();
        for (boolean done = false; !done;) {
            int eventType = parser.next();
            if (eventType == 2) {
                if ("id".equals(parser.getName())) {
                	advancedSearchIQResponse.setId(parser.nextText());
                }
                if ("apikey".equals(parser.getName())) {
                	advancedSearchIQResponse.setApikey(parser.nextText());
                }
                if ("totalnumber".equals(parser.getName())) {
                	advancedSearchIQResponse.setTotalnumber(parser.nextText());
                }
                if ("user".equals(parser.getName())) {
                	person = new Person();  
                	//parser.getAttributeCount();
                	person.setStatus(parser.getAttributeValue(0));
                	
                	person.set_Name(parser.getAttributeValue(1)!=null?parser.getAttributeValue(1):"");
                	person.setUid(parser.getAttributeValue(2)!=null?parser.getAttributeValue(2):"");
//                	if(parser.getAttributeCount()==4)
                		person.setEmail(parser.getAttributeValue(3)!=null?parser.getAttributeValue(3):"");
                		person.setGender(parser.getAttributeValue(4)!=null?parser.getAttributeValue(4):"");
                		person.setBirthday(parser.getAttributeValue(5)!=null?parser.getAttributeValue(5):"");
                		person.setUsersign(parser.getAttributeValue(6)!=null?parser.getAttributeValue(6):"");
                	person.setName(parser.nextText());
                	
//                	advancedSearchIQResponse.users.add(parser.nextText());
//                	parser.getAttributeValue(0);
                	advancedSearchIQResponse.users.add(person);
                }
                
            } else if (eventType == 3
                    && "advancedsearchiq".equals(parser.getName())) {
                done = true;
            }
        }

        return advancedSearchIQResponse;
    }

}
