package com.stb.core.test.msg;

import android.os.Parcel;
import android.os.Parcelable;

public class Person implements Parcelable{ 
	
    /** Parcelable.Creator needs by Android. */
    public static final Parcelable.Creator<Person> CREATOR = new Parcelable.Creator<Person>() {

	@Override
	public Person createFromParcel(Parcel source) {
	    return new Person(source);
	}

	@Override
	public Person[] newArray(int size) {
	    return new Person[size];
	}
    };
    
	public Person(){}
	
    private Person(final Parcel in) {
    	name = in.readString();
    	status = in.readString();
    	_name = in.readString();
    	uid = in.readString();
    	email = in.readString();
    	gender = in.readString();
    	birthday = in.readString();
    	usersign = in.readString();
    	creationdate = in.readString();
    	affiliation = in.readString();
    }
    
    @Override
    public void writeToParcel(Parcel dest, int flags) {
//    	super.writeToParcel(dest, flags);//问题出现在没有加入这行代码  super.writeToParcel(out, flags);
		dest.writeString(name);
		dest.writeString(status);
		dest.writeString(_name);
		dest.writeString(uid);
		dest.writeString(email);
		dest.writeString(gender);
		dest.writeString(birthday);
		dest.writeString(usersign);
		dest.writeString(creationdate);
		dest.writeString(affiliation);
    }
    @Override
    public int describeContents() {
    	return 0;
    }
    
	private String name;  
	private String status;  
	private String _name="";
	private String uid="";
	private String email="";
	private String gender;
	private String birthday;
	private String usersign;
    private String creationdate = "";
    
    private String affiliation = "";//20141121 added by allen
	
	public String getStatus() {  
		return status;  
	}  
	public void setStatus(String status) {  
		this.status = status;  
	}  
	public String getName() {  
		return name;  
	}  
	public void setName(String name) {  
		this.name = name;  
	} 
	public String getUid() {  
		return uid;  
	}  
	public void setUid(String uid) {  
		this.uid = uid;  
	} 
	public String get_Name() {  
		return _name;  
	}  
	public void set_Name(String _name) {  
		this._name = _name;  
	} 
	public String getEmail() {  
		return email;  
	}  
	public void setEmail(String email) {  
		this.email = email;  
	} 
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getUsersign() {
		return usersign;
	}
	public void setUsersign(String usersign) {
		this.usersign = usersign;
	}

    public void setCreationdate(String creationdate){
    	this.creationdate = creationdate;
    }
    public String getCreationdate(){
    	return creationdate;
    }
    
    public void setAffiliation(String affiliation){
    	this.affiliation = affiliation;
    }
    public String getAffiliation(){
    	return affiliation;
    }
}
