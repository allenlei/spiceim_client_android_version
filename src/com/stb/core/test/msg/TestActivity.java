package com.stb.core.test.msg;


import java.util.ArrayList;









import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.util.StringUtils;

//import com.example.android.bitmapfun.util.AsyncTask;
import com.beem.push.utils.AsyncTask;
import com.example.android.bitmapfun.util.ObjectCache;
//import com.stb.core.pojo.UploadFileRequest;
import com.spice.im.R;
import com.stb.core.chat.ContactGroup;
import com.stb.isharemessage.BeemApplication;
import com.stb.isharemessage.IConnectionStatusCallback;
//import com.stb.isharemessage.service.Contact;
import com.stb.isharemessage.service.XmppConnectionAdapter;
import com.stb.isharemessage.service.aidl.IXmppFacade;
import com.stb.isharemessage.utils.BeemConnectivity;
//import com.test.HandyTextView;
//import com.test.MainActivity;
//import com.test.R;

//import dalvik.system.VMRuntime;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class TestActivity extends Activity implements IConnectionStatusCallback,OnClickListener{
	private View mNetErrorView;
	private TextView mConnect_status_info;
	private ImageView mConnect_status_btn;
	private Context mContext;
    private exPhoneCallListener myPhoneCallListener = null;
    private TelephonyManager tm = null;
	
    private final static float TARGET_HEAP_UTILIZATION = 0.75f;
    private final static int CWJ_HEAP_SIZE = 6* 1024* 1024 ;
    private static final Intent SERVICE_INTENT = new Intent();
	static {
		//pkg,cls
//		SERVICE_INTENT.setComponent(new ComponentName("com.test", "com.stb.isharemessage.BeemService"));
//		SERVICE_INTENT.setComponent(new ComponentName("com.stb.isharemessage", "com.stb.isharemessage.BeemService"));
		SERVICE_INTENT.setComponent(new ComponentName("com.spice.im", "com.stb.isharemessage.BeemService"));//����Ӧ��pkg��ƣ���service����pkg���
	}
	private static ExecutorService LIMITED_TASK_EXECUTOR;
    static {
    	LIMITED_TASK_EXECUTOR = (ExecutorService) Executors.newFixedThreadPool(7); 
    }; 
    protected List<AsyncTask<Void, Void, Boolean>> mAsyncTasks = new ArrayList<AsyncTask<Void, Void, Boolean>>();
	protected void putAsyncTask(AsyncTask<Void, Void, Boolean> asyncTask) {
		mAsyncTasks.add(asyncTask.executeOnExecutor(LIMITED_TASK_EXECUTOR, null));
	}

	protected void clearAsyncTask() {
		Iterator<AsyncTask<Void, Void, Boolean>> iterator = mAsyncTasks
				.iterator();
		while (iterator.hasNext()) {
			AsyncTask<Void, Void, Boolean> asyncTask = iterator.next();
			if (asyncTask != null && !asyncTask.isCancelled()) {
				asyncTask.cancel(true);
			}
		}
		mAsyncTasks.clear();
	}
	private final ServiceConnection mServConn = new BeemServiceConnection();
    private boolean mBinded = false;
    private IXmppFacade mXmppFacade;
    protected FlippingLoadingDialog mLoadingDialog;
	protected void showLoadingDialog(String text) {
		if (text != null) {
			mLoadingDialog.setText(text);
		}
		mLoadingDialog.show();
	}

	protected void dismissLoadingDialog() {
		if (mLoadingDialog.isShowing()) {
			mLoadingDialog.dismiss();
		}
	}
	
	private Button btn;
	private Button btn2;
	private final OkListener mOkListener = new OkListener();
	private TextView udidTextView;
	private EditText partneridEditText;
	private EditText partneruseridEditText;
    public void onCreate(Bundle savedInstanceState) {
//    	VMRuntime.getRuntime().setTargetHeapUtilization(TARGET_HEAP_UTILIZATION);
//    	VMRuntime.getRuntime().setMinimumHeapSize(CWJ_HEAP_SIZE); //������Сheap�ڴ�Ϊ6MB��С����Ȼ�����ڴ�Խ���˵������ͨ���ֶ�����GCȥ����
    	requestWindowFeature(Window.FEATURE_NO_TITLE);
    	super.onCreate(savedInstanceState);
    	setContentView(R.layout.msgtest);
    	mLoadingDialog = new FlippingLoadingDialog(this, "�����ύ��");
    	btn = (Button)findViewById(R.id.button1);
        btn.setOnClickListener(mOkListener);
        btn2 = (Button)findViewById(R.id.button2);
        btn2.setOnClickListener(mOkListener);
        udidTextView = (TextView)findViewById(R.id.udid_name);
        partneridEditText = (EditText)findViewById(R.id.partnerid);
        partneruseridEditText = (EditText)findViewById(R.id.partneruserid);
        mContext = this;
        mNetErrorView = findViewById(R.id.net_status_bar_top);
        mConnect_status_info = (TextView)mNetErrorView.findViewById(R.id.net_status_bar_info_top2);
        mConnect_status_btn = (ImageView)mNetErrorView.findViewById(R.id.net_status_bar_btn);
        mConnect_status_btn.setOnClickListener(this);
        
	    IntentFilter mFilter = new IntentFilter();
	    mFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION); // ��ӽ�����������״̬�ı��Action
	    registerReceiver(mNetWorkReceiver, mFilter);
	    
        /* ����Լ�ʵ�ֵ�PhoneStateListener */
        myPhoneCallListener = new exPhoneCallListener();
       
       /* ȡ�õ绰���� */
        tm =(TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
       
        /* ע��绰ͨ��Listener */
        tm.listen(myPhoneCallListener,PhoneStateListener.LISTEN_CALL_STATE);
        
    	BeemApplication.getInstance().addActivity(this);
    	
//    	ObjectCache.saveStatus("isConnected", true, this);//��ʶΪ�ֶ�����
    }
	@Override
	public void onClick(View v) {
		//��������
		if(v.getId() == R.id.net_status_bar_btn){
			setNetworkMethod(this);
		}
	}
    @Override
    protected void onResume() {
		super.onResume();
		if (!mBinded){
		    mBinded = getApplicationContext().bindService(SERVICE_INTENT, mServConn, BIND_AUTO_CREATE);	    
		}
    }
    public void onPause() {
    	super.onPause();
    	Log.e("================2015 TestActivity.java ================", "++++++++++++++onPause()++++++++++++++mBinded="+mBinded);
//		if (mBinded) {
//			unbindService(mServConn);
//			mBinded = false;
//		}
    }
    /**
     * ��дfinish()����
     */
    @Override
    public void finish() {
        //super.finish(); //��ס��Ҫִ�д˾�
        moveTaskToBack(true); //���ø�activity�������ڣ�����ִ��onDestroy()
    }   
 
    public void finishNew() {
        super.finish(); //��סҪִ�д˾�
        moveTaskToBack(false);
    }
    @Override
    protected void onDestroy() {
		super.onDestroy();
		Log.e("================2015 TestActivity.java ================", "++++++++++++++onDestroy()000++++++++++++++mBinded="+mBinded);
		clearAsyncTask();
    	Log.e("================2015 TestActivity.java ================", "++++++++++++++onDestroy()111++++++++++++++");
 	    XmppConnectionAdapter.isPause = true;
		if(XmppConnectionAdapter.instance!=null)
			XmppConnectionAdapter.instance.resetApplication();
		XmppConnectionAdapter.instance = null;
		stopService(SERVICE_INTENT);
		Log.e("================2015 TestActivity.java ================", "++++++++++++++onDestroy()222++++++++++++++");
//		loadingView.isStop = true;
	    try{
		    if(mXmppFacade!=null && mXmppFacade.getXmppConnectionAdapter()!=null)
		    	mXmppFacade.getXmppConnectionAdapter().unRegisterConnectionStatusCallback();
	    }catch (RemoteException e) {
	    	e.printStackTrace();
	    }
		if (mBinded) {
			getApplicationContext().unbindService(mServConn);
		    mBinded = false;
		}

		mXmppFacade = null;
		
		unregisterReceiver(mNetWorkReceiver); // ɾ��㲥
		if(tm!=null){
			tm.listen(myPhoneCallListener,PhoneStateListener.LISTEN_NONE);//ȡ������
			tm = null;
		}
		if(myPhoneCallListener!=null)
			myPhoneCallListener = null;
		
		BeemApplication.getInstance().exit();
		Log.e("================2015 TestActivity.java ================", "++++++++++++++onDestroy()333++++++++++++++");
    }
    /**
     * The service connection used to connect to the Beem service.
     */
    private class BeemServiceConnection implements ServiceConnection {
		/**
		 * Constructor.
		 */
		public BeemServiceConnection() {
		}
	
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
		    mXmppFacade = IXmppFacade.Stub.asInterface(service);
		    if(mXmppFacade!=null){
		    	mBinded = true;
		    	try{
		    		mXmppFacade.getXmppConnectionAdapter().registerConnectionStatusCallback(TestActivity.this);
		    		
		    	}catch(Exception e){
		    		e.printStackTrace();
		    	}
		    }
		}
		@Override
		public void onServiceDisconnected(ComponentName name) {
		    try{
			    if(mXmppFacade!=null && mXmppFacade.getXmppConnectionAdapter()!=null)
			    	mXmppFacade.getXmppConnectionAdapter().unRegisterConnectionStatusCallback();
		    }catch (RemoteException e) {
		    	e.printStackTrace();
		    }
		    mXmppFacade = null;
		    mBinded = false;
		}
    }
    private List<ContactGroup> mListContact;
	private String searchkey = "test";
	private String searchtype = "0";
	private String ages = "12";
	private String genders = "1";
	private String distances = "5000";
	private String online = "0";
	private String[] errorMsg = new String[]{"��Ǹ,û���ҵ���ؽ��.",
			"����������1-1,���Ժ�����.",
			"����������1-2,���Ժ�����.",
			"����������1-3,���Ժ�����.",
			"�������Ӳ�����,���������������.",
			"",
			"�����쳣,�Ժ�����!"};
	private int errorType = 5;
    private int currentPage = 0;
    private int numberresult = 50;
	public void initContactList(){
		Log.e("================2015 TestActivity.java ================", "++++++++++++++initContactList()111++++++++++++++");
		if(BeemConnectivity.isConnected( getApplicationContext())){
			Log.e("================2015 TestActivity.java ================", "++++++++++++++initContactList()222++++++++++++++");
		    try {
		    	if(mXmppFacade!=null){
		    		Log.e("================2015 TestActivity.java ================", "++++++++++++++initContactList()333++++++++++++++");
			    	if (mXmppFacade.getXmppConnectionAdapter()!=null 
							&& mXmppFacade.getXmppConnectionAdapter().getAdaptee()!=null
							&& mXmppFacade.getXmppConnectionAdapter().getAdaptee().isConnected() 
			    			&& mXmppFacade.getXmppConnectionAdapter().getAdaptee().isAuthenticated()
			    			&& mXmppFacade.getXmppConnectionAdapter().getAdaptee().isAlive()
			    			){
			    		
			            AdvancedSearchIQ reqXML = new AdvancedSearchIQ();
			            reqXML.setId("1234567890");
			            reqXML.setApikey("abcdefghijklmnopqrstuvwxyz");
			            reqXML.setSearchkey(searchkey);
			            reqXML.setSearchtype(searchtype);
			            reqXML.setAges(ages);
			            reqXML.setGenders(genders);
			            reqXML.setDistances("distances");
			            reqXML.setOnline("online");
			            reqXML.setStartIndex(""+currentPage*numberresult);
			            reqXML.setNumResults(""+numberresult);
			            reqXML.setType(IQ.Type.SET);
			            String elementName = "advancedsearchiq"; 
			    		String namespace = "com:stb:advancedsearchiq";
			            AdvancedSearchIQResponseProvider provider = new AdvancedSearchIQResponseProvider();
			            Packet rt =  mXmppFacade.getXmppConnectionAdapter().excute(reqXML, "advancedsearchiq", "com:stb:advancedsearchiq", provider);
			            
			            if(rt!=null){
			                Person person = null;
			                ContactGroup contact = null;
			                if (rt instanceof AdvancedSearchIQResponse) {
			                	final AdvancedSearchIQResponse advancedSearchIQResponse = (AdvancedSearchIQResponse) rt;

			                    if (advancedSearchIQResponse.getChildElementXML().contains(
			                            "com:stb:advancedsearchiq")) {
			    					TestActivity.this.runOnUiThread(new Runnable() {
				                    	
            							@Override
            							public void run() {
            								showCustomToast("������Ӧ����Ϣ��"+advancedSearchIQResponse.toXML().toString());
            							}
            						});
			                        String Id = advancedSearchIQResponse.getId();
			                        String Apikey = advancedSearchIQResponse.getApikey();
			                        String totalnumber = advancedSearchIQResponse.getTotalnumber();
			                        ArrayList users = advancedSearchIQResponse.getUsers();
			                        Log.e("��Ӧpacket������...............", "Id="+Id+";Apikey="+Apikey+";totalnumber="+totalnumber); 
//			                        mListContact = new ArrayList<Contact>(); 
//			                        for(int k=0;k<users.size();k++){
//			                        	Log.e("��Ӧpacket������...............", "users"+k+"="+((Person)users.get(k)).getName()+ 
//			                        			";status="+((Person)users.get(k)).getStatus()+
//			                        			";_name="+((Person)users.get(k)).get_Name()+
//			                        			";uid="+((Person)users.get(k)).getUid()+
//			                        			";email="+((Person)users.get(k)).getEmail()); 
////			                        	results.add(new Contact((String)users.get(k)));
//			                        	person = (Person)users.get(k);
//			                        	contact = new Contact(person.getName());
//			                        	contact.setStatus(Integer.parseInt(person.getStatus()));
//			                        	contact.setName(person.get_Name());
//			                        	contact.setAvatarId(((Person)users.get(k)).getEmail());//
//			                        	contact.setGender(((Person)users.get(k)).getGender());
//			                        	contact.setBirthday(((Person)users.get(k)).getBirthday());
//			                        	contact.setSign(((Person)users.get(k)).getUsersign());
//			                        	mListContact.add(contact);
//			                        }
			                        users = null;
			                    }
			                } 
			            }
			            
			    		Log.e("================2015 TestActivity.java ================", "++++++++++++++initContactList()444++++++++++++++");

			    	    if(mListContact!=null && mListContact.size()!=0)
			    	    	errorType = 5;
			    	    else{
			    	    	if(mListContact!=null && mListContact.size()==0)
			    	    		errorType = 0;
			    	    	else
			    	    		errorType = 6;
			    	    }
						mBinded = true;//20130804 added by allen
			    	}else
			    		errorType = 1;
		    	}else
		    		errorType = 2;
				
		    } catch (RemoteException e) {
		    	errorType = 3;
		    	e.printStackTrace();
		    	
		    } catch (Exception e){
		    	errorType = 3;
		    	e.printStackTrace();
		    }
	    }else{
	    	errorType = 4;
	    }
	}
	
	private void getContactList() {

		putAsyncTask(new AsyncTask<Void, Void, Boolean>() {

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				showLoadingDialog("���ڼ���,���Ժ�...");
			}

			@Override
			protected Boolean doInBackground(Void... params) {
				initContactList();
				return true;
			}

			@Override
			protected void onPostExecute(Boolean result) {
				super.onPostExecute(result);
				dismissLoadingDialog();
				if (!result) {
					showCustomToast("��ݼ���ʧ��...");
					mHandler2.sendEmptyMessage(3);
				} else {
					mHandler2.sendEmptyMessage(0);
				}
			}

		});
	}
	
	String retcode = "9999";//0000 success,0001 false���Ѱ󶨣�,9999
	String memo = "";
	public void initBindingPartnerUserid(String partneruserid){
		Log.e("================2015 TestActivity.java ================", "++++++++++++++initBindingPartnerUserid()111++++++++++++++");
		if(BeemConnectivity.isConnected( getApplicationContext())){
			Log.e("================2015 TestActivity.java ================", "++++++++++++++initBindingPartnerUserid()222++++++++++++++");
		    try {
		    	if(mXmppFacade!=null){
		    		Log.e("================2015 TestActivity.java ================", "++++++++++++++initBindingPartnerUserid()333++++++++++++++");
			    	if (mXmppFacade.getXmppConnectionAdapter()!=null 
							&& mXmppFacade.getXmppConnectionAdapter().getAdaptee()!=null
							&& mXmppFacade.getXmppConnectionAdapter().getAdaptee().isConnected() 
			    			&& mXmppFacade.getXmppConnectionAdapter().getAdaptee().isAuthenticated()
			    			&& mXmppFacade.getXmppConnectionAdapter().getAdaptee().isAlive()
			    			){
			    		
			    		BindingPartnerIQ reqXML = new BindingPartnerIQ();
			            reqXML.setId("1234567890");
			            reqXML.setApikey("abcdefghijklmnopqrstuvwxyz");
			            reqXML.setPartnerid(mXmppFacade.getXmppConnectionAdapter().getMService().getPartnerid());
			            reqXML.setPartneruserid(partneruserid);
			            reqXML.setUuid(mXmppFacade.getXmppConnectionAdapter().getAdaptee().UDID);
			            String hash_src = mXmppFacade.getXmppConnectionAdapter().getMService().getPartnerid()
			            +partneruserid
			            +mXmppFacade.getXmppConnectionAdapter().getAdaptee().UDID;
			            reqXML.setHash(EncryptionUtil.getHash2(hash_src, "SHA"));
			            reqXML.setType(IQ.Type.SET);
			            String elementName = "bindingpartneriq"; 
			    		String namespace = "com:isharemessage:bindingpartneriq";
			    		BindingPartnerIQResponseProvider provider = new BindingPartnerIQResponseProvider();
			            Packet rt =  mXmppFacade.getXmppConnectionAdapter().excute(reqXML, "bindingpartneriq", "com:isharemessage:bindingpartneriq", provider);
			            
			            if(rt!=null){
			                if (rt instanceof BindingPartnerIQResponse) {
			                	final BindingPartnerIQResponse bindingPartnerIQResponse = (BindingPartnerIQResponse) rt;

			                    if (bindingPartnerIQResponse.getChildElementXML().contains(
			                            "com:isharemessage:bindingpartneriq")) {
			    					TestActivity.this.runOnUiThread(new Runnable() {
				                    	
            							@Override
            							public void run() {
            								showCustomToast("������Ӧ����Ϣ��"+bindingPartnerIQResponse.toXML().toString());
            							}
            						});
			                        String Id = bindingPartnerIQResponse.getId();
			                        retcode = bindingPartnerIQResponse.getRetcode();
			                        memo = bindingPartnerIQResponse.getMemo();
			                    }
			                } 
			            }
			            
			    		Log.e("================2015 TestActivity.java ================", "++++++++++++++initBindingPartnerUserid()444++++++++++++++");

			    	    if(retcode!=null && retcode.equalsIgnoreCase("0000"))
			    	    	errorType = 5;
			    	    else{
			    	    	if(retcode!=null && retcode.equalsIgnoreCase("0001"))
			    	    		errorType = 0;
			    	    	else
			    	    		errorType = 6;
			    	    }
						mBinded = true;//20130804 added by allen
			    	}else
			    		errorType = 1;
		    	}else
		    		errorType = 2;
				
		    } catch (RemoteException e) {
		    	errorType = 3;
		    	e.printStackTrace();
		    	
		    } catch (Exception e){
		    	errorType = 3;
		    	e.printStackTrace();
		    }
	    }else{
	    	errorType = 4;
	    }
	}
	private void startBindingPartnerUserid(final String partneruserid) {

		putAsyncTask(new AsyncTask<Void, Void, Boolean>() {

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				showLoadingDialog("���ڼ���,���Ժ�...");
			}

			@Override
			protected Boolean doInBackground(Void... params) {
				initBindingPartnerUserid(partneruserid);
				return true;
			}

			@Override
			protected void onPostExecute(Boolean result) {
				super.onPostExecute(result);
				dismissLoadingDialog();
				if (!result) {
					showCustomToast("��ݼ���ʧ��...");
					mHandler2.sendEmptyMessage(3);
				} else {
					mHandler2.sendEmptyMessage(0);
				}
			}

		});
	}
	/** ��ʾ�Զ���Toast��ʾ(����String) **/
	protected void showCustomToast(String text) {
		View toastRoot = LayoutInflater.from(TestActivity.this).inflate(
				R.layout.common_toast, null);
		((HandyTextView) toastRoot.findViewById(R.id.toast_text)).setText(text);
		Toast toast = new Toast(TestActivity.this);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.setDuration(Toast.LENGTH_SHORT);
		toast.setView(toastRoot);
		toast.show();
	}
	Handler mHandler2 = new Handler() {

		@Override
		public void handleMessage(android.os.Message msg) {
			super.handleMessage(msg);
			switch (msg.what) {
			case 0:
				if(errorType==5 && mListContact!=null){
					ContactGroup contact = null;
			        for (int i = 0; i < mListContact.size(); i++) {
			        	contact = (ContactGroup)mListContact.get(i);
			        	Log.e("MMMMMMMMMMMM20140714(2)MMMMMMMMMMMM", "++++++++++++++"+contact.toString()+"++++++++++++");
			        }
				}else{
		        	showCustomToast(errorMsg[errorType]);
		        }
				break;

			case 1:
				
				showCustomToast("�Ѵﶥ��!");
				break;
			case 2:
				
				showCustomToast("�Ѵ�׶�!");
				break;
			default:
				
				break;
			}
		}

	};
    private class OkListener implements OnClickListener {
    	
		/**
		 * Constructor.
		 */
		public OkListener() { }
	
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.button1:
				getContactList();
				break;
			case R.id.button2:
				String partneruserid = partneruseridEditText.getText().toString().trim();
				startBindingPartnerUserid(partneruserid);
				break;
			}
		}
    };
    
	@Override
	public void connectionStatusChanged(int connectedState, String reason) {
		Log.e("MMMMM20140604MMMMMMMMMMMM", "++++++++++++++connectionStatusChanged0++++++++++++");
		switch (connectedState) {
		case 0://connectionClosed
			mHandler3.sendEmptyMessage(0);
			break;
		case 1://connectionClosedOnError
//			mConnectErrorView.setVisibility(View.VISIBLE);
//			mNetErrorView.setVisibility(View.VISIBLE);
//			mConnect_status_info.setText("�����쳣!");
			mHandler3.sendEmptyMessage(1);
			break;
		case 2://reconnectingIn
//			mNetErrorView.setVisibility(View.VISIBLE);
//			mConnect_status_info.setText("������...");
			mHandler3.sendEmptyMessage(2);
			break;
		case 3://reconnectionFailed
//			mNetErrorView.setVisibility(View.VISIBLE);
//			mConnect_status_info.setText("����ʧ��!");
			mHandler3.sendEmptyMessage(3);
			break;
		case 4://reconnectionSuccessful
//			mNetErrorView.setVisibility(View.GONE);
			mHandler3.sendEmptyMessage(4);

		default:
			break;
		}
	}
	Handler mHandler3 = new Handler() {

		@Override
		public void handleMessage(android.os.Message msg) {
			super.handleMessage(msg);
			switch (msg.what) {
			case 0:
				break;

			case 1:
				mNetErrorView.setVisibility(View.VISIBLE);
				if(BeemConnectivity.isConnected(mContext))
				mConnect_status_info.setText("�����쳣!");
				break;

			case 2:
				mNetErrorView.setVisibility(View.VISIBLE);
				if(BeemConnectivity.isConnected(mContext))
				mConnect_status_info.setText("������...");
				break;
			case 3:
				mNetErrorView.setVisibility(View.VISIBLE);
				if(BeemConnectivity.isConnected(mContext))
				mConnect_status_info.setText("����ʧ��!");
				break;
				
			case 4:
				mNetErrorView.setVisibility(View.GONE);
				String udid = "";
				String partnerid = "";
				try{
					udid = mXmppFacade.getXmppConnectionAdapter().getAdaptee().UDID;
					partnerid = mXmppFacade.getXmppConnectionAdapter().getMService().getPartnerid();
				}catch(Exception e){
					e.printStackTrace();
				}
				udidTextView.setText("udid="+udid);
				partneridEditText.setText(partnerid);
//				getContactList();//20141025 added by allen
				break;
			case 5:
				mNetErrorView.setVisibility(View.VISIBLE);
				mConnect_status_info.setText("��ǰ���粻���ã���������������á�");
				break;
			case 6:
				if(mXmppFacade!=null 
//						&& mXmppFacade.getXmppConnectionAdapter()!=null 
//						&& mXmppFacade.getXmppConnectionAdapter().getAdaptee()!=null//&& !mXmppFacade.getXmppConnectionAdapter().getAdaptee().isAuthenticated()
//						&& mXmppFacade.getXmppConnectionAdapter().getAdaptee().isConnected()
//						&& mXmppFacade.getXmppConnectionAdapter().getAdaptee().isAuthenticated()
						&& BeemConnectivity.isConnected(mContext)){
					
				}else{
					mNetErrorView.setVisibility(View.VISIBLE);
					if(BeemConnectivity.isConnected(mContext))
					mConnect_status_info.setText("�������,������...");
				}
				break;	
			}
		}

	};
	private BroadcastReceiver mNetWorkReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();  
            if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {  
                Log.d("PoupWindowContactList", "����״̬�Ѿ��ı�");  
//                connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);  
//                info = connectivityManager.getActiveNetworkInfo();    
//                if(info != null && info.isAvailable()) {  
//                    String name = info.getTypeName();  
//                    Log.d(tag, "��ǰ������ƣ�" + name);  
//                    //doSomething()  
//                } else {  
//                    Log.d(tag, "û�п�������");  
//                  //doSomething()  
//                }  
                if(BeemConnectivity.isConnected(context)){
//                	mNetErrorView.setVisibility(View.GONE);
//                	isconnect = 0;
                	mHandler3.sendEmptyMessage(6);//4
                }else{
//                	mNetErrorView.setVisibility(View.VISIBLE);
//                	isconnect = 1;
                	mHandler3.sendEmptyMessage(5);
                	Log.d("PoupWindowContactList", "��ǰ���粻���ã���������������á�");
                }
            } 
        }
	};
	
	private boolean phonestate = false;//false���� true����绰��绰����ʱ 
    /* �ڲ�class�̳�PhoneStateListener */
    public class exPhoneCallListener extends PhoneStateListener
    {
        /* ��дonCallStateChanged
        ��״̬�ı�ʱ�ı�myTextView1�����ּ���ɫ */
        public void onCallStateChanged(int state, String incomingNumber)
        {
          switch (state)
          {
            /* ���κ�״̬ʱ :˵��û�в���绰�Ľ����ʱ��Ҳ�����ڲ���绰ǰ�͹ҵ绰������*/
            case TelephonyManager.CALL_STATE_IDLE:
            	Log.e("================20131216 ���κε绰״̬ʱ================", "���κε绰״̬ʱ");
            	if(phonestate){
            		onPhoneStateResume();
            	}
            	phonestate = false;
              break;
            /* ����绰ʱ :������һ���绰���ڡ������򼤻�����û��һ���绰��ͨ����ȴ��״̬*/
            case TelephonyManager.CALL_STATE_OFFHOOK:
            	Log.e("================20131216 ����绰ʱ================", "����绰ʱ");
            	onPhoneStatePause();
            	phonestate = true;
              break;
            /* �绰����ʱ :��ͨ���Ĺ����*/
            case TelephonyManager.CALL_STATE_RINGING:
//              getContactPeople(incomingNumber);
            	Log.e("================20131216 �绰����ʱ================", "�绰����ʱ");
//            	onBackPressed();
            	onPhoneStatePause();
            	phonestate = true;
              break;
            default:
              break;
          }
          super.onCallStateChanged(state, incomingNumber);
        }
    }
    
    protected void onPhoneStatePause() {
//		super.onPause();
    	try{

		if (mBinded) {
			getApplicationContext().unbindService(mServConn);
		    mBinded = false;
		}
		mXmppFacade = null;
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    }
    
    protected void onPhoneStateResume() {
//		super.onResume();
    	try{
		if (!mBinded){
//		    new Thread()
//		    {
//		        @Override
//		        public void run()
//		        {
		    mBinded = getApplicationContext().bindService(SERVICE_INTENT, mServConn, BIND_AUTO_CREATE);

//		        }
//		    }.start();		    
		}
		if (mBinded){

		}
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    }
    
    /*
     * �������������
     * */
    public static void setNetworkMethod(final Context context){
        //��ʾ�Ի���
        AlertDialog.Builder builder=new AlertDialog.Builder(context);
        builder.setTitle("����������ʾ").setMessage("�������Ӳ�����,�Ƿ��������?").setPositiveButton("����", new DialogInterface.OnClickListener() {
            
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                Intent intent=null;
                //�ж��ֻ�ϵͳ�İ汾  ��API����10 ����3.0�����ϰ汾 
                if(android.os.Build.VERSION.SDK_INT>10){
                    intent = new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS);
                }else{
                    intent = new Intent();
                    ComponentName component = new ComponentName("com.android.settings","com.android.settings.WirelessSettings");
                    intent.setComponent(component);
                    intent.setAction("android.intent.action.VIEW");
                }
                context.startActivity(intent);
            }
        }).setNegativeButton("ȡ��", new DialogInterface.OnClickListener() {
            
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        }).show();
    }
}
