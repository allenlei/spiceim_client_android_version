/*
 * Copyright (C) 2010 Moduad Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.stb.core.test.msg;

import java.util.ArrayList;

import org.jivesoftware.smack.packet.IQ;

/** 
 * This class represents a notifcatin IQ packet.
 *
 * @author Sehwan Noh (devnoh@gmail.com)
 */
public class AdvancedSearchIQResponse extends IQ {

    private String id;

    private String apikey;

    private String totalnumber;

    public ArrayList users = new ArrayList();
    
    Person person = null;


    public AdvancedSearchIQResponse() {
    }

    @Override
    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append("advancedsearchiq").append(" xmlns=\"").append(
                "com:stb:advancedsearchiq").append("\">");
        if (id != null) {
            buf.append("<id>").append(id).append("</id>");
        }
        if (apikey != null) {
            buf.append("<apikey>").append(apikey).append("</apikey>");
        }
        if (totalnumber != null) {
            buf.append("<totalnumber>").append(totalnumber).append("</totalnumber>");
        }
        if (users != null && users.size()!=0) {
        	
            buf.append("<users>");
            for(int i=0;i<users.size();i++){
            	person = (Person)users.get(i);
//            	buf.append("<user>").append(users.get(i)).append("</user>");
            	//<user status="100" "_name=" "uid=test" "email=null">
            	buf.append("<user status=\""+person.getStatus()+"\" "+"_name=\""+person.get_Name()+"\" "+"uid=\""+person.getUid()+"\" "+"email=\""+person.getEmail()+"\" "+"gender=\""+person.getGender()+"\" "+"birthday=\""+person.getBirthday()+"\" "+"usersign=\""+person.getUsersign()+"\""+">").append(person.getName()).append("</user>");
            }
            buf.append("</users>");
        }
        buf.append("</").append("advancedsearchiq").append(">");
        return buf.toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getTotalnumber() {
        return totalnumber;
    }

    public void setTotalnumber(String totalnumber) {
        this.totalnumber = totalnumber;
    }

    public ArrayList getUsers() {
        return users;
    }

    public void setUsers(ArrayList users) {
        this.users = users;
    }

}
