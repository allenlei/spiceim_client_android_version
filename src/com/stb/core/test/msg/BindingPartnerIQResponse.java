package com.stb.core.test.msg;


import org.jivesoftware.smack.packet.IQ;

public class BindingPartnerIQResponse extends IQ {

    private String id;
    
    private String uuid;

    private String retcode;//0000 success,0001 false（已绑定）,0002 false(hash校验失败),9999
    
    private String memo;//状态描述:绑定成功，绑定失败（已绑定无需重复绑定）,绑定失败（原因）

    public BindingPartnerIQResponse() {
    }

    @Override
    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append("bindingpartneriq").append(" xmlns=\"").append(
                "com:isharemessage:bindingpartneriq").append("\">");
        if (id != null) {
            buf.append("<id>").append(id).append("</id>");
        }
        if (uuid != null) {
            buf.append("<uuid>").append(uuid).append("</uuid>");
        }
        if(retcode!=null){
        	buf.append("<retcode>").append(retcode).append("</retcode>");
        }
        if(memo!=null){
        	buf.append("<memo>").append(memo).append("</memo>");
        }
        buf.append("</").append("bindingpartneriq").append(">");
        return buf.toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
	public String getRetcode() {
		return retcode;
	}
	public void setRetcode(String retcode) {
		this.retcode = retcode;
	}

	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
}
