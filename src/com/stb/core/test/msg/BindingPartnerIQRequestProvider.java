package com.stb.core.test.msg;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;

public class BindingPartnerIQRequestProvider implements IQProvider {
    public BindingPartnerIQRequestProvider() {
    }

    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {

    	BindingPartnerIQ bindingPartnerIQ = new BindingPartnerIQ();
        for (boolean done = false; !done;) {
            int eventType = parser.next();
            if (eventType == 2) {
                if ("id".equals(parser.getName())) {
                	bindingPartnerIQ.setId(parser.nextText());
                }
                if ("apikey".equals(parser.getName())) {
                	bindingPartnerIQ.setApikey(parser.nextText());
                }
                if ("partnerid".equals(parser.getName())) {
                	bindingPartnerIQ.setPartnerid(parser.nextText());
                }
                if ("partneruserid".equals(parser.getName())) {
                	bindingPartnerIQ.setPartneruserid(parser.nextText());
                }
                if ("uuid".equals(parser.getName())) {
                	bindingPartnerIQ.setUuid(parser.nextText());
                }
                if ("hash".equals(parser.getName())) {
                	bindingPartnerIQ.setHash(parser.nextText());
                }
            } else if (eventType == 3
                    && "bindingpartneriq".equals(parser.getName())) {//elementName = bindingpartneriq  	
                done = true;
            }
        }
        
        return bindingPartnerIQ;
        		
    }
  
}
