/*
 * Copyright (C) 2010 Moduad Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.stb.core.test.msg;

import org.jivesoftware.smack.packet.IQ;

/** 
 * This class represents a notifcatin IQ packet.
 *
 * @author Sehwan Noh (devnoh@gmail.com)
 */
public class AdvancedSearchIQ extends IQ {

    private String id;

    private String apikey;

    private String searchkey;

    private String searchtype;

    private String ages;
    
    private String genders;
    
    private String distances;
    
    private String online;
    
    private String startIndex;
    
    private String numResults;

    public AdvancedSearchIQ() {
    }

    @Override
    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append("advancedsearchiq").append(" xmlns=\"").append(
                "com:stb:advancedsearchiq").append("\">");
        if (id != null) {
            buf.append("<id>").append(id).append("</id>");
        }
        if (apikey != null) {
            buf.append("<apikey>").append(apikey).append("</apikey>");
        }
        if (searchkey != null) {
            buf.append("<searchkey>").append(searchkey).append("</searchkey>");
        }
        if (searchtype != null) {
            buf.append("<searchtype>").append(searchtype).append("</searchtype>");
        }
        if (ages != null) {
            buf.append("<ages>").append(ages).append("</ages>");
        }
        if (genders != null) {
            buf.append("<genders>").append(genders).append("</genders>");
        }
        if (distances != null) {
            buf.append("<distances>").append(distances).append("</distances>");
        }
        if (online != null) {
            buf.append("<online>").append(online).append("</online>");
        }
        if (startIndex != null) {
            buf.append("<startIndex>").append(startIndex).append("</startIndex>");
        }
        if (numResults != null) {
            buf.append("<numResults>").append(numResults).append("</numResults>");
        }
        buf.append("</").append("advancedsearchiq").append(">");
        return buf.toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getSearchkey() {
        return searchkey;
    }

    public void setSearchkey(String searchkey) {
        this.searchkey = searchkey;
    }

    public String getSearchtype() {
        return searchtype;
    }

    public void setSearchtype(String searchtype) {
        this.searchtype = searchtype;
    }
    
    public String getAges() {
        return ages;
    }

    public void setAges(String ages) {
        this.ages = ages;
    }

    public String getGenders() {
        return genders;
    }

    public void setGenders(String genders) {
        this.genders = genders;
    }
    
    public String getDistances() {
        return distances;
    }

    public void setDistances(String distances) {
        this.distances = distances;
    }
    
    public String getOnline() {
        return online;
    }

    public void setOnline(String online) {
        this.online = online;
    }
    
    public String getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(String startIndex) {
        this.startIndex = startIndex;
    }
    
    public String getNumResults() {
        return numResults;
    }

    public void setNumResults(String numResults) {
        this.numResults = numResults;
    }

}
