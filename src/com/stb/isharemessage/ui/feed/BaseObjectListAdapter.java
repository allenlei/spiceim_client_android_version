package com.stb.isharemessage.ui.feed;

//package com.immomo.momo.android;

import java.util.ArrayList;


import java.util.List;



//import com.stb.isharemessage.R;
import com.dodola.model.DuitangInfo;
//import com.stb.isharemessage.ui.HandyTextView;
//import com.stb.isharemessage.utils.Entity;

import com.spice.im.R;
import com.spice.im.utils.Entity;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Toast;
import com.spice.im.ui.HandyTextView;

//import com.immomo.momo.android.activity.R;
//import com.immomo.momo.android.entity.Entity;
//import com.immomo.momo.android.view.HandyTextView;

public class BaseObjectListAdapter extends BaseAdapter {

//	protected BaseApplication mApplication;
	protected Context mContext;
	protected LayoutInflater mInflater;
	protected List<? extends Entity> mDatas = new ArrayList<Entity>();//java 泛型
	
	public void clearDatas(){//added by allen 20141216
		mDatas.clear();
    }
	//向“未知集合”中添加对象不是类型安全的，这会导致编译错误，唯一例外的是null。此外从“未知集合”中获得的对象的类型也只能是Object类型
	/**
	 * 	Collection<?> c = new ArrayList<String>();  
		c.add("1"); // The method add(capture#1-of ?) in the type Collectoin<capture#1 of ?> is not applicable for the arguments (String)  
		c.add(null);  
		Object o = c.get(0);  
	 */
//    public void addItemLast(List<? extends Entity> datas) {
//    	if(datas!=null){
//    		mDatas.clear();
//        	for (Entity info : datas) {
//        		mDatas.add(info);
//            }
//    	}
//    }

	public BaseObjectListAdapter(Context context,
			List<? extends Entity> datas) {//BaseApplication application,
//		mApplication = application;
		mContext = context;
		mInflater = LayoutInflater.from(context);
		if (datas != null) {
			mDatas = datas;
		}
	}

	@Override
	public int getCount() {
		return mDatas.size();
	}

	@Override
	public Object getItem(int position) {
		return mDatas.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return null;
	}

	public List<? extends Entity> getDatas() {
		return mDatas;
	}

	protected void showCustomToast(String text) {
		View toastRoot = LayoutInflater.from(mContext).inflate(
				R.layout.common_toast, null);
		((HandyTextView) toastRoot.findViewById(R.id.toast_text)).setText(text);
		Toast toast = new Toast(mContext);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.setDuration(Toast.LENGTH_SHORT);
		toast.setView(toastRoot);
		toast.show();
	}
}

