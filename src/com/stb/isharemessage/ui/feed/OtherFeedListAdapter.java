package com.stb.isharemessage.ui.feed;

//package com.immomo.momo.android.adapter;

import java.util.ArrayList;


import java.util.List;

import org.jivesoftware.smack.util.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

//import com.stb.isharemessage.R;















import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.text.ClipboardManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.AbsListView.RecyclerListener;













import com.nostra13.universalimageloader.core.ImageLoader;
//import com.immomo.momo.android.BaseApplication;
//import com.immomo.momo.android.BaseObjectListAdapter;
////import com.immomo.momo.android.R;
//import com.immomo.momo.android.activity.FeedProfileActivity;
//import com.immomo.momo.android.activity.R;
//import com.immomo.momo.android.dialog.FlippingLoadingDialog;
//import com.immomo.momo.android.dialog.SimpleListDialog;
//import com.example.android.bitmapfun.util.ImageFetcher;
//import com.immomo.momo.android.dialog.SimpleListDialog.onSimpleListItemClickListener;
//import com.immomo.momo.android.entity.Entity;
//import com.immomo.momo.android.entity.Feed;
//import com.immomo.momo.android.entity.NearByPeople;
//import com.immomo.momo.android.entity.NearByPeopleProfile;
//import com.immomo.momo.android.popupwindow.OtherFeedListPopupWindow;
//import com.immomo.momo.android.popupwindow.OtherFeedListPopupWindow.onOtherFeedListPopupItemClickListner;
//import com.immomo.momo.android.view.EmoticonsTextView;
//import com.immomo.momo.android.view.HandyTextView;
import com.sdp.custom.CImageMarkView;
import com.sdp.custom.ImagePoint;
import com.spice.im.FlippingLoadingDialog;
import com.spice.im.R;
import com.spice.im.SimpleListDialog;
import com.spice.im.SpiceApplication;
import com.spice.im.SimpleListDialog.onSimpleListItemClickListener;
import com.spice.im.SimpleListDialogAdapter;
import com.spice.im.ui.HandyTextView;
import com.spice.im.utils.Entity;
import com.spice.im.utils.Feed;
import com.spice.im.utils.ImageFetcher;
import com.spiceim.db.TContactGroupAdapter;
import com.stb.isharemessage.service.XmppConnectionAdapter;
//import com.stb.isharemessage.ui.HandyTextView;
import com.stb.isharemessage.ui.feed.OtherFeedListPopupWindow.onOtherFeedListPopupItemClickListner;
//import com.stb.isharemessage.ui.register.FlippingLoadingDialog;
//import com.stb.isharemessage.ui.register.SimpleListDialog;
//import com.stb.isharemessage.ui.register.SimpleListDialogAdapter;
//import com.stb.isharemessage.ui.register.SimpleListDialog.onSimpleListItemClickListener;
//import com.stb.isharemessage.utils.Entity;
//import com.stb.isharemessage.utils.Feed;
import com.tritrees.easygridview.widget.EasyGridView;
import com.tritrees.easygridview.widget.HeadImg;

@SuppressWarnings("deprecation")
public class OtherFeedListAdapter extends BaseObjectListAdapter implements
		onSimpleListItemClickListener, onOtherFeedListPopupItemClickListner,RecyclerListener {

//	private NearByPeopleProfile mProfile;
//	private NearByPeople mPeople;
	private OtherFeedListPopupWindow mPopupWindow;
	private int mWidthAndHeight;
	private int mPosition;
	private SimpleListDialog mDialog;
	
	private ImageFetcher mImageFetcher;
	private TContactGroupAdapter tContactGroupAdapter = null;
	String headpath = "";
	
	private Activity mActivity;
	private String mOtherusername;
	
	private Display display;
	private int mScreenWidth;
	private int mScreenHeight;
	
	private int mType = 2;

//	public OtherFeedListAdapter(NearByPeopleProfile profile,
//			NearByPeople people, BaseApplication application, Context context,
//			List<? extends Entity> datas) {
	public OtherFeedListAdapter(Activity activity,Context context,
			List<? extends Entity> datas) {	
		super(context, datas);
		mActivity = activity;
		tContactGroupAdapter = TContactGroupAdapter.getInstance(mContext);
//		mOtherusername = otherusername;
//		mProfile = profile;
//		mPeople = people;
		mWidthAndHeight = (int) TypedValue.applyDimension(
				TypedValue.COMPLEX_UNIT_DIP, 120, context.getResources()
						.getDisplayMetrics());
		mPopupWindow = new OtherFeedListPopupWindow(context, mWidthAndHeight,
				mWidthAndHeight);
		mPopupWindow.setOnOtherFeedListPopupItemClickListner(this);
		
//		display = mActivity.getWindowManager().getDefaultDisplay();
//		Width =  display.getWidth();//返回px值
		DisplayMetrics metric = new DisplayMetrics();
		mActivity.getWindowManager().getDefaultDisplay().getMetrics(metric);
		mScreenWidth = metric.widthPixels - 12;
		mScreenHeight = metric.heightPixels;
		
        mImageFetcher = new ImageFetcher(mContext, mScreenWidth);
        mImageFetcher.setUListype(1);
        mImageFetcher.setLoadingImage(R.drawable.empty_photo);
        mImageFetcher.setExitTasksEarly(false);
        
//        try{
//        	mType = Integer.parseInt(mtype);
//        }catch(Exception e){
//        	mType = 2;
//        }
        
	}
	
	public void setOtherusername(String otherusername){
		mOtherusername = otherusername;
	}
	public void setMType(String mtype){
		try{
        	mType = Integer.parseInt(mtype);
        }catch(Exception e){
        	mType = 2;
        }
	}
	
//	private View[] mFeedFanerBlocks;
	private Feed feed;
	private String[] imgArray = null;
	private String[] fanerArray = null;
	private String img = null;
	private String faner = null;
	private ArrayList<ImagePoint> pointList = null;
	private String fanerTemp = null;
	private JSONArray array = null;
	private ImagePoint imagePoint = null;
	private JSONObject object = null;
	private String markTempId = null;
	private String x = null;
	private String y = null;
	private String markStr = null; 
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.listitem_feed, null);
			holder = new ViewHolder();
			holder.root = (RelativeLayout) convertView
					.findViewById(R.id.feed_item_layout_root);
			holder.avatar = (ImageView) convertView
					.findViewById(R.id.feed_item_iv_avatar);
			holder.time = (HandyTextView) convertView
					.findViewById(R.id.feed_item_htv_time);
			holder.name = (HandyTextView) convertView
					.findViewById(R.id.feed_item_htv_name);
			holder.content = (EmoticonsTextView) convertView
					.findViewById(R.id.feed_item_etv_content);

//			mFeedFanerBlocks = new View[4];
//			mFeedFanerBlocks[0] = convertView.findViewById(R.id.feed_faner_block0);
////			mFeedFanerBlocks[0].setVisibility(View.GONE);
//			mFeedFanerBlocks[1] = convertView.findViewById(R.id.feed_faner_block1);
////			mFeedFanerBlocks[1].setVisibility(View.GONE);
//			mFeedFanerBlocks[2] = convertView.findViewById(R.id.feed_faner_block2);
////			mFeedFanerBlocks[2].setVisibility(View.GONE);
//			mFeedFanerBlocks[3] = convertView.findViewById(R.id.feed_faner_block3);
////			mFeedFanerBlocks[3].setVisibility(View.GONE);
//			
//			holder.contentImage0 = (CImageMarkView) mFeedFanerBlocks[0]
//					.findViewById(R.id.feed_item_iv_content);
//			holder.contentImage_cover0 = (ImageView) mFeedFanerBlocks[0]
//			.findViewById(R.id.feed_item_iv_content_cover);
			holder.contentImage0 = (CImageMarkView) convertView
			.findViewById(R.id.feed_item_iv_content);
			holder.contentImage_cover0 = (ImageView) convertView
			.findViewById(R.id.feed_item_iv_content_cover);
//			
//			holder.contentImage1 = (CImageMarkView) mFeedFanerBlocks[1]
// 			.findViewById(R.id.feed_item_iv_content);
// 			holder.contentImage_cover1 = (ImageView) mFeedFanerBlocks[1]
// 			.findViewById(R.id.feed_item_iv_content_cover);
//			
// 			holder.contentImage2 = (CImageMarkView) mFeedFanerBlocks[2]
//  			.findViewById(R.id.feed_item_iv_content);
//  			holder.contentImage_cover2 = (ImageView) mFeedFanerBlocks[2]
//  			.findViewById(R.id.feed_item_iv_content_cover);
//
// 			holder.contentImage3 = (CImageMarkView) mFeedFanerBlocks[3]
//  			.findViewById(R.id.feed_item_iv_content);
//  			holder.contentImage_cover3 = (ImageView) mFeedFanerBlocks[3]
//  			.findViewById(R.id.feed_item_iv_content_cover);
			
			
			
  			
			holder.more = (ImageButton) convertView
					.findViewById(R.id.feed_item_ib_more);
			holder.comment = (LinearLayout) convertView
					.findViewById(R.id.feed_item_layout_comment);
			holder.commentCount = (HandyTextView) convertView
					.findViewById(R.id.feed_item_htv_commentcount);
			holder.site = (HandyTextView) convertView
					.findViewById(R.id.feed_item_htv_site);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		feed = (Feed) getItem(position);
//		if(mType==1)
//			mImageFetcher.loadNormalImage(XmppConnectionAdapter.downloadPrefix+feed.getUser_id()+"/"+feed.getHeadimg(), holder.avatar,1,36,36,null);
			
		if(tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(feed.getUser_id()+"@0"))!=null)
			headpath = tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(feed.getUser_id()+"@0")).getAvatarPath();	
		if(headpath!=null
	    		&& headpath.length()!=0
	    		&& !headpath.equalsIgnoreCase("null"))	
//			ImageLoader.getInstance().displayImage(XmppConnectionAdapter.downloadPrefix.replace("/javacenterhome/","/")+headpath, holder.avatar,SpiceApplication.getInstance().options);
		ImageLoader.getInstance().displayImage(XmppConnectionAdapter.downloadPrefix+headpath, holder.avatar,SpiceApplication.getInstance().options);	
		else
			holder.avatar.setImageResource(R.drawable.empty_photo4);
//		else
//		mImageFetcher.loadNormalImage(XmppConnectionAdapter.downloadPrefix+mOtherusername+"/"+feed.getHeadimg(), holder.avatar,1,36,36);
		holder.name.setText(feed.getNickname());
		holder.time.setText(feed.getTime());
		holder.content.setText(feed.getContent());
		
		Log.e("MMMMMMMMMMMM20140919OtherFeedListAdapterMMMMMMMMMMMM", "++++++++++++++img="+feed.getContentImage());
		Log.e("MMMMMMMMMMMM20140919OtherFeedListAdapterMMMMMMMMMMMM", "++++++++++++++faner="+feed.getFaner());
		
		if (feed.getContentImage() == null
				|| feed.getContentImage().equalsIgnoreCase("null")
				|| feed.getContentImage().length()==0) {
//			holder.contentImage.setVisibility(View.GONE);

//			mFeedFanerBlocks[0].setVisibility(View.GONE);
			holder.contentImage0.setVisibility(View.GONE);
			holder.contentImage_cover0.setVisibility(View.GONE);
			
//			mFeedFanerBlocks[1].setVisibility(View.GONE);
//			holder.contentImage1.setVisibility(View.GONE);
//			holder.contentImage_cover1.setVisibility(View.GONE);
//			mFeedFanerBlocks[2].setVisibility(View.GONE);
//			holder.contentImage2.setVisibility(View.GONE);
//			holder.contentImage_cover2.setVisibility(View.GONE);
//			mFeedFanerBlocks[3].setVisibility(View.GONE);
//			holder.contentImage3.setVisibility(View.GONE);
//			holder.contentImage_cover3.setVisibility(View.GONE);
		} else {
//			holder.contentImage.setVisibility(View.VISIBLE);
			
			imgArray = null;
			fanerArray = null;
			img = feed.getContentImage();
			if(img!=null 
					&& !img.equalsIgnoreCase("null")
					&& img.length()!=0){
//				img = img.substring(0, img.length()-1);
				imgArray = img.split("\\|");
//				mImageFetcher.loadNormalImage(XmppConnectionAdapter.downloadPrefix+mOtherusername+"/"+img, holder.contentImage_cover,mScreenWidth,0,0);//背景图
			}
			faner = feed.getFaner();
			if(faner!=null 
					&& !faner.equalsIgnoreCase("null")
					&& faner.length()!=0){
//				faner = faner.substring(0, faner.length()-1);
//				faner = faner.substring(img.length()+1, faner.length());
				fanerArray = faner.split("\\|");
			}
			if(imgArray!=null){
//				if(imgArray.length<4){
//					for(int k=3;k>=imgArray.length;k--){
//						mFeedFanerBlocks[k].setVisibility(View.GONE);
//						if(k==0){
//						holder.contentImage0.setVisibility(View.GONE);
//						holder.contentImage_cover0.setVisibility(View.GONE);
//						}else if(k==1){
//						holder.contentImage1.setVisibility(View.GONE);
//						holder.contentImage_cover1.setVisibility(View.GONE);
//						}else if(k==2){
//						holder.contentImage2.setVisibility(View.GONE);
//						holder.contentImage_cover2.setVisibility(View.GONE);
//						}else if(k==3){
//						holder.contentImage3.setVisibility(View.GONE);
//						holder.contentImage_cover3.setVisibility(View.GONE);
//						}
//					}
//				}
				switch(imgArray.length){
					case 0:
						break;
					case 1:
//						mFeedFanerBlocks[0].setVisibility(View.VISIBLE);
						holder.contentImage0.setVisibility(View.VISIBLE);
						holder.contentImage_cover0.setVisibility(View.VISIBLE);
						get(holder,feed.getUser_id(),0);
//						mFeedFanerBlocks[1].setVisibility(View.GONE);
//						holder.contentImage1.setVisibility(View.GONE);
//						holder.contentImage_cover1.setVisibility(View.GONE);
//						mFeedFanerBlocks[2].setVisibility(View.GONE);
//						holder.contentImage2.setVisibility(View.GONE);
//						holder.contentImage_cover2.setVisibility(View.GONE);
//						mFeedFanerBlocks[3].setVisibility(View.GONE);
//						holder.contentImage3.setVisibility(View.GONE);
//						holder.contentImage_cover3.setVisibility(View.GONE);
						break;
					case 2:
//						mFeedFanerBlocks[0].setVisibility(View.VISIBLE);
						holder.contentImage0.setVisibility(View.VISIBLE);
						holder.contentImage_cover0.setVisibility(View.VISIBLE);
						get(holder,feed.getUser_id(),0);
//						mFeedFanerBlocks[1].setVisibility(View.VISIBLE);
//						holder.contentImage1.setVisibility(View.VISIBLE);
//						holder.contentImage_cover1.setVisibility(View.VISIBLE);
//						get(holder,1);
//						mFeedFanerBlocks[2].setVisibility(View.GONE);
//						holder.contentImage2.setVisibility(View.GONE);
//						holder.contentImage_cover2.setVisibility(View.GONE);
//						mFeedFanerBlocks[3].setVisibility(View.GONE);
//						holder.contentImage3.setVisibility(View.GONE);
//						holder.contentImage_cover3.setVisibility(View.GONE);
						break;
					case 3:
//						mFeedFanerBlocks[0].setVisibility(View.VISIBLE);
						holder.contentImage0.setVisibility(View.VISIBLE);
						holder.contentImage_cover0.setVisibility(View.VISIBLE);
						get(holder,feed.getUser_id(),0);
//						mFeedFanerBlocks[1].setVisibility(View.VISIBLE);
//						holder.contentImage1.setVisibility(View.VISIBLE);
//						holder.contentImage_cover1.setVisibility(View.VISIBLE);
//						get(holder,1);
//						mFeedFanerBlocks[2].setVisibility(View.VISIBLE);
//						holder.contentImage2.setVisibility(View.VISIBLE);
//						holder.contentImage_cover2.setVisibility(View.VISIBLE);
//						get(holder,2);
//						mFeedFanerBlocks[3].setVisibility(View.GONE);
//						holder.contentImage3.setVisibility(View.GONE);
//						holder.contentImage_cover3.setVisibility(View.GONE);
						break;
					case 4:
//						mFeedFanerBlocks[0].setVisibility(View.VISIBLE);
						holder.contentImage0.setVisibility(View.VISIBLE);
						holder.contentImage_cover0.setVisibility(View.VISIBLE);
						get(holder,feed.getUser_id(),0);
//						mFeedFanerBlocks[1].setVisibility(View.VISIBLE);
//						holder.contentImage1.setVisibility(View.VISIBLE);
//						holder.contentImage_cover1.setVisibility(View.VISIBLE);
//						get(holder,1);
//						mFeedFanerBlocks[2].setVisibility(View.VISIBLE);
//						holder.contentImage2.setVisibility(View.VISIBLE);
//						holder.contentImage_cover2.setVisibility(View.VISIBLE);
//						get(holder,2);
//						mFeedFanerBlocks[3].setVisibility(View.VISIBLE);
//						holder.contentImage3.setVisibility(View.VISIBLE);
//						holder.contentImage_cover3.setVisibility(View.VISIBLE);
//						get(holder,3);
						break;
				}
//			    for(int m=0;m<imgArray.length;m++){
//					
//					if(m==0)
//						mImageFetcher.loadNormalImage(XmppConnectionAdapter.downloadPrefix+mOtherusername+"/"+imgArray[m], holder.contentImage_cover0,mScreenWidth,0,0);//背景图
//					else if(m==1)
//						mImageFetcher.loadNormalImage(XmppConnectionAdapter.downloadPrefix+mOtherusername+"/"+imgArray[m], holder.contentImage_cover1,mScreenWidth,0,0);//背景图
//					else if(m==2)
//						mImageFetcher.loadNormalImage(XmppConnectionAdapter.downloadPrefix+mOtherusername+"/"+imgArray[m], holder.contentImage_cover2,mScreenWidth,0,0);//背景图
//					else if(m==3)
//						mImageFetcher.loadNormalImage(XmppConnectionAdapter.downloadPrefix+mOtherusername+"/"+imgArray[m], holder.contentImage_cover3,mScreenWidth,0,0);//背景图
//					pointList = new ArrayList<ImagePoint>();
//					if(fanerArray!=null && fanerArray.length>=m){
//						for(int n=0;n<fanerArray.length;n++){
//							if(fanerArray[n].startsWith(imgArray[m])){
//								fanerTemp = fanerArray[n].substring(imgArray[m].length()+1, fanerArray[n].length());
//								if(fanerTemp!=null
//										&& !fanerTemp.equalsIgnoreCase("null")
//										&& fanerTemp.length()!=0)
//								try {
//									array = new JSONArray(fanerTemp);
//									imagePoint = null;
//									for (int i = 0; i < array.length(); i++) {
//										object = array.getJSONObject(i);
//										markTempId = object.getString("markTempId");
//										x = object.getString("x");
//										y = object.getString("y");
//										markStr = object.getString("markStr");
//										imagePoint = new ImagePoint(Integer.parseInt(markTempId),
//										Float.valueOf(x).floatValue(),
//										Float.valueOf(y).floatValue(),
//										markStr);
//										pointList.add(imagePoint);
//									}
//								} catch (JSONException e) {
//									e.printStackTrace();
//								} catch(Exception e){
//									e.printStackTrace();
//								}
//								break;
//							}
//						}
//					}
//					if(m==0)
//						holder.contentImage0.init(mActivity, null,false,pointList);
//					else if(m==1)
//						holder.contentImage1.init(mActivity, null,false,pointList);
//					else if(m==2)
//						holder.contentImage2.init(mActivity, null,false,pointList);
//					else if(m==3)
//						holder.contentImage3.init(mActivity, null,false,pointList);
//			    }
			}
		}
		holder.site.setText(feed.getSite());
		holder.commentCount.setText(feed.getCommentCount() + "");
		holder.comment.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				mPosition = position;
				Intent intent = new Intent(mContext, FeedProfileActivity.class);
//				intent.putExtra("entity_profile", mProfile);
//				intent.putExtra("entity_people", mPeople);
				intent.putExtra("otherusername", mOtherusername);
				intent.putExtra("entity_feed", (Feed) getItem(mPosition));
//				intent.putExtra("", feed.getNickname());
				mContext.startActivity(intent);
			}
		});
		holder.more.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				mPosition = position;
				int[] location = new int[2];
				arg0.getLocationOnScreen(location);
				mPopupWindow.showAtLocation(arg0, Gravity.NO_GRAVITY,
						location[0], location[1] - mWidthAndHeight + 30);
			}
		});
		holder.root.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mPosition = position;
				Intent intent = new Intent(mContext, FeedProfileActivity.class);
//				intent.putExtra("entity_profile", mProfile);
//				intent.putExtra("entity_people", mPeople);
				intent.putExtra("entity_feed", (Feed) getItem(mPosition));
				mContext.startActivity(intent);
			}
		});
		return convertView;
	}

	public void get(ViewHolder holder,String uid,int m){
		if(m==0){
//			if(mType==1)
//				;
//			else
			//mOtherusername
//			mImageFetcher.loadNormalImage(XmppConnectionAdapter.downloadPrefix+feed.getUser_id()+"/"+imgArray[m], holder.contentImage_cover0,mScreenWidth,0,0,null);//背景图
//		    "/jeesite/userfiles/26/images/photo/2017/12/"+imgArray[m]
//		    ImageLoader.getInstance().displayImage(XmppConnectionAdapter.downloadPrefix.replace("/javacenterhome/","/")+"/jeesiteoa/userfiles/"+uid+"/images/photo/"+imgArray[m], holder.contentImage_cover0,SpiceApplication.getInstance().options);
		    ImageLoader.getInstance().displayImage(XmppConnectionAdapter.downloadPrefix+"/userfiles/"+uid+"/images/photo/"+imgArray[m], holder.contentImage_cover0,SpiceApplication.getInstance().options);
		}
		
//		else if(m==1){
//			mImageFetcher.loadNormalImage(XmppConnectionAdapter.downloadPrefix+feed.getUser_id()+"/"+imgArray[m], holder.contentImage_cover1,mScreenWidth,0,0,null);//背景图
//		}else if(m==2){
//			mImageFetcher.loadNormalImage(XmppConnectionAdapter.downloadPrefix+feed.getUser_id()+"/"+imgArray[m], holder.contentImage_cover2,mScreenWidth,0,0,null);//背景图
//		}else if(m==3){
//			mImageFetcher.loadNormalImage(XmppConnectionAdapter.downloadPrefix+feed.getUser_id()+"/"+imgArray[m], holder.contentImage_cover3,mScreenWidth,0,0,null);//背景图
//		}
		pointList = new ArrayList<ImagePoint>();
		if(fanerArray!=null && fanerArray.length>=m){
			for(int n=0;n<fanerArray.length;n++){
				if(fanerArray[n].startsWith(imgArray[m])){
					fanerTemp = fanerArray[n].substring(imgArray[m].length()+1, fanerArray[n].length());
					if(fanerTemp!=null
							&& !fanerTemp.equalsIgnoreCase("null")
							&& fanerTemp.length()!=0)
					try {
						array = new JSONArray(fanerTemp);
						imagePoint = null;
						for (int i = 0; i < array.length(); i++) {
							object = array.getJSONObject(i);
							markTempId = object.getString("markTempId");
							x = object.getString("x");
							y = object.getString("y");
							markStr = object.getString("markStr");
							imagePoint = new ImagePoint(Integer.parseInt(markTempId),
							Float.valueOf(x).floatValue(),
							Float.valueOf(y).floatValue(),
							markStr);
							pointList.add(imagePoint);
						}
					} catch (JSONException e) {
						e.printStackTrace();
					} catch(Exception e){
						e.printStackTrace();
					}
					break;
				}
			}
		}
		if(m==0)
			holder.contentImage0.init(mActivity, null,false,pointList);
//		else if(m==1)
//			holder.contentImage1.init(mActivity, null,false,pointList);
//		else if(m==2)
//			holder.contentImage2.init(mActivity, null,false,pointList);
//		else if(m==3)
//			holder.contentImage3.init(mActivity, null,false,pointList);
	}
	
    /**
    249	     * 当列表单元滚动到可是区域外时清除掉已记录的图片视图
    250	     *
    251	     * @param view
    252	     */
    @Override
    public void onMovedToScrapHeap(View view) {
        ViewHolder holder = (ViewHolder) view.getTag();
//    	this.imageViews.remove(holder.avatar);
//    	this.imageViews.remove(holder.picPhoto);
        
        BitmapDrawable drawable = (BitmapDrawable)holder.contentImage_cover0.getDrawable();
        Bitmap bmp = drawable.getBitmap();
    	if (null != bmp && !bmp.isRecycled()){
    		bmp.recycle();
    		bmp = null;
    	}
    	holder.contentImage_cover0.setImageBitmap(null);
        
    	BitmapDrawable drawable2 = (BitmapDrawable)holder.avatar.getDrawable();
        Bitmap bmp2 = drawable2.getBitmap();
    	if (null != bmp2 && !bmp2.isRecycled()){
    		bmp2.recycle();
    		bmp2 = null;
    	}
    	holder.avatar.setImageBitmap(null);
    }
    
	class ViewHolder {
		RelativeLayout root;
		ImageView avatar;
		HandyTextView time;
		HandyTextView name;
		EmoticonsTextView content;
		CImageMarkView contentImage0;
		ImageView contentImage_cover0;
//		CImageMarkView contentImage1;
//		ImageView contentImage_cover1;
//		CImageMarkView contentImage2;
//		ImageView contentImage_cover2;
//		CImageMarkView contentImage3;
//		ImageView contentImage_cover3;
		ImageButton more;
		LinearLayout comment;
		HandyTextView commentCount;
		HandyTextView site;
	}

	@Override
	public void onItemClick(int position) {
		final FlippingLoadingDialog dialog = new FlippingLoadingDialog(
				mContext, "正在提交,请稍后...");
		dialog.show();
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				dialog.dismiss();
				showCustomToast("举报的信息已提交");
			}
		}, 1500);
	}

	@Override
	public void onCopy(View v) {
		Feed feed = (Feed) getItem(mPosition);
		String text = feed.getContent();
		ClipboardManager m = (ClipboardManager) mContext
				.getSystemService(Context.CLIPBOARD_SERVICE);
		m.setText(text);
		showCustomToast("已成功复制文本");
	}

	@Override
	public void onReport(View v) {
		String[] codes = mContext.getResources().getStringArray(
				R.array.reportfeed_items);
		mDialog = new SimpleListDialog(mContext);
		mDialog.setTitle("举报留言");
		mDialog.setTitleLineVisibility(View.GONE);
		mDialog.setAdapter(new SimpleListDialogAdapter(mContext, codes));
		mDialog.setOnSimpleListItemClickListener(this);
		mDialog.show();
	}
}

