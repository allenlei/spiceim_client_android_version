/*
 * Copyright (C) 2010 Moduad Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.stb.isharemessage.ui.feed;

import java.util.ArrayList;


import org.jivesoftware.smack.packet.IQ;

/** 
 * This class represents a notifcatin IQ packet.
 *
 * @author Sehwan Noh (devnoh@gmail.com)
 */
public class DynamicsCommentsIQResponse extends IQ {

    private String id;

    private String apikey;

    private String totalnumber;
    
    private String cttotal;

    public ArrayList dynamics = new ArrayList();
    
    DynamicsCommentsItem dynamicsItem = null;


    private String retcode;
    private String memo;
    public DynamicsCommentsIQResponse() {
    }

    @Override
    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append("dynamicscommentsiq").append(" xmlns=\"").append(
                "com:stb:dynamicscommentsiq").append("\">");
        if (id != null) {
            buf.append("<id>").append(id).append("</id>");
        }
        if (apikey != null) {
            buf.append("<apikey>").append(apikey).append("</apikey>");
        }
        if (totalnumber != null) {
            buf.append("<totalnumber>").append(totalnumber).append("</totalnumber>");
        }
        if (cttotal != null) {
            buf.append("<cttotal>").append(cttotal).append("</cttotal>");
        }
        if (dynamics != null && dynamics.size()!=0) {
        	
            buf.append("<dynamics>");
            for(int i=0;i<dynamics.size();i++){
            	dynamicsItem = (DynamicsCommentsItem)dynamics.get(i);
//            	buf.append("<user>").append(users.get(i)).append("</user>");
            	//<user status="100" "_name=" "uid=test" "email=null">
            	buf.append("<dynamic id=\""+dynamicsItem.getId()+"\" uid=\""+dynamicsItem.getUser_id()+"\" "+"email=\""+dynamicsItem.getEmail()+"\" "+"distance=\""+dynamicsItem.getDistance()+"\" "+"time=\""+dynamicsItem.getTime()+"\" "+"gender=\""+dynamicsItem.getGender()+"\" "+"birthday=\""+dynamicsItem.getBirthday()+"\" "+"name=\""+dynamicsItem.getName()+"\" originalid=\""+dynamicsItem.getOriginalid()+"\""+">")
            	.append("<memo>").append(dynamicsItem.getMemo()).append("</memo>")
            	.append("<pictures>").append(dynamicsItem.getPictures()).append("</pictures>")
            	.append("<faner>").append(dynamicsItem.getFaner()).append("</faner>")
            	.append("</dynamic>");
            }
            buf.append("</dynamics>");
        }
        if(retcode!=null){
        	buf.append("<retcode>").append(retcode).append("</retcode>");
        }
        if(memo!=null){
        	buf.append("<memo>").append(memo).append("</memo>");
        }
        buf.append("</").append("dynamicscommentsiq").append(">");
        return buf.toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getTotalnumber() {
        return totalnumber;
    }

    public void setTotalnumber(String totalnumber) {
        this.totalnumber = totalnumber;
    }
    
    public String getCttotal() {
        return cttotal;
    }

    public void setCttotal(String cttotal) {
        this.cttotal = cttotal;
    }

    public ArrayList getDynamics() {
        return dynamics;
    }

    public void setDynamics(ArrayList dynamics) {
        this.dynamics = dynamics;
    }
	public String getRetcode() {
		return retcode;
	}
	public void setRetcode(String retcode) {
		this.retcode = retcode;
	}

	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
}
