package com.stb.isharemessage.ui.feed;

//package com.immomo.momo.android.adapter;

import java.util.List;





import com.spice.im.BaseArrayListAdapter;
import com.spice.im.R;
//import com.stb.isharemessage.R;
//import com.stb.isharemessage.BeemApplication;
//import com.stb.isharemessage.ui.register.BaseArrayListAdapter;




import com.spice.im.SpiceApplication;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

//import com.immomo.momo.android.BaseApplication;
//import com.immomo.momo.android.BaseArrayListAdapter;
////import com.immomo.momo.android.R;
//import com.immomo.momo.android.activity.R;

public class EmoteAdapter extends BaseArrayListAdapter {

	public EmoteAdapter(Context context, List<String> datas) {
		super(context, datas);
	}
    private String name;
    private int id;
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.listitem_emote, null);
			holder = new ViewHolder();
			holder.mIvImage = (ImageView) convertView
					.findViewById(R.id.emote_item_iv_image);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		name = (String) getItem(position);
		id = SpiceApplication.mEmoticonsId.get(name);
		holder.mIvImage.setImageResource(id);
		return convertView;
	}

	class ViewHolder {
		ImageView mIvImage;
	}
}

