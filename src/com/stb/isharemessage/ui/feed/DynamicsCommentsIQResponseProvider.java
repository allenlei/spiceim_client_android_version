/*
 * Copyright (C) 2010 Moduad Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.stb.isharemessage.ui.feed;

import java.util.HashMap;



import org.jivesoftware.smack.packet.IQ;

import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;

/** 
 * This class parses incoming IQ packets to NotificationIQ objects.
 *
 * @author Sehwan Noh (devnoh@gmail.com)
 */
public class DynamicsCommentsIQResponseProvider implements IQProvider {
//	HashMap hm = new HashMap();
	DynamicsCommentsItem dynamicsItem = null;  
	String oid = null;
    public DynamicsCommentsIQResponseProvider() {
    }

    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {

    	DynamicsCommentsIQResponse dynamicsIQResponse = new DynamicsCommentsIQResponse();
        for (boolean done = false; !done;) {
            int eventType = parser.next();
            if (eventType == 2) {
                if ("id".equals(parser.getName())) {
                	dynamicsIQResponse.setId(parser.nextText());
                }
                if ("apikey".equals(parser.getName())) {
                	dynamicsIQResponse.setApikey(parser.nextText());
                }
                if ("totalnumber".equals(parser.getName())) {
                	dynamicsIQResponse.setTotalnumber(parser.nextText());
                }
                if ("cttotal".equals(parser.getName())) {
                	dynamicsIQResponse.setCttotal(parser.nextText());
                }
                if ("dynamic".equals(parser.getName())) {
                	dynamicsItem = new DynamicsCommentsItem();  
                	//parser.getAttributeCount();
                	dynamicsItem.setId(Integer.parseInt(parser.getAttributeValue(0)));
//                	dynamicsItem.setStatus(parser.getAttributeValue(1));
//                	
//                	dynamicsItem.set_Name(parser.getAttributeValue(2)!=null?parser.getAttributeValue(2):"");
                	dynamicsItem.setUser_id(parser.getAttributeValue(1)!=null?parser.getAttributeValue(1):"");
//                	if(parser.getAttributeCount()==5){
                	dynamicsItem.setEmail(parser.getAttributeValue(2)!=null?parser.getAttributeValue(2):"");
                	dynamicsItem.setDistance(parser.getAttributeValue(3)!=null?parser.getAttributeValue(3):"");
                	dynamicsItem.setTime(parser.getAttributeValue(4)!=null?parser.getAttributeValue(4):"");
                	dynamicsItem.setGender(parser.getAttributeValue(5)!=null?parser.getAttributeValue(5):"");
                	dynamicsItem.setBirthday(parser.getAttributeValue(6)!=null?parser.getAttributeValue(6):"");
                	dynamicsItem.setName(parser.getAttributeValue(7)!=null?parser.getAttributeValue(7):"");
                	oid = parser.getAttributeValue(8)!=null?parser.getAttributeValue(8):"0";
                	try{
                		dynamicsItem.setOriginalid(Integer.parseInt(oid));
                	}catch(Exception e){
                		dynamicsItem.setOriginalid(0);
                	}
//                	}else
//                		xgpsInfo.setDistance(parser.getAttributeValue(3)!=null?parser.getAttributeValue(3):"");
                	
//                	dynamicsItem.setUsername(parser.getAttributeValue(10)!=null?parser.getAttributeValue(10):"");
                	
//                	advancedSearchIQResponse.users.add(parser.nextText());
//                	parser.getAttributeValue(0);
                	
                	eventType = parser.next();
                	if ("memo".equals(parser.getName())){
                		dynamicsItem.setMemo(parser.nextText());
                	}
                	eventType = parser.next();
                	if ("pictures".equals(parser.getName())){
                		dynamicsItem.setPictures(parser.nextText());
                	}
                	eventType = parser.next();
                	if ("faner".equals(parser.getName())){
                		dynamicsItem.setFaner(parser.nextText());
                	}
                	dynamicsIQResponse.dynamics.add(dynamicsItem);
                }
//                if(dynamicsItem!=null){
//                if ("memo".equals(parser.getName())){
//            		dynamicsItem.setMemo(parser.nextText());
//            	}
//            	if ("pictures".equals(parser.getName())){
//            		dynamicsItem.setPictures(parser.nextText());
//            	}
//            	if ("faner".equals(parser.getName())){
//            		dynamicsItem.setFaner(parser.nextText());
//            	}
////            	dynamicsIQResponse.dynamics.add(dynamicsItem);
//                }
                if ("retcode".equals(parser.getName())) {
                	dynamicsIQResponse.setRetcode(parser.nextText());
                }
                if ("memo".equals(parser.getName())) {
                	dynamicsIQResponse.setMemo(parser.nextText());
                }
            } else if (eventType == 3
                    && "dynamicscommentsiq".equals(parser.getName())) {
                done = true;
            }
        }

        return dynamicsIQResponse;
    }

}
