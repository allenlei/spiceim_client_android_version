/*
 * Copyright (C) 2010 Moduad Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.stb.isharemessage.ui.feed;

import org.jivesoftware.smack.packet.IQ;

/** 
 * This class represents a notifcatin IQ packet.
 *
 * @author Sehwan Noh (devnoh@gmail.com)
 */
public class DynamicsCommentsIQ extends IQ {

    private String id;

    private String apikey;
    
    private String mtype;
    
    private String dynamicid;
    
    private String originalid;

    private String username;
    
    private String otherusername;

    private String longitude;

    private String latitude;
    
    private String memo;
    
    private String pictures;
    
    private String faner;
    
    private String time;
    
    private String geolocation;
    
    private String radius;
    
    private String startIndex;
    
    private String numResults;
    private String hashcode;//apikey+username(uid)+time 
    public DynamicsCommentsIQ() {
    }

    @Override
    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append("dynamicscommentsiq").append(" xmlns=\"").append(
                "com:stb:dynamicscommentsiq").append("\">");
        if (id != null) {
            buf.append("<id>").append(id).append("</id>");
        }
        if (apikey != null) {
            buf.append("<apikey>").append(apikey).append("</apikey>");
        }
        if (mtype != null) {
            buf.append("<mtype>").append(mtype).append("</mtype>");
        }
        if (dynamicid != null) {
            buf.append("<dynamicid>").append(dynamicid).append("</dynamicid>");
        }
        if (originalid != null) {
            buf.append("<originalid>").append(originalid).append("</originalid>");
        }
        if (username != null) {
            buf.append("<username>").append(username).append("</username>");
        }
        if (otherusername != null) {
            buf.append("<otherusername>").append(otherusername).append("</otherusername>");
        }
        if (longitude != null) {
            buf.append("<longitude>").append(longitude).append("</longitude>");
        }
        if (latitude != null) {
            buf.append("<latitude>").append(latitude).append("</latitude>");
        }
        if (memo != null) {
            buf.append("<memo>").append(memo).append("</memo>");
        }
        if (pictures != null) {
            buf.append("<pictures>").append(pictures).append("</pictures>");
        }
        if (faner != null) {
            buf.append("<faner>").append(faner).append("</faner>");
        }
        if (time != null) {
            buf.append("<time>").append(time).append("</time>");
        }
        if (geolocation != null) {
            buf.append("<geolocation>").append(geolocation).append("</geolocation>");
        }
        
        if (radius != null) {
            buf.append("<radius>").append(radius).append("</radius>");
        }
        
        if (startIndex != null) {
            buf.append("<startIndex>").append(startIndex).append("</startIndex>");
        }
        if (numResults != null) {
            buf.append("<numResults>").append(numResults).append("</numResults>");
        }
        if (hashcode != null) {
            buf.append("<hashcode>").append(hashcode).append("</hashcode>");
        }
        buf.append("</").append("dynamicscommentsiq").append(">");
        return buf.toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }
    public String getMtype() {
        return mtype;
    }

    public void setMtype(String mtype) {
        this.mtype = mtype;
    }
    
    public String getDynamicid() {
        return dynamicid;
    }

    public void setDynamicid(String dynamicid) {
        this.dynamicid = dynamicid;
    }
    public String getOriginalid() {
        return originalid;
    }

    public void setOriginalid(String originalid) {
        this.originalid = originalid;
    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getOtherusername() {
        return otherusername;
    }

    public void setOtherusername(String otherusername) {
        this.otherusername = otherusername;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
    
    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public String getPictures() {
		return pictures;
	}
	public void setPictures(String pictures) {
		this.pictures = pictures;
	}
	public String getFaner() {
		return faner;
	}
	public void setFaner(String faner) {
		this.faner = faner;
	}
	
    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
    
    public String getGeolocation() {
        return geolocation;
    }

    public void setGeolocation(String geolocation) {
        this.geolocation = geolocation;
    }
    
    
    public String getRadius() {
        return radius;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }
    
    
    public String getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(String startIndex) {
        this.startIndex = startIndex;
    }
    
    public String getNumResults() {
        return numResults;
    }

    public void setNumResults(String numResults) {
        this.numResults = numResults;
    }
	public String getHashcode() {
		return hashcode;
	}
	public void setHashcode(String hashcode) {
		this.hashcode = hashcode;
	}
}
