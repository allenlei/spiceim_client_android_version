/*
 * Copyright (C) 2010 Moduad Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.stb.isharemessage.ui.feed;

import org.jivesoftware.smack.packet.IQ;

import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;


public class DynamicsIQProvider implements IQProvider{
    public DynamicsIQProvider() {
    }
    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {

    	DynamicsIQ gpsIQ = new DynamicsIQ();
        for (boolean done = false; !done;) {
            int eventType = parser.next();
            if (eventType == 2) {
                if ("id".equals(parser.getName())) {
                	gpsIQ.setId(parser.nextText());
                }
                if ("apikey".equals(parser.getName())) {
                	gpsIQ.setApikey(parser.nextText());
                }
                if ("mtype".equals(parser.getName())) {
                	gpsIQ.setMtype(parser.nextText());
                }
                if ("dynamicid".equals(parser.getName())) {
                	gpsIQ.setDynamicid(parser.nextText());
                }
                if ("username".equals(parser.getName())) {
                	gpsIQ.setUsername(parser.nextText());
                }
                if ("otherusername".equals(parser.getName())) {
                	gpsIQ.setOtherusername(parser.nextText());
                }
                if ("longitude".equals(parser.getName())) {
                	gpsIQ.setLongitude(parser.nextText());
                }
                if ("latitude".equals(parser.getName())) {
                	gpsIQ.setLatitude(parser.nextText());
                }
                if ("memo".equals(parser.getName())) {
                	gpsIQ.setMemo(parser.nextText());
                }
                if ("pictures".equals(parser.getName())) {
                	gpsIQ.setPictures(parser.nextText());
                }
                if ("faner".equals(parser.getName())) {
                	gpsIQ.setFaner(parser.nextText());
                }
                if ("time".equals(parser.getName())) {
                	gpsIQ.setTime(parser.nextText());
                }
                if ("geolocation".equals(parser.getName())) {
                	gpsIQ.setGeolocation(parser.nextText());
                }
                if ("radius".equals(parser.getName())) {
                	gpsIQ.setRadius(parser.nextText());
                }
                if ("startIndex".equals(parser.getName())) {
                	gpsIQ.setStartIndex(parser.nextText());
                }
                if ("numResults".equals(parser.getName())) {
                	gpsIQ.setNumResults(parser.nextText());
                }
                if ("hashcode".equals(parser.getName())) {
                	gpsIQ.setHashcode(parser.nextText());
                }
            } else if (eventType == 3
                    && "dynamicsiq".equals(parser.getName())) {
                done = true;
            }
        }

        return gpsIQ;
    }
}
