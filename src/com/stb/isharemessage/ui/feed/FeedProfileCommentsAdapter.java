package com.stb.isharemessage.ui.feed;

//package com.immomo.momo.android.adapter;

import java.util.List;


//import com.stb.isharemessage.R;
//import com.stb.isharemessage.ui.HandyTextView;
//import com.stb.isharemessage.utils.Entity;
//import com.stb.isharemessage.utils.FeedComment;








import org.jivesoftware.smack.util.StringUtils;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.spice.im.R;
import com.spice.im.SpiceApplication;
import com.spice.im.ui.HandyTextView;
import com.spice.im.utils.Entity;
import com.spice.im.utils.FeedComment;
import com.spiceim.db.TContactGroupAdapter;
import com.stb.isharemessage.service.XmppConnectionAdapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

//import com.immomo.momo.android.BaseApplication;
//import com.immomo.momo.android.BaseObjectListAdapter;
////import com.immomo.momo.android.R;
//import com.immomo.momo.android.activity.R;
//import com.immomo.momo.android.entity.Entity;
//import com.immomo.momo.android.entity.FeedComment;
//import com.immomo.momo.android.view.EmoticonsTextView;
//import com.immomo.momo.android.view.HandyTextView;

public class FeedProfileCommentsAdapter extends BaseObjectListAdapter {
	private TContactGroupAdapter tContactGroupAdapter = null;
	String headpath = "";
	public FeedProfileCommentsAdapter(
			Context context, List<? extends Entity> datas) {//BaseApplication application,
		super(context, datas);
		tContactGroupAdapter = TContactGroupAdapter.getInstance(mContext);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = mInflater
					.inflate(R.layout.listitem_feedcomment, null);
			holder = new ViewHolder();
			holder.mIvAvatar = (ImageView) convertView
					.findViewById(R.id.feedcomment_item_iv_avatar);
			holder.mEtvName = (EmoticonsTextView) convertView
					.findViewById(R.id.feedcomment_item_etv_name);
			holder.mEtvContent = (EmoticonsTextView) convertView
					.findViewById(R.id.feedcomment_item_etv_content);
			holder.mHtvTime = (HandyTextView) convertView
					.findViewById(R.id.feedcomment_item_htv_time);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		FeedComment comment = (FeedComment) getItem(position);
		
//		holder.mIvAvatar.setImageBitmap(mApplication.getAvatar(comment
//				.getAvatar()));
		if(tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(comment.getAvatar()+"@0"))!=null)
			headpath = tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(comment.getAvatar()+"@0")).getAvatarPath();	
		if(headpath!=null
	    		&& headpath.length()!=0
	    		&& !headpath.equalsIgnoreCase("null"))	
			ImageLoader.getInstance().displayImage(XmppConnectionAdapter.downloadPrefix.replace("/javacenterhome/","/")+headpath, holder.mIvAvatar,SpiceApplication.getInstance().options);
		else
			holder.mIvAvatar.setImageResource(R.drawable.empty_photo4);
		holder.mEtvName.setText(comment.getName());
		holder.mEtvContent.setText(comment.getContent());
		holder.mHtvTime.setText(comment.getTime());
		return convertView;
	}

	class ViewHolder {
		ImageView mIvAvatar;
		EmoticonsTextView mEtvName;
//		HandyTextView mEtvName;
		EmoticonsTextView mEtvContent;
//		HandyTextView mEtvContent;
		HandyTextView mHtvTime;
	}
}

