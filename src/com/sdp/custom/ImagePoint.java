	package com.sdp.custom;
	
	//package com.sdp.model;
	
//	import java.util.Random;
	
	public class ImagePoint {
		
	
		/**
		 * 添加标注是的临时id
		 */
		private int markTempId;
		private float x;
		private float y;
		private String markStr;
		public ImagePoint(){}
		public ImagePoint(int Id,float x1,float y1,String markStr1){
			markTempId = Id;
			x = x1;
			y = y1;
			markStr = markStr1;
		}
		public float getX() {
			return x;
		}
		public void setX(float x) {
			this.x = x;
		}
		public float getY() {
			return y;
		}
		public void setY(float y) {
			this.y = y;
		}
		public int getMarkTempId() {
			return markTempId;
		}
		public void setMarkTempId(int markTempId) {
			this.markTempId = markTempId;
		}
		public String getMarkStr() {
			return markStr;
	//		int a = new Random().nextInt(100);
	//		return "随机数："+a+"测试特别长呀长呀长呀长呀长呀，我很长，不知道输入多少个字了";
		}
		public void setMarkStr(String markStr) {
			this.markStr = markStr;
		}
		//json格式{"markTempId":"markTempId","x":"x","y":"y","markStr":"markStr"}
		public String toString(){
	        StringBuilder buf = new StringBuilder();
//	        buf.append("<").append("imagepoint").append("\">"); 
//	        buf.append("<id>").append(markTempId).append("</id>");
//	        buf.append("<x>").append(x).append("</x>");
//	        buf.append("<y>").append(y).append("</y>");
//	        buf.append("<mark>").append(markStr).append("</mark>");
//	        buf.append("</").append("imagepoint").append(">");
	        buf.append("{\"").append("markTempId").append("\":\"").append(markTempId).append("\",");
	        buf.append("\"").append("x").append("\":\"").append(x).append("\",");
	        buf.append("\"").append("y").append("\":\"").append(y).append("\",");
	        buf.append("\"").append("markStr").append("\":\"").append(markStr).append("\"}");
	        return buf.toString();
		}
		
	
	}