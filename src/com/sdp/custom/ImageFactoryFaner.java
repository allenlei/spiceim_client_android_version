	package com.sdp.custom;
	
	import java.util.ArrayList;

import com.spice.im.ImageFactory;
import com.spice.im.ImageFactoryActivity;
import com.spice.im.R;
import com.spice.im.utils.PhotoUtils;




import android.graphics.Bitmap;
import android.view.View;
	
//	import com.stb.isharemessage.R;
//import com.stb.isharemessage.ui.register.ImageFactory;
//import com.stb.isharemessage.ui.register.ImageFactoryActivity;
//import com.speed.im.ImageFactory;
//import com.speed.im.ImageFactoryActivity;
//import com.speed.im.R;
//import com.stb.isharemessage.utils.PhotoUtils;
	
	public class ImageFactoryFaner extends ImageFactory{
		private CImageMarkView cv;
		private String mPath;
		private Bitmap mBitmap;
//		private Bitmap mSelectBitmap;
		public ImageFactoryFaner(ImageFactoryActivity activity,
				View contentRootView) {
			super(activity, contentRootView);
		}
	
		@Override
		public void initViews() {
			cv = (CImageMarkView) findViewById(R.id.mark);
		}
	
		@Override
		public void initEvents() {
	
		}
		public Bitmap getBitmap() {
//			return mSelectBitmap;
			return mBitmap;
		}
		public ArrayList<ImagePoint> getPointList(){
			return cv.getPointList();
		}
		public String getPointListString(){
			StringBuilder buf = new StringBuilder();
			if(cv!=null && cv.getPointList()!=null && cv.getPointList().size()!=0){
//				buf.append("<").append("imagepoints").append("\">"); 
//				buf.append("<").append("total").append("\">"); 
//				buf.append(cv.getPointList().size());
//				buf.append("</").append("total").append(">");
				buf.append("[");
				for(int i=0;i<cv.getPointList().size()-1;i++){
					buf.append(cv.getPointList().get(i).toString()).append(",");
				}
				buf.append(cv.getPointList().get(cv.getPointList().size()-1).toString());
//				buf.append("</").append("imagepoints").append(">");
				buf.append("]");
			}
			return buf.toString();
		}
		public void init(String path) {
			mPath = path;
			mBitmap = PhotoUtils.getBitmapFromFile(mPath);
			if (mBitmap != null) {
//				mSelectBitmap = mBitmap;
				cv.init(mActivity, mBitmap,true,null);
			}
		}
	}