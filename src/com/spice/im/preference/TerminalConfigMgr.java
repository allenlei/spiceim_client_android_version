package com.spice.im.preference;

import android.content.Context;

import com.spice.im.SpiceApplication;
import com.spice.im.ui.DataSaveManager;
import com.stb.isharemessage.BeemApplication;

public class TerminalConfigMgr {
	private static String rl_change_offline_pin;//设置消息通知铃声 BeemApplication.NOTIFICATION_SOUND_KEY
	private static String ll_offline_pin_setting;//消息振动提示 boolean BeemApplication.NOTIFICATION_VIBRATE_KEY
	private static String ll_offline_show_setting;//不显示离线用户 boolean BeemApplication.SHOW_OFFLINE_CONTACTS_KEY
	private static String ll_screenoff_setting;//屏幕关闭自动修改状态boolean  BeemApplication.USE_AUTO_AWAY_KEY
	private static String ll_screenoff_show_setting;//设置屏幕关闭状态签名信息 BeemApplication.AUTO_AWAY_MSG_KEY
	private static String key_specific_server = "settings_key_specific_server";//设置连接服务器 boolean settings_key_specific_server
	private static String ll_serveraddr_setting = "settings_key_xmpp_server";//服务器地址 settings_key_xmpp_server
	private static String ll_serverport_setting = "settings_key_xmpp_port";//服务器端口 settings_key_xmpp_port
	//不显示离线群hide_groups  BeemApplication.HIDE_GROUPS_KEY
	//保存聊天历史记录settings_key_history
	//聊天历史记录保存路径settings_chat_history_path   BeemApplication.CHAT_HISTORY_KEY
	//使用默认聊天窗口样式use_compact_chat_ui  BeemApplication.USE_COMPACT_CHAT_UI_KEY
	//用户名account_username   BeemApplication.ACCOUNT_USERNAME_KEY
	//密码account_password    BeemApplication.ACCOUNT_PASSWORD_KEY
	//connection_resource  Beem  BeemApplication.CONNECTION_RESOURCE_KEY
	//客户端连接优先级connection_priority 0  BeemApplication.CONNECTION_PRIORITY_KEY
	//代理服务器proxy_use  BeemApplication.PROXY_USE_KEY
	//代理服务器类型proxy_type http socks4 socks5  BeemApplication.PROXY_TYPE_KEY
	//代理服务器地址proxy_server  BeemApplication.PROXY_SERVER_KEY
	//代理服务器端口proxy_port  BeemApplication.PROXY_PORT_KEY
	//代理认证用户名proxy_username  BeemApplication.PROXY_USERNAME_KEY
	//代理认证密码proxy_password  BeemApplication.PROXY_PASSWORD_KEY
	//使用SSL/TLS连接settings_key_xmpp_tls_use
	//打开xmpp debug模式smack_debug   BeemApplication.SMACK_DEBUG_KEY
	//重连时间间隔（秒）settings_key_reco_delay 10
	//完整JID做为登录用户名full_jid_login  BeemApplication.FULL_JID_LOGIN_KEY
	
	//设置消息通知铃声
	public static void setNotificationSound(Context context, String flag){
		try
		{
			DataSaveManager.setPrefenceString(context, BeemApplication.NOTIFICATION_SOUND_KEY, flag);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	public static String getNotificationSound(Context context)
	{
		String notificationsound = null;
		try
		{
			notificationsound = DataSaveManager.getPreferenceString(context, BeemApplication.NOTIFICATION_SOUND_KEY);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		return notificationsound;
	}
	//消息振动提示
	public static void setNotificationVibrate(Context context,boolean flag){
		try
		{
			DataSaveManager.setPrefenceBoolean(context, BeemApplication.NOTIFICATION_VIBRATE_KEY, flag);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	public static boolean getNotificationVibrate(Context context)
	{
		boolean status = false;
		try
		{
			status = DataSaveManager.getPreferenceBoolean(context, BeemApplication.NOTIFICATION_VIBRATE_KEY);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return true;
		}
		return status;
	}
	//不显示离线用户
	public static void setShowOfflineContacts(Context context,boolean flag){
		try
		{
			DataSaveManager.setPrefenceBoolean(context, SpiceApplication.SHOW_OFFLINE_CONTACTS_KEY, flag);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	public static boolean getShowOfflineContacts(Context context)
	{
		boolean status = false;
		try
		{
			status = DataSaveManager.getPreferenceBoolean(context, SpiceApplication.SHOW_OFFLINE_CONTACTS_KEY);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return true;
		}
		return status;
	}
	//屏幕关闭自动修改状态
	public static void setUseAutoAway(Context context,boolean flag){
		try
		{
			DataSaveManager.setPrefenceBoolean(context, BeemApplication.USE_AUTO_AWAY_KEY, flag);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	public static boolean getUseAutoAway(Context context)
	{
		boolean status = false;
		try
		{
			status = DataSaveManager.getPreferenceBoolean(context, BeemApplication.USE_AUTO_AWAY_KEY);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return true;
		}
		return status;
	}
	//设置屏幕关闭状态签名信息
	public static void setAutoAwayMsg(Context context, String flag){
		try
		{
			DataSaveManager.setPrefenceString(context, SpiceApplication.AUTO_AWAY_MSG_KEY, flag);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	public static String getAutoAwayMsg(Context context)
	{
		String serverport = null;
		try
		{
			serverport = DataSaveManager.getPreferenceString(context, SpiceApplication.AUTO_AWAY_MSG_KEY);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		return serverport;
	}
	//设置连接服务器
	public static void setSpecificServer(Context context,boolean flag){
		try
		{
			DataSaveManager.setPrefenceBoolean(context, key_specific_server, flag);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	public static boolean getSpecificServer(Context context)
	{
		boolean status = false;
		try
		{
			status = DataSaveManager.getPreferenceBoolean(context, key_specific_server);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return true;
		}
		return status;
	}
	//服务器地址
	public static void setServerAddr(Context context, String flag){
		try
		{
			DataSaveManager.setPrefenceString(context, ll_serveraddr_setting, flag);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	public static String getServerAddr(Context context)
	{
		String serveraddr = null;
		try
		{
			serveraddr = DataSaveManager.getPreferenceString(context, ll_serveraddr_setting);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		return serveraddr;
	}
	//服务器端口
	public static void setServerPort(Context context, String flag){
		try
		{
			DataSaveManager.setPrefenceString(context, ll_serverport_setting, flag);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	public static String getServerPort(Context context)
	{
		String serverport = null;
		try
		{
			serverport = DataSaveManager.getPreferenceString(context, ll_serverport_setting);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		return serverport;
	}	
}
