package com.spice.im.preference;

import java.io.File;


import java.util.ArrayList;


import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.util.StringUtils;
//import org.jivesoftware.smackx.bookmark.BookmarkedConference;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



















import com.speed.im.login.EncryptionUtil;
import com.spice.im.BaseDialog;
import com.spice.im.FlippingLoadingDialog;
import com.spice.im.Login;
import com.spice.im.OtherProfileActivity;
import com.spice.im.R;
import com.spice.im.RoundImageView;
import com.spice.im.SpiceApplication;
import com.spice.im.friend.SearchIQ;
import com.spice.im.friend.SearchIQResponse;
import com.spice.im.friend.SearchIQResponseProvider;
import com.spice.im.ui.HandyTextView;
import com.spice.im.utils.HeaderLayout;
import com.spice.im.utils.HeaderLayout.HeaderStyle;
import com.spice.im.utils.HeaderLayout.onRightImageButtonClickListener;
import com.spice.im.utils.ImageFetcher;
import com.spice.im.utils.TextUtils;
import com.spiceim.db.TContactGroupAdapter;
import com.stb.core.chat.ContactGroup;
import com.stb.core.uuid.DeviceUuidFactory2;
////import com.allen.ui.profile.ManagermentActivity;
//import com.allen.ui.profile.ProfileActivity;
//import com.allen.updateapk.DownloadApkThread;
//import com.allen.updateapk.HttpConnect;
//import com.allen.updateapk.VersionService;
//
//
//import com.stb.isharemessage.R;
import com.stb.isharemessage.service.aidl.IXmppFacade;
import com.dodola.model.DuitangInfo;
import com.dodola.model.DuitangInfoAdapter;
import com.example.android.bitmapfun.util.AsyncTask;
//import com.example.android.bitmapfun.util.ImageFetcher;
import com.example.android.bitmapfun.util.ObjectCache;
import com.stb.isharemessage.BeemApplication;
import com.stb.isharemessage.BeemService;
//import com.stb.isharemessage.service.Contact;
import com.stb.isharemessage.service.XmppConnectionAdapter;
//import com.stb.isharemessage.ui.register.BaseActivity;
//import com.stb.isharemessage.ui.register.BaseDialog;
//import com.stb.isharemessage.ui.register.FlippingLoadingDialog;
//import com.stb.isharemessage.ui.wizard.AccConfActivity;
import com.stb.isharemessage.utils.BeemBroadcastReceiver;
import com.stb.isharemessage.utils.BeemConnectivity;
//import com.stb.isharemessage.utils.HeaderLayout;
//import com.stb.isharemessage.utils.TextUtils;
//import com.stb.isharemessage.utils.HeaderLayout.HeaderStyle;
//import com.stb.isharemessage.utils.HeaderLayout.onRightImageButtonClickListener;



















import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class NoLoginPreferenceActivity extends Activity implements OnClickListener,OnCheckedChangeListener{

//    private static final Intent SERVICE_INTENT = new Intent();
//
//    static {
//    	SERVICE_INTENT.setComponent(new ComponentName("com.spice.im", "com.stb.isharemessage.BeemService"));//自身应用pkg名称，非service所在pkg名称
//    }
//    private boolean mBinded = false;
//    private IXmppFacade mXmppFacade;
//    
    private Context mContext;
//    
//    private final ServiceConnection mServConn = new BeemServiceConnection();
//    //  private final OnClickListener mOnClickOk = new MyOnClickListener();
//    private final BeemBroadcastReceiver mReceiver = new BeemBroadcastReceiver();
//    private ContactGroup contact = null;
//    public ImageFetcher mImageFetcher;
////    private List<ContactGroup> mListContact;
//    private HashMap linkedList = new HashMap();
//    private RoundImageView imageView;
//    private String imgurl = "";
//    private HandyTextView nickNameTextView;
//    private String nickname;
//    
//    private LinearLayout user_item_layout_gender;
//    private ImageView user_item_iv_gender;//性别
//    private String sex;
//    private HandyTextView ageTextView;
//    private String age;
//    
//    private HandyTextView accountTextView;
//    private String account; 
//    private String JIDWithRes;
//   
//    private HandyTextView signTextView;
//    private String sign; 
    
//    protected TextView more_profile;
//    private RelativeLayout more_profile2;
    
    private HeaderLayout mHeaderLayout;
    
    protected FlippingLoadingDialog mLoadingDialog;
    
    
    ToggleButton tb_offlinePin;//消息振动提示
    ImageButton ib_offlinePin;
    
    RelativeLayout rl_changeOfflinePin;//设置消息通知铃声
    RelativeLayout rl_verify_version;//检测新版本
    RelativeLayout rl_about_us;//关于我们
    
//    ToggleButton tb_p2p;//不显示离线用户
//    ImageButton ib_p2p;
//    ToggleButton tb_screenoff;//屏幕关闭修改状态
//    ImageButton ib_screenoff;
//    
//    
//    EditText et_screenoff_show_setting;//屏幕关闭状态签名
////    EditText et_serveraddr;//服务器地址
////    EditText et_serverport;//服务器端口
    
    private Button mCancelBt;
    
    private String mLogin = "";//just for ObjectCache prefix
    private SharedPreferences mSettings;
//    private Context mContext;
    
    
//    private DuitangInfoAdapter duitangInfoAdapter = null;//用户信息存入本地数据库
    
    
	private String apkvercheck = "";
	public String downloadPrefix;
	
	private JSONArray array = null;
	private JSONObject object = null;
    private String apkversion = "";
    private String apkname = "";
    private String verdesc = "";
    private String mustupdateornot = "0";//0 false,1 true
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
    	mSettings = PreferenceManager.getDefaultSharedPreferences(this);
    	String tmpJid = mSettings.getString(BeemApplication.ACCOUNT_USERNAME_KEY, "").trim();
//    	mLogin = StringUtils.parseName(tmpJid);
    	mLogin = tmpJid;
    	
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_nologinpreference);
        
        Properties props = new Properties();
        try {
            int id = this.getResources().getIdentifier("androidpn", "raw",
            		this.getPackageName());
            props.load(this.getResources().openRawResource(id));
        } catch (Exception e) {
//            Log.e(LOGTAG, "Could not find the properties file.", e);
            // e.printStackTrace();
        }
        apkvercheck = props.getProperty("apkvercheck", "");
        downloadPrefix = props.getProperty("downloadPrefix", "");
        
		mHeaderLayout = (HeaderLayout) findViewById(R.id.login_header);
		mHeaderLayout.init(HeaderStyle.TITLE_RIGHT_IMAGEBUTTON);
//		mHeaderLayout.setDefaultTitle("应用设置", null);
		
		mHeaderLayout.setTitleRightImageButton("应用设置", null,
				R.drawable.return2,
				new OnRightImageButtonClickListener());
		
		mLoadingDialog = new FlippingLoadingDialog(this, "请求提交中");
		
//		imageView = (RoundImageView) findViewById(R.id.headlogo); 
//        nickNameTextView = (HandyTextView)findViewById(R.id.user_item_htv_name);  
//
//        user_item_layout_gender = (LinearLayout)findViewById(R.id.user_item_layout_gender);
//        user_item_iv_gender = (ImageView)findViewById(R.id.user_item_iv_gender);
//        ageTextView = (HandyTextView)findViewById(R.id.user_item_htv_age); 
//        
//        accountTextView = (HandyTextView)findViewById(R.id.user_item_htv_account);
//
//        signTextView = (HandyTextView)findViewById(R.id.user_item_htv_sign);

        
//        more_profile = (TextView) findViewById(R.id.more_profile);
////        if(more_profile!=null)
//        	more_profile.setOnClickListener(this);
//        
//        more_profile2 = (RelativeLayout) findViewById(R.id.more_profile2);
////        if(more_profile2!=null)
//        	more_profile2.setOnClickListener(this);
		
        tb_offlinePin = (ToggleButton) findViewById(R.id.tb_offlinePin);
        tb_offlinePin.setOnCheckedChangeListener(this);
        ib_offlinePin = (ImageButton) findViewById(R.id.ib_offlinePin);
        
        rl_changeOfflinePin = (RelativeLayout) findViewById(R.id.rl_change_offline_pin);
        rl_changeOfflinePin.setOnClickListener(this);
        
        rl_verify_version = (RelativeLayout) findViewById(R.id.rl_verify_version);
        rl_verify_version.setOnClickListener(this);
        
        rl_about_us = (RelativeLayout) findViewById(R.id.rl_about_us);
        rl_about_us.setOnClickListener(this);

//        tb_p2p = (ToggleButton) findViewById(R.id.tb_p2p);
//        tb_p2p.setOnCheckedChangeListener(this);
//        ib_p2p = (ImageButton) findViewById(R.id.ib_p2p);
//        
//        tb_screenoff = (ToggleButton) findViewById(R.id.tb_screenoff);
//        tb_screenoff.setOnCheckedChangeListener(this);
//        ib_screenoff = (ImageButton) findViewById(R.id.ib_screenoff);
        
        
        // 如果消息振动提示是关闭状态，则禁用 rl_changeOfflinePin
        if (TerminalConfigMgr.getNotificationVibrate(this)) {
            // 脱机PIN
            tb_offlinePin.setChecked(true);
//            rl_changeOfflinePin.setVisibility(View.VISIBLE);
//            bFirstChangeFlag = false;
        } else {
            // 联机PIN
            tb_offlinePin.setChecked(false);
//            rl_changeOfflinePin.setVisibility(View.GONE);
//            bFirstChangeFlag = false;
        }
//        if (TerminalConfigMgr.getShowOfflineContacts(this)) {
//        	tb_p2p.setChecked(true);
//        }else{
//        	tb_p2p.setChecked(false);
//        }
//        if (TerminalConfigMgr.getUseAutoAway(this)) {
//        	tb_screenoff.setChecked(true);
//        }else{
//        	tb_screenoff.setChecked(false);
//        }
//        
//        et_screenoff_show_setting = (EditText) findViewById(R.id.et_screenoff_show_setting);
//        et_screenoff_show_setting.clearFocus();
//        et_screenoff_show_setting.setText(String.valueOf(TerminalConfigMgr.getAutoAwayMsg(this)));
        
//        et_serveraddr = (EditText) findViewById(R.id.et_serveraddr);
//        et_serveraddr.clearFocus();
//        et_serveraddr.setText(String.valueOf(TerminalConfigMgr.getServerAddr(this)));
//        
//        et_serverport = (EditText) findViewById(R.id.et_serverport);
//        et_serverport.clearFocus();
//        et_serverport.setText(String.valueOf(TerminalConfigMgr.getServerPort(this)));
        
        mCancelBt = (Button) findViewById(R.id.loginanim_cancel_button);//安全退出
        mCancelBt.setOnClickListener(this);
		
//	    mImageFetcher = new ImageFetcher(this, 240);
//	    mImageFetcher.setUListype(0);
//	    mImageFetcher.setLoadingImage(R.drawable.empty_photo);
//	    mImageFetcher.setExitTasksEarly(false);
//	    this.registerReceiver(mReceiver, new IntentFilter(BeemBroadcastReceiver.BEEM_CONNECTION_CLOSED));
	    

//	    duitangInfoAdapter = DuitangInfoAdapter.getInstance(this);
		BeemApplication.getInstance().addActivity(this);
    }
    private Intent intent;
//    private BaseDialog mBaseDialog;
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.rl_verify_version://检测新版本
//			yesOrNoUpdataApk();
			break;
		case R.id.rl_about_us://关于我们
//			intent = new Intent(this, AboutUsActivity.class);//AdvancedSearch ProfileActivity
////        	intent = new Intent(this, AddDynamicsActivity.class);
//        	intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(intent);
//		    finish();
			break;
//		case R.id.rl_change_offline_pin:
//			if(ObjectCache.getStatus("isConnected", getApplicationContext())
//					&& BeemConnectivity.isConnected(getApplicationContext())){
////	        	intent = new Intent(this, ModifyPasswordActivity.class);//AdvancedSearch ProfileActivity
//////	        	intent = new Intent(this, AddDynamicsActivity.class);
////	        	intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////	            startActivity(intent);
////			    finish();
//			}
//			break;
//		case R.id.more_profile:
//////			if(this.JIDWithRes!=null){
////			if(ObjectCache.getStatus("isConnected", getApplicationContext())
////					&& BeemConnectivity.isConnected(getApplicationContext())){
////				try {
////			    	if(mXmppFacade!=null){
////				    	if (mXmppFacade.getXmppConnectionAdapter()!=null 
////								&& mXmppFacade.getXmppConnectionAdapter().getAdaptee()!=null
////								&& mXmppFacade.getXmppConnectionAdapter().getAdaptee().isConnected() 
////				    			&& mXmppFacade.getXmppConnectionAdapter().getAdaptee().isAuthenticated()
////				    			&& mXmppFacade.getXmppConnectionAdapter().getAdaptee().isAlive()
//////								&& mXmppFacade.getXmppConnectionAdapter().login()//注意，第三方登陆login之前，需要调用框架本身的auth接口
////								&& mXmppFacade.getXmppConnectionAdapter().getJID()!=null
////								&& mXmppFacade.getXmppConnectionAdapter().getJID().length()!=0
////								&& !mXmppFacade.getXmppConnectionAdapter().getJID().equalsIgnoreCase("null")
////				    			){
////	        	intent = new Intent(this, OtherProfileActivity.class);//ManagermentActivity.class
////	        	SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
////	    	    String login = settings.getString(BeemApplication.ACCOUNT_USERNAME_KEY, "");
////	        	intent.putExtra("searchkey", login);
//////	        	intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////	            startActivity(intent);
////	            finish();
////				    	}
////			    	}
////				}catch(Exception e){
////					
////				}
////			}
//////			}
//			break;			
//		case R.id.more_profile2:
//////			if(this.JIDWithRes!=null){
////			if(ObjectCache.getStatus("isConnected", getApplicationContext())
////					&& BeemConnectivity.isConnected(getApplicationContext())){
////	        	intent = new Intent(this, OtherProfileActivity.class);
////	        	intent.putExtra("JIDWithRes", this.JIDWithRes);
////	        	intent.putExtra("roomname", "");
////	        	intent.putExtra("nickname", nickname);
////	        	intent.putExtra("name", nickname);
//////	        	intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////	            startActivity(intent);
////	            finish();
////			}
//////			}
//			break;
		case R.id.loginanim_cancel_button:
			mBaseDialog = BaseDialog.getDialog(NoLoginPreferenceActivity.this, "安全退出", "您确定要退出应用吗?",
					"确定", new DialogInterface.OnClickListener() {
		
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							
							XmppConnectionAdapter.isPause = true;
							XmppConnectionAdapter.isReconnectFlag = false;
							if(XmppConnectionAdapter.instance!=null)
								XmppConnectionAdapter.instance.resetApplication();
							XmppConnectionAdapter.instance = null;
//							stopService(SERVICE_INTENT);
//							finishNew();
							finish();
							SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(NoLoginPreferenceActivity.this);
						    String login = settings.getString(BeemApplication.ACCOUNT_USERNAME_KEY, "");
						    
							(new DeviceUuidFactory2(mContext,login)).remove(mContext);//用户手动退出登录时，删除uuid记录，防止统一部手机上，使用不同账号登录时uuid重复问题
							SpiceApplication.getInstance().exit();
							BeemApplication.getInstance().exit();
					        // 杀死该应用进程
					        android.os.Process.killProcess(android.os.Process.myPid());
					        System.exit(0);
						}
			},"取消", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();;
				}
			});
			mBaseDialog.show();
			
			break;
		default:
			break;
		}
	}
//	Handler mHandler = new Handler() {
//
//		@Override
//		public void handleMessage(android.os.Message msg) {
//			super.handleMessage(msg);
//			switch (msg.what) {
//			case 0:
//				if(errorType==5){
//				if(imageView != null && imgurl!=null)
//					mImageFetcher.loadRoundImage(imgurl, imageView,null);
//		        if(nickNameTextView != null && nickname !=null)
//		        	nickNameTextView.setText(nickname);
////		        if(ageTextView != null && age !=null)
////		        	ageTextView.setText(age);
//	            if(user_item_iv_gender!=null && sex!=null
//	            		&& !sex.equalsIgnoreCase("null")
//	            		&& sex.length()!=0){
//	            	if(sex.equals("1")){//1男，2女
//	            		user_item_layout_gender.setBackgroundResource(R.drawable.bg_gender_male);
//	            		user_item_iv_gender.setImageResource(R.drawable.ic_user_male);
//	            	}else{
//	            		user_item_layout_gender.setBackgroundResource(R.drawable.bg_gender_famal);
//	            		user_item_iv_gender.setImageResource(R.drawable.ic_user_famale);
//	            	}
//	            }
//	            int age1 = 0;
//	            if(ageTextView != null && age !=null
//	            		&& !age.equalsIgnoreCase("null")
//	            		&& age.length()!=0){
//	            	String[] yyyymmdd = age.split("-");
//	            	try{
//	            		age1 = TextUtils.getAge(Integer.parseInt(yyyymmdd[0]),
//	            				Integer.parseInt(yyyymmdd[1]),
//	            				Integer.parseInt(yyyymmdd[2]));
//	            	}catch(Exception e){
//	            		age1 = 0;
//	            	}
//	            	ageTextView.setText(age1+"");
//	            }
//		        if(accountTextView != null && account !=null)
//		        	accountTextView.setText(account);
//		        if(signTextView != null && sign !=null)
//		        	signTextView.setText(sign);
//				}else{
////					Toast.makeText(GpsSearchResPullRefListActivity.this,errorMsg[errorType], 1000).show();
//					showCustomToast(errorMsg[errorType]);
//				}
//				break;
//			case 1:
////				doNewVersionUpdate(nowVersion, newVersion,st);
//				break;
//			default:
//				
//				break;
//			}
//		}
//
//	};
//	private void getProfile() {
//
//			putAsyncTask(new com.beem.push.utils.AsyncTask<Void, Void, Boolean>() {
//
//				@Override
//				protected void onPreExecute() {
//					super.onPreExecute();
//				    /**加载前先显示缓存数据start*/
//					Log.e("MMMMMMMMMMMM####20140810登录状态####MMMMMMMMMMMM", "++++++++++++++登录状态="+ObjectCache.getStatus("isConnected", getApplicationContext())+"++++++++++++");
//				    if(ObjectCache.getStatus("isConnected", getApplicationContext())){
//						errorType = 5;
//						preLoad();
////						mHandler.sendEmptyMessage(0);
//						bulid();
//				    }
//				    /**加载前先显示缓存数据end*/
//					showLoadingDialog("正在加载,请稍后...");
//				}
//
//				@Override
//				protected Boolean doInBackground(Void... params) {
//					initProfile();
//					return true;
//				}
//
//				@Override
//				protected void onPostExecute(Boolean result) {
//					super.onPostExecute(result);
//					dismissLoadingDialog();
//					if (!result) {
//						showCustomToast("数据加载失败...");
//					} else {
//						mHandler.sendEmptyMessage(0);
//					}
//				}
//
//			});
//	}
//    public void initProfile(){
//    	if(ObjectCache.getStatus("isConnected", getApplicationContext())){//已登录
//	    	if(BeemConnectivity.isConnected(getApplicationContext())){
//			    try{
//			    	if(mXmppFacade!=null){
//					    if(mXmppFacade.getXmppConnectionAdapter()!=null 
//								&& mXmppFacade.getXmppConnectionAdapter().getAdaptee()!=null//&& !mXmppFacade.getXmppConnectionAdapter().getAdaptee().isAuthenticated()
//								&& mXmppFacade.getXmppConnectionAdapter().getAdaptee().isConnected()
//								&& mXmppFacade.getXmppConnectionAdapter().getAdaptee().isAuthenticated()
//								&& mXmppFacade.getXmppConnectionAdapter().getAdaptee().isAlive()){
//				    		SearchIQ reqXML = new SearchIQ();
//				            reqXML.setId("1234567890");
//				            reqXML.setApikey("abcdefghijklmnopqrstuvwxyz");
//				            reqXML.setUid(StringUtils.parseBareAddress2(mXmppFacade.getXmppConnectionAdapter().getJID()));//2
//				            reqXML.setSearchkey(mXmppFacade.getXmppConnectionAdapter().getName());//searchkey
//				            String uuid = mXmppFacade.getXmppConnectionAdapter().getAdaptee().getUUID(this);
//				            reqXML.setUuid(uuid);
////				            String hashcode = "";//apikey+view+orderby+uuid 使用登录成功后返回的sessionid作为密码做3des运算
//				            String hash_dest_src = "abcdefghijklmnopqrstuvwxyz"+ StringUtils.parseBareAddress2(mXmppFacade.getXmppConnectionAdapter().getJID())+uuid;
//				            String hash_dest = EncryptionUtil.getHash2(hash_dest_src, "SHA");
//				            reqXML.setHashcode(hash_dest);
//				            reqXML.setType(IQ.Type.SET);
//				            String elementName = "searchiq"; 
//				    		String namespace = "com:isharemessage:searchiq";
//				    		SearchIQResponseProvider provider = new SearchIQResponseProvider();
//				            Packet rt =  mXmppFacade.getXmppConnectionAdapter().excute(reqXML, "searchiq", "com:isharemessage:searchiq", provider);
//				            
//				            if(rt!=null){
//				                if (rt instanceof SearchIQResponse) {
//				                	final SearchIQResponse searchIQResponse = (SearchIQResponse) rt;
//
//				                    if (searchIQResponse.getChildElementXML().contains(
//				                            "com:isharemessage:searchiq")) {
////				    					MainActivity.this.runOnUiThread(new Runnable() {
////					                    	
////	            							@Override
////	            							public void run() {
////	            								showCustomToast("服务器应答消息："+contactGroupListIQResponse.toXML().toString());
////	            							}
////	            						});
//				                        String Id = searchIQResponse.getId();
//				                        String Apikey = searchIQResponse.getApikey();
//				                        HashMap cGroups = searchIQResponse.getCGroups();
//				                        if(cGroups.size()!=0){
////					                        linkedList = new LinkedList(cGroups);
//				                        	linkedList = cGroups;
////				                        	for(int q=0;q<cGroups.size();q++){
////				                        		linkedList.add(cGroups.get(q));
////				                        	}
//					                        Log.e("响应packet结果解析...............", "Id="+Id+";Apikey="+Apikey); 
//		//			                        mListContact = new ArrayList<Contact>(); 
//		//			                        for(int k=0;k<cGroups.size();k++){
//		//			                        	Log.e("响应packet结果解析...............", "cGroups"); 
//		////			                        	mListContact.add(contact);
//		//			                        }
//					                        cGroups = null;
////					                        return true;
//				                        }
//				                    }
//				                } 
//				            }
//				    		
//				    		
//				    		Log.e("================2015 TestActivity.java ================", "++++++++++++++initContactList()444++++++++++++++");
//
//				    	    if(linkedList!=null && linkedList.size()!=0)
//				    	    	errorType = 5;
//				    	    else{
//				    	    	if(linkedList!=null && linkedList.size()==0)
//				    	    		errorType = 0;
//				    	    	else
//				    	    		errorType = 6;
//				    	    }
//							mBinded = true;//20130804 added by allen
//			
//					    }else
//				    		errorType = 1;
//			    	}else
//			    		errorType = 2;
//		        } catch (RemoteException e) {
//		        	errorType = 3;
//			    	e.printStackTrace();
//			    } catch (Exception e){
//			    	errorType = 3;
//			    	e.printStackTrace();
//			    }
//	    	}else{
//		    	errorType = 4;
//		    }
//    	}else{
//    		if(BeemConnectivity.isConnected(getApplicationContext()))
//    			errorType = 6;
//    		else
//    			errorType = 4;
//    	}
//    }
//    /**
//     * connection to service.
//     * @author nikita
//     */
//    private class BeemServiceConnection implements ServiceConnection {
//
//		/**
//		 * constructor.
//		 */
//		public BeemServiceConnection() {
//		}
//	
//		/**
//		 * {@inheritDoc}
//		 */
//		@Override
//		public void onServiceConnected(ComponentName name, IBinder service) {
//		    mXmppFacade = IXmppFacade.Stub.asInterface(service);
//		    if(mXmppFacade!=null)
//		    	mBinded = true;//20130804 added by allen
//		    getProfile();
//		}
//	
//		/**
//		 * {@inheritDoc}
//		 */
//		@Override
//		public void onServiceDisconnected(ComponentName name) {
//		    mXmppFacade = null;
//		    mBinded = false;//20130804 added by allen
//		}
//    }
    private static ExecutorService LIMITED_TASK_EXECUTOR;  
    static {  
        LIMITED_TASK_EXECUTOR = (ExecutorService) Executors.newFixedThreadPool(7);    
    }; 
    public List<com.beem.push.utils.AsyncTask<Void, Void, Boolean>> mAsyncTasks = new ArrayList<com.beem.push.utils.AsyncTask<Void, Void, Boolean>>();
    public void putAsyncTask(com.beem.push.utils.AsyncTask<Void, Void, Boolean> asyncTask) {
		mAsyncTasks.add(asyncTask.executeOnExecutor(LIMITED_TASK_EXECUTOR, null));
//		mAsyncTasks.add(asyncTask.execute());
	}

    public void clearAsyncTask() {
		Iterator<com.beem.push.utils.AsyncTask<Void, Void, Boolean>> iterator = mAsyncTasks
				.iterator();
		while (iterator.hasNext()) {
			com.beem.push.utils.AsyncTask<Void, Void, Boolean> asyncTask = iterator.next();
			if (asyncTask != null && !asyncTask.isCancelled()) {
				asyncTask.cancel(true);
			}
		}
		mAsyncTasks.clear();
	}
	

	protected void showLoadingDialog(String text) {
		if (text != null) {
			mLoadingDialog.setText(text);
		}
		mLoadingDialog.show();
	}

	protected void dismissLoadingDialog() {
		if (mLoadingDialog.isShowing()) {
			mLoadingDialog.dismiss();
		}
	}

	/** 显示自定义Toast提示(来自String) **/
	protected void showCustomToast(String text) {
		View toastRoot = LayoutInflater.from(NoLoginPreferenceActivity.this).inflate(
				R.layout.common_toast, null);
		((HandyTextView) toastRoot.findViewById(R.id.toast_text)).setText(text);
		Toast toast = new Toast(NoLoginPreferenceActivity.this);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.setDuration(Toast.LENGTH_SHORT);
		toast.setView(toastRoot);
		toast.show();
	}
    /**
     * {@inheritDoc}
     */
    @Override
    protected void onResume() {
		super.onResume();
//		mImageFetcher.setExitTasksEarly(false);
//		if (!mBinded){
//			Intent intent = new Intent(this, BeemService.class);
////			intent.putExtra("uFlag_startfromsetting", "1");
//			mBinded = bindService(intent, mServConn, BIND_AUTO_CREATE);
//		}
    }
    @Override
    protected void onPause() {
		super.onPause();
//		mImageFetcher.setExitTasksEarly(true);
//		if (mBinded) {
//			unbindService(mServConn);
//			mBinded = false;
//		}
    }

    /* (non-Javadoc)
     * @see android.app.Activity#onDestroy()
     */
    @Override
    protected void onDestroy() {
		super.onDestroy();
//		mImageFetcher.onCancel_RoundImage();
//		if (mBinded) {
//			unbindService(mServConn);
//			mBinded = false;
//		}
//		this.unregisterReceiver(mReceiver);
		if(mBaseDialog2!=null && mBaseDialog2.isShowing())
			mBaseDialog2.cancel();
		if(mBaseDialog!=null && mBaseDialog.isShowing())
			mBaseDialog.cancel();
		clearAsyncTask();
//		SpiceApplication.getInstance().exit();
//        // 保存配置信息
//        try {
////            EditText et_screenoff_show_setting;//屏幕关闭状态签名
////            EditText et_serveraddr;//服务器地址
////            EditText et_serverport;//服务器端口
////            threshold = Integer.valueOf(et_upload.getText().toString());
//            TerminalConfigMgr.setAutoAwayMsg(this, et_screenoff_show_setting.getText().toString());
////            TerminalConfigMgr.setServerAddr(this, et_serveraddr.getText().toString());
////            TerminalConfigMgr.setServerPort(this, et_serverport.getText().toString());
//        } catch (Exception e) {
//            // TODO: handle exception
//        }
    }
    
    private RelativeLayout.LayoutParams params;
    private TranslateAnimation animation;
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//        if (buttonView.getId() == R.id.tb_p2p) {//不显示离线用户
//            params = (RelativeLayout.LayoutParams) ib_p2p
//                    .getLayoutParams();
//            if (isChecked) {
//                // 调整位置
//                // off -> on
//                params.addRule(RelativeLayout.ALIGN_RIGHT, -1);
//                params.addRule(RelativeLayout.ALIGN_LEFT, R.id.tb_p2p);
//
//                ib_p2p.setLayoutParams(params);
//                ib_p2p.setImageResource(R.drawable.progress_thumb_selector);
//                tb_p2p.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
//                // 播放动画
//                animation = new TranslateAnimation(dip2px(
//                        this, 40), 0, 0, 0);
//                animation.setDuration(200);
//                ib_p2p.startAnimation(animation);
////                TerminalConfigMgr.isWifiP2P = true;
////                TerminalConfigMgr.setP2PFlag(this, TerminalConfigMgr.isWifiP2P);
//                TerminalConfigMgr.setShowOfflineContacts(this,true);
//
//            } else {
//                // 调整位置
//                // on -> off
//                params.addRule(RelativeLayout.ALIGN_RIGHT, R.id.tb_p2p);
//                params.addRule(RelativeLayout.ALIGN_LEFT, -1);
//                ib_p2p.setLayoutParams(params);
//                ib_p2p.setImageResource(R.drawable.progress_thumb_off_selector);
//                tb_p2p.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
//                // 播放动画
//                animation = new TranslateAnimation(dip2px(
//                        this, -40), 0, 0, 0);
//                animation.setDuration(200);
//                ib_p2p.startAnimation(animation);
//
////                TerminalConfigMgr.isWifiP2P = false;
////                TerminalConfigMgr.setP2PFlag(this, TerminalConfigMgr.isWifiP2P);
//                TerminalConfigMgr.setShowOfflineContacts(this,false);
//            }
//        } else 
        	
        	if (buttonView.getId() == R.id.tb_offlinePin) {//消息振动提示

            params = (RelativeLayout.LayoutParams) ib_offlinePin
                    .getLayoutParams();
            if (isChecked) {
                // 调整位置
                // off -> on
                params.addRule(RelativeLayout.ALIGN_RIGHT, -1);
                params.addRule(RelativeLayout.ALIGN_LEFT, R.id.tb_offlinePin);

                ib_offlinePin.setLayoutParams(params);
                ib_offlinePin
                        .setImageResource(R.drawable.progress_thumb_selector);
                tb_offlinePin.setGravity(Gravity.RIGHT
                        | Gravity.CENTER_VERTICAL);
                // 播放动画
                animation = new TranslateAnimation(dip2px(
                        this, 40), 0, 0, 0);
                animation.setDuration(200);
                ib_offlinePin.startAnimation(animation);

//                rl_changeOfflinePin.setVisibility(View.VISIBLE);

//                if (bFirstChangeFlag == false) {
////                    Bundle bundle = new Bundle();
////                    Intent intent;
////
////                    intent = new Intent(this, LoginActivity.class);
////                    bundle.putString("Function", "setActivity");
////
////                    intent.putExtras(bundle);
////                    startActivityWithAnim(intent);
////                    finish();
//                }
                TerminalConfigMgr.setNotificationVibrate(this, true);
            } else {
                // 调整位置
                // on -> off
                params.addRule(RelativeLayout.ALIGN_RIGHT, R.id.tb_offlinePin);
                params.addRule(RelativeLayout.ALIGN_LEFT, -1);
                ib_offlinePin.setLayoutParams(params);
                ib_offlinePin
                        .setImageResource(R.drawable.progress_thumb_off_selector);
                tb_offlinePin
                        .setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
                // 播放动画
                animation = new TranslateAnimation(dip2px(
                        this, -40), 0, 0, 0);
                animation.setDuration(200);
                ib_offlinePin.startAnimation(animation);

//                rl_changeOfflinePin.setVisibility(View.GONE);
//                TerminalConfigMgr.setOnlineLoginVerfyFlag(this, "1");
                TerminalConfigMgr.setNotificationVibrate(this, false);
            }
        }
//        else if (buttonView.getId() == R.id.tb_screenoff) {//屏幕关闭修改状态
//
//            params = (RelativeLayout.LayoutParams) ib_screenoff
//                    .getLayoutParams();
//            if (isChecked) {
//                // 调整位置
//                // off -> on
//                params.addRule(RelativeLayout.ALIGN_RIGHT, -1);
//                params.addRule(RelativeLayout.ALIGN_LEFT, R.id.tb_screenoff);
//
//                ib_screenoff.setLayoutParams(params);
//                ib_screenoff
//                        .setImageResource(R.drawable.progress_thumb_selector);
//                tb_screenoff.setGravity(Gravity.RIGHT
//                        | Gravity.CENTER_VERTICAL);
//                // 播放动画
//                animation = new TranslateAnimation(dip2px(
//                        this, 40), 0, 0, 0);
//                animation.setDuration(200);
//                ib_screenoff.startAnimation(animation);
//                TerminalConfigMgr.setUseAutoAway(this,true);
//
////                rl_changeOfflinePin.setVisibility(View.VISIBLE);
//
////                if (bFirstChangeFlag == false) {
//////                    Bundle bundle = new Bundle();
//////                    Intent intent;
//////
//////                    intent = new Intent(this, LoginActivity.class);
//////                    bundle.putString("Function", "setActivity");
//////
//////                    intent.putExtras(bundle);
//////                    startActivityWithAnim(intent);
//////                    finish();
////                }
//            } else {
//                // 调整位置
//                // on -> off
//                params.addRule(RelativeLayout.ALIGN_RIGHT, R.id.tb_screenoff);
//                params.addRule(RelativeLayout.ALIGN_LEFT, -1);
//                ib_screenoff.setLayoutParams(params);
//                ib_screenoff
//                        .setImageResource(R.drawable.progress_thumb_off_selector);
//                tb_screenoff
//                        .setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
//                // 播放动画
//                animation = new TranslateAnimation(dip2px(
//                        this, -40), 0, 0, 0);
//                animation.setDuration(200);
//                ib_screenoff.startAnimation(animation);
//                TerminalConfigMgr.setUseAutoAway(this,false);
//
////                rl_changeOfflinePin.setVisibility(View.GONE);
////                TerminalConfigMgr.setOnlineLoginVerfyFlag(this, "1");
//            }
//        }
    }
    
    public int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    public int px2dip(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }
	private class OnRightImageButtonClickListener implements
	onRightImageButtonClickListener {
	
		@Override
		public void onClick() {
			startActivity(new Intent(NoLoginPreferenceActivity.this, Login.class));
			NoLoginPreferenceActivity.this.finish();
		}
	}
	
	private String[] errorMsg = new String[]{"抱歉,没有找到相关结果.",
			"服务连接中1-1,请稍候再试.",
			"服务连接中1-2,请稍候再试.",
			"服务连接中1-3,请稍候再试.",
			"网络连接不可用,请检查你的网络设置.",
			"已登录",
			"未登录",
			"请求异常,稍候重试!"};
	private int errorType = 5;
	
	private TContactGroupAdapter tContactGroupAdapter = null;
	public void preLoad(){//联网前先显示缓存内容
//		Object obj2 = ObjectCache.get("mContact_"+mLogin+"_Preference",this);
//		Log.e("MMMMMMMMMMMM####20140810缓存不为空####MMMMMMMMMMMM", "++++++++++++++缓存不为空="+(obj2!=null));
//		if(obj2!=null){
//			contact = (Contact)obj2;
//        	nickname = contact.getName();
//        	sex = contact.getGender();
//        	age = contact.getBirthday();
//        	account = StringUtils.parseName(contact.getJIDWithRes());
//        	JIDWithRes = contact.getJIDWithRes();
//        	sign = contact.getSign();
//        	
//            if(contact.getAvatarId()!=null 
//            		&& contact.getAvatarId().length()!=0 
//            		&& !contact.getAvatarId().equalsIgnoreCase("null")
//            		&& (
//            				contact.getAvatarId().toLowerCase().endsWith(".jpg")
//            				|| contact.getAvatarId().toLowerCase().endsWith(".png")
//            				|| contact.getAvatarId().toLowerCase().endsWith(".bmp")
//            				|| contact.getAvatarId().toLowerCase().endsWith(".gif")
//            			))
//            	imgurl = XmppConnectionAdapter.downloadPrefix+StringUtils.parseName(contact.getJIDWithRes())+"/"+contact.getAvatarId();//+"thumbnail_"
//		}
//		
//		tContactGroupAdapter = TContactGroupAdapter.getInstance(this);
//		if(tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(mMsgText.getBareJid()))!=null){
//			
//		}
	}
//	public void bulid(){
//		if(errorType==5){
//			if(imageView != null && imgurl!=null)
//				mImageFetcher.loadRoundImage(imgurl, imageView,null);
//	        if(nickNameTextView != null && nickname !=null)
//	        	nickNameTextView.setText(nickname);
////	        if(ageTextView != null && age !=null)
////	        	ageTextView.setText(age);
//            if(user_item_iv_gender!=null && sex!=null
//            		&& !sex.equalsIgnoreCase("null")
//            		&& sex.length()!=0){
//            	if(sex.equals("1")){//1男，2女
//            		user_item_layout_gender.setBackgroundResource(R.drawable.bg_gender_male);
//            		user_item_iv_gender.setImageResource(R.drawable.ic_user_male);
//            	}else{
//            		user_item_layout_gender.setBackgroundResource(R.drawable.bg_gender_famal);
//            		user_item_iv_gender.setImageResource(R.drawable.ic_user_famale);
//            	}
//            }
//            int age1 = 0;
//            if(ageTextView != null && age !=null
//            		&& !age.equalsIgnoreCase("null")
//            		&& age.length()!=0){
//            	String[] yyyymmdd = age.split("-");
//            	try{
//            		age1 = TextUtils.getAge(Integer.parseInt(yyyymmdd[0]),
//            				Integer.parseInt(yyyymmdd[1]),
//            				Integer.parseInt(yyyymmdd[2]));
//            	}catch(Exception e){
//            		age1 = 0;
//            	}
//            	ageTextView.setText(age1+"");
//            }
//	        if(accountTextView != null && account !=null)
//	        	accountTextView.setText(account);
//	        if(signTextView != null && sign !=null)
//	        	signTextView.setText(sign);
//			}else{
////				Toast.makeText(GpsSearchResPullRefListActivity.this,errorMsg[errorType], 1000).show();
//				showCustomToast(errorMsg[errorType]);
//			}
//	}
	

	private BaseDialog mBaseDialog2;
	private BaseDialog mBaseDialog;
	
//	private String packageName;
//	private String nowVersion = "";
//	private String newVersion = "";
////	private ProgressDialog pBar;
//	/**
//	 * Role:是否更新版本<BR>
//	 * Date:2012-4-5<BR>
//	 * 
//	 * @author ZHENSHI)peijiangping
//	 */
//	private void yesOrNoUpdataApk() {
////		packageName = this.getPackageName();
////		String nowVersion = getVerCode(AccConfActivity.this);
////		String newVersion = goToCheckNewVersion();
////		if (newVersion.equals("Error")) {
////			return 0;
////		}
////		if (Double.valueOf(newVersion) > Double.valueOf(nowVersion)) {
////			doNewVersionUpdate(nowVersion, newVersion);
////		}
////		return 1;
//		packageName = this.getPackageName();
//		putAsyncTask(new com.beem.push.utils.AsyncTask<Void, Void, Boolean>() {
//			@Override
//			protected void onPreExecute() {
//				super.onPreExecute();
//				showLoadingDialog("正在检测,请稍候...");
//			}
//
//			@Override
//			protected Boolean doInBackground(Void... params) {
//				
//				nowVersion = getVerCode(PreferenceActivity.this);
//				newVersion = goToCheckNewVersion(nowVersion);
//				return true;
//			}
//
//			@Override
//			protected void onPostExecute(Boolean result) {
//				super.onPostExecute(result);
//				dismissLoadingDialog();
//				if (!result) {
////					showCustomToast("删除失败...");
//					showCustomToast("版本检测失败...");
//				}else{
////					showCustomToast("删除成功...");
//					try{
//						if (newVersion.equals("Error"))
//							showCustomToast("版本检测失败...");
//						else if (Double.valueOf(newVersion) > Double.valueOf(nowVersion)) {
//							
//							mHandler.sendEmptyMessage(1);
//						}else
//							showCustomToast("当前已是最新版本.");
//					}catch(Exception e){
//						showCustomToast("版本检测失败...");
//					}
//				}
////				mHandler.sendEmptyMessage(0);
//				
//			}
//
//		});
//	}
//	/**
//	 * Role:是否进行更新提示框<BR>
//	 * Date:2012-4-5<BR>
//	 * 
//	 * @author ZHENSHI)peijiangping
//	 */
//	private BaseDialog mBaseDialog;
//	private void doNewVersionUpdate(String nowVersion, String newVersion,StringTokenizer st) {
//		StringBuffer sb = new StringBuffer();
//		sb.append("当前版本:");
//		sb.append(nowVersion);
//		sb.append("\n发现新版本:");
//		sb.append(newVersion);
//		sb.append("\n更新说明:\n");
////		sb.append(verdesc);
////		if(verdescArry!=null)
////		for(int i=0;i<verdescArry.length;i++){
////			sb.append(verdescArry[i]+"\n");
////		}
//		if(st!=null)
//		while(st.hasMoreTokens()){
////			System.out.println("20141223返回值解析verdescArry[j]=" + st.nextToken());
//			sb.append(st.nextToken()+"\n");
//		}
//		if(mustupdateornot.equals("0"))
//			sb.append("\n是否更新?");
//		else
//			sb.append("\n您必须下载更新才能正常使用!");
////		Dialog dialog = new AlertDialog.Builder(AccConfActivity.this)
////				.setTitle("软件更新")
////				.setMessage(sb.toString())
////				// 设置内容
////				.setPositiveButton("更新",// 设置确定按钮
////						new DialogInterface.OnClickListener() {
////							@Override
////							public void onClick(DialogInterface dialog,
////									int which) {
////								pBar = new ProgressDialog(
////										AccConfActivity.this);
////								pBar.setTitle("正在下载");
////								pBar.setMessage("请稍候...");
////								// pBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
////								pBar.show();
////								goToDownloadApk();
////							}
////						})
////				.setNegativeButton("暂不更新",
////						new DialogInterface.OnClickListener() {
////							public void onClick(DialogInterface dialog,
////									int whichButton) {
////								// 点击"取消"按钮之后退出程序
//////								finish();
////								dialog.dismiss();
////							}
////						}).create();// 创建
////		// 显示对话框
////		dialog.show();
//		
//		mBaseDialog = BaseDialog.getDialog(PreferenceActivity.this, "软件更新", sb.toString(),
//				"更新", new DialogInterface.OnClickListener() {
//	
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//						dialog.dismiss();
////						pBar = new ProgressDialog(
////								AccConfActivity.this);
////						pBar.setTitle("正在下载");
////						pBar.setMessage("请稍候...");
////						// pBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
////						pBar.show();
//						Beginning();
//						goToDownloadApk();
//					}
//				},(mustupdateornot.equals("0"))?"暂不更新":"取消并退出", new DialogInterface.OnClickListener() {
//	
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//						if(mustupdateornot.equals("0"))
//							dialog.dismiss();
//						else
//							finish();
//					}
//				});
//		mBaseDialog.show();
//	}
////	private ProgressBar pb;
////	private TextView tv;
//	public static int loading_process;
//	private BaseDialog mBaseDialog2;
//	public void Beginning(){
////		LinearLayout ll = (LinearLayout) LayoutInflater.from(AccConfActivity.this).inflate(
////				R.layout.loadapk, null);
////		pb = (ProgressBar) ll.findViewById(R.id.down_pb);
////		tv = (TextView) ll.findViewById(R.id.tv);
//		
//		mBaseDialog2 = BaseDialog.getDialog(PreferenceActivity.this, "版本更新进度提示", "加载进度",true,
//				"后台下载", new DialogInterface.OnClickListener() {
//	
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//						Intent intent=new Intent(PreferenceActivity.this, VersionService.class);  
//						startService(intent);
//						dialog.dismiss();
//					}
//				});
//		mBaseDialog2.show();
//		
//		
//	}
//	/**
//	 * Role:开启下载apk的线程<BR>
//	 * Date:2012-4-6<BR>
//	 * 
//	 * @author ZHENSHI)peijiangping
//	 */
//	private void goToDownloadApk() {
//		new Thread(new DownloadApkThread(handler,downloadPrefix+apkname,apkname)).start();
//	}
//	public Handler handler = new Handler() {
//		public void handleMessage(android.os.Message msg) {
//			if (!Thread.currentThread().isInterrupted()) {
//				switch (msg.what) {
//				case 1:
////					pb.setProgress(msg.arg1);
//					mBaseDialog2.getProgress().setProgress(msg.arg1);
//					loading_process = msg.arg1;
////					tv.setText("已为您加载了：" + loading_process + "%");
//					mBaseDialog2.getHtvMessage().setText("已为您加载了：" + loading_process + "%");
//					break;
//				case 2:
//					
//					Intent intent = new Intent(Intent.ACTION_VIEW);
//					intent.setDataAndType(Uri.fromFile(new File("/sdcard/update", apkname)),
//							"application/vnd.android.package-archive");
//					startActivity(intent);
//					mBaseDialog2.dismiss();
//					break;
//				case -1:
//					mBaseDialog2.dismiss();
//					String error = msg.getData().getString("error");
////					Toast.makeText(AccConfActivity.this, error, 1).show();
//					showCustomToast("下载失败！！"+error);
//					break;
//				}
//			}
//			super.handleMessage(msg);
//			
//		}
//	};
//	/**
//	 * Role:去获取当前的应用最新版本<BR>
//	 * Date:2012-4-5<BR>
//	 * 
//	 * @author ZHENSHI)peijiangping
//	 */
////    String[] verdescArry = null;
//    StringTokenizer st = null;
//	private String goToCheckNewVersion(String nowVersion) {
//		System.out.println("goToCheckNewVersion");
//		String result = null;
////		final String url = "http://192.168.1.41:8080/TestHttpServlet/GetVersionServlet";
////		final String url = "http://10.0.2.2:8080/apkvercheck/GetVersionServlet";
////		HttpConnect hc = new HttpConnect(url, this);
//		HttpConnect hc = new HttpConnect(apkvercheck,this);
//		List<NameValuePair> params = new ArrayList<NameValuePair>();  
//        params.add(new BasicNameValuePair("nowVersion", nowVersion));
////		result = hc.getDataAsString(null);
//        result = hc.getDataAsString(params);
//		
//		if (result.equals("Error")) {
//			return "Error";
//		}
//		try {
//			array = new JSONArray(result);
//			for (int i = 0; i < array.length(); i++) {
//				object = array.getJSONObject(i);
//				apkversion = object.getString("apkversion");
//				apkname = object.getString("apkname");
//				verdesc = object.getString("verdesc");
//				mustupdateornot = object.getString("mustupdateornot");
//				if(mustupdateornot==null 
//						|| mustupdateornot.equalsIgnoreCase("null")
//						|| mustupdateornot.length()==0)
//					mustupdateornot = "0";
////				verdescArry = (verdesc).split("//|");
////				verdesc = "";
////				for(int j=0;j<verdescArry.length;j++){
////					System.out.println("20141223返回值解析verdescArry["+j+"]=" + verdescArry[j]);
////				}
//				st = new StringTokenizer(verdesc , "|") ;
////				while(st.hasMoreTokens()){
////					System.out.println("20141223返回值解析verdescArry[j]=" + st.nextToken());
////				}
//				System.out.println("20141223返回值解析apkversion" + apkversion);
//				System.out.println("20141223返回值解析apkname" + apkname);
//				System.out.println("20141223返回值解析verdesc" + verdesc);
//				System.out.println("20141223返回值解析mustupdateornot" + mustupdateornot);
//			}
//		} catch (JSONException e) {
//			e.printStackTrace();
//		} catch(Exception e){
//			e.printStackTrace();
//		}
//		return apkversion;
//	}
//
//	/**
//	 * Role:取得程序的当前版本<BR>
//	 * Date:2012-4-5<BR>
//	 * 
//	 * @author ZHENSHI)peijiangping
//	 */
//	private String getVerCode(Context context) {
//		int verCode = 0;
//		String verName = null;
//		try {
//			verCode = context.getPackageManager()
//					.getPackageInfo(packageName, 0).versionCode;
//			verName = context.getPackageManager()
//					.getPackageInfo(packageName, 0).versionName;
//		} catch (NameNotFoundException e) {
//			System.out.println("no");
//		}
//		System.out.println("verCode" + verCode + "===" + "verName" + verName);
//		return verName;
//	}
}
