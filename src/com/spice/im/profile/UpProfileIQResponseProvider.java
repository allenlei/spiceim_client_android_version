package com.spice.im.profile;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;


public class UpProfileIQResponseProvider implements IQProvider{
	public UpProfileIQResponseProvider(){
		
	}
    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {

    	UpProfileIQResponse upProfileIQResponse = new UpProfileIQResponse();
        for (boolean done = false; !done;) {
            int eventType = parser.next();
            if (eventType == 2) {
                if ("id".equals(parser.getName())) {
                	upProfileIQResponse.setId(parser.nextText());
                }
                if ("apikey".equals(parser.getName())) {
                	upProfileIQResponse.setApikey(parser.nextText());
                }
                if ("retcode".equals(parser.getName())) {
                	upProfileIQResponse.setRetcode(parser.nextText());
                }
                if ("memo".equals(parser.getName())) {
                	upProfileIQResponse.setMemo(parser.nextText());
                }
            } else if (eventType == 3
                    && "updateprofileiq".equals(parser.getName())) {
                done = true;
            }
        }

        return upProfileIQResponse;
    }
}

