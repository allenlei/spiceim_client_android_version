package com.spice.im.profile;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;


public class UpProfileIQProvider implements IQProvider{
    public UpProfileIQProvider() {
    }
    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {

    	UpProfileIQ upProfileIQ = new UpProfileIQ();
        for (boolean done = false; !done;) {
            int eventType = parser.next();
            if (eventType == 2) {
                if ("id".equals(parser.getName())) {
                	upProfileIQ.setId(parser.nextText());
                }
                if ("apikey".equals(parser.getName())) {
                	upProfileIQ.setApikey(parser.nextText());
                }
                if ("uid".equals(parser.getName())) {
                	upProfileIQ.setUid(parser.nextText());
                }
                if ("login_name".equals(parser.getName())) {
                	upProfileIQ.setLogin_name(parser.nextText());
                }
                if ("name".equals(parser.getName())) {
                	upProfileIQ.setName(parser.nextText());
                }
                if ("note".equals(parser.getName())) {
                	upProfileIQ.setNote(parser.nextText());
                }
                if ("sex".equals(parser.getName())) {
                	upProfileIQ.setSex(parser.nextText());
                }
                if ("birthyear".equals(parser.getName())) {
                	upProfileIQ.setBirthyear(parser.nextText());
                }
                if ("birthmonth".equals(parser.getName())) {
                	upProfileIQ.setBirthmonth(parser.nextText());
                }
                if ("birthday".equals(parser.getName())) {
                	upProfileIQ.setBirthday(parser.nextText());
                }
                if ("uuid".equals(parser.getName())) {
                	upProfileIQ.setUuid(parser.nextText());
                }
                if ("hashcode".equals(parser.getName())) {
                	upProfileIQ.setHashcode(parser.nextText());
                }
            } else if (eventType == 3
                    && "updateprofileiq".equals(parser.getName())) {
                done = true;
            }
        }

        return upProfileIQ;
    }
}
