package com.spice.im.profile;

import org.jivesoftware.smack.packet.IQ;

public class UpProfileIQ extends IQ{
    //elementName = updateprofileiq
	//namespace = com:isharemessage:updateprofileiq
    private String id;

    private String apikey;
    
    private String uid;//当前用户id
    
    private String login_name;//用户名
    
    private String name;//姓名
    
    private String note;//个性签名
    
    private String sex;//性别
    
    private String birthyear;//出生年份
    
    private String birthmonth;//出生月份
    
    private String birthday;//出生日期
    
    private String uuid;
    
    private String hashcode;//apikey+uid+uuid 使用登录成功后返回的sessionid作为密码3des运算
    
    public UpProfileIQ(){}
    
    @Override
    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append("updateprofileiq").append(" xmlns=\"").append(
                "com:isharemessage:updateprofileiq").append("\">");
        if (id != null) {
            buf.append("<id>").append(id).append("</id>");
        }
        if (apikey != null) {
            buf.append("<apikey>").append(apikey).append("</apikey>");
        }
        if (uid != null) {
            buf.append("<uid>").append(uid).append("</uid>");
        }
        if (login_name != null) {
            buf.append("<login_name>").append(login_name).append("</login_name>");
        }
        if (name != null) {
            buf.append("<name>").append(name).append("</name>");
        }
        if (note != null) {
            buf.append("<note>").append(note).append("</note>");
        }
        if (sex != null) {
            buf.append("<sex>").append(sex).append("</sex>");
        }
        if (birthyear != null) {
            buf.append("<birthyear>").append(birthyear).append("</birthyear>");
        }
        if (birthmonth != null) {
            buf.append("<birthmonth>").append(birthmonth).append("</birthmonth>");
        }
        if (birthday != null) {
            buf.append("<birthday>").append(birthday).append("</birthday>");
        }
        if (uuid != null) {
            buf.append("<uuid>").append(uuid).append("</uuid>");
        }
        if (hashcode != null) {
            buf.append("<hashcode>").append(hashcode).append("</hashcode>");
        }
        buf.append("</").append("updateprofileiq").append(">");
        return buf.toString();
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

	public String getApikey() {
		return apikey;
	}
	public void setApikey(String apikey) {
		this.apikey = apikey;
	}
	
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getLogin_name() {
		return login_name;
	}
	public void setLogin_name(String login_name) {
		this.login_name = login_name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getBirthyear() {
		return birthyear;
	}
	public void setBirthyear(String birthyear) {
		this.birthyear = birthyear;
	}
	public String getBirthmonth() {
		return birthmonth;
	}
	public void setBirthmonth(String birthmonth) {
		this.birthmonth = birthmonth;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getHashcode() {
		return hashcode;
	}
	public void setHashcode(String hashcode) {
		this.hashcode = hashcode;
	}
}
