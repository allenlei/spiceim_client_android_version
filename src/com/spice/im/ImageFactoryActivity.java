package com.spice.im;

//package com.immomo.momo.android.activity.imagefactory;

//import com.stb.isharemessage.R;


import com.sdp.custom.ImageFactoryFaner;
import com.spice.im.utils.HeaderLayout;
import com.spice.im.utils.PhotoUtils;
import com.spice.im.utils.HeaderLayout.HeaderStyle;
import com.spice.im.utils.HeaderLayout.onRightImageButtonClickListener;

//import com.stb.isharemessage.utils.HeaderLayout;
//import com.stb.isharemessage.utils.PhotoUtils;
//import com.stb.isharemessage.utils.HeaderLayout.HeaderStyle;
//import com.stb.isharemessage.utils.HeaderLayout.onRightImageButtonClickListener;
////import com.sdp.custom.ImageFactoryFaner;

import android.content.Context;
import android.content.Intent;


import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ViewFlipper;

//import com.immomo.momo.android.BaseActivity;
////import com.immomo.momo.android.R;
//import com.immomo.momo.android.activity.R;
//import com.immomo.momo.android.util.PhotoUtils;
//import com.immomo.momo.android.view.HeaderLayout;
//import com.immomo.momo.android.view.HeaderLayout.HeaderStyle;
//import com.immomo.momo.android.view.HeaderLayout.onRightImageButtonClickListener;

public class ImageFactoryActivity extends BaseActivity {
	private HeaderLayout mHeaderLayout;
	private ViewFlipper mVfFlipper;
	private Button mBtnLeft;
	private Button mBtnRight;
	
//	private Button mBtnMiddleFaner;

//	private ImageFactoryCrop mImageFactoryCrop;
//	private ImageFactoryFliter mImageFactoryFliter;
	private ImageFactoryFaner mImageFactoryFaner;
	private String mPath;
	private String mNewPath;
	private int mIndex = 0;//0 crop裁图 1 fliter滤镜 2 faner范儿标签
	private String mType;

	public static final String TYPE = "type";
//	public static final String CROP = "crop";
//	public static final String FLITER = "fliter";
	public static final String FANER = "faner";//给照片添加标签
	
	private Context context = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_imagefactory);
		context = this;
		initViews();
		initEvents();
		init();
	}

	@Override
	protected void initViews() {
		mHeaderLayout = (HeaderLayout) findViewById(R.id.imagefactory_header);
		mHeaderLayout.init(HeaderStyle.TITLE_RIGHT_IMAGEBUTTON);
		mVfFlipper = (ViewFlipper) findViewById(R.id.imagefactory_vf_viewflipper);
		mBtnLeft = (Button) findViewById(R.id.imagefactory_btn_left);
		mBtnRight = (Button) findViewById(R.id.imagefactory_btn_right);
//		mBtnMiddleFaner = (Button) findViewById(R.id.imagefactory_btn_middle_faner);
	}

	@Override
	protected void initEvents() {
		mBtnLeft.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
//				if (mIndex == 0) {
//					setResult(RESULT_CANCELED);
//					finish();
//				} else if(mIndex == 1){
//					if (FLITER.equals(mType)) {
//						setResult(RESULT_CANCELED);
//						finish();
//					} else {
//						mIndex = 0;
//						initImageFactory();
//						mVfFlipper.setInAnimation(ImageFactoryActivity.this,
//								R.anim.push_right_in);
//						mVfFlipper.setOutAnimation(ImageFactoryActivity.this,
//								R.anim.push_right_out);
//						mVfFlipper.showPrevious();
//					}
//				}
//				else if(mIndex == 2){
					mIndex = 1;
					initImageFactory();
					mVfFlipper.setInAnimation(ImageFactoryActivity.this,
							R.anim.push_right_in);
					mVfFlipper.setOutAnimation(ImageFactoryActivity.this,
							R.anim.push_right_out);
					mVfFlipper.showPrevious();
					
//				}
			}
		});
		mBtnRight.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
//				System.gc();//20151210
//				if (mIndex == 2) {//1
					mNewPath = PhotoUtils.savePhotoToSDCard(context,mImageFactoryFaner
							.getBitmap());//mImageFactoryFaner.getBitmap() 
					Intent intent = new Intent();
					intent.putExtra("path", mNewPath);
					intent.putExtra("fanermark", mImageFactoryFaner.getPointListString());//20140825
					setResult(RESULT_OK, intent);
					finish();
//				} else if(mIndex == 0){
//					mNewPath = PhotoUtils.savePhotoToSDCard(context,mImageFactoryCrop
//							.cropAndSave());
//					mIndex = 1;
//					initImageFactory();
//					mVfFlipper.setInAnimation(ImageFactoryActivity.this,
//							R.anim.push_left_in);
//					mVfFlipper.setOutAnimation(ImageFactoryActivity.this,
//							R.anim.push_left_out);
//					mVfFlipper.showNext();
//				}
//				else if(mIndex == 1){
//					mNewPath = PhotoUtils.savePhotoToSDCard(context,mImageFactoryFliter
//							.getBitmap());//20140919添加
//					mIndex = 2;
//					initImageFactory();
//					mVfFlipper.setInAnimation(ImageFactoryActivity.this,
//							R.anim.push_left_in);
//					mVfFlipper.setOutAnimation(ImageFactoryActivity.this,
//							R.anim.push_left_out);
//					mVfFlipper.showNext();
//				}
			}
		});
	}

	@Override
	public void onBackPressed() {
//		if (mIndex == 0) {
//			setResult(RESULT_CANCELED);
//			finish();
//		} else if(mIndex == 1){
//			if (FLITER.equals(mType)) {
//				setResult(RESULT_CANCELED);
//				finish();
//			} else {
//				mIndex = 0;
//				initImageFactory();
//				mVfFlipper.setInAnimation(ImageFactoryActivity.this,
//						R.anim.push_right_in);
//				mVfFlipper.setOutAnimation(ImageFactoryActivity.this,
//						R.anim.push_right_out);
//				mVfFlipper.showPrevious();
//			}
//		}
//		else if(mIndex == 2){
			mIndex = 1;
			initImageFactory();
			mVfFlipper.setInAnimation(ImageFactoryActivity.this,
					R.anim.push_right_in);
			mVfFlipper.setOutAnimation(ImageFactoryActivity.this,
					R.anim.push_right_out);
			mVfFlipper.showPrevious();
//		}
	}

	private void init() {
		mPath = getIntent().getStringExtra("path");
		mType = getIntent().getStringExtra(TYPE);
		mNewPath = new String(mPath);
//		if (CROP.equals(mType)) {
//			mIndex = 0;
//		} else if (FLITER.equals(mType)) {
//			mIndex = 1;
//			mVfFlipper.showPrevious();
//		}else if(FANER.equals(mType)){
			mIndex = 2;
//		}
		initImageFactory();
		
	}

	private void initImageFactory() {
		switch (mIndex) {
//		case 0:
////			mBtnMiddleFaner.setVisibility(View.GONE);
//			if (mImageFactoryCrop == null) {
//				mImageFactoryCrop = new ImageFactoryCrop(this,
//						mVfFlipper.getChildAt(0));
//			}
//			mImageFactoryCrop.init(mPath, mScreenWidth, mScreenHeight);
//			mHeaderLayout.setTitleRightImageButton("裁切图片", null,
//					R.drawable.ic_topbar_rotation,
//					new OnRightImageButtonClickListener());
//			mBtnLeft.setText("取    消");
//			mBtnRight.setText("确    认");
//
//			break;
//
//		case 1:
////			mBtnMiddleFaner.setVisibility(View.VISIBLE);
//			if (mImageFactoryFliter == null) {
//				mImageFactoryFliter = new ImageFactoryFliter(this,
//						mVfFlipper.getChildAt(1));
//			}
//			mImageFactoryFliter.init(mNewPath);
//			mHeaderLayout.setTitleRightImageButton("图片滤镜", null,
//					R.drawable.ic_topbar_rotation,
//					new OnRightImageButtonClickListener());
//			mBtnLeft.setText("取    消");
////			mBtnMiddleFaner.setText("添加范儿标签");
//			mBtnRight.setText("完    成");
//			break;
		case 2:
			if (mImageFactoryFaner == null) {
				mImageFactoryFaner = new ImageFactoryFaner(this,
						mVfFlipper.getChildAt(0));//0,1,2
			}
			mImageFactoryFaner.init(mNewPath);
//			mHeaderLayout.setTitleRightImageButton("SHOW范儿标签", null,
//					R.drawable.ic_topbar_rotation,
//					new OnRightImageButtonClickListener());
			mHeaderLayout.init(HeaderStyle.DEFAULT_TITLE);
			mHeaderLayout.setDefaultTitle("SHOW范儿标签", null);
			mBtnLeft.setText("取    消");
//			mBtnMiddleFaner.setText("添加范儿标签");
			mBtnRight.setText("完    成");
			break;
		}
	}

	private class OnRightImageButtonClickListener implements
			onRightImageButtonClickListener {

		@Override
		public void onClick() {
			switch (mIndex) {
//			case 0:
//				if (mImageFactoryCrop != null) {
//					mImageFactoryCrop.Rotate();
//				}
//				break;
//
//			case 1:
//				if (mImageFactoryFliter != null) {
//					mImageFactoryFliter.Rotate();
//				}
//				break;
			}
		}
	}
}

