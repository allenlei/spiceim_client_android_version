package com.spice.im;

import java.io.File;




import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;




import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
//import org.jivesoftware.smack.util.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spice.im.preference.NoLoginPreferenceActivity;
import com.spice.im.preference.PreferenceActivity;
import com.spice.im.ui.HandyTextView;
import com.spice.im.utils.AsyncTask;
import com.spice.im.utils.HeaderLayout;
import com.spice.im.utils.TextUtils;
import com.spice.im.utils.HeaderLayout.HeaderStyle;
import com.stb.isharemessage.BeemApplication;

//import com.allen.updateapk.DownloadApkThread;
//import com.allen.updateapk.HttpConnect;
//import com.allen.updateapk.VersionService;
//import com.stb.isharemessage.R;
//import com.example.android.bitmapfun.util.AsyncTask;
//import com.speed.im.utils.AsyncTask;
//import com.speed.im.utils.AsyncTask.Status;
////import com.stb.isharemessage.BeemApplication;
////import com.stb.isharemessage.ui.ContactFrameActivity;
//import com.stb.isharemessage.ui.HandyTextView;
////import com.stb.isharemessage.ui.Login;
////import com.stb.isharemessage.ui.PreferenceActivity;
////import com.stb.isharemessage.ui.register.BaseActivity;
////import com.stb.isharemessage.ui.register.BaseDialog;
////import com.stb.isharemessage.ui.register.FlippingLoadingDialog;
////import com.stb.isharemessage.ui.register.RegisterActivity;
//import com.stb.isharemessage.utils.HeaderLayout;
//import com.stb.isharemessage.utils.HeaderLayout.HeaderStyle;



import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.LoginFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


public class AccConfActivity  extends FragmentActivity implements OnClickListener{
	private static final int MANUAL_CONFIGURATION = 1;
	private HeaderLayout mHeaderLayout;
	private EditText mEtAccount;//帐号
	private EditText mEtPwd;//密码
	private HandyTextView mHtvForgotPassword;//忘记密码
	private HandyTextView mHtvCreateAccount;//创建帐号
	private Button mBtnManualSetup;//设置
	private Button mBtnLogin;//登陆
	private String mAccount;
	private String mPassword;
    private final JidTextWatcher mJidTextWatcher = new JidTextWatcher();
    private final PasswordTextWatcher mPasswordTextWatcher = new PasswordTextWatcher();
    private boolean mValidJid;
    private boolean mValidPassword;
	
	private String apkvercheck = "";
	public String downloadPrefix;
	
	private JSONArray array = null;
	private JSONObject object = null;
    private String apkversion = "";
    private String apkname = "";
    private String verdesc = "";
    private String mustupdateornot = "0";//0 false,1 true
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
        Properties props = new Properties();
        try {
            int id = this.getResources().getIdentifier("androidpn", "raw",
            		this.getPackageName());
            props.load(this.getResources().openRawResource(id));
        } catch (Exception e) {
//            Log.e(LOGTAG, "Could not find the properties file.", e);
            // e.printStackTrace();
        }
        apkvercheck = props.getProperty("apkvercheck", "");
        downloadPrefix = props.getProperty("downloadPrefix", "");
		
		mHeaderLayout = (HeaderLayout) findViewById(R.id.login_header);
		mHeaderLayout.init(HeaderStyle.DEFAULT_TITLE);
		mHeaderLayout.setDefaultTitle("登录", null);
		mEtAccount = (EditText) findViewById(R.id.login_et_account);
		mEtPwd = (EditText) findViewById(R.id.login_et_pwd);
		mHtvForgotPassword = (HandyTextView) findViewById(R.id.login_htv_forgotpassword);
		TextUtils.addUnderlineText(this, mHtvForgotPassword, 0,
				mHtvForgotPassword.getText().length());
		mHtvCreateAccount = (HandyTextView) findViewById(R.id.login_htv_createaccount);
		TextUtils.addUnderlineText(this, mHtvCreateAccount, 0,
				mHtvCreateAccount.getText().length());
		mBtnManualSetup = (Button) findViewById(R.id.manual_setup);
		mBtnLogin = (Button) findViewById(R.id.login_btn_login);
		
		InputFilter[] orgFilters = mEtAccount.getFilters();
		InputFilter[] newFilters = new InputFilter[orgFilters.length + 1];
		int i;
		for (i = 0; i < orgFilters.length; i++)
		    newFilters[i] = orgFilters[i];
		newFilters[i] = new LoginFilter.UsernameFilterGeneric();
		mEtAccount.setFilters(newFilters);
		mEtAccount.addTextChangedListener(mJidTextWatcher);
		mEtPwd.addTextChangedListener(mPasswordTextWatcher);
		
		mHtvForgotPassword.setOnClickListener(this);
		mHtvCreateAccount.setOnClickListener(this);
		mBtnManualSetup.setOnClickListener(this);
		mBtnLogin.setOnClickListener(this);
		
        Intent intent = getIntent();
        String mTextViewStr = intent.getStringExtra("mTextViewStr");
        if(mTextViewStr!=null && mTextViewStr.length()!=0 && !mTextViewStr.equalsIgnoreCase("null"))
        Toast.makeText(AccConfActivity.this, mTextViewStr, Toast.LENGTH_SHORT).show();
        
        
	    SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
	    String login = settings.getString(BeemApplication.ACCOUNT_USERNAME_KEY, "");
	    String password = settings.getString(BeemApplication.ACCOUNT_PASSWORD_KEY, "");
	    checkUsername(login);
	    checkPassword(password);
	    if(mValidJid && mValidPassword){
		    mEtAccount.setText(login);
		    mEtPwd.setText(password);
		    mBtnLogin.setEnabled(mValidJid && mValidPassword);
	    }
	    
	    SpiceApplication.getInstance().addActivity(this);
	    yesOrNoUpdataApk();
	}
//    @Override
//    protected void onResume() {
//		super.onResume();
//		InputFilter[] orgFilters = mEtAccount.getFilters();
//		InputFilter[] newFilters = new InputFilter[orgFilters.length + 1];
//		int i;
//		for (i = 0; i < orgFilters.length; i++)
//		    newFilters[i] = orgFilters[i];
//		newFilters[i] = new LoginFilter.UsernameFilterGeneric();
//		mEtAccount.setFilters(newFilters);
//		mEtAccount.addTextChangedListener(mJidTextWatcher);
//		mEtPwd.addTextChangedListener(mPasswordTextWatcher);
//    }
	@Override
	public void onClick(View v) {
		Intent i = null;
		switch (v.getId()) {
		case R.id.login_htv_forgotpassword:
//			startActivity(FindPwdTabsActivity.class);
			break;

		case R.id.login_htv_createaccount:
			i = new Intent(this, RegisterActivity.class);
		    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		    startActivity(i);
			break;

		case R.id.manual_setup:
//			finish();
		    i = new Intent(this, NoLoginPreferenceActivity.class);//Settings
//		    i = new Intent(this, com.allen.ui.setting.NewPreferences.class);
		    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		    startActivityForResult(i, MANUAL_CONFIGURATION);
		    finish();
			break;

		case R.id.login_btn_login:
//			login();
		    configureAccount();//save account into sqlite
		    i = new Intent(this, Login.class);
		    i.putExtra("isConfig", 1);//0 false 1 true
		    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		    startActivity(i);
		    finish();
			break;		
		}
	}
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == MANUAL_CONFIGURATION) {
		    SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
		    String login = settings.getString(BeemApplication.ACCOUNT_USERNAME_KEY, "");
		    String password = settings.getString(BeemApplication.ACCOUNT_PASSWORD_KEY, "");
		    mEtAccount.setText(login);
		    mEtPwd.setText(password);
		    checkUsername(login);
		    checkPassword(password);
		    mBtnLogin.setEnabled(mValidJid && mValidPassword);
		}
    }
    /**
     * Store the account in the settings.
     */
    private void configureAccount() {
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
		SharedPreferences.Editor edit = settings.edit();
		edit.putString(BeemApplication.ACCOUNT_USERNAME_KEY, mEtAccount.getText().toString());
		edit.putString(BeemApplication.ACCOUNT_PASSWORD_KEY, mEtPwd.getText().toString());
		edit.commit();
    }
    /**
     * Check that the username is really a JID.
     * @param username the username to check.
     */
    private void checkUsername(String username) {
//		String name = StringUtils.parseName(username);
    	String name = username;
//		String server = StringUtils.parseServer(username);
//		if (android.text.TextUtils.isEmpty(name) || android.text.TextUtils.isEmpty(server)) {
    	if (android.text.TextUtils.isEmpty(name)) {
		    mValidJid = false;
		} else {
		    mValidJid = true;
		}
    }

    /**
     * Check password.
     * @param password the password to check.
     */
    private void checkPassword(String password) {
		if (password.length() > 0)
		    mValidPassword = true;
		else
		    mValidPassword = false;
    }
	/**
     * Text watcher to test the existence of a password.
     */
    private class PasswordTextWatcher implements TextWatcher {

		/**
		 * Constructor.
		 */
		public PasswordTextWatcher() {
		}
	
		@Override
		public void afterTextChanged(Editable s) {
		    checkPassword(s.toString());
		    mBtnLogin.setEnabled(mValidJid && mValidPassword);
		}
	
		@Override
		public void beforeTextChanged(CharSequence  s, int start, int count, int after) {
		}
	
		@Override
		public void onTextChanged(CharSequence  s, int start, int before, int count) {
		}
    }

    /**
     * TextWatcher to check the validity of a JID.
     */
    private class JidTextWatcher implements TextWatcher {

		/**
		 * Constructor.
		 */
		public JidTextWatcher() {
		}
	
		@Override
		public void afterTextChanged(Editable s) {
		    checkUsername(s.toString());
		    mBtnLogin.setEnabled(mValidJid && mValidPassword);
		}
	
		@Override
		public void beforeTextChanged(CharSequence  s, int start, int count, int after) {
		}
	
		@Override
		public void onTextChanged(CharSequence  s, int start, int before, int count) {
		}
    }
    
    
    //校验软件版本20141222 start
	/** 显示自定义Toast提示(来自String) **/
	protected void showCustomToast(String text) {
		View toastRoot = LayoutInflater.from(AccConfActivity.this).inflate(
				R.layout.common_toast, null);
		((HandyTextView) toastRoot.findViewById(R.id.toast_text)).setText(text);
		Toast toast = new Toast(AccConfActivity.this);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.setDuration(Toast.LENGTH_SHORT);
		toast.setView(toastRoot);
		toast.show();
	}
	private String packageName;
	private String nowVersion = "";
	private String newVersion = "";
//	private ProgressDialog pBar;
	/**
	 * Role:是否更新版本<BR>
	 * Date:2012-4-5<BR>
	 * 
	 * @author ZHENSHI)peijiangping
	 */
	private void yesOrNoUpdataApk() {
//		packageName = this.getPackageName();
//		String nowVersion = getVerCode(AccConfActivity.this);
//		String newVersion = goToCheckNewVersion();
//		if (newVersion.equals("Error")) {
//			return 0;
//		}
//		if (Double.valueOf(newVersion) > Double.valueOf(nowVersion)) {
//			doNewVersionUpdate(nowVersion, newVersion);
//		}
//		return 1;
		packageName = this.getPackageName();
		putAsyncTask(new AsyncTask<Void, Void, Boolean>() {
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
//				showLoadingDialog("正在删除,请稍候...");
			}

			@Override
			protected Boolean doInBackground(Void... params) {
				
				nowVersion = getVerCode(AccConfActivity.this);
//				newVersion = goToCheckNewVersion(nowVersion);
				return true;
			}

			@Override
			protected void onPostExecute(Boolean result) {
				super.onPostExecute(result);
//				dismissLoadingDialog();
				if (!result) {
//					showCustomToast("删除失败...");
				}else{
//					showCustomToast("删除成功...");
					try{
						if (newVersion.equals("Error"))
							;
						else if (Double.valueOf(newVersion) > Double.valueOf(nowVersion)) {
							
							mHandler.sendEmptyMessage(0);
						}
					}catch(Exception e){
						
					}
				}
//				mHandler.sendEmptyMessage(0);
				
			}

		});
	}
	Handler mHandler = new Handler() {

		@Override
		public void handleMessage(android.os.Message msg) {
			super.handleMessage(msg);
			switch (msg.what) {
			case 0:
				doNewVersionUpdate(nowVersion, newVersion,st);
				break;
			default:
				break;
			}
		}

	};
	/**
	 * Role:是否进行更新提示框<BR>
	 * Date:2012-4-5<BR>
	 * 
	 * @author ZHENSHI)peijiangping
	 */
	private BaseDialog mBaseDialog;
	private void doNewVersionUpdate(String nowVersion, String newVersion,StringTokenizer st) {
		StringBuffer sb = new StringBuffer();
		sb.append("当前版本:");
		sb.append(nowVersion);
		sb.append("\n发现新版本:");
		sb.append(newVersion);
		sb.append("\n更新说明:\n");
//		sb.append(verdesc);
//		if(verdescArry!=null)
//		for(int i=0;i<verdescArry.length;i++){
//			sb.append(verdescArry[i]+"\n");
//		}
		if(st!=null)
		while(st.hasMoreTokens()){
//			System.out.println("20141223返回值解析verdescArry[j]=" + st.nextToken());
			sb.append(st.nextToken()+"\n");
		}
		if(mustupdateornot.equals("0"))
			sb.append("\n是否更新?");
		else
			sb.append("\n您必须下载更新才能正常使用!");
//		Dialog dialog = new AlertDialog.Builder(AccConfActivity.this)
//				.setTitle("软件更新")
//				.setMessage(sb.toString())
//				// 设置内容
//				.setPositiveButton("更新",// 设置确定按钮
//						new DialogInterface.OnClickListener() {
//							@Override
//							public void onClick(DialogInterface dialog,
//									int which) {
//								pBar = new ProgressDialog(
//										AccConfActivity.this);
//								pBar.setTitle("正在下载");
//								pBar.setMessage("请稍候...");
//								// pBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//								pBar.show();
//								goToDownloadApk();
//							}
//						})
//				.setNegativeButton("暂不更新",
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,
//									int whichButton) {
//								// 点击"取消"按钮之后退出程序
////								finish();
//								dialog.dismiss();
//							}
//						}).create();// 创建
//		// 显示对话框
//		dialog.show();
		
		mBaseDialog = BaseDialog.getDialog(AccConfActivity.this, "软件更新", sb.toString(),
				"更新", new DialogInterface.OnClickListener() {
	
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
//						pBar = new ProgressDialog(
//								AccConfActivity.this);
//						pBar.setTitle("正在下载");
//						pBar.setMessage("请稍候...");
//						// pBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//						pBar.show();
						Beginning();
						goToDownloadApk();
					}
				},(mustupdateornot.equals("0"))?"暂不更新":"取消并退出", new DialogInterface.OnClickListener() {
	
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(mustupdateornot.equals("0"))
							dialog.dismiss();
						else
							finish();
					}
				});
		mBaseDialog.show();
	}
//	private ProgressBar pb;
//	private TextView tv;
	public static int loading_process;
	private BaseDialog mBaseDialog2;
	public void Beginning(){
//		LinearLayout ll = (LinearLayout) LayoutInflater.from(AccConfActivity.this).inflate(
//				R.layout.loadapk, null);
//		pb = (ProgressBar) ll.findViewById(R.id.down_pb);
//		tv = (TextView) ll.findViewById(R.id.tv);
		
		mBaseDialog2 = BaseDialog.getDialog(AccConfActivity.this, "版本更新进度提示", "加载进度",true,
				"后台下载", new DialogInterface.OnClickListener() {
	
					@Override
					public void onClick(DialogInterface dialog, int which) {
//						Intent intent=new Intent(AccConfActivity.this, VersionService.class);  
//						startService(intent);
						dialog.dismiss();
					}
				});
		mBaseDialog2.show();
		
//		android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(AccConfActivity.this);
//		builder.setView(ll);
//		builder.setTitle("版本更新进度提示");
//		builder.setNegativeButton("后台下载",
//				new DialogInterface.OnClickListener() {
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//						Intent intent=new Intent(AccConfActivity.this, VersionService.class);  
//						startService(intent);
//						dialog.dismiss();
//					}
//				});
//		
//		builder.show();
		
	}
	/**
	 * Role:开启下载apk的线程<BR>
	 * Date:2012-4-6<BR>
	 * 
	 * @author ZHENSHI)peijiangping
	 */
	private void goToDownloadApk() {
//		new Thread(new DownloadApkThread(handler,downloadPrefix+apkname,apkname)).start();
	}

	public Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			if (!Thread.currentThread().isInterrupted()) {
				switch (msg.what) {
				case 1:
//					pb.setProgress(msg.arg1);
					mBaseDialog2.getProgress().setProgress(msg.arg1);
					loading_process = msg.arg1;
//					tv.setText("已为您加载了：" + loading_process + "%");
					mBaseDialog2.getHtvMessage().setText("已为您加载了：" + loading_process + "%");
					break;
				case 2:
					
					Intent intent = new Intent(Intent.ACTION_VIEW);
					intent.setDataAndType(Uri.fromFile(new File("/sdcard/update", apkname)),
							"application/vnd.android.package-archive");
					startActivity(intent);
					mBaseDialog2.dismiss();
					break;
				case -1:
					mBaseDialog2.dismiss();
					String error = msg.getData().getString("error");
//					Toast.makeText(AccConfActivity.this, error, 1).show();
					showCustomToast("下载失败！！"+error);
					break;
				}
			}
			super.handleMessage(msg);
//			if (msg.what == 1) {
//				showCustomToast("下载成功！！");
//				Intent intent = new Intent(Intent.ACTION_VIEW);
//				intent.setDataAndType(
//						Uri.fromFile(new File("/sdcard/update/", apkname)),
//						"application/vnd.android.package-archive");//apkname "updata.apk"
//				startActivity(intent);
//			} else {
//				showCustomToast("下载失败！！");
//			}
//			pBar.cancel();
			
		}
	};

	/**
	 * Role:去获取当前的应用最新版本<BR>
	 * Date:2012-4-5<BR>
	 * 
	 * @author ZHENSHI)peijiangping
	 */
//    String[] verdescArry = null;
    StringTokenizer st = null;
//	private String goToCheckNewVersion(String nowVersion) {
//		System.out.println("goToCheckNewVersion");
//		String result = null;
////		final String url = "http://192.168.1.41:8080/TestHttpServlet/GetVersionServlet";
////		final String url = "http://10.0.2.2:8080/apkvercheck/GetVersionServlet";
////		HttpConnect hc = new HttpConnect(url, this);
//		HttpConnect hc = new HttpConnect(apkvercheck,this);
//		List<NameValuePair> params = new ArrayList<NameValuePair>();  
////        params.add(new BasicNameValuePair("nowVersion", nowVersion));
////        params.add(new BasicNameValuePair("ver_type_name", "0001"));
////		result = hc.getDataAsString(null);
//        result = hc.getDataAsString(params);
//		
//		if (result.equals("Error")) {
//			return "Error";
//		}
//		try {
//			array = new JSONArray(result);
//			for (int i = 0; i < array.length(); i++) {
//				object = array.getJSONObject(i);
//				apkversion = object.getString("apkversion");
//				apkname = object.getString("apkname");
//				verdesc = object.getString("verdesc");
//				mustupdateornot = object.getString("mustupdateornot");
//				if(mustupdateornot==null 
//						|| mustupdateornot.equalsIgnoreCase("null")
//						|| mustupdateornot.length()==0)
//					mustupdateornot = "0";
////				verdescArry = (verdesc).split("//|");
////				verdesc = "";
////				for(int j=0;j<verdescArry.length;j++){
////					System.out.println("20141223返回值解析verdescArry["+j+"]=" + verdescArry[j]);
////				}
//				st = new StringTokenizer(verdesc , "|") ;
////				while(st.hasMoreTokens()){
////					System.out.println("20141223返回值解析verdescArry[j]=" + st.nextToken());
////				}
//				System.out.println("20141223返回值解析apkversion" + apkversion);
//				System.out.println("20141223返回值解析apkname" + apkname);
//				System.out.println("20141223返回值解析verdesc" + verdesc);
//				System.out.println("20141223返回值解析mustupdateornot" + mustupdateornot);
//			}
//		} catch (JSONException e) {
//			e.printStackTrace();
//		} catch(Exception e){
//			e.printStackTrace();
//		}
//		return apkversion;
//	}

	/**
	 * Role:取得程序的当前版本<BR>
	 * Date:2012-4-5<BR>
	 * 
	 * @author ZHENSHI)peijiangping
	 */
	private String getVerCode(Context context) {
		int verCode = 0;
		String verName = null;
		try {
			verCode = context.getPackageManager()
					.getPackageInfo(packageName, 0).versionCode;
			verName = context.getPackageManager()
					.getPackageInfo(packageName, 0).versionName;
		} catch (NameNotFoundException e) {
			System.out.println("no");
		}
		System.out.println("verCode" + verCode + "===" + "verName" + verName);
		return verName;
	}
	private static ExecutorService LIMITED_TASK_EXECUTOR;
    static {
    	LIMITED_TASK_EXECUTOR = (ExecutorService) Executors.newFixedThreadPool(7); 
    }; 
	protected List<AsyncTask<Void, Void, Boolean>> mAsyncTasks = new ArrayList<AsyncTask<Void, Void, Boolean>>();
	protected FlippingLoadingDialog mLoadingDialog;
	protected void putAsyncTask(AsyncTask<Void, Void, Boolean> asyncTask) {
//		mAsyncTasks.add(asyncTask.execute());
		mAsyncTasks.add(asyncTask.executeOnExecutor(LIMITED_TASK_EXECUTOR, null));
	}

	protected void clearAsyncTask() {
		Iterator<AsyncTask<Void, Void, Boolean>> iterator = mAsyncTasks
				.iterator();
		while (iterator.hasNext()) {
			AsyncTask<Void, Void, Boolean> asyncTask = iterator.next();
			if (asyncTask != null && !asyncTask.isCancelled()) {
				asyncTask.cancel(true);
			}
		}
		mAsyncTasks.clear();
	}
	@Override
	protected void onDestroy() {
		if(mBaseDialog2!=null && mBaseDialog2.isShowing())
			mBaseDialog2.cancel();
		if(mBaseDialog!=null && mBaseDialog.isShowing())
			mBaseDialog.cancel();
		clearAsyncTask();
		super.onDestroy();
	}
    //校验软件版本20141222 end
}
