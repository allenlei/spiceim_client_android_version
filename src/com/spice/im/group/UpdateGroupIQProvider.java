package com.spice.im.group;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;


public class UpdateGroupIQProvider implements IQProvider{
    public UpdateGroupIQProvider() {
    }
    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {

    	UpdateGroupIQ updateGroupIQ = new UpdateGroupIQ();
        for (boolean done = false; !done;) {
            int eventType = parser.next();
            if (eventType == 2) {
                if ("id".equals(parser.getName())) {
                	updateGroupIQ.setId(parser.nextText());
                }
                if ("apikey".equals(parser.getName())) {
                	updateGroupIQ.setApikey(parser.nextText());
                }
                if ("uid".equals(parser.getName())) {
                	updateGroupIQ.setUid(parser.nextText());
                }
                if ("tagid".equals(parser.getName())) {
                	updateGroupIQ.setTagid(parser.nextText());
                }
                if ("ownerid".equals(parser.getName())) {
                	updateGroupIQ.setOwnerid(parser.nextText());
                }
                if ("groupname".equals(parser.getName())) {
                	updateGroupIQ.setGroupname(parser.nextText());
                }
                if ("pic".equals(parser.getName())) {
                	updateGroupIQ.setPic(parser.nextText());
                }
                if ("uuid".equals(parser.getName())) {
                	updateGroupIQ.setUuid(parser.nextText());
                }
                if ("hashcode".equals(parser.getName())) {
                	updateGroupIQ.setHashcode(parser.nextText());
                }
            } else if (eventType == 3
                    && "updategroupiq".equals(parser.getName())) {
                done = true;
            }
        }

        return updateGroupIQ;
    }
}
