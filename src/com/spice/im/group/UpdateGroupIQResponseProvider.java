package com.spice.im.group;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;


public class UpdateGroupIQResponseProvider implements IQProvider{
	public UpdateGroupIQResponseProvider(){
		
	}
    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {

    	UpdateGroupIQResponse updateGroupIQResponse = new UpdateGroupIQResponse();
        for (boolean done = false; !done;) {
            int eventType = parser.next();
            if (eventType == 2) {
                if ("id".equals(parser.getName())) {
                	updateGroupIQResponse.setId(parser.nextText());
                }
                if ("apikey".equals(parser.getName())) {
                	updateGroupIQResponse.setApikey(parser.nextText());
                }
                if ("retcode".equals(parser.getName())) {
                	updateGroupIQResponse.setRetcode(parser.nextText());
                }
                if ("memo".equals(parser.getName())) {
                	updateGroupIQResponse.setMemo(parser.nextText());
                }
            } else if (eventType == 3
                    && "updategroupiq".equals(parser.getName())) {
                done = true;
            }
        }

        return updateGroupIQResponse;
    }
}

