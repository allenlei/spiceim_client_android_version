package com.spice.im.group;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dodowaterfall.widget.FlowView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.spice.im.R;
import com.spice.im.SpiceApplication;
import com.spice.im.ui.HandyTextView;
import com.spice.im.utils.ImageFetcher;
import com.stb.core.chat.ContactGroup;
import com.stb.isharemessage.service.XmppConnectionAdapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class ContactAdapter extends BaseAdapter {
//	private ImageFetcher mImageFetcher;
	private Context ct;
	private ListView lvContact;
	private ArrayList<HashMap<String, Object>> data;
	public Map<Integer, Boolean> checkedMap;

	public ArrayList<HashMap<String, Object>> getData() {
		return data;
	}
	private Drawable getImage(String touxiang) {
		switch (Integer.parseInt(touxiang)) {
		case 0:
			return ct.getResources().getDrawable(R.drawable.a1);
		case 1:
			return ct.getResources().getDrawable(R.drawable.a2);
		case 2:
			return ct.getResources().getDrawable(R.drawable.a3);
		case 3:
			return ct.getResources().getDrawable(R.drawable.a4);
		case 4:
			return ct.getResources().getDrawable(R.drawable.a5);
		case 5:
			return ct.getResources().getDrawable(R.drawable.a1);
		case 6:
			return ct.getResources().getDrawable(R.drawable.a2);
		case 7:
			return ct.getResources().getDrawable(R.drawable.a3);
		case 8:
			return ct.getResources().getDrawable(R.drawable.a4);
		case 9:
			return ct.getResources().getDrawable(R.drawable.a5);
		default:
			throw new IllegalArgumentException("错误的图片索引");
		}
	}
	public static ArrayList<HashMap<String, Object>> queryContact(Context ct,
			String where, String[] args, String orderColumn) {
		ArrayList<HashMap<String, Object>> data = new ArrayList<HashMap<String, Object>>();

		for (int i = 0; i < 100; i++) {
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("id", i + 1);
			map.put("name", "name" + i);
			map.put("touxiang", (i % 10));
			data.add(map);
		}
		return data;
	}

//	private void setData() {
//		data = queryContact(ct, null, null, null);
//		for (HashMap<String, Object> m : data) {
//			checkedMap.put(Integer.parseInt(m.get("id") + ""), false);
//		}
//	}

	public void setData(List<ContactGroup> friends_cGroup){
		data = new ArrayList<HashMap<String, Object>>();
		ContactGroup cGroup = null;
		int cgrouptype = 0;
		if(friends_cGroup!=null){
			for(int i=0;i<friends_cGroup.size();i++){
				cGroup = (ContactGroup)friends_cGroup.get(i);
				cgrouptype = cGroup.getType();////0 contact,1 group
				if(cgrouptype==0){
					HashMap<String, Object> map = new HashMap<String, Object>();
					map.put("id", cGroup.getUid());
					if(cGroup.getName()!=null
							&& !cGroup.getName().equalsIgnoreCase("null")
							&& cGroup.getName().length()!=0)
						map.put("name", cGroup.getName());
					else
						map.put("name", cGroup.getUsername());
					map.put("touxiang", cGroup.getAvatarPath());
					Log.d("@@@@@@@@@@GroupNew2Activity ContactAdapter@@@@@@@@@@", "@@@@@@@@@@cGroup.getPic()="+cGroup.getPic());
					Log.d("@@@@@@@@@@GroupNew2Activity ContactAdapter@@@@@@@@@@", "@@@@@@@@@@cGroup.getAvatarPath()="+cGroup.getAvatarPath());
					data.add(map);
				}
			}
		}
		for (HashMap<String, Object> m : data) {
			checkedMap.put(Integer.parseInt(m.get("id") + ""), false);
		}
	}
	public ContactAdapter(Context ct) {//,ImageFetcher imageFetcher
		this.ct = ct;
		checkedMap = new HashMap<Integer, Boolean>();
//		mImageFetcher = imageFetcher;
//		setData();
	}

	public Map<Integer, Boolean> getCheckedMap() {
		return checkedMap;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		HashMap<String, Object> map = data.get(position);
		final String name = map.get("name") + "";
		final String touxiang = map.get("touxiang") + "";
		Log.d("@@@@@@@@@@GroupNew2Activity ContactAdapter@@@@@@@@@@", "@@@@@@@@@@touxiang="+touxiang);
		ViewHolder viewHolder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(ct).inflate(
					R.layout.listitem_contactscheckbox, null);//contact_item  item_batch_friend   listitem_contactscheckbox.xml

			viewHolder = new ViewHolder();
			viewHolder.checkBox = (CheckBox) convertView
					.findViewById(R.id.item_checkbox);//contactitem_select_cb  batch_item_check item_checkbox
//			// 分割栏
//			viewHolder.tvCatalog = (TextView) convertView
//					.findViewById(R.id.contactitem_catalog);//contactitem_catalog  letter
			viewHolder.name = (HandyTextView) convertView
					.findViewById(R.id.user_item_htv_name);//item_name friend_name
			viewHolder.touxiang = (FlowView) convertView
					.findViewById(R.id.news_pic);//contactitem_touxiang  friend_avatar
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		boolean isShowTitle = false;

		viewHolder.checkBox.setChecked(checkedMap.get(Integer.parseInt(""
				+ map.get("id"))));
//		if (isShowTitle) {
//			viewHolder.tvCatalog.setVisibility(View.VISIBLE);
//		} else {
//			viewHolder.tvCatalog.setVisibility(View.GONE);
//		}
//		viewHolder.touxiang.setImageDrawable(getImage(touxiang));
//		viewHolder.touxiang.setImageDrawable(getImage("0"));
//		mImageFetcher.loadImage(touxiang.replace("localhost", "10.0.2.2"), viewHolder.touxiang);
		String s1 = "E:/develop/apache-tomcat-6.0.48/webapps/javacenterhome/data/avatar/000/00/00/01_avatar_middle.jpg";
		s1 = touxiang;
		int idx = s1.indexOf("/data/avatar/");
		String path = "";
		if(idx!=-1){
			path = s1.substring(idx+1);
		}
//		mImageFetcher.loadImage(XmppConnectionAdapter.downloadPrefix+path, viewHolder.touxiang);
		ImageLoader.getInstance().displayImage(XmppConnectionAdapter.downloadPrefix.replace("/javacenterhome/","/")+s1, viewHolder.touxiang,SpiceApplication.getInstance().getWholeOptions());
//		mImageFetcher.loadImage(touxiang.replace("localhost", "10.0.2.2"), viewHolder.touxiang);
		viewHolder.name.setText(name);
		return convertView;
	}

	static class ViewHolder {
//		TextView tvCatalog;
		CheckBox checkBox;
//		ImageView touxiang;
		FlowView touxiang;
//		TextView name;
		HandyTextView name;

	}

}