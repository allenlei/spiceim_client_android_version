package com.spice.im.group;

/**
 * 
 * @author buptzhaofang@163.com Nov 12, 2015
 *
 */
public class SortModel2 extends SortModel{
	private String money;
	private boolean check;
	private boolean hava;

	public boolean isHava() {
		return hava;
	}

	public void setHava(boolean hava) {
		this.hava = hava;
	}

	public boolean isCheck() {
		return check;
	}

	public void setCheck(boolean check) {
		this.check = check;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

}
