package com.spice.im.group;

import org.jivesoftware.smack.packet.IQ;

public class MucInviteIQ extends IQ{
    //elementName = mucinviteiq
	//namespace = com:isharemessage:mucinviteiq
    private String id;

    private String apikey;
    
    private String uid;
    
    private String username;
    
    private String tagid;
    
    private String tagname;
    
    private String tagimg;
    
    private String uuid;
    
    private String ids;//拟邀请加入群组的用户uid|username，可以是多个人，每个uid|username之间以逗号,分割
    
    private String hashcode;//apikey+uid+uuid+tagid 使用登录成功后返回的sessionid作为密码3des运算
    
    public MucInviteIQ(){}
    
    @Override
    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append("mucinviteiq").append(" xmlns=\"").append(
                "com:isharemessage:mucinviteiq").append("\">");
        if (id != null) {
            buf.append("<id>").append(id).append("</id>");
        }
        if (apikey != null) {
            buf.append("<apikey>").append(apikey).append("</apikey>");
        }
        if (uid != null) {
            buf.append("<uid>").append(uid).append("</uid>");
        }
        if (username != null) {
            buf.append("<username>").append(username).append("</username>");
        }
        if (tagid != null) {
            buf.append("<tagid>").append(tagid).append("</tagid>");
        }
        if (tagname != null) {
            buf.append("<tagname>").append(tagname).append("</tagname>");
        }
        if (tagimg != null) {
            buf.append("<tagimg>").append(tagimg).append("</tagimg>");
        }
        if (uuid != null) {
            buf.append("<uuid>").append(uuid).append("</uuid>");
        }
        if (ids != null) {
            buf.append("<ids>").append(ids).append("</ids>");
        }
        if (hashcode != null) {
            buf.append("<hashcode>").append(hashcode).append("</hashcode>");
        }
        buf.append("</").append("mucinviteiq").append(">");
        return buf.toString();
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

	public String getApikey() {
		return apikey;
	}
	public void setApikey(String apikey) {
		this.apikey = apikey;
	}
	
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getTagid() {
		return tagid;
	}
	public void setTagid(String tagid) {
		this.tagid = tagid;
	}
	public String getTagname() {
		return tagname;
	}
	public void setTagname(String tagname) {
		this.tagname = tagname;
	}
	public String getTagimg() {
		return tagimg;
	}
	public void setTagimg(String tagimg) {
		this.tagimg = tagimg;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getIds() {
		return ids;
	}
	public void setIds(String ids) {
		this.ids = ids;
	}
	public String getHashcode() {
		return hashcode;
	}
	public void setHashcode(String hashcode) {
		this.hashcode = hashcode;
	}
}
