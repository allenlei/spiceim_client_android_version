package com.spice.im.group;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;


public class MucInviteConfirmIQProvider implements IQProvider{
    public MucInviteConfirmIQProvider() {
    }
    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {

    	MucInviteConfirmIQ mucInviteIQ = new MucInviteConfirmIQ();
        for (boolean done = false; !done;) {
            int eventType = parser.next();
            if (eventType == 2) {
                if ("id".equals(parser.getName())) {
                	mucInviteIQ.setId(parser.nextText());
                }
                if ("apikey".equals(parser.getName())) {
                	mucInviteIQ.setApikey(parser.nextText());
                }
                if ("uid".equals(parser.getName())) {
                	mucInviteIQ.setUid(parser.nextText());
                }
                if ("username".equals(parser.getName())) {
                	mucInviteIQ.setUsername(parser.nextText());
                }
                if ("tagid".equals(parser.getName())) {
                	mucInviteIQ.setTagid(parser.nextText());
                }
                if ("r".equals(parser.getName())) {
                	mucInviteIQ.setR(parser.nextText());
                }
                if ("uuid".equals(parser.getName())) {
                	mucInviteIQ.setUuid(parser.nextText());
                }
                if ("hashcode".equals(parser.getName())) {
                	mucInviteIQ.setHashcode(parser.nextText());
                }
            } else if (eventType == 3
                    && "mucinviteconfirmiq".equals(parser.getName())) {
                done = true;
            }
        }

        return mucInviteIQ;
    }
}
