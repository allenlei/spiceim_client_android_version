package com.spice.im.group;

/**
 * 
 * @author buptzhaofang@163.com Nov 11, 2015 2:39:14 PM
 * 
 */
public class StringConfig {
	public static enum CURRENT_PAGE {
		HOME, ZONE, CASH, INDIVIDUAL
	};

	public static enum VALIDATION_TYPE {
		REGISTER, FINDPSW, PAYMENT, OPENSHOPS, OPENSHOPPER, LOGIN, BIND, SUPPLIERVERIFY
	};

	public static final int SHOW_TOAST = 0x00010001;
	public static final int ACTIVITY_FINISH = 0x00010002;

	public static final String APPID = "1994d28";
	public static final String APPKEY = "a110ee8bf5c39fd4";
	// RLY APPID
	public static final String RLY_APPKEY = "aaf98f89510f639f01511a039e3926b7";
	// RLY_APPKEY
	public static final String RLY_APPTOKEN = "a3f3935f00b289cf848567ca4d8e2497";

	// openweather app id
	public static final String OPENWEATHER_APPID = "c913a4b8e024e8f4";

	// openweather private_key
	public static final String OPENWEATHER_KEY = "610666_SmartWeatherAPI_484ed06";

	// baidu weather apikey
	public static final String BAIDU_API_KEY = "31cf149a3ea299d04c435fcea644a0cb";

	//appid
	//请同时修改  androidmanifest.xml里面，.PayActivityd里的属性<data android:scheme="wxb4ba3c02aa476ea1"/>为新设置的appid
	public static final String WX_APP_ID = "wx97111f68355b018d";

	public static final String QQ_APP_ID = "1101012990";

	public static final String BUGLY_APP_ID = "1400009578";

	public static final String WEIBO_APP_KEY = "3444234334";

	// 微信支付 商户号 MCH_ID
	public static final String WX_MCH_ID = "1250186301";

	//  微信支付 API密钥，在商户平台设置
	public static final String WX_API_KEY = "e10adc3949ba59abbe56e057f20f883e";



	// isetting config
	public static final String SMALL_FREE_SETTINGS = "small_free";
	public static final String COINS_FIRST_SETTING = "coins_first";

	public static final String DEVICE_TYPE = "android";

	// 支付宝 公钥
	public static final String ALIPAY_RSA_PUBLIC = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDDI6d306Q8fIfCOaTXyiUeJHkrIvYISRcc73s3vF1ZT7XN8RNPwJxo8pWaJMmvyTn9N4HQ632qJBVHf8sxHi/fEsraprwCtzvzQETrNRwVxLO5jVmRGi60j8Ue1efIlzPXV9je9mkjzOmdssymZkh2QhUrCmZYI/FCEa3/cNMW0QIDAQAB";
	// 商户私钥，pkcs8格式
//	public static final String ALIPAY_RSA_PRIVATE = "MIICXAIBAAKBgQDhAARzu33eUz5GY+tW3x7BK2mRY5133nfsqPn7b2/afLT6f6o9uEBJ55OMfX1EkmdCA+N7TwrIe8FK9TdLqKorgznvCi55cXOTCLu1mdYW0a0XS6J5VxGYlqCbTwsKIYmgi00kOVUn4/uC2iiRvQDpM4jfQtH+lzr5xR+KuT59iwIDAQABAoGAMfrtAkBd32siZESB3JRHoqVgF/nG2v/CKAIJNb81W5VkJlkBmiA5t7EHZ6fgkcDryxBOEfAm9w32jA4YsGfRFNq0xgMibM0lV1Cu0ZmzojD44+HtvDwRpjWEHIftQMigVU2iCFFlq1wjWT1WGWt69Y3460LZT9tkFg6if0KfK4ECQQD1VOGj/3PuFrowrLtloBvoo/sd9dOzCp/eewC/pH6kgSlkPWiFRFQJPDgU3+Pii76DNXmSlMjQcRffYWy46CahAkEA6sjMaEBBfCaAdlHUdS7FsByvPln+1Z5WQAlumO9yqSa/bUWULsXeIJ3tUBM1OpHY1SPcfuLPxaoHRSXwnN6wqwJANu1PK5mZChhajWVO+5zSQs7b/UQ5pp9dVcFyVHXuyMQGUWQST6wRqJOr9rh93A3vlI7XbkMTht+bOa8lWWzYoQJAfK1Y7tuAat8/RJW7zuahkB4LaX789o94mdToaEpvhyfOa8aRJC1rAcMbrHQmxg2BKd47Tx4+22apKa02EvemtQJBAOsFkkvoDuYxHnuqAcsVd7ohUmj6Cm+ozmdIIG9DatM430XHpPBHNH5oE3/foAAwV9fj5vQSbfBaXgmk70lGldU=";
	public static final String ALIPAY_RSA_PRIVATE = "MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAMdm+h3oCF8g4XYJlTye857G3MCPVj8c6s/G8gy6Hc402gGNFK7ju67s/KuHEMw1Ou98b+A7aEoPdlDU817WXPlqJCD53pK4J46lR38CPNyeGZoGJc12TVyknPiufIBOacpPIPwfkt9ui0gqWekFfaolwRj2HS6Te1OOmoEgnPaVAgMBAAECgYBCIXXcC0MTgY0kn4L+Ct1TEDLQLfl1jvqVFC7cZ7FQ3RhcIVT1t5NcLT6TCIVapGC6YjOcUzeAMGaV4jogfslwXFa5zo/i/gjsRFOHduKJeKblPjTKBQULYAP1CbfK0JNNRWPpnroJVnQcSFtAVG2UfV86/mZri6HSU8+5fPTxoQJBAO83I2vhNm9p/SGEYIkypzZv55MMh44tBXg/dViYWIYfcDusX3WK+EG8O7IyDvZuRqSYWfgXWtATrRrKejsPkQkCQQDVZLPm5TtsfQsuU0N5o/soyUygI2lWgt9jTo3VcJTAFGLCm4bzCVPS0Vk8Anuu/3Ql0n1eSAuLXxmEsLF8ErgtAkBt/7TSDqWxZdN4bh7V9+zcI2khPUm4Hwu817FmI+fRBPPe/MyIiHbQA3aP4mTu/JaPaV81rceclu+quiCqt+XBAkAV6dN3oI6ro2OkjDRfp3FBTPqrbzeABMx/3C9AfeEooG9L4MDI8GpFOe5Z9T9SRzVcNPNe3TMiSs/KCoMPZLS5AkAEn3VXFFItDVpBTU3UQjxwEwX87oKRCA9O9a9xFutOseF/mioIPRxyadlnVd5unVpX/JTYkIDxu4fUEzJ+SEGD";

	// 商户PID
	public static final String PARTNER = "2088911588468051";
	// 商户收款账号
	public static final String SELLER = "1741795896@qq.com";

	// 滴滴开放平台
	public static final String DIDI_APP_ID = "didi636E44655A6E7375686D6973534D77";
	public static final String DIDI_APP_KEY = "7f81df7df6e47b5dd262009fe0f487e9";

}
