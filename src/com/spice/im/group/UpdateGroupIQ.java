package com.spice.im.group;

import org.jivesoftware.smack.packet.IQ;

public class UpdateGroupIQ extends IQ{
    //elementName = updategroupiq
	//namespace = com:isharemessage:updategroupiq
    private String id;

    private String apikey;
    
    private String uid;//当前用户id
    
    private String tagid;//群组id
    
    private String ownerid;//群组用户id
    
    private String groupname;
    
    private String pic;//http://59.52.226.115:8189/javacenterhome/9999/IMG20171208191157.jpg  群头像
    
    private String uuid;
    
    private String hashcode;//apikey+uid+uuid+tagid+ownerid 使用登录成功后返回的sessionid作为密码3des运算
    
    public UpdateGroupIQ(){}
    
    @Override
    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append("updategroupiq").append(" xmlns=\"").append(
                "com:isharemessage:updategroupiq").append("\">");
        if (id != null) {
            buf.append("<id>").append(id).append("</id>");
        }
        if (apikey != null) {
            buf.append("<apikey>").append(apikey).append("</apikey>");
        }
        if (uid != null) {
            buf.append("<uid>").append(uid).append("</uid>");
        }
        if (tagid != null) {
            buf.append("<tagid>").append(tagid).append("</tagid>");
        }
        if (ownerid != null) {
            buf.append("<ownerid>").append(ownerid).append("</ownerid>");
        }
        if (groupname != null) {
            buf.append("<groupname>").append(groupname).append("</groupname>");
        }
        if (pic != null) {
            buf.append("<pic>").append(pic).append("</pic>");
        }
        if (uuid != null) {
            buf.append("<uuid>").append(uuid).append("</uuid>");
        }
        if (hashcode != null) {
            buf.append("<hashcode>").append(hashcode).append("</hashcode>");
        }
        buf.append("</").append("updategroupiq").append(">");
        return buf.toString();
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

	public String getApikey() {
		return apikey;
	}
	public void setApikey(String apikey) {
		this.apikey = apikey;
	}
	
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getTagid() {
		return tagid;
	}
	public void setTagid(String tagid) {
		this.tagid = tagid;
	}
	public String getOwnerid() {
		return ownerid;
	}
	public void setOwnerid(String ownerid) {
		this.ownerid = ownerid;
	}
	public String getGroupname() {
		return groupname;
	}
	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}
	public String getPic() {
		return pic;
	}
	public void setPic(String pic) {
		this.pic = pic;
	}
	
	
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getHashcode() {
		return hashcode;
	}
	public void setHashcode(String hashcode) {
		this.hashcode = hashcode;
	}
}
