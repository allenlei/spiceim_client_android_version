package com.spice.im.group;

import org.jivesoftware.smack.packet.IQ;

public class MucCreateIQ extends IQ{
    //elementName = muccreateiq
	//namespace = com:isharemessage:muccreateiq
    private String id;

    private String apikey;
    
    private String uid;
    
    private String username;
    
    private String key = "";
    
    private String pic;
    
    private String fieldid;
    
    private String uuid;
    
    private String ids;//拟邀请加入群组的用户uid|username，可以是多个人，每个uid|username之间以逗号,分割
    
    private String hashcode;//apikey+uid+uuid+key 使用登录成功后返回的sessionid作为密码3des运算
    
    public MucCreateIQ(){}
    
    @Override
    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append("muccreateiq").append(" xmlns=\"").append(
                "com:isharemessage:muccreateiq").append("\">");
        if (id != null) {
            buf.append("<id>").append(id).append("</id>");
        }
        if (apikey != null) {
            buf.append("<apikey>").append(apikey).append("</apikey>");
        }
        if (uid != null) {
            buf.append("<uid>").append(uid).append("</uid>");
        }
        if (username != null) {
            buf.append("<username>").append(username).append("</username>");
        }
        if (key != null) {
            buf.append("<key>").append(key).append("</key>");
        }
        if (pic != null) {
            buf.append("<pic>").append(pic).append("</pic>");
        }
        if (fieldid != null) {
            buf.append("<fieldid>").append(fieldid).append("</fieldid>");
        }
        if (uuid != null) {
            buf.append("<uuid>").append(uuid).append("</uuid>");
        }
        if (ids != null) {
            buf.append("<ids>").append(ids).append("</ids>");
        }        
        if (hashcode != null) {
            buf.append("<hashcode>").append(hashcode).append("</hashcode>");
        }
        buf.append("</").append("muccreateiq").append(">");
        return buf.toString();
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

	public String getApikey() {
		return apikey;
	}
	public void setApikey(String apikey) {
		this.apikey = apikey;
	}
	
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getPic() {
		return pic;
	}
	public void setPic(String pic) {
		this.pic = pic;
	}
	public String getFieldid() {
		return fieldid;
	}
	public void setFieldid(String fieldid) {
		this.fieldid = fieldid;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getIds() {
		return ids;
	}
	public void setIds(String ids) {
		this.ids = ids;
	}	
	public String getHashcode() {
		return hashcode;
	}
	public void setHashcode(String hashcode) {
		this.hashcode = hashcode;
	}
}
