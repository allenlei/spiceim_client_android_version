package com.spice.im.group;

import java.io.Serializable;

/**
 * 
 * @author buptzhaofang@163.com Nov 12, 2015
 *
 */
public class SortModel implements Serializable{

	private static final long serialVersionUID = -5005442024177965890L;
	private String uid;
	private String name;   
	private String sortLetters;
	private String imgUrl;
	private boolean hasAuth;

	public void setHasAuth(boolean auth) {
		this.hasAuth = auth;
	}

	public boolean getAuth() {
		return this.hasAuth;
	}

	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSortLetters() {
		return sortLetters;
	}
	public void setSortLetters(String sortLetters) {
		this.sortLetters = sortLetters;
	}

	private Object mObject;

	public void setObject(Object object) {
		this.mObject = object;
	}

	public Object getObject() {
		return mObject;
	}
}
