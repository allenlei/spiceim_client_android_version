package com.spice.im.group;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;

//import com.feiyangweilai.base.entity.PublishClassifyItem;
//import com.feiyangweilai.base.enviroment.Environment;
//import com.feiyangweilai.base.net.RequestFinishCallback;
//import com.feiyangweilai.base.net.RequestServerManager;
//import com.feiyangweilai.base.net.result.PublishClassifyListResult;
//import com.feiyangweilai.base.net.specialrequest.RequestClassifyList;
//import com.ishowtu.hairfamily.R;


import java.util.List;

import com.spice.im.R;

/**
 * 选择所属分类对话框
 * 
 * @autor:Bin
 * @version:1.0
 * @contact 15105695536
 * @created:2015-12-19 下午4:40:24
 **/
public class DeleteDialog extends Dialog {
	
	private Context mContext = null;
		
	private TextView delete;
	private View.OnClickListener listener;
	
	public DeleteDialog(Context context,View.OnClickListener listener) {
		super(context, R.style.MyAlertDialog);
		this.mContext = context;
		this.listener = listener;
		
		LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		getWindow().setGravity(Gravity.CENTER);
		View view = LayoutInflater.from(context).inflate(R.layout.delete_layout, null);
		setContentView(view, lp);
		setCancelable(true);
		delete = (TextView) view.findViewById(R.id.delete);
		delete.setOnClickListener(listener);
	}

	public DeleteDialog(Context context,View.OnClickListener listener, String text) {
		super(context, R.style.MyAlertDialog);
		this.mContext = context;
		this.listener = listener;

		LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		getWindow().setGravity(Gravity.CENTER);
		View view = LayoutInflater.from(context).inflate(R.layout.delete_layout, null);
		setContentView(view, lp);
		((TextView)findViewById(R.id.delete)).setText(text);
		setCancelable(true);
		delete = (TextView) view.findViewById(R.id.delete);
		delete.setOnClickListener(listener);
	}
	

}