package com.spice.im.group;

import android.graphics.Bitmap;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions.Builder;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.spice.im.R;

/**
 * ImageLoader的Options类
 * 
 * @author buptzhaofang@163.com Aug 6, 2015 8:47:35 AM
 *
 */
public class Options {
	public static DisplayImageOptions getOptions() {
		Builder options = new Builder()
				.showImageOnLoading(R.drawable.default_load_img)
				.showImageForEmptyUri(R.drawable.default_load_img)
				.showImageOnFail(R.drawable.default_load_img).cacheInMemory(true)
				.cacheOnDisk(true).considerExifParams(true)
				.imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.resetViewBeforeLoading(true)
				.displayer(new FadeInBitmapDisplayer(100));

		return options.build();
	}
	
	
	public static DisplayImageOptions getZonePicOptions() {
		Builder options = new Builder()
//				.showImageOnLoading(R.drawable.default_load_img)
//				.showImageForEmptyUri(R.drawable.default_load_img)
//				.showImageOnFail(R.drawable.default_load_img)
				.cacheInMemory(true)
				.cacheOnDisk(true)
				.considerExifParams(true)
				.imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.resetViewBeforeLoading(true);
//				.displayer(new FadeInBitmapDisplayer(100))

		return options.build();
	}

	public static DisplayImageOptions getOptions(int roundPixel) {
		Builder options = new Builder()
				.showImageOnLoading(R.drawable.default_load_img)
				.showImageForEmptyUri(R.drawable.default_avatar)
				.showImageOnFail(R.drawable.default_load_img).cacheInMemory(true)
				.cacheOnDisk(true).considerExifParams(true)
				.imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.resetViewBeforeLoading(true)
				.displayer(new FadeInBitmapDisplayer(100));
		options.displayer(new RoundedBitmapDisplayer(roundPixel));

		return options.build();
	}
	
	public static DisplayImageOptions getOptions(int roundPixel, int imageRes) {
		Builder options = new Builder()
				.showImageOnLoading(imageRes)
				.showImageForEmptyUri(imageRes)
				.showImageOnFail(imageRes).cacheInMemory(true)
				.cacheOnDisk(true).considerExifParams(true)
				.imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.resetViewBeforeLoading(true)
				.displayer(new FadeInBitmapDisplayer(100));
		options.displayer(new RoundedBitmapDisplayer(roundPixel));

		return options.build();
	}

	/**
	 * 自定义目标图片
	 * 
	 * @param needRound
	 * @param roundPixel
	 * @param resourceId
	 * @return
	 */
	public static DisplayImageOptions getOptions(boolean needRound,
			int roundPixel, int resourceId) {
		Builder options = new Builder()
				.showImageOnLoading(resourceId)
				.showImageForEmptyUri(resourceId).showImageOnFail(resourceId)
				.cacheInMemory(true).cacheOnDisk(true).considerExifParams(true)
				.imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.resetViewBeforeLoading(true)
				.displayer(new FadeInBitmapDisplayer(100));
		if (needRound)
			options.displayer(new RoundedBitmapDisplayer(roundPixel));

		return options.build();
	}
}
