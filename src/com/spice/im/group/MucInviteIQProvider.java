package com.spice.im.group;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;


public class MucInviteIQProvider implements IQProvider{
    public MucInviteIQProvider() {
    }
    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {

    	MucInviteIQ mucInviteIQ = new MucInviteIQ();
        for (boolean done = false; !done;) {
            int eventType = parser.next();
            if (eventType == 2) {
                if ("id".equals(parser.getName())) {
                	mucInviteIQ.setId(parser.nextText());
                }
                if ("apikey".equals(parser.getName())) {
                	mucInviteIQ.setApikey(parser.nextText());
                }
                if ("uid".equals(parser.getName())) {
                	mucInviteIQ.setUid(parser.nextText());
                }
                if ("username".equals(parser.getName())) {
                	mucInviteIQ.setUsername(parser.nextText());
                }
                if ("tagid".equals(parser.getName())) {
                	mucInviteIQ.setTagid(parser.nextText());
                }
                if ("tagname".equals(parser.getName())) {
                	mucInviteIQ.setTagname(parser.nextText());
                }
                if ("tagimg".equals(parser.getName())) {
                	mucInviteIQ.setTagimg(parser.nextText());
                }
                if ("uuid".equals(parser.getName())) {
                	mucInviteIQ.setUuid(parser.nextText());
                }
                if ("ids".equals(parser.getName())) {
                	mucInviteIQ.setIds(parser.nextText());
                }
                if ("hashcode".equals(parser.getName())) {
                	mucInviteIQ.setHashcode(parser.nextText());
                }
            } else if (eventType == 3
                    && "mucinviteiq".equals(parser.getName())) {
                done = true;
            }
        }

        return mucInviteIQ;
    }
}
