/**
 * Copyright (C) 2013-2014 EaseMob Technologies. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.spice.im.group;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

public class EaseExpandGridView extends GridView {
//implements
//android.widget.AdapterView.OnItemClickListener{

    public EaseExpandGridView(Context context) {
        super(context);
    }

    public EaseExpandGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);
    }

//    @Override
//    public boolean dispatchTouchEvent(MotionEvent ev) {
//        if (ev.getAction() == MotionEvent.ACTION_MOVE) {
//            return true;
//        }
//        return super.dispatchTouchEvent(ev);
//    }
//	@Override
//	public void onItemClick(AdapterView<?> parent, View view, int position,
//			long id)
//	{
////		if (isInDeleteMode) {
////			if(position>=memberItems.size()){
////				showCustomToast("当为删除状态时，单击空白区域，回到正常状态");
////                isInDeleteMode = false;
////                this.getAdapter().notifyDataSetChanged();
////			}
////		}
//		Toast.makeText(this.getContext(), "222当为删除状态时，单击空白区域，回到正常状态", Toast.LENGTH_SHORT).show();
//	}

}
