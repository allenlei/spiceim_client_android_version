package com.spice.im.group;

import org.jivesoftware.smack.packet.IQ;

public class ViewGroupMemberIQ extends IQ{
    //elementName = viewgroupmemberiq
	//namespace = com:isharemessage:viewgroupmemberiq
    private String id;

    private String apikey;
    
    private String uid;
    
    private String tagid = "";
    
    private String page = "";
    
    private String keystr = "";
    
    private String uuid;
    
    private String hashcode;//apikey+uid+uuid 使用登录成功后返回的sessionid作为密码3des运算
    
    public ViewGroupMemberIQ(){}
    
    @Override
    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append("viewgroupmemberiq").append(" xmlns=\"").append(
                "com:isharemessage:viewgroupmemberiq").append("\">");
        if (id != null) {
            buf.append("<id>").append(id).append("</id>");
        }
        if (apikey != null) {
            buf.append("<apikey>").append(apikey).append("</apikey>");
        }
        if (uid != null) {
            buf.append("<uid>").append(uid).append("</uid>");
        }
        if (tagid != null) {
            buf.append("<tagid>").append(tagid).append("</tagid>");
        }
        if (page != null) {
            buf.append("<page>").append(page).append("</page>");
        }
        if (keystr != null) {
            buf.append("<keystr>").append(keystr).append("</keystr>");
        }
        if (uuid != null) {
            buf.append("<uuid>").append(uuid).append("</uuid>");
        }
        if (hashcode != null) {
            buf.append("<hashcode>").append(hashcode).append("</hashcode>");
        }
        buf.append("</").append("viewgroupmemberiq").append(">");
        return buf.toString();
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

	public String getApikey() {
		return apikey;
	}
	public void setApikey(String apikey) {
		this.apikey = apikey;
	}
	
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	
	public String getTagid() {
		return tagid;
	}
	public void setTagid(String tagid) {
		this.tagid = tagid;
	}
	
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	
	public String getKeystr() {
		return keystr;
	}
	public void setKeystr(String keystr) {
		this.keystr = keystr;
	}
	
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getHashcode() {
		return hashcode;
	}
	public void setHashcode(String hashcode) {
		this.hashcode = hashcode;
	}
}
