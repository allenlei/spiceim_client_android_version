package com.spice.im.group;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;


public class MucCreateIQResponseProvider implements IQProvider{
	public MucCreateIQResponseProvider(){
		
	}
    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {

    	MucCreateIQResponse mucCreateIQResponse = new MucCreateIQResponse();
        for (boolean done = false; !done;) {
            int eventType = parser.next();
            if (eventType == 2) {
                if ("id".equals(parser.getName())) {
                	mucCreateIQResponse.setId(parser.nextText());
                }
                if ("apikey".equals(parser.getName())) {
                	mucCreateIQResponse.setApikey(parser.nextText());
                }
                if ("retcode".equals(parser.getName())) {
                	mucCreateIQResponse.setRetcode(parser.nextText());
                }
                if ("memo".equals(parser.getName())) {
                	mucCreateIQResponse.setMemo(parser.nextText());
                }
            } else if (eventType == 3
                    && "muccreateiq".equals(parser.getName())) {
                done = true;
            }
        }

        return mucCreateIQResponse;
    }
}

