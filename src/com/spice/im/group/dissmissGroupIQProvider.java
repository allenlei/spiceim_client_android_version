package com.spice.im.group;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;


public class dissmissGroupIQProvider implements IQProvider{
    public dissmissGroupIQProvider() {
    }
    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {

    	dismissGroupIQ exitGroupIQ = new dismissGroupIQ();
        for (boolean done = false; !done;) {
            int eventType = parser.next();
            if (eventType == 2) {
                if ("id".equals(parser.getName())) {
                	exitGroupIQ.setId(parser.nextText());
                }
                if ("apikey".equals(parser.getName())) {
                	exitGroupIQ.setApikey(parser.nextText());
                }
                if ("uid".equals(parser.getName())) {
                	exitGroupIQ.setUid(parser.nextText());
                }
                if ("tagid".equals(parser.getName())) {
                	exitGroupIQ.setTagid(parser.nextText());
                }
                if ("ownerid".equals(parser.getName())) {
                	exitGroupIQ.setOwnerid(parser.nextText());
                }
                if ("uuid".equals(parser.getName())) {
                	exitGroupIQ.setUuid(parser.nextText());
                }
                if ("hashcode".equals(parser.getName())) {
                	exitGroupIQ.setHashcode(parser.nextText());
                }
            } else if (eventType == 3
                    && "dismissgroupiq".equals(parser.getName())) {
                done = true;
            }
        }

        return exitGroupIQ;
    }
}
