package com.spice.im.group;

import java.util.ArrayList;

import com.dodowaterfall.widget.FlowView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.spice.im.R;
import com.spice.im.SpiceApplication;
import com.spice.im.group.GroupNew2Activity.CheckedImg;
import com.spice.im.utils.ImageFetcher;
import com.stb.isharemessage.service.XmppConnectionAdapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;



public class CheckedImgAdapter extends BaseAdapter {
//	private ImageFetcher mImageFetcher;
	private Context ct;
	private ArrayList<CheckedImg> data;
	
	private String defaultimg;
	
	public void setDefaultimg(String mDefalutimg){
		mDefalutimg = defaultimg;
		 Log.e("================000CheckedImgAdapter.java ================", "++++++++++++++defaultimg++++++++++++++"+defaultimg);
	}
	
	public String getDefaultimg(){
		return defaultimg;
	}

	public ArrayList<CheckedImg> getData() {
		return data;
	}

	public CheckedImgAdapter(Context ct, ArrayList<CheckedImg> data) {//,ImageFetcher imageFetcher
		this.ct = ct;
		this.data = data;
//		mImageFetcher = imageFetcher;
	}

	public void addImg(String name, String touxiang, String id) {
		CheckedImg map = new CheckedImg();
		map.name = name;
		map.touxiang = touxiang;
		map.id = id;
		data.add(map);
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		CheckedImg map = data.get(position);
		final String touxiang = map.touxiang;
		ViewHolder viewHolder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(ct).inflate(
					R.layout.checked_contact_item, null);

			viewHolder = new ViewHolder();
			viewHolder.touxiang = (FlowView) convertView
					.findViewById(R.id.contactitem_touxiang);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		viewHolder.touxiang.set_url("newgroup");
//		if (map.id.equals("default")){
//			String s1 = getDefaultimg();
//			Log.e("================111CheckedImgAdapter.java ================", "++++++++++++++defaultimg++++++++++++++"+s1);
//			if(s1!=null
//					&& !s1.equalsIgnoreCase("null")
//					&& s1.length()!=0){
//				int idx = s1.indexOf("/data/avatar/");
//				String path = "";
//				if(idx!=-1){
//					path = s1.substring(idx+1);
//				}
//				mImageFetcher.loadImage(XmppConnectionAdapter.downloadPrefix+path, viewHolder.touxiang);
//			}else
//			viewHolder.touxiang.setImageDrawable(ct.getResources().getDrawable(
//					R.drawable.empty_photo4));
//		}else{ 
////			viewHolder.touxiang.setImageDrawable(getImage(touxiang));
////			viewHolder.touxiang.setImageDrawable(getImage("0"));
			String s1 = "E:/develop/apache-tomcat-6.0.48/webapps/javacenterhome/data/avatar/000/00/00/01_avatar_middle.jpg";
			s1 = touxiang;
			int idx = s1.indexOf("/data/avatar/");
			String path = "";
			if(idx!=-1){
				path = s1.substring(idx+1);
			}
//			mImageFetcher.loadImage(XmppConnectionAdapter.downloadPrefix+path, viewHolder.touxiang);
			ImageLoader.getInstance().displayImage(XmppConnectionAdapter.downloadPrefix.replace("/javacenterhome/","/")+s1, viewHolder.touxiang,SpiceApplication.getInstance().getWholeOptions());
//		}
		viewHolder.id = map.id;
		return convertView;
	}

	private Drawable getImage(String touxiang) {
		switch (Integer.parseInt(touxiang)) {
		case 0:
			return ct.getResources().getDrawable(R.drawable.a1);
		case 1:
			return ct.getResources().getDrawable(R.drawable.a2);
		case 2:
			return ct.getResources().getDrawable(R.drawable.a3);
		case 3:
			return ct.getResources().getDrawable(R.drawable.a4);
		case 4:
			return ct.getResources().getDrawable(R.drawable.a5);
		case 5:
			return ct.getResources().getDrawable(R.drawable.a1);
		case 6:
			return ct.getResources().getDrawable(R.drawable.a2);
		case 7:
			return ct.getResources().getDrawable(R.drawable.a3);
		case 8:
			return ct.getResources().getDrawable(R.drawable.a4);
		case 9:
			return ct.getResources().getDrawable(R.drawable.a5);
		default:
			throw new IllegalArgumentException("错误的图片索引");
		}
	}

	static class ViewHolder {
		String id;
		FlowView touxiang;
	}
}