package com.spice.im.group;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;


public class ViewGroupMemberIQProvider implements IQProvider{
    public ViewGroupMemberIQProvider() {
    }
    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {

    	ViewGroupMemberIQ viewGroupMemberIQ = new ViewGroupMemberIQ();
        for (boolean done = false; !done;) {
            int eventType = parser.next();
            if (eventType == 2) {
                if ("id".equals(parser.getName())) {
                	viewGroupMemberIQ.setId(parser.nextText());
                }
                if ("apikey".equals(parser.getName())) {
                	viewGroupMemberIQ.setApikey(parser.nextText());
                }
                if ("uid".equals(parser.getName())) {
                	viewGroupMemberIQ.setUid(parser.nextText());
                }
                if ("tagid".equals(parser.getName())) {
                	viewGroupMemberIQ.setTagid(parser.nextText());
                }
                if ("page".equals(parser.getName())) {
                	viewGroupMemberIQ.setPage(parser.nextText());
                }
                if ("keystr".equals(parser.getName())) {
                	viewGroupMemberIQ.setKeystr(parser.nextText());
                }
                if ("uuid".equals(parser.getName())) {
                	viewGroupMemberIQ.setUuid(parser.nextText());
                }
                if ("hashcode".equals(parser.getName())) {
                	viewGroupMemberIQ.setHashcode(parser.nextText());
                }
            } else if (eventType == 3
                    && "viewgroupmemberiq".equals(parser.getName())) {
                done = true;
            }
        }

        return viewGroupMemberIQ;
    }
}
