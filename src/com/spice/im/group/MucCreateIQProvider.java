package com.spice.im.group;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;


public class MucCreateIQProvider implements IQProvider{
    public MucCreateIQProvider() {
    }
    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {

    	MucCreateIQ mucCreateIQ = new MucCreateIQ();
        for (boolean done = false; !done;) {
            int eventType = parser.next();
            if (eventType == 2) {
                if ("id".equals(parser.getName())) {
                	mucCreateIQ.setId(parser.nextText());
                }
                if ("apikey".equals(parser.getName())) {
                	mucCreateIQ.setApikey(parser.nextText());
                }
                if ("uid".equals(parser.getName())) {
                	mucCreateIQ.setUid(parser.nextText());
                }
                if ("username".equals(parser.getName())) {
                	mucCreateIQ.setUsername(parser.nextText());
                }
                if ("key".equals(parser.getName())) {
                	mucCreateIQ.setKey(parser.nextText());
                }
                if ("pic".equals(parser.getName())) {
                	mucCreateIQ.setPic(parser.nextText());
                }
                if ("fieldid".equals(parser.getName())) {
                	mucCreateIQ.setFieldid(parser.nextText());
                }
                if ("uuid".equals(parser.getName())) {
                	mucCreateIQ.setUuid(parser.nextText());
                }
                if ("ids".equals(parser.getName())) {
                	mucCreateIQ.setIds(parser.nextText());
                }
                if ("hashcode".equals(parser.getName())) {
                	mucCreateIQ.setHashcode(parser.nextText());
                }
            } else if (eventType == 3
                    && "muccreateiq".equals(parser.getName())) {
                done = true;
            }
        }

        return mucCreateIQ;
    }
}
