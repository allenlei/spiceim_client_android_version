package com.spice.im.group;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.jivesoftware.smack.packet.IQ;

public class dismissGroupIQResponse extends IQ{
    private String id;

    private String apikey;

    private String retcode;//0000 success,0001 false（用户名或密码错误）,0002 false(hash校验失败),9999
    
    private String memo;//状态描述:登录成功，登录失败（用户名或密码错误）,登录失败（原因）
    
    public dismissGroupIQResponse() {
    }
    
    @Override
    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append("dismissgroupiq").append(" xmlns=\"").append(
                "com:isharemessage:dismissgroupiq").append("\">");
        if (id != null) {
            buf.append("<id>").append(id).append("</id>");
        }
        if (apikey != null) {
            buf.append("<apikey>").append(apikey).append("</apikey>");
        }
        if(retcode!=null){
        	buf.append("<retcode>").append(retcode).append("</retcode>");
        }
        if(memo!=null){
        	buf.append("<memo>").append(memo).append("</memo>");
        }
        buf.append("</").append("dismissgroupiq").append(">");
        return buf.toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

	public String getRetcode() {
		return retcode;
	}
	public void setRetcode(String retcode) {
		this.retcode = retcode;
	}

	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
}
