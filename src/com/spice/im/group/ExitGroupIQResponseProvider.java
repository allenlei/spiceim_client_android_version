package com.spice.im.group;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;


public class ExitGroupIQResponseProvider implements IQProvider{
	public ExitGroupIQResponseProvider(){
		
	}
    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {

    	ExitGroupIQResponse exitGroupIQResponse = new ExitGroupIQResponse();
        for (boolean done = false; !done;) {
            int eventType = parser.next();
            if (eventType == 2) {
                if ("id".equals(parser.getName())) {
                	exitGroupIQResponse.setId(parser.nextText());
                }
                if ("apikey".equals(parser.getName())) {
                	exitGroupIQResponse.setApikey(parser.nextText());
                }
                if ("retcode".equals(parser.getName())) {
                	exitGroupIQResponse.setRetcode(parser.nextText());
                }
                if ("memo".equals(parser.getName())) {
                	exitGroupIQResponse.setMemo(parser.nextText());
                }
            } else if (eventType == 3
                    && "exitgroupiq".equals(parser.getName())) {
                done = true;
            }
        }

        return exitGroupIQResponse;
    }
}

