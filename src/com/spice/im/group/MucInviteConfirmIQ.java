package com.spice.im.group;

import org.jivesoftware.smack.packet.IQ;

public class MucInviteConfirmIQ extends IQ{
    //elementName = mucinviteconfirmiq
	//namespace = com:isharemessage:mucinviteconfirmiq
    private String id;

    private String apikey;
    
    private String uid;
    
    private String username;
    
    private String tagid;
    
    private String r;//用户确认加入r=1或拒绝加入r=0群组
    
    private String uuid;
    
    private String hashcode;//apikey+uid+uuid+tagid 使用登录成功后返回的sessionid作为密码3des运算
    
    public MucInviteConfirmIQ(){}
    
    @Override
    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append("mucinviteconfirmiq").append(" xmlns=\"").append(
                "com:isharemessage:mucinviteconfirmiq").append("\">");
        if (id != null) {
            buf.append("<id>").append(id).append("</id>");
        }
        if (apikey != null) {
            buf.append("<apikey>").append(apikey).append("</apikey>");
        }
        if (uid != null) {
            buf.append("<uid>").append(uid).append("</uid>");
        }
        if (username != null) {
            buf.append("<username>").append(username).append("</username>");
        }
        if (tagid != null) {
            buf.append("<tagid>").append(tagid).append("</tagid>");
        }
        if (r != null) {
            buf.append("<r>").append(r).append("</r>");
        }
        if (uuid != null) {
            buf.append("<uuid>").append(uuid).append("</uuid>");
        }
        if (hashcode != null) {
            buf.append("<hashcode>").append(hashcode).append("</hashcode>");
        }
        buf.append("</").append("mucinviteconfirmiq").append(">");
        return buf.toString();
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

	public String getApikey() {
		return apikey;
	}
	public void setApikey(String apikey) {
		this.apikey = apikey;
	}
	
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getTagid() {
		return tagid;
	}
	public void setTagid(String tagid) {
		this.tagid = tagid;
	}
	public String getR() {
		return r;
	}
	public void setR(String r) {
		this.r = r;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getHashcode() {
		return hashcode;
	}
	public void setHashcode(String hashcode) {
		this.hashcode = hashcode;
	}
}
