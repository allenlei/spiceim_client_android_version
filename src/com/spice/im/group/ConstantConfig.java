package com.spice.im.group;

/**
 *
 * @author buptzhaofang@163.com Nov 18, 2015 3:42:19 PM
 *
 */
public class ConstantConfig {
	public static boolean isShowCustom=true;// 是否显示全局自定义功能
	
	public static String currentCity="";
	
	
//	public static final String user_code=UrlConfig.host+"qr/u/";
	
//	public static final String group_code=UrlConfig.host+"qr/g/";

	public static final String weixin_code = "http://weixin.qq.com";
	
	public static final String http_code="http://";
	
	public static final String https_code="https://";

	// 环信
	public static final String NEW_FRIENDS_USERNAME = "item_new_friends";
	public static final String GROUP_USERNAME = "item_groups";
	public static final String CHAT_ROOM = "item_chatroom";
	public static final String MESSAGE_ATTR_IS_VOICE_CALL = "is_voice_call";
	public static final String MESSAGE_ATTR_IS_VIDEO_CALL = "is_video_call";
	public static final String ACCOUNT_REMOVED = "account_removed";
	public static final String CHAT_ROBOT = "item_robots";
	public static final String MESSAGE_ATTR_ROBOT_MSGTYPE = "msgtype";

	public static final String FRIENDLIST_UPDATE_ACTION = "com.feiyangweilai.friendlist_update";

	public static final int AVATAR_RADIUS = 3;
	public static final int AVATAR_RADIUS_10 = 10;
	public static final int AVATAR_RADIUS_20 = 20;
	public static final int AVATAR_RADIUS_30 = 30;
	public static final int AVATAR_RADIUS_50 = 50;
}
