package com.spice.im.utils;

//package cn.com.ctbri.pp2transitcard.tools.utils;



import java.util.ArrayList;

import com.spice.im.R;
import com.spice.im.utils.MyDialog.MyDialogListener;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;
//import cn.com.ctbri.pp2transitcard.tools.R;
//import cn.com.ctbri.pp2transitcard.tools.utils.MyDialog.MyDialogListener;
import android.widget.AdapterView.OnItemClickListener;

/**
 * 提供各种dialog的使用
 * 
 * @author poet
 *
 */
public class DialogUtil {
	private DialogUtil() {
	};

	public static final int DIALOG_PROGRESS = 0;
	public static final int DIALOG_ALERT = 1;

//	public static Dialog getAlertDialog(){
//		AlertDialog.Builder;
//	}
	
	public static Dialog getDialog(Activity context, int type) {
		switch (type) {
		case DIALOG_PROGRESS:
			// 返回进度条对话框
			//Dialog dialog = new WaitProgressDialog(context, R.style.MyDialog);
			Dialog dialog = new NewWaitProgressDialog(context, R.style.MyDialog,"");
			dialog.setCancelable(false);
			return dialog;
		default:
			break;
		}
		return null;
	}

	public static MyAlertDialog getMyAlertDialog(String title, String[] itemList,
			Activity activity, MyAlertDialog.MyDialogListener listener, int type,OnItemClickListener contentListener) {
		MyAlertDialog mdialog = new MyAlertDialog(activity, R.style.MyDialog, listener,
				type);
		mdialog.setTitleTxt(title);
		mdialog.MyDialogSetMsg(itemList);
		mdialog.setContentListener(contentListener);
		return mdialog;
	}
	
	public static MyAlertDialog getMyAlertDialog(String title, String[] itemList,
			Activity activity, MyAlertDialog.MyDialogListener listener, int type) {
		MyAlertDialog mdialog = new MyAlertDialog(activity, R.style.MyDialog, listener,
				type);
		mdialog.setTitleTxt(title);
		mdialog.MyDialogSetMsg(itemList);
//		mdialog.setContentListener(contentListener);
		return mdialog;
	}
	
	public static MyDialog getMyDialog(String title, CharSequence msg,
			Activity activity, MyDialogListener listener, int type) {
		MyDialog mdialog = new MyDialog(activity, R.style.MyDialog, listener,
				type);
		mdialog.setTitleTxt(title);
		mdialog.MyDialogSetMsg(msg);
		return mdialog;
	}
	
	public static MyDialogPopWinSelect getMyDialogPopWinSelect(String title, CharSequence msg,
			Activity activity, MyDialogPopWinSelect.MyDialogListener listener, int type , ArrayList aClassSections, String CurrentUid ,String procinsid) {
		MyDialogPopWinSelect mdialog = new MyDialogPopWinSelect(activity, R.style.MyDialog, listener,
				type , aClassSections,CurrentUid,procinsid);
		mdialog.setTitleTxt(title);
		mdialog.MyDialogSetMsg(msg);
		return mdialog;
	}
	
	public static MyDialogPopWinGroup getMyDialogPopWinGroup(String title, CharSequence msg,
			Activity activity, MyDialogPopWinGroup.MyDialogListener listener, int type , String CurrentUid ,String procinsid, String groupName,String tagimg) {
		MyDialogPopWinGroup mdialog = new MyDialogPopWinGroup(activity, R.style.MyDialog, listener,
				type , CurrentUid,procinsid,groupName,tagimg);
		mdialog.setTitleTxt(title);
		mdialog.MyDialogSetMsg(msg);
		return mdialog;
	}
	
	public static MyDialogPopWinProfile getMyDialogPopWinProfile(String title, CharSequence msg,
			Activity activity, MyDialogPopWinProfile.MyDialogListener listener, int type , String CurrentUid ,String OtherUid,String CurrentName,
		       String CurrentNote,
		       String CurrentSex,
		       String CurrentBirthyear,
		       String CurrentBirthmonth,
		       String CurrentBirthday) {
		MyDialogPopWinProfile mdialog = new MyDialogPopWinProfile(activity, R.style.MyDialog, listener,
				type , CurrentUid,OtherUid,CurrentName,
			       CurrentNote,
			       CurrentSex,
			       CurrentBirthyear,
			       CurrentBirthmonth,
			       CurrentBirthday);
		mdialog.setTitleTxt(title);
		mdialog.MyDialogSetMsg(msg);
		return mdialog;
	}
	
	public static NewWaitProgressDialog getNewWaitDialog( String msg,
			Activity activity) {
		NewWaitProgressDialog dd =  new NewWaitProgressDialog(activity, R.style.MyDialog, msg);
		dd.setCancelable(false);
		return dd ;
	}
	
	  public static class NewWaitProgressDialog extends Dialog {

			private TextView waitingDialogText;
			private CharSequence charSequence = "处理中...";

			public NewWaitProgressDialog(Context context, int theme,
					CharSequence charSequence) {
				super(context, theme);
				this.charSequence = charSequence;
			}

			public NewWaitProgressDialog(Context context, CharSequence charSequence) {
				super(context);
				this.charSequence = charSequence;
			}

			@Override
			protected void onCreate(Bundle savedInstanceState) {
				super.onCreate(savedInstanceState);
				// View root = findViewById(R.layout.view_progress_dialog);
				setContentView(R.layout.view_progress_dialog_new);
				waitingDialogText = (TextView) findViewById(R.id.dialog_waiting_txt);
				AnimationDrawable anim = (AnimationDrawable) ((ImageView) findViewById(R.id.dialog_waiting_img))
						.getDrawable();
				setText(charSequence);
				anim.start();
			}

			public void setText(CharSequence charSequence) {
				this.charSequence = charSequence;
				if (waitingDialogText != null && !TextUtils.isEmpty(charSequence)) {
					waitingDialogText.setText(charSequence);
				}
			}

		}
}

class WaitProgressDialog extends Dialog {

	public WaitProgressDialog(Context context, boolean cancelable,
			OnCancelListener cancelListener) {
		super(context, cancelable, cancelListener);
	}

	public WaitProgressDialog(Context context, int theme) {
		super(context, theme);
	}

	public WaitProgressDialog(Context context) {
		super(context);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_progress_dialog);
	}

}



