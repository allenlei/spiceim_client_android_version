package com.spice.im.utils;

//import com.stb.appearancetime.R;


import com.spice.im.R;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
//import cn.com.ctbri.pp2transitcard.tools.ConstValuesLib;
//import cn.com.ctbri.pp2transitcard.tools.R;

public class MyAlertDialog extends Dialog implements
		android.view.View.OnClickListener, ConstValuesLib {
	public static final int ButtonCancle = 0;// 显示取消按钮
	public static final int ButtonConfirm = 1;// 显示确定按钮
	public static final int ButtonBoth = 2;// 显示全部按钮
	public static final int ButtonNone = 3;// 不显示任何的按钮
	View view = null;
	Button btn_confirm;
	Button btn_cancel;
	int buttonNums = 2;
	ListView listview;
	TextView tx_title;
	Activity activity;
	boolean visiable = true;
	MyDialogListener listener;
//	CharSequence g_Msg = "";
	String[] itemList;
	ListAdapter mAdapter;
	String title = "";
	String posiBtnMsg = "";
	String negaBtnMsg = "";
	ViewGroup container;
	// 提示信息
	private String msg = null;
	private TextView textViewDialog;
	private View lineSeperator;

	OnItemClickListener contentListener;

	public MyAlertDialog(Activity activity) {
		super(activity);
		this.activity = activity;
	}

	public MyAlertDialog(Activity activity, int theme, MyDialogListener listener,
			int btnNums) {
		super(activity, theme);
		this.activity = activity;
		this.listener = listener;
		buttonNums = btnNums;
	}

	public MyAlertDialog(Activity activity, int theme, MyDialogListener listener,
			int btnNums, String[] itemList) {
		super(activity, theme);
		this.activity = activity;
		this.listener = listener;
		buttonNums = btnNums;
//		this.msg = msg;
		this.itemList = itemList;
	}

	public void setContentListener(OnItemClickListener contentListener) {
		this.contentListener = contentListener;
//		listview.setOnItemClickListener(this.contentListener);
	}

	public void setTitleTxt(String msg) {
		title = msg;
	}

	public void MyDialogSetMsg(String[] itemList) {
		this.itemList = itemList;
	}

	public void setPosiBtnMsg(String posiMsg) {
		posiBtnMsg = posiMsg;
	}

	public void setNegaBtnMsg(String negaMsg) {
		negaBtnMsg = negaMsg;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.myalertdialoglist);
		btn_confirm = (Button) findViewById(R.id.btn_confirm);
		btn_confirm.setOnClickListener(this);
		btn_cancel = (Button) findViewById(R.id.btn_cancel);
		btn_cancel.setOnClickListener(this);
		container = (ViewGroup) findViewById(R.id.rl_container);
		lineSeperator = findViewById(R.id.dialog_btn_line_seperator);
		if (buttonNums == ButtonConfirm) {
			btn_cancel.setVisibility(View.GONE);
			lineSeperator.setVisibility(View.GONE);
			btn_confirm.setText(R.string.btn_cancel);//R.string.i_kuown
			btn_confirm
					.setBackgroundResource(R.drawable.common_res_alertdialog_full_btn_selector);
		} else if (buttonNums == ButtonCancle) {
			btn_confirm.setVisibility(View.GONE);
			lineSeperator.setVisibility(View.GONE);
			btn_cancel
					.setBackgroundResource(R.drawable.common_res_alertdialog_full_btn_selector);
		} else if (buttonNums == ButtonNone) {
			btn_confirm.setVisibility(View.GONE);
			btn_cancel
					.setBackgroundResource(R.drawable.common_res_base_alertdialog_full_btn_bg_norrmal);
		}
		if (view != null) {
			container.removeAllViews();
			android.widget.RelativeLayout.LayoutParams params = new android.widget.RelativeLayout.LayoutParams(
					android.widget.RelativeLayout.LayoutParams.FILL_PARENT,
					android.widget.RelativeLayout.LayoutParams.WRAP_CONTENT);
			container.addView(view, params);
		}
		listview = (ListView) findViewById(R.id.textViewDialog);
		tx_title = (TextView) findViewById(R.id.txt_str_prompt);

		// 如果提示信息不为null则显示
//		if (g_Msg != null && !"".equals(g_Msg)) {
//			theView.setText(g_Msg);
//			if (g_Msg.toString().contains("常见问题")) {
//				theView.setOnClickListener(new View.OnClickListener() {
//
//					@Override
//					public void onClick(View v) {
//						Intent intent = new Intent(activity, HelpActivity.class);
//						activity.startActivity(intent);
//						MyDialog.this.dismiss();
//					}
//				});
//			}
//		}
		container.setOnClickListener(this);
		if (itemList != null && itemList.length!=0) {
			mAdapter = new ArrayAdapter(this.activity, R.layout.listitem, itemList);
			listview.setAdapter(mAdapter);
			if(contentListener!=null)
	        listview.setOnItemClickListener(contentListener);
//	        listview.setOnClickListener(this);        
		}
	}

	@Override
	protected void onStart() {
		if (!visiable) {
			listview.setVisibility(View.GONE);
		}
		container.setOnClickListener(this);
		if (itemList != null && itemList.length!=0) {
			mAdapter = new ArrayAdapter(this.activity, R.layout.listitem, itemList);
			listview.setAdapter(mAdapter);
			if(contentListener!=null)
	        listview.setOnItemClickListener(contentListener);
//	        listview.setOnClickListener(this);
		}
		if (title != null && !title.equals("")) {
			tx_title.setText(title);
		}
		if (!posiBtnMsg.equals("")) {
			btn_confirm.setText(posiBtnMsg);
		}
		if (!negaBtnMsg.equals("")) {
			btn_cancel.setText(negaBtnMsg);
		}
		super.onStart();
	}

	@Override
	public void onClick(View v) {
		if (listener != null) {
			if (v.getId() == R.id.btn_confirm) {
				if(listener != null)
				listener.onPositiveClick(this, v);
			} else if (v.getId() == R.id.btn_cancel) {
				if(listener != null)
				listener.onNegativeClick(this, v);
			}
//			else if (v.getId() == R.id.textViewDialog) {
//				if(contentListener != null)
////				contentListener.onClickContent(this, v, g_Msg);
//					listview.setOnItemClickListener(contentListener);
//			}
		}
		if (isShowing()) {
			dismiss();
		}
	}

	public void setMsgViewVisiable(boolean visiable) {
		this.visiable = visiable;
	}

	public interface MyDialogListener {
		public void onPositiveClick(Dialog dialog, View view);

		public void onNegativeClick(Dialog dialog, View view);
	}

//	public interface MyDialogContentClickListener {
//		public void onClickContent(Dialog dialog, View view,
//				CharSequence content);
//	}

	@Override
	public void show() {
		if (activity != null && !activity.isFinishing()) {
			super.show();
		}
	}

	public void setView(View view) {
		this.view = view;
	}
}
