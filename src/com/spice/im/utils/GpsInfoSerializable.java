package com.spice.im.utils;

import java.io.Serializable;

public class GpsInfoSerializable implements Serializable{
	private static final long serialVersionUID = 1L;
	private String mJID;
	private String mStatus;
	private String mName;
	private String mUid;
	public void setJID(String jid){
		this.mJID = jid;
	}
	public String getJID(){
		return this.mJID;
	}
	public void setStatus(String status){
		this.mStatus = status;
	}
	public String getStatus(){
		return this.mStatus;
	}
	public void setName(String name){
		this.mName = name;
	}
	public String getName(){
		return this.mName;
	}
	public void setUid(String uid){
		this.mUid = uid;
	}
	public String getUid(){
		return this.mUid;
	}
    private String mEmail;
    public void setEmail(String email){
    	mEmail = email;
    }
    public String getEmail(){
    	return mEmail;
    }
    private String mDistance;
    public void setDistance(String distance){
    	mDistance = distance;
    }
    public String getDistance(){
    	return mDistance;
    }
    private int mCttotal;
    public void setCttotal(int cttotal){
    	mCttotal = cttotal;
    }
    public int getCttotal(){
    	return mCttotal;
    }
    private String mTime = "";//���λʱ��?
	public String getTime() {  
		return mTime;  
	}  
	public void setTime(String time) {  
		this.mTime = time;  
	} 
	private String mGender;
	private String mBirthday;
	private String mUsersign;
	public String getGender() {
		return mGender;
	}
	public void setGender(String gender) {
		this.mGender = gender;
	}
	public String getBirthday() {
		return mBirthday;
	}
	public void setBirthday(String birthday) {
		this.mBirthday = birthday;
	}
	public String getUsersign() {
		return mUsersign;
	}
	public void setUsersign(String usersign) {
		this.mUsersign = usersign;
	}
}
