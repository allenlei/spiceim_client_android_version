package com.spice.im.utils;

import java.io.Serializable;

public class ContactSerializable implements Serializable{
	private static final long serialVersionUID = 1L;
	private String jidWithRes;
	private String avatarId;
	private String name;
	private String msgState;
	public void setJidWithRes(String jidWithRes){
		this.jidWithRes = jidWithRes;
	}
	public String getJidWithRes(){
		return this.jidWithRes;
	}
	public void setAvatarId(String avatarId){
		this.avatarId = avatarId;
	}
	public String getAvatarId(){
		return this.avatarId;
	}
	public void setName(String name){
		this.name = name;
	}
	public String getName(){
		return this.name;
	}
	public void setMsgState(String msgState){
		this.msgState = msgState;
	}
	public String getMsgState(){
		return this.msgState;
	}
    private String mGender = "";
    public void setGender(String gender){
    	mGender = gender;
    }
    public String getGender(){
    	return mGender;
    }
    
    private String mBirthday = "";
    public void setBirthday(String birthday){
    	mBirthday = birthday;
    }
    public String getBirthday(){
    	return mBirthday;
    }
    private String mSign = "";
    public void setSign(String sign){
    	mSign = sign;
    }
    public String getSign(){
    	return mSign;
    }
}
