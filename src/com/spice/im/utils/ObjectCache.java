package com.spice.im.utils;


import java.io.ByteArrayInputStream;

import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.WeakHashMap;

//import org.bouncycastle2.util.encoders.Base64;
//import org.jivesoftware.smackx.bookmark.BookmarkedConference;
//
////import org.jivesoftware.smack.util.Base64;
//
////import org.bouncycastle2.util.encoders.Base64;
//
////import org.jivesoftware.smack.util.Base64;
//
//import com.beem.push.utils.Constants;
//import com.stb.isharemessage.service.Contact;
//import com.stb.isharemessage.service.GpsInfo;

//import android.support.v4.util.LruCache;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class ObjectCache {
    public static final String TAG = "ObjectCache";

//    // Default memory cache size
//    private static final int DEFAULT_MEM_CACHE_SIZE = 1024 * 1024 * 5; // 5MB
//
//    // Default disk cache size
//    private static final int DEFAULT_DISK_CACHE_SIZE = 1024 * 1024 * 10; // 10MB
//    // Constants to easily toggle various caches
//    private static final boolean DEFAULT_MEM_CACHE_ENABLED = true;
//    private static final boolean DEFAULT_DISK_CACHE_ENABLED = true;
//    private static final boolean DEFAULT_CLEAR_DISK_CACHE_ON_START = false;
//
//    private DiskLruCache mDiskCache;
//    private LruCache<String, Object> mMemoryCache;
  //WeakReference Map: key=string, value=Object
    public static HashMap<String, Object> cache = new HashMap<String, Object>();//WeakHashMap
    
    /**
     * Search the memory cache by a unique key.
     * @param key Should be unique.
     * @return The Bitmap object in memory cache corresponding to specific key.
     * */
    public static Object get(String key,Context context){
    	Log.d(TAG, "get of memory cache: key=" + key);
    	Object obj = null;
        if(key != null){
////        	if(key.startsWith("mContactList_") || key.startsWith("mtmpGroupList_") || key.startsWith("mContactListcol_"))
//        	try{
//	        	SharedPreferences sharedPreferences = context.getSharedPreferences(
//	                    Constants.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
//	        	String mobilesString = sharedPreferences.getString(key, "");
//	        	byte[] mobileBytes = Base64.decode(mobilesString.getBytes());
//	        	ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(mobileBytes);
//	        	ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
//	        	obj = objectInputStream.readObject();
//	        	objectInputStream.close();
//	        	if(key.startsWith("mtmpGroupList_")){
//		        	List<String> result = (List<String>)obj;
//		        	for(int m=0;m<result.size();m++){
//		        		Log.d(TAG, "get of memory cache: key=" + m +" " + result.get(m));
//		        	}
//		        	return result;
//	        	}else if(key.startsWith("mContactList_")){
//		        	List<ContactSerializable> result = (List<ContactSerializable>)obj;
//		        	List<Contact> result_to = new ArrayList<Contact>();
//		        	Contact contact = null;
//		        	for(int m=0;m<result.size();m++){
//		        		Log.d(TAG, "get of memory cache: key=" + m +" " + result.get(m).getJidWithRes()+";"+result.get(m).getMsgState());
//		        		contact = new Contact(result.get(m).getJidWithRes());
//		        		contact.setAvatarId(result.get(m).getAvatarId());
//		        		contact.setName(result.get(m).getName());
//		        		contact.setMsgState(result.get(m).getMsgState());
//		        		contact.setGender(result.get(m).getGender());
//		        		contact.setBirthday(result.get(m).getBirthday());
//		        		contact.setSign(result.get(m).getSign());
//		        		result_to.add(contact);
//		        	}
//		        	return result_to;
//	        	}else if(key.startsWith("mListGpsTemp_")){
//		        	List<GpsInfoSerializable> result = (List<GpsInfoSerializable>)obj;
//		        	List<GpsInfo> result_to = new ArrayList<GpsInfo>();
//		        	GpsInfo gpsInfo = null;
//		        	for(int m=0;m<result.size();m++){
//		        		Log.d(TAG, "get of memory cache: key=" + m +" " + result.get(m).getJID()+";"+result.get(m).getStatus());
//		        		gpsInfo = new GpsInfo(result.get(m).getJID());
//		        		gpsInfo.setStatus(result.get(m).getStatus());
//		        		gpsInfo.setName(result.get(m).getName());
//		        		gpsInfo.setUid(result.get(m).getUid());
//		        		gpsInfo.setEmail(result.get(m).getEmail());
//		        		gpsInfo.setDistance(result.get(m).getDistance());
//		        		gpsInfo.setCttotal(result.get(m).getCttotal());
//		        		gpsInfo.setTime(result.get(m).getTime());
//		        		gpsInfo.setGender(result.get(m).getGender());
//		        		gpsInfo.setBirthday(result.get(m).getBirthday());
//		        		gpsInfo.setUsersign(result.get(m).getUsersign());
//		        		result_to.add(gpsInfo);
//		        	}
//		        	return result_to;
//	        	}else if(key.startsWith("mContactListcol_")){
//	        		List<BookmarkedConferenceSerializable> conference = (List<BookmarkedConferenceSerializable>)obj;
//		        	Collection<BookmarkedConference> result_to = new ArrayList<BookmarkedConference>();
//		        	BookmarkedConference bookmarkedConference = null;
//		        	for(int m=0;m<conference.size();m++){
//		        		Log.d(TAG, "get of memory cache: key=" + m +" " + conference.get(m).getJid());
//		        		bookmarkedConference = new BookmarkedConference(conference.get(m).getJid());
//		        		bookmarkedConference.setName(bookmarkedConference.getJid().substring(0, bookmarkedConference.getJid().indexOf("@conference.")));
//            			bookmarkedConference.setAutoJoin(bookmarkedConference.isAutoJoin());
//            			bookmarkedConference.setNickname(bookmarkedConference.getNickname());
//            			bookmarkedConference.setPassword(bookmarkedConference.getPassword());
//		        		result_to.add(bookmarkedConference);
//		        	}
//		        	return result_to;
//	        	}else if(key.startsWith("mContact_")){
//		        	ContactSerializable result = (ContactSerializable)obj;
//		        	Contact contact = null;
//		        		Log.d(TAG, "get of memory cache: key=" + result.getJidWithRes()+";"+result.getMsgState());
//		        		contact = new Contact(result.getJidWithRes());
//		        		contact.setAvatarId(result.getAvatarId());
//		        		contact.setName(result.getName());
//		        		contact.setMsgState(result.getMsgState());
//		        		contact.setGender(result.getGender());
//		        		contact.setBirthday(result.getBirthday());
//		        		contact.setSign(result.getSign());
//		        	
//		        	return contact;
//	        	}
	        	
	        	return obj;
//        	}catch(Exception e){
//        		return null;
//        	}
//            return cache.get(key);
        }
        return null;
    }
    /**
     * Put a bitmap into cache with a unique key.
     * @param key Should be unique.
     * @param value A bitmap.
     * */
    public static void put(String key, Object value,Context context){
        if(key != null && !"".equals(key) && value != null){
//            cache.put(key, value);
            //Log.i(TAG, "cache bitmap: " + key);
            Log.d(TAG, "size of memory cache: " + cache.size());
//            if(key.startsWith("mContactList_") 
//            		|| key.startsWith("mtmpGroupList_")
//            		|| key.startsWith("mContactListcol_")){
	            List<ContactSerializable> jidandstatus = new ArrayList<ContactSerializable>();
	            List<GpsInfoSerializable> list_gpsinfo = new ArrayList<GpsInfoSerializable>();
	            List<BookmarkedConferenceSerializable> conference = new ArrayList<BookmarkedConferenceSerializable>();
	            ContactSerializable mContact = new ContactSerializable();
//	            ContactSerializable mContact = null;
	            if(key.startsWith("mContactList_")){
//		            List list = null;
//		            Contact contact = null;
//		            ContactSerializable contactSerial = null;
//		            if(value instanceof List){
//		            	list = (List)value;
//		            	for(int i=0;i<list.size();i++){
//		            		if(list.get(i) instanceof Contact){
//			            		contact = (Contact)list.get(i);
//			            		contactSerial = new ContactSerializable();
//			            		contactSerial.setName(contact.getName());
//			            		contactSerial.setJidWithRes(contact.getJIDWithRes());
//			            		contactSerial.setMsgState(contact.getMsgState());
//			            		contactSerial.setAvatarId(contact.getAvatarId());
//			            		contactSerial.setGender(contact.getGender());
//			            		contactSerial.setBirthday(contact.getBirthday());
//			            		contactSerial.setSign(contact.getSign());
//	//		            		jidandstatus.add(contact.getJIDWithRes()+contact.getMsgState());
//			            		jidandstatus.add(contactSerial);
//			            		Log.d(TAG, "size of memory cache: " + contact.getJIDWithRes()+contact.getMsgState());
//		            		}
//		            	}
//		            }
	            }else if(key.startsWith("mListGpsTemp_")){
//		            List list = null;
//		            GpsInfo gpsInfo = null;
//		            GpsInfoSerializable gpsInfoSerial = null;
//		            if(value instanceof List){
//		            	list = (List)value;
//		            	for(int i=0;i<list.size();i++){
//		            		if(list.get(i) instanceof GpsInfo){
//			            		gpsInfo = (GpsInfo)list.get(i);
//			            		gpsInfoSerial = new GpsInfoSerializable();
//			            		gpsInfoSerial.setJID(gpsInfo.getJID());
//			            		gpsInfoSerial.setStatus(gpsInfo.getStatus());
//			            		gpsInfoSerial.setName(gpsInfo.getName());
//			            		gpsInfoSerial.setUid(gpsInfo.getUid());
//			            		gpsInfoSerial.setEmail(gpsInfo.getEmail());
//			            		gpsInfoSerial.setDistance(gpsInfo.getDistance());
//			            		gpsInfoSerial.setCttotal(gpsInfo.getCttotal());
//			            		gpsInfoSerial.setTime(gpsInfo.getTime());
//			            		gpsInfoSerial.setGender(gpsInfo.getGender());
//			            		gpsInfoSerial.setBirthday(gpsInfo.getBirthday());
//			            		gpsInfoSerial.setUsersign(gpsInfo.getUsersign());
//	//		            		jidandstatus.add(contact.getJIDWithRes()+contact.getMsgState());
//			            		list_gpsinfo.add(gpsInfoSerial);
//			            		Log.d(TAG, "size of memory cache: " + gpsInfo.getJID()+gpsInfo.getStatus());
//		            		}
//		            	}
//		            }
	            }else if(key.startsWith("mContactListcol_")){
//		            Collection col = null;
//		            BookmarkedConference bookmarkedConference = null;
//		            BookmarkedConferenceSerializable bookmarkedConferenceSerial = null;
//		            if(value instanceof Collection){
//		            	col = (Collection)value;
//		            	Iterator it = col.iterator();
//		            	while(it.hasNext()){
////		            		if(list.get(i) instanceof BookmarkedConference){
////		            			bookmarkedConference = (BookmarkedConference)list.get(i);
//		            			bookmarkedConference = (BookmarkedConference)it.next();
//		            			bookmarkedConferenceSerial = new BookmarkedConferenceSerializable();
//		            			bookmarkedConferenceSerial.setName(bookmarkedConference.getJid().substring(0, bookmarkedConference.getJid().indexOf("@conference.")));
//		            			bookmarkedConferenceSerial.setJid(bookmarkedConference.getJid());
//		            			bookmarkedConferenceSerial.setAutoJoin(bookmarkedConference.isAutoJoin());
//		            			bookmarkedConferenceSerial.setNickname(bookmarkedConference.getNickname());
//		            			bookmarkedConferenceSerial.setPassword(bookmarkedConference.getPassword());
//	//		            		jidandstatus.add(contact.getJIDWithRes()+contact.getMsgState());
//		            			conference.add(bookmarkedConferenceSerial);
//			            		Log.d(TAG, "size of memory cache: " + bookmarkedConference.getJid());
////		            		}
//		            	}
//		            }
	            }else if(key.startsWith("mContact_")){
//		            Contact contact = null;
//		            if(value instanceof Contact){
//		            	contact = (Contact)value;
//		            	mContact.setName(contact.getName());
//		            	mContact.setJidWithRes(contact.getJIDWithRes());
//		            	mContact.setMsgState(contact.getMsgState());
//		            	mContact.setAvatarId(contact.getAvatarId());
//		            	mContact.setGender(contact.getGender());
//		            	mContact.setBirthday(contact.getBirthday());
//		            	mContact.setSign(contact.getSign());
//			            Log.d(TAG, "size of memory cache: " + contact.getJIDWithRes()+contact.getMsgState());
//		            }
	            }
	            try{
		            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
		            if(key.startsWith("mContactList_"))
		            	objectOutputStream.writeObject(jidandstatus);//value
		            else if(key.startsWith("mListGpsTemp_"))
		            	objectOutputStream.writeObject(list_gpsinfo);//value
		            else if(key.startsWith("mtmpGroupList_"))
		            	objectOutputStream.writeObject(value);
		            else if(key.startsWith("mContactListcol_"))
		            	objectOutputStream.writeObject(conference);
		            else if(key.startsWith("mContact_"))
		            	objectOutputStream.writeObject(mContact);
		            else
		            	objectOutputStream.writeObject(value);
//		        	SharedPreferences sharedPreferences = context.getSharedPreferences(
//		                    Constants.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
//		        	Editor editor = sharedPreferences.edit();
//		            String mobilesString = new String(Base64.encode(byteArrayOutputStream.toByteArray()));
//		            editor.putString(key, mobilesString);
//		            editor.commit();
		            objectOutputStream.close();
	            }catch(Exception e){
	            	e.printStackTrace();
	            }
            }
//            get(key,context);
//        }
    }
    /**
     * clear the memory cache.
     * */
    public static void clear() {
    	Log.d(TAG, "clear memory cache");
        cache.clear();
        
    }
    //key=isConnected ;value=true or false
    public static void saveStatus(String key, boolean value,Context context){
//    public static void saveStatus(String key, Object value,Context context){	
        try{
////            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
////            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
////            objectOutputStream.writeObject(value);
//        	SharedPreferences sharedPreferences = context.getSharedPreferences(
//                    Constants.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
//        	Editor editor = sharedPreferences.edit();
////            String mobilesString = new String(Base64.encode(byteArrayOutputStream.toByteArray()));
////            editor.putString(key, mobilesString);
////            editor.commit();
////            objectOutputStream.close();
//			editor.putBoolean(key, value);
//			editor.commit();
            Log.d(TAG, "getStatus: "+getStatus(key,context));
        }catch(Exception e){
        	e.printStackTrace();
        }
    }
    public static boolean getStatus(String key,Context context){
    	boolean obj = false;
        if(key != null){
        	try{
//	        	SharedPreferences sharedPreferences = context.getSharedPreferences(
//	                    Constants.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
////	        	String mobilesString = sharedPreferences.getString(key, "");
////	        	byte[] mobileBytes = Base64.decode(mobilesString.getBytes());
////	        	ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(mobileBytes);
////	        	ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
////	        	obj = (Boolean)objectInputStream.readObject();
////	        	objectInputStream.close();
////	        	return obj;
//	        	return sharedPreferences.getBoolean(key, false);
        	}catch(Exception e){
        		return false;
        	}
//            return cache.get(key);
        }
        return false;
    }
}
