package com.spice.im.utils;


import android.os.Parcel;

import android.os.Parcelable;

public class Feed extends Entity implements Parcelable {
	public static final String TIME = "time";
	public static final String CONTENT = "content";
	public static final String CONTENT_IMAGE = "content_image";
	public static final String FANER = "faner";
	public static final String SITE = "site";
	public static final String COMMENT_COUNT = "comment_count";
	private String dynamicid;
	private String originalid;
	private String time;
	private String content;
	private String contentImage;
	private String faner;
	private String site;
	private int commentCount;
	private String headimg;
	private String nickname;
	private String user_id;

	public Feed() {
		super();
	}

	public Feed(String dynamicid,String originalid,String time, String content, String contentImage,String faner, String site,
			int commentCount,String headimg,String nickname,String user_id) {
		super();
		this.dynamicid = dynamicid;
		this.originalid = originalid;
		this.time = time;
		this.content = content;
		this.contentImage = contentImage;
		this.faner = faner;
		this.site = site;
		this.commentCount = commentCount;
		this.headimg = headimg;
		this.nickname = nickname;
		this.user_id = user_id;
	}

	public String getDynamicid() {
		return dynamicid;
	}

	public void setDynamicid(String dynamicid) {
		this.dynamicid = dynamicid;
	}
	
	public String getOriginalid() {
		return originalid;
	}

	public void setOriginalid(String originalid) {
		this.originalid = originalid;
	}
	
	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getContentImage() {
		return contentImage;
	}

	public void setContentImage(String contentImage) {
		this.contentImage = contentImage;
	}
	
	public String getFaner() {
		return faner;
	}

	public void setFaner(String faner) {
		this.faner = faner;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public int getCommentCount() {
		return commentCount;
	}

	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}
	public void setHeadimg(String headimg) {
		this.headimg = headimg;
	}

	public String getHeadimg() {
		return headimg;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getNickname() {
		return nickname;
	}
	
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getUser_id() {
		return user_id;
	}
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(dynamicid);
		dest.writeString(originalid);
		dest.writeString(time);
		dest.writeString(content);
		dest.writeString(contentImage);
		dest.writeString(faner);
		dest.writeString(site);
		dest.writeInt(commentCount);
		dest.writeString(headimg);
		dest.writeString(nickname);
		dest.writeString(user_id);
	}

	public static final Parcelable.Creator<Feed> CREATOR = new Parcelable.Creator<Feed>() {

		@Override
		public Feed createFromParcel(Parcel source) {
			Feed feed = new Feed();
			feed.setDynamicid(source.readString());
			feed.setOriginalid(source.readString());
			feed.setTime(source.readString());
			feed.setContent(source.readString());
			feed.setContentImage(source.readString());
			feed.setFaner(source.readString());
			feed.setSite(source.readString());
			feed.setCommentCount(source.readInt());
			feed.setHeadimg(source.readString());
			feed.setNickname(source.readString());
			feed.setUser_id(source.readString());
			return feed;
		}

		@Override
		public Feed[] newArray(int size) {
			return new Feed[size];
		}
	};
}
