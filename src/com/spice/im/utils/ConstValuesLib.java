package com.spice.im.utils;

public interface ConstValuesLib {
	
	public static final String RESPONSE_UNKNOW_ERROE = "9999";
	public static final String RESPONSE_UNKNOW_ERROE_MSG = "未知错误";
	
	public static final String RESPONSE_READCARD_UNSUPPORT = "9998";
	public static final String RESPONSE_READCARD_UNSUPPORT_MSG = "不支持的卡片";

	public static final String RESPONSE_SUCCESS= "0000";
	public static final String RESPONSE_SUCCESS_MSG = "成功";
	
	
	
	public static String NETWORK_ERROR = "网络故障，暂时无法充值，请稍候重试" ;
	public static String RULE_ID_ERROR = "获取路由规则失败，请重试" ;
	
}
