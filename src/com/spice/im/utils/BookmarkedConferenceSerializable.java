package com.spice.im.utils;

import java.io.Serializable;

public class BookmarkedConferenceSerializable implements Serializable{
	private static final long serialVersionUID = 2L;
    private String name;
    private boolean autoJoin;
    private String jid;
    private String nickname;
    private String password;
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public boolean getAutoJoin() {
        return autoJoin;
    }
    public void setAutoJoin(boolean autoJoin) {
        this.autoJoin = autoJoin;
    }
    public String getJid() {
        return jid;
    }
    public void setJid(String jid) {
        this.jid = jid;
    }
    public String getNickname() {
        return nickname;
    }
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
}
