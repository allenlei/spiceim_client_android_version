/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.spice.im.utils;

import android.content.Context;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

//import com.huewu.pla.sample.BuildConfig;

/**
 * A simple subclass of {@link ImageResizer} that fetches and resizes images fetched from a URL.
 */
public class ImageFetcher extends ImageResizer {
    private static final String TAG = "ImageFetcher";
    public static final int HTTP_CACHE_SIZE = 10 * 1024 * 1024; // 10MB
    public static final String HTTP_CACHE_DIR = "http";

    /**
     * Initialize providing a target image width and height for the processing images.
     *
     * @param context
     * @param imageWidth
     * @param imageHeight
     */
    public ImageFetcher(Context context, int imageWidth, int imageHeight) {
        super(context, imageWidth, imageHeight);
        init(context);
    }

    /**
     * Initialize providing a single target image size (used for both width and height);
     *
     * @param context
     * @param imageSize
     */
    public ImageFetcher(Context context, int imageSize) {
        super(context, imageSize);
        init(context);
    }

    private void init(Context context) {
        checkConnection(context);
    }

    /**
     * Simple network connection check.
     *
     * @param context
     */
    private void checkConnection(Context context) {
        final ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo == null || !networkInfo.isConnectedOrConnecting()) {
            Toast.makeText(context, "No network connection found.", Toast.LENGTH_LONG).show();
            Log.e(TAG, "checkConnection - no connection found");
        }
    }

    /**
     * The main process method, which will be called by the ImageWorker in the AsyncTask background
     * thread.
     *
     * @param data The data to load the bitmap, in this case, a regular http URL
     * @return The downloaded and resized bitmap
     */
    public Bitmap processBitmap(String data) {//private
//        if (BuildConfig.DEBUG) {
////            Log.d(TAG, "processBitmap - " + data);
//        }

        // Download a bitmap, write it to a file
        final File f = downloadBitmap(mContext, data);
        
        if (f != null) {
            // Return a sampled down version
            return decodeSampledBitmapFromFile(f.toString(), mImageWidth, mImageHeight);
        }

        return null;
    }

    @Override
    protected Bitmap processBitmap(Object data) {
    	if(data!=null)
    		return processBitmap(String.valueOf(data));
    	else 
    		return null;
    }

    
    
    /**
     * Download a bitmap from a URL, write it to a disk and return the File pointer. This
     * implementation uses a simple disk cache.
     *
     * @param context The context to use
     * @param urlString The URL to fetch
     * @return A File pointing to the fetched bitmap
     */
    public static File downloadBitmap(Context context, String urlString) {
    	File cacheDir = null;
    	DiskLruCache cache = null;
    	File cacheFile = null;
        HttpURLConnection urlConnection = null;
        BufferedOutputStream out = null;
        InputStream in = null;
        URL url = null;
		BufferedInputStream buf = null;
		Bitmap bitmap = null;
		File cacheFile2 = null;
    	try{
	    	
	//        final File cacheDir = DiskLruCache.getDiskCacheDir(context, HTTP_CACHE_DIR);
	    		cacheDir = DiskLruCache.getDiskCacheDir(context, HTTP_CACHE_DIR);
	
	//        final DiskLruCache cache = DiskLruCache.openCache(context, cacheDir, HTTP_CACHE_SIZE);
	    		cache = DiskLruCache.openCache(context, cacheDir, HTTP_CACHE_SIZE);
	        //final 
	//        File cacheFile = null;
	        if(urlString!=null && !urlString.equalsIgnoreCase("null"))
	        	try{
	        		cacheFile = new File(cache.createFilePath(urlString));//存储空间不足 cache = null
	        	}catch(Exception e){
	        		e.printStackTrace();
	        	}
	        
	        if(cacheFile==null){
	        	Log.e("※※※※※20130902ImageFetcher存储空间不足※※※※※", "※※存储空间不足");
	        	try{
	        		cacheFile = new File(context.getCacheDir().getPath()+ File.separator + "cache_" + URLEncoder.encode(urlString.replace("*", ""), "UTF-8"));
	            if(!cacheFile.exists())  
		        {  
		            boolean b = cacheFile.mkdirs();  
	////	            Log.e("file", "文件不存在  创建文件    "+b);  
		        }else{  
	////	            Log.e("file", "文件存在");  
		        } 
	        	}catch(Exception e){
	        		e.printStackTrace();
	        	}
	        }
	
	        if (cache.containsKey(urlString)) {
	            return cacheFile;
	        }
	        if (urlString.toUpperCase().startsWith("HTTP")) {
		        Utils.disableConnectionReuseIfNecessary();
	//	        HttpURLConnection urlConnection = null;
	//	        BufferedOutputStream out = null;
	//	        InputStream in = null;
		        try {
	//	            final URL url = new URL(urlString);
		        	url = new URL(urlString);
		            urlConnection = (HttpURLConnection) url.openConnection();
		            urlConnection.setConnectTimeout(5000);
		            urlConnection.setReadTimeout(10000);
	//	            final InputStream in =
		            in = new BufferedInputStream(urlConnection.getInputStream(), Utils.IO_BUFFER_SIZE);
		            
		            out = new BufferedOutputStream(new FileOutputStream(cacheFile), Utils.IO_BUFFER_SIZE);
		
		            int b;
		            while ((b = in.read()) != -1) {
		                out.write(b);
		            }
		            in.close();//20140624
		            return cacheFile;
		
		        } catch (final IOException e) {
		            Log.e(TAG, "Error in downloadBitmap - " + e);
		        } finally {
		            if (urlConnection != null) {
		                urlConnection.disconnect();
		                urlConnection = null;
		            }
		            if (out != null) {
		                try {
		                    out.close();
		                    out = null;
		                } catch (final IOException e) {
		                    Log.e(TAG, "Error in downloadBitmap - " + e);
		                }
		            }
		            if (in != null) {
		            	try{
		            		in.close();
		            		in = null;
		            	}catch(Exception e){}
		            }
		            url = null;
		            
		        }
		
		        return null;
	        }else{
	//			BufferedInputStream buf = null;
	//			Bitmap bitmap = null;
				try {
	//				File cacheFile2 = new File(urlString);
					cacheFile2 = new File(urlString);
					buf = new BufferedInputStream(new FileInputStream(cacheFile2));
					bitmap = BitmapFactory.decodeStream(buf);
					
					cache.put(urlString, bitmap);
					return cacheFile;
				}catch(Exception e){
					return null;
				}finally{
					if(buf!=null){
						try{
							buf.close();
							buf = null;
						}catch(Exception e){}
					}
					if(bitmap!=null){//added by allen 20140624
						try{
							bitmap.recycle();
							bitmap = null;
						}catch(Exception e){}
					}
					cacheFile2 = null;
				}
	        }
    	}catch(Exception e){
    		e.printStackTrace();
    		return null;
    	}finally{
    		cache = null;
    		cacheDir = null;
    	}
    }
}
