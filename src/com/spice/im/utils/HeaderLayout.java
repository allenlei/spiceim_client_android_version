package com.spice.im.utils;


//import com.stb.isharemessage.R;


//import com.speed.im.R;
//
//import com.stb.isharemessage.ui.HandyTextView;
//import com.stb.isharemessage.utils.HeaderSpinner.onSpinnerClickListener;
//import com.stb.isharemessage.utils.SwitcherButton.onSwitcherButtonClickListener;

import com.spice.im.R;
import com.spice.im.ui.HandyTextView;
import com.spice.im.utils.HeaderSpinner.onSpinnerClickListener;
import com.spice.im.utils.SwitcherButton.onSwitcherButtonClickListener;

import android.content.Context;


import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;



public class HeaderLayout extends LinearLayout {

	private LayoutInflater mInflater;
	private View mHeader;
	private ImageView mIvLogo;
	private LinearLayout mLayoutLeftContainer;
	private LinearLayout mLayoutMiddleContainer;
	private LinearLayout mLayoutRightContainer;

	// ����
	private LinearLayout mLayoutTitle;
	private ScrollingTextView mStvTitle;
	private HandyTextView mHtvSubTitle;

	// ����
	private RelativeLayout mLayoutSearch;
	private EditText mEtSearch;
	private Button mBtnSearchClear;
	private ImageView mIvSearchLoading;
	private onSearchListener mOnSearchListener;

	// �ұ��ı�
	private HandyTextView mHtvRightText;

	// �ұ߰�ť
	private LinearLayout mLayoutRightImageButtonLayout;
	private ImageButton mIbRightImageButton;
	private onRightImageButtonClickListener mRightImageButtonClickListener;
	
	private LinearLayout mLayoutRight1ImageButtonLayout;
	private LinearLayout mLayoutRight2ImageButtonLayout;
	private ImageButton mIbRightImageButton1;
	private onRight1ImageButtonClickListener mRight1ImageButtonClickListener;
	private ImageButton mIbRightImageButton2;
	private onRight2ImageButtonClickListener mRight2ImageButtonClickListener;
	

	private HeaderSpinner mHsSpinner;
	private HeaderSpinner mHsSpinner2;
	private LinearLayout mLayoutMiddleImageButtonLayout;
	private ImageButton mIbMiddleImageButton;
	private ImageView mIvMiddleLine;
	private onMiddleImageButtonClickListener mMiddleImageButtonClickListener;

	private SwitcherButton mSbRightSwitcherButton;
	private onSwitcherButtonClickListener mSwitcherButtonClickListener;

	public HeaderLayout(Context context) {
		super(context);
		init(context);
	}

	public HeaderLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public void init(Context context) {
		mInflater = LayoutInflater.from(context);
		mHeader = mInflater.inflate(R.layout.common_headerbar, null);
		addView(mHeader);
		initViews();

	}

	public void initViews() {
		mIvLogo = (ImageView) findViewByHeaderId(R.id.header_iv_logo);
		mIvLogo.setVisibility(View.VISIBLE);
		mLayoutLeftContainer = (LinearLayout) findViewByHeaderId(R.id.header_layout_leftview_container);
		mLayoutLeftContainer.setVisibility(View.VISIBLE);
		mLayoutMiddleContainer = (LinearLayout) findViewByHeaderId(R.id.header_layout_middleview_container);
		mLayoutRightContainer = (LinearLayout) findViewByHeaderId(R.id.header_layout_rightview_container);
		mLayoutTitle = (LinearLayout) findViewByHeaderId(R.id.header_layout_title);
		mStvTitle = (ScrollingTextView) findViewByHeaderId(R.id.header_stv_title);
		mHtvSubTitle = (HandyTextView) findViewByHeaderId(R.id.header_htv_subtitle);

		mLayoutSearch = (RelativeLayout) findViewByHeaderId(R.id.header_layout_search);
		mEtSearch = (EditText) findViewByHeaderId(R.id.header_et_search);
		mBtnSearchClear = (Button) findViewByHeaderId(R.id.header_btn_searchclear);
		mIvSearchLoading = (ImageView) findViewByHeaderId(R.id.header_iv_searchloading);

		mHsSpinner = (HeaderSpinner) findViewByHeaderId(R.id.header_hs_spinner);
		mHsSpinner2 = (HeaderSpinner) findViewByHeaderId(R.id.header_hs_spinner2);
		mLayoutMiddleImageButtonLayout = (LinearLayout) findViewByHeaderId(R.id.header_layout_middle_imagebuttonlayout);
		mIbMiddleImageButton = (ImageButton) findViewByHeaderId(R.id.header_ib_middle_imagebutton);
		mIvMiddleLine=(ImageView)findViewByHeaderId(R.id.header_iv_middle_line);
		
		mSbRightSwitcherButton = (SwitcherButton) findViewByHeaderId(R.id.header_sb_rightview_switcherbutton);
	}

	public View findViewByHeaderId(int id) {
		return mHeader.findViewById(id);
	}

	public void init(HeaderStyle style) {
		switch (style) {
		case DEFAULT_TITLE:
			defaultTitle();
			break;

		case TITLE_RIGHT_TEXT:
			titleRightText();
			break;

		case TITLE_RIGHT_IMAGEBUTTON:
			titleRightImageButton();
			break;

		case TITLE_NEARBY_PEOPLE:
			titleNearBy(true);
			break;
		case TITLE_CONTACTLIST:
			titleContactList();
			break;
		case TITLE_NEARBY_GROUP:
			titleNearBy(false);
			break;

		case TITLE_CHAT:
			titleChat();
			break;
		case TITLE_CHAT2:
			titleChat2();
			break;
			
		}
	}

	/**
	 * Ĭ��ֻ�б���
	 */
	private void defaultTitle() {
		mLayoutTitle.setVisibility(View.VISIBLE);
		mLayoutMiddleContainer.removeAllViews();
		mLayoutRightContainer.removeAllViews();
	}

	/**
	 * ���Ĭ�ϱ�������?
	 * 
	 * @param title
	 * @param subTitle
	 */
	public void setDefaultTitle(CharSequence title, CharSequence subTitle) {
		if (title != null) {
			mStvTitle.setText(title);
		} else {
			mStvTitle.setVisibility(View.GONE);
		}
		if (subTitle != null) {
			mHtvSubTitle.setText(subTitle);
		} else {
			mHtvSubTitle.setVisibility(View.GONE);
		}
	}

	/**
	 * �����Լ��ұ����ı�����
	 */
	private void titleRightText() {
		mLayoutTitle.setVisibility(View.VISIBLE);
		mLayoutMiddleContainer.removeAllViews();
		mLayoutRightContainer.removeAllViews();
		View mRightText = mInflater.inflate(R.layout.include_header_righttext,
				null);
		mLayoutRightContainer.addView(mRightText);
		mHtvRightText = (HandyTextView) mRightText
				.findViewById(R.id.header_htv_righttext);
	}

	/**
	 * ��ӱ����Լ��ұ��ı�����?
	 * 
	 * @param title
	 * @param subTitle
	 * @param rightText
	 */
	public void setTitleRightText(CharSequence title, CharSequence subTitle,
			CharSequence rightText) {
		setDefaultTitle(title, subTitle);
		if (mHtvRightText != null && rightText != null) {
			mHtvRightText.setText(rightText);
		}
	}

	/**
	 * �����Լ��ұ�ͼƬ��ť
	 */
	private void titleRightImageButton() {
		mLayoutTitle.setVisibility(View.VISIBLE);
		mLayoutMiddleContainer.removeAllViews();
		mLayoutRightContainer.removeAllViews();
		View mRightImageButton = mInflater.inflate(
				R.layout.include_header_rightimagebutton, null);
		mLayoutRightContainer.addView(mRightImageButton);
		mLayoutRightImageButtonLayout = (LinearLayout) mRightImageButton
				.findViewById(R.id.header_layout_right_imagebuttonlayout);
		mIbRightImageButton = (ImageButton) mRightImageButton
				.findViewById(R.id.header_ib_right_imagebutton);
		mLayoutRightImageButtonLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (mRightImageButtonClickListener != null) {
					mRightImageButtonClickListener.onClick();
				}
			}
		});
	}

	public void setTitleRightImageButton(CharSequence title,
			CharSequence subTitle, int id,
			onRightImageButtonClickListener listener) {
		setDefaultTitle(title, subTitle);
		if (mIbRightImageButton != null && id > 0) {
			mIbRightImageButton.setImageResource(id);
			setOnRightImageButtonClickListener(listener);
		}
	}

	/**
	 * �������?
	 */
	private void titleNearBy(boolean isPeople) {
		mSbRightSwitcherButton.setVisibility(View.VISIBLE);
		if (isPeople) {
			mLayoutTitle.setVisibility(View.GONE);
			mHsSpinner.setVisibility(View.VISIBLE);
			mHsSpinner2.setVisibility(View.VISIBLE);
			mLayoutMiddleImageButtonLayout.setVisibility(View.GONE);
		} else {
			mLayoutTitle.setVisibility(View.VISIBLE);
			mHsSpinner.setVisibility(View.GONE);
			mHsSpinner2.setVisibility(View.GONE);
			mLayoutMiddleImageButtonLayout.setVisibility(View.VISIBLE);
		}
	}
	
	private void titleContactList() {
		mSbRightSwitcherButton.setVisibility(View.VISIBLE);
//		if (isPeople) {
//			mLayoutTitle.setVisibility(View.GONE);
//			mHsSpinner.setVisibility(View.VISIBLE);
//			mHsSpinner2.setVisibility(View.VISIBLE);
//			mLayoutMiddleImageButtonLayout.setVisibility(View.GONE);
//		} else {
			mLayoutTitle.setVisibility(View.VISIBLE);
//			mHsSpinner.setVisibility(View.GONE);
//			mHsSpinner2.setVisibility(View.GONE);
			mLayoutMiddleImageButtonLayout.setVisibility(View.VISIBLE);
//		}
	}
//	private void titleNearBy2(boolean isPeople) {
//		mSbRightSwitcherButton.setVisibility(View.VISIBLE);
//		if (isPeople) {
//			mLayoutTitle.setVisibility(View.GONE);
//			mHsSpinner.setVisibility(View.VISIBLE);
//			mHsSpinner2.setVisibility(View.VISIBLE);
//			mLayoutMiddleImageButtonLayout.setVisibility(View.GONE);
//		} else {
//			mLayoutTitle.setVisibility(View.VISIBLE);
//			mHsSpinner.setVisibility(View.GONE);
//			mHsSpinner2.setVisibility(View.GONE);
//			mLayoutMiddleImageButtonLayout.setVisibility(View.VISIBLE);
//		}
//	}

	public HeaderSpinner setTitleNearBy(CharSequence spinnerText,
			onSpinnerClickListener spinnerClickListener, CharSequence title,
			int middleImageId,
			onMiddleImageButtonClickListener middleImageButtonClickListener,
			CharSequence switcherLeftText, CharSequence switcherRightText,
			onSwitcherButtonClickListener switcherButtonClickListener) {

		mHsSpinner.setText(spinnerText);
		mHsSpinner.setOnSpinnerClickListener(spinnerClickListener);
		setDefaultTitle(title, null);
		if (middleImageId > 0) {
			mIbMiddleImageButton.setImageResource(middleImageId);
		}
		mMiddleImageButtonClickListener = middleImageButtonClickListener;
		mLayoutMiddleImageButtonLayout
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						if (mMiddleImageButtonClickListener != null) {
							mMiddleImageButtonClickListener.onClick();
						}
					}
				});
		mSbRightSwitcherButton.setLeftText(switcherLeftText);
		mSbRightSwitcherButton.setRightText(switcherRightText);
		mSwitcherButtonClickListener = switcherButtonClickListener;
		mSbRightSwitcherButton
				.setOnSwitcherButtonClickListener(mSwitcherButtonClickListener);
		return mHsSpinner;
	}
	
	public HeaderSpinner setTitleNearBy21(CharSequence spinnerText,
			onSpinnerClickListener spinnerClickListener,
			CharSequence switcherLeftText, CharSequence switcherRightText,
			onSwitcherButtonClickListener switcherButtonClickListener) {

		mHsSpinner.setText(spinnerText);
		mHsSpinner.setOnSpinnerClickListener(spinnerClickListener);
		setDefaultTitle(spinnerText, null);
////		if (middleImageId > 0) {
////			mIbMiddleImageButton.setImageResource(middleImageId);
////		}
////		mMiddleImageButtonClickListener = middleImageButtonClickListener;
		mLayoutMiddleImageButtonLayout
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						if (mMiddleImageButtonClickListener != null) {
							mMiddleImageButtonClickListener.onClick();
						}
					}
				});
		mSbRightSwitcherButton.setLeftText(switcherLeftText);
		mSbRightSwitcherButton.setRightText(switcherRightText);
		mSwitcherButtonClickListener = switcherButtonClickListener;
		mSbRightSwitcherButton
				.setOnSwitcherButtonClickListener(mSwitcherButtonClickListener);
//		mLayoutLeftContainer.removeAllViews();
		mLayoutLeftContainer.removeView(mHsSpinner);
		mLayoutRightContainer.removeAllViews();//20140428
		return mHsSpinner;
	}
	public HeaderSpinner setTitleNearBy22(CharSequence spinnerText2,
			onSpinnerClickListener spinnerClickListener2) {

		mHsSpinner2.setText(spinnerText2);
		mHsSpinner2.setOnSpinnerClickListener(spinnerClickListener2);
		mLayoutLeftContainer.removeView(mHsSpinner);
		mLayoutRightContainer.removeAllViews();//20140428
		return mHsSpinner2;
	}

	public void showSearch() {
		mLayoutSearch.setVisibility(View.VISIBLE);
		mEtSearch.requestFocus();
		mLayoutTitle.setVisibility(View.GONE);
		mLayoutMiddleContainer.setVisibility(View.GONE);
		mLayoutRightContainer.setVisibility(View.GONE);
	}

	public void dismissSearch() {
		mLayoutSearch.setVisibility(View.GONE);
		mLayoutTitle.setVisibility(View.VISIBLE);
		mLayoutMiddleContainer.setVisibility(View.VISIBLE);
		mLayoutRightContainer.setVisibility(View.VISIBLE);
	}

	public boolean searchIsShowing() {
		if (mLayoutSearch.getVisibility() == View.VISIBLE) {
			return true;
		}
		return false;
	}

	public void clearSearch() {
		mEtSearch.setText(null);
	}

	public void initSearch(onSearchListener listener) {
		dismissSearch();
		setOnSearchListener(listener);
		mEtSearch.setText(null);
		changeSearchState(SearchState.INPUT);
		mBtnSearchClear.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				clearSearch();
			}
		});
		mEtSearch.setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_ENTER
						&& event.getRepeatCount() == 0) {
					if (mOnSearchListener != null) {
						mOnSearchListener.onSearch(mEtSearch);
					}
				}
				return false;
			}
		});
	}

	public void changeSearchState(SearchState state) {
		switch (state) {
		case INPUT:
			mBtnSearchClear.setVisibility(View.VISIBLE);
			mIvSearchLoading.clearAnimation();
			mIvSearchLoading.setVisibility(View.GONE);
			break;

		case SEARCH:
			mBtnSearchClear.setVisibility(View.GONE);
			Animation anim = AnimationUtils.loadAnimation(getContext(),
					R.anim.loading);
			mIvSearchLoading.clearAnimation();
			mIvSearchLoading.startAnimation(anim);
			mIvSearchLoading.setVisibility(View.VISIBLE);
			break;
		}
	}

	private void titleChat() {
		mLayoutTitle.setVisibility(View.VISIBLE);
		mLayoutRightContainer.removeAllViews();
		mIvMiddleLine.setVisibility(View.GONE);
		View mRightImageButton = mInflater.inflate(
				R.layout.include_header_rightimagebutton, null);
		mLayoutRightContainer.addView(mRightImageButton);
		mLayoutRightImageButtonLayout = (LinearLayout) mRightImageButton
				.findViewById(R.id.header_layout_right_imagebuttonlayout);
		mIbRightImageButton = (ImageButton) mRightImageButton
				.findViewById(R.id.header_ib_right_imagebutton);
		mLayoutRightImageButtonLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (mRightImageButtonClickListener != null) {
					mRightImageButtonClickListener.onClick();
				}
			}
		});
		mLayoutMiddleImageButtonLayout
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						if (mMiddleImageButtonClickListener != null) {
							mMiddleImageButtonClickListener.onClick();
						}
					}
				});
	}
	
	private void titleChat2() {
		mLayoutTitle.setVisibility(View.VISIBLE);
		mLayoutRightContainer.removeAllViews();
		mIvMiddleLine.setVisibility(View.GONE);
		View mRightImageButton = mInflater.inflate(
				R.layout.include_header_right2imagebutton, null);
		mLayoutRightContainer.addView(mRightImageButton);
		mLayoutRight1ImageButtonLayout = (LinearLayout) mRightImageButton
				.findViewById(R.id.header_layout_right_1imagebuttonlayout);
		
		mIbRightImageButton1 = (ImageButton) mRightImageButton
				.findViewById(R.id.header_ib_right_1imagebutton);
		mLayoutRight1ImageButtonLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (mRight1ImageButtonClickListener != null) {
					mRight1ImageButtonClickListener.onClick();
				}
			}
		});
		mLayoutRight2ImageButtonLayout = (LinearLayout) mRightImageButton
		.findViewById(R.id.header_layout_right_2imagebuttonlayout);
		mIbRightImageButton2 = (ImageButton) mRightImageButton
		.findViewById(R.id.header_ib_right_2imagebutton);
		mLayoutRight2ImageButtonLayout.setOnClickListener(new OnClickListener() {
		
			@Override
			public void onClick(View arg0) {
				if (mRight2ImageButtonClickListener != null) {
					mRight2ImageButtonClickListener.onClick();
				}
			}
		});
		
		mLayoutMiddleImageButtonLayout
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						if (mMiddleImageButtonClickListener != null) {
							mMiddleImageButtonClickListener.onClick();
						}
					}
				});
	}

	public void setTitleChat(int iconImageId, int iconImageBg,
			CharSequence title, CharSequence subTitle, int middleImageId,
			onMiddleImageButtonClickListener middleImageButtonClickListener,
			int rightImageId,
			onRightImageButtonClickListener rightImageButtonClickListener) {
		mIvLogo.setImageResource(iconImageId);
		mIvLogo.setBackgroundResource(iconImageBg);
		setDefaultTitle(title, subTitle);
		mIbMiddleImageButton.setImageResource(middleImageId);
		mIbRightImageButton.setImageResource(rightImageId);
		mMiddleImageButtonClickListener = middleImageButtonClickListener;
		mRightImageButtonClickListener = rightImageButtonClickListener;

	}

	public void setTitleChat2(int iconImageId, int iconImageBg,
			CharSequence title, CharSequence subTitle, int middleImageId,
			onMiddleImageButtonClickListener middleImageButtonClickListener,
			int rightImageId1,
			onRight1ImageButtonClickListener right1ImageButtonClickListener,
			int rightImageId2,
			onRight2ImageButtonClickListener right2ImageButtonClickListener) {
		mIvLogo.setImageResource(iconImageId);
		mIvLogo.setBackgroundResource(iconImageBg);
		setDefaultTitle(title, subTitle);
		mIbMiddleImageButton.setImageResource(middleImageId);
		mIbRightImageButton1.setImageResource(rightImageId1);
		mIbRightImageButton2.setImageResource(rightImageId2);
		mMiddleImageButtonClickListener = middleImageButtonClickListener;
		mRight1ImageButtonClickListener = right1ImageButtonClickListener;
		mRight2ImageButtonClickListener = right2ImageButtonClickListener;
	}
	
	public enum HeaderStyle {
		DEFAULT_TITLE, TITLE_RIGHT_TEXT, TITLE_RIGHT_IMAGEBUTTON, TITLE_NEARBY_PEOPLE, TITLE_NEARBY_GROUP, TITLE_CHAT, TITLE_CONTACTLIST,TITLE_CHAT2;
	}

	public enum SearchState {
		INPUT, SEARCH;
	}

	public void setOnSearchListener(onSearchListener onSearchListener) {
		mOnSearchListener = onSearchListener;
	}

	public void setOnMiddleImageButtonClickListener(
			onMiddleImageButtonClickListener listener) {
		mMiddleImageButtonClickListener = listener;
	}

	public void setOnRightImageButtonClickListener(
			onRightImageButtonClickListener listener) {
		mRightImageButtonClickListener = listener;
	}

	public interface onMiddleImageButtonClickListener {
		void onClick();
	}

	public interface onRightImageButtonClickListener {
		void onClick();
	}
	
	public interface onRight1ImageButtonClickListener {
		void onClick();
	}
	public interface onRight2ImageButtonClickListener {
		void onClick();
	}
	
	public interface onSearchListener {
		void onSearch(EditText et);
	}
	
//	private CharSequence title1 = "";
//	private CharSequence title2 = "";
	public void setTitleContactList(int iconImageId, int iconImageBg,
			CharSequence spinnerText,CharSequence title,
			int middleImageId,
			onMiddleImageButtonClickListener middleImageButtonClickListener,
			CharSequence switcherLeftText, CharSequence switcherRightText,
			onSwitcherButtonClickListener switcherButtonClickListener) {
		mSbRightSwitcherButton.setVisibility(View.VISIBLE);
		mIvLogo.setImageResource(iconImageId);
		mIvLogo.setBackgroundResource(iconImageBg);
//		title1 = spinnerText;
//		title2 = title;
		setDefaultTitle(spinnerText, null);
		if (middleImageId > 0) {
			mIbMiddleImageButton.setImageResource(middleImageId);
		}
		mMiddleImageButtonClickListener = middleImageButtonClickListener;
		mLayoutMiddleImageButtonLayout
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						if (mMiddleImageButtonClickListener != null) {
							mMiddleImageButtonClickListener.onClick();
						}
					}
				});
		mSbRightSwitcherButton.setLeftText(switcherLeftText);
		mSbRightSwitcherButton.setRightText(switcherRightText);
		mSwitcherButtonClickListener = switcherButtonClickListener;
		mSbRightSwitcherButton
				.setOnSwitcherButtonClickListener(mSwitcherButtonClickListener);
	}
}

