package com.spice.im.friend;

import org.jivesoftware.smack.packet.IQ;

public class FriendAddIQ extends IQ{
    //elementName = friendaddiq
	//namespace = com:isharemessage:friendaddiq
    private String id;

    private String apikey;
    
    private String uid;
    
    private String username = "";
    
    private String uuid;
    
    private String fuid;//好友uid
    
    private String note;//留言备注
    
    private String op;//add ignore
    
    private String hashcode;//apikey+uid+fuid 使用登录成功后返回的sessionid作为密码3des运算
    
    public FriendAddIQ(){}
    
    @Override
    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append("friendaddiq").append(" xmlns=\"").append(
                "com:isharemessage:friendaddiq").append("\">");
        if (id != null) {
            buf.append("<id>").append(id).append("</id>");
        }
        if (apikey != null) {
            buf.append("<apikey>").append(apikey).append("</apikey>");
        }
        if (uid != null) {
            buf.append("<uid>").append(uid).append("</uid>");
        }
        if (username != null) {
            buf.append("<username>").append(username).append("</username>");
        }
        if (uuid != null) {
            buf.append("<uuid>").append(uuid).append("</uuid>");
        }
        if (fuid != null) {
            buf.append("<fuid>").append(fuid).append("</fuid>");
        }
        if (note != null) {
            buf.append("<note>").append(note).append("</note>");
        }
        if (op != null) {
            buf.append("<op>").append(op).append("</op>");
        }
        if (hashcode != null) {
            buf.append("<hashcode>").append(hashcode).append("</hashcode>");
        }
        buf.append("</").append("friendaddiq").append(">");
        return buf.toString();
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

	public String getApikey() {
		return apikey;
	}
	public void setApikey(String apikey) {
		this.apikey = apikey;
	}
	
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getFuid() {
		return fuid;
	}
	public void setFuid(String fuid) {
		this.fuid = fuid;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getOp() {
		return op;
	}
	public void setOp(String op) {
		this.op = op;
	}
	public String getHashcode() {
		return hashcode;
	}
	public void setHashcode(String hashcode) {
		this.hashcode = hashcode;
	}
}
