package com.spice.im.friend;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;


public class SearchIQProvider implements IQProvider{
    public SearchIQProvider() {
    }
    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {

    	SearchIQ searchIQ = new SearchIQ();
        for (boolean done = false; !done;) {
            int eventType = parser.next();
            if (eventType == 2) {
                if ("id".equals(parser.getName())) {
                	searchIQ.setId(parser.nextText());
                }
                if ("apikey".equals(parser.getName())) {
                	searchIQ.setApikey(parser.nextText());
                }
                if ("uid".equals(parser.getName())) {
                	searchIQ.setUid(parser.nextText());
                }
                if ("searchkey".equals(parser.getName())) {
                	searchIQ.setSearchkey(parser.nextText());
                }
                if ("uuid".equals(parser.getName())) {
                	searchIQ.setUuid(parser.nextText());
                }
                if ("hashcode".equals(parser.getName())) {
                	searchIQ.setHashcode(parser.nextText());
                }
            } else if (eventType == 3
                    && "searchiq".equals(parser.getName())) {
                done = true;
            }
        }

        return searchIQ;
    }
}
