package com.spice.im.friend;

import org.jivesoftware.smack.packet.IQ;

public class SearchIQ extends IQ{
    //elementName = searchiq
	//namespace = com:isharemessage:searchiq
    private String id;

    private String apikey;
    
    private String uid;
    
    private String searchkey = "";
    
    private String uuid;
    
    private String hashcode;//apikey+uid+uuid 使用登录成功后返回的sessionid作为密码3des运算
    
    public SearchIQ(){}
    
    @Override
    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append("searchiq").append(" xmlns=\"").append(
                "com:isharemessage:searchiq").append("\">");
        if (id != null) {
            buf.append("<id>").append(id).append("</id>");
        }
        if (apikey != null) {
            buf.append("<apikey>").append(apikey).append("</apikey>");
        }
        if (uid != null) {
            buf.append("<uid>").append(uid).append("</uid>");
        }
        if (searchkey != null) {
            buf.append("<searchkey>").append(searchkey).append("</searchkey>");
        }
        if (uuid != null) {
            buf.append("<uuid>").append(uuid).append("</uuid>");
        }
        if (hashcode != null) {
            buf.append("<hashcode>").append(hashcode).append("</hashcode>");
        }
        buf.append("</").append("searchiq").append(">");
        return buf.toString();
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

	public String getApikey() {
		return apikey;
	}
	public void setApikey(String apikey) {
		this.apikey = apikey;
	}
	
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	
	public String getSearchkey() {
		return searchkey;
	}
	public void setSearchkey(String searchkey) {
		this.searchkey = searchkey;
	}
	
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getHashcode() {
		return hashcode;
	}
	public void setHashcode(String hashcode) {
		this.hashcode = hashcode;
	}
}
