package com.spice.im.friend;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.jivesoftware.smack.packet.IQ;

public class FriendAddIQResponse extends IQ{
    private String id;

    private String apikey;

    private String retcode;//0000/0010/0011/0012 好友添加请求发送成功,0001 false ,0002 false(hash校验失败),9999
    //9998 no_authority_expiration
    //0003 已经是好友
    //0004 非实名不能添加好友
    //0005 space不能为空
    //0006 黑名单用户
    //0007 已达到最大好友数
    //0008 已达到最大好友数
    //0009 no_privilege_videophoto
    //0010 好友邀请信息写入成功
    //0011 好友邀请信息写入失败，邀请曾经已发送，等待用户确认
    //0012 好友邀请信息写入成功
    //0013 拒绝好友添加邀请成功
    
    private String memo;//状态描述:好友添加请求发送成功，好友添加请求发送失败
    
    public FriendAddIQResponse() {
    }
    
    @Override
    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append("friendaddiq").append(" xmlns=\"").append(
                "com:isharemessage:friendaddiq").append("\">");
        if (id != null) {
            buf.append("<id>").append(id).append("</id>");
        }
        if (apikey != null) {
            buf.append("<apikey>").append(apikey).append("</apikey>");
        }
        if(retcode!=null){
        	buf.append("<retcode>").append(retcode).append("</retcode>");
        }
        if(memo!=null){
        	buf.append("<memo>").append(memo).append("</memo>");
        }
        buf.append("</").append("friendaddiq").append(">");
        return buf.toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

	public String getRetcode() {
		return retcode;
	}
	public void setRetcode(String retcode) {
		this.retcode = retcode;
	}

	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
}
