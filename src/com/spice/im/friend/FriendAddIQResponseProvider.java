package com.spice.im.friend;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;


public class FriendAddIQResponseProvider implements IQProvider{
	public FriendAddIQResponseProvider(){
		
	}
    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {

    	FriendAddIQResponse friendAddIQResponse = new FriendAddIQResponse();
        for (boolean done = false; !done;) {
            int eventType = parser.next();
            if (eventType == 2) {
                if ("id".equals(parser.getName())) {
                	friendAddIQResponse.setId(parser.nextText());
                }
                if ("apikey".equals(parser.getName())) {
                	friendAddIQResponse.setApikey(parser.nextText());
                }
                if ("retcode".equals(parser.getName())) {
                	friendAddIQResponse.setRetcode(parser.nextText());
                }
                if ("memo".equals(parser.getName())) {
                	friendAddIQResponse.setMemo(parser.nextText());
                }
            } else if (eventType == 3
                    && "friendaddiq".equals(parser.getName())) {
                done = true;
            }
        }

        return friendAddIQResponse;
    }
}

