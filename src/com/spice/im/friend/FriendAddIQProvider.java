package com.spice.im.friend;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;


public class FriendAddIQProvider implements IQProvider{
    public FriendAddIQProvider() {
    }
    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {

    	FriendAddIQ friendAddIQ = new FriendAddIQ();
        for (boolean done = false; !done;) {
            int eventType = parser.next();
            if (eventType == 2) {
                if ("id".equals(parser.getName())) {
                	friendAddIQ.setId(parser.nextText());
                }
                if ("apikey".equals(parser.getName())) {
                	friendAddIQ.setApikey(parser.nextText());
                }
                if ("uid".equals(parser.getName())) {
                	friendAddIQ.setUid(parser.nextText());
                }
                if ("username".equals(parser.getName())) {
                	friendAddIQ.setUsername(parser.nextText());
                }
                if ("uuid".equals(parser.getName())) {
                	friendAddIQ.setUuid(parser.nextText());
                }
                if ("fuid".equals(parser.getName())) {
                	friendAddIQ.setFuid(parser.nextText());
                }
                if ("note".equals(parser.getName())) {
                	friendAddIQ.setNote(parser.nextText());
                }
                if ("op".equals(parser.getName())) {
                	friendAddIQ.setOp(parser.nextText());
                }
                if ("hashcode".equals(parser.getName())) {
                	friendAddIQ.setHashcode(parser.nextText());
                }
            } else if (eventType == 3
                    && "friendaddiq".equals(parser.getName())) {
                done = true;
            }
        }

        return friendAddIQ;
    }
}
