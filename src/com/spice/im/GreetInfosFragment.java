package com.spice.im;

import java.lang.reflect.Field;

import android.annotation.SuppressLint;
import android.app.Activity;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.spice.im.ContactFrameActivity.CustomListAdapter;
import com.spice.im.ContactFrameActivity.GreetListOnClick;
import com.spice.im.ContactFrameActivity.GreetListOnLongClick;
import com.spice.im.ContactFrameActivity.MyOnRefreshListener2;
@SuppressLint("ValidFragment")
public class GreetInfosFragment extends BaseFragment {
	public PullToRefreshListView ptrlv;
	private CustomListAdapter mAdapter;
	private OnPtrlvListener2 mListener;
	public void setAdapter(CustomListAdapter adapter){
		mAdapter = adapter;
	}
	private GreetListOnClick mOnGreetClick;
	public void setOnGreetClick(GreetListOnClick onGreetClick){
		mOnGreetClick = onGreetClick;
	}
	private GreetListOnLongClick mGreetLongClick;
	public void setOnGreetLongClick(GreetListOnLongClick onGreetLongClick){
		mGreetLongClick = onGreetLongClick;
	}
	
    private MyOnRefreshListener2 mMyOnRefreshListener2;
	public void setMyOnRefreshListener2(MyOnRefreshListener2 myOnRefreshListener2){
		mMyOnRefreshListener2 = myOnRefreshListener2;
	}
	public GreetInfosFragment() {
		super();
	}

	public GreetInfosFragment(Activity activity,
			Context context) {
		
		super(activity, context);
		Log.e("MMMMMMMMMMMM20141031GreetInfosFragment(init111)MMMMMMMMMMMM", "++++++++++++++20141031GreetInfosFragment(init111)");
//		mAdapter = adapter;
//		mOnContactClick = onContactClick;
//		mMyOnRefreshListener2 = myOnRefreshListener2;
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.e("MMMMMMMMMMMM20141031GreetInfosFragment(init222)MMMMMMMMMMMM", "++++++++++++++20141031GreetInfosFragment(init222)");
		mView = inflater.inflate(R.layout.fragment_contactlists, container,
				false);
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	protected void initViews() {
		Log.e("MMMMMMMMMMMM20141031GreetInfosFragment(init333)MMMMMMMMMMMM", "++++++++++++++20141031GreetInfosFragment(init333)");
		ptrlv = (PullToRefreshListView) findViewById(R.id.ptrlvHeadLineNews);
	}
//	@Override
//	protected void initEvents() {
//		
//	}
//	@Override
	protected void initEvents() {
        ptrlv.setOnItemClickListener(mOnGreetClick);
//        ptrlv.setOnLongClickListener(mGreetLongClick);
        ptrlv.setOnItemLongClickListener(mGreetLongClick);//长按操作，删除记录
        ptrlv.setMode(Mode.BOTH);
        ptrlv.setOnRefreshListener(mMyOnRefreshListener2);
//      ptrlv.getRefreshableView().setRecyclerListener();
        ptrlv.setAdapter(mAdapter);
        mListener.onPtrlvSet2(ptrlv);
	}

	@Override
	protected void init() {
//		getPeoples();
	}
	
    public interface OnPtrlvListener2 {
        public void onPtrlvSet2(PullToRefreshListView ptrlv);
    }
    
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnPtrlvListener2) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnPtrlvListener");
        }
    }
    
    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager =
                    Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
