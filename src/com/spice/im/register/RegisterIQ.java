package com.spice.im.register;

import org.jivesoftware.smack.packet.IQ;

public class RegisterIQ extends IQ{
	//用户注册：填写用户名、密码、确认密码、邮箱
    //elementName = registeriq
	//namespace = com:isharemessage:registeriq
    private String id;

    private String apikey;
    
//    private String uid;//该字段无用
    
    private String username;
    
    private String password;
    
    private String password2;
    
    private String email;
    
    private String uuid;
    
    private String hashcode;//apikey+uuid 使用登录成功后返回的sessionid作为密码3des运算
    
    public RegisterIQ(){}
    
    @Override
    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append("registeriq").append(" xmlns=\"").append(
                "com:isharemessage:registeriq").append("\">");
        if (id != null) {
            buf.append("<id>").append(id).append("</id>");
        }
        if (apikey != null) {
            buf.append("<apikey>").append(apikey).append("</apikey>");
        }
//        if (uid != null) {
//            buf.append("<uid>").append(uid).append("</uid>");
//        }
        if (username != null) {
            buf.append("<username>").append(username).append("</username>");
        }
        if (password != null) {
            buf.append("<password>").append(password).append("</password>");
        }
        if (password2 != null) {
            buf.append("<password2>").append(password2).append("</password2>");
        }
        if (email != null) {
            buf.append("<email>").append(email).append("</email>");
        }
        if (uuid != null) {
            buf.append("<uuid>").append(uuid).append("</uuid>");
        }
        if (hashcode != null) {
            buf.append("<hashcode>").append(hashcode).append("</hashcode>");
        }
        buf.append("</").append("registeriq").append(">");
        return buf.toString();
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

	public String getApikey() {
		return apikey;
	}
	public void setApikey(String apikey) {
		this.apikey = apikey;
	}
	
//	public String getUid() {
//		return uid;
//	}
//	public void setUid(String uid) {
//		this.uid = uid;
//	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getPassword2() {
		return password2;
	}
	public void setPassword2(String password2) {
		this.password2 = password2;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getHashcode() {
		return hashcode;
	}
	public void setHashcode(String hashcode) {
		this.hashcode = hashcode;
	}
}
