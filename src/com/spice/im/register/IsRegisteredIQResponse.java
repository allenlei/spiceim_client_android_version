package com.spice.im.register;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.jivesoftware.smack.packet.IQ;

public class IsRegisteredIQResponse extends IQ{
    private String id;

    private String apikey;
    
    private String isregister;//是否已注册 : 0 未注册 1 已注册额
    
    private String verification;//验证码

    private String retcode;//0000 success,0001 用户名不能为空,0002 false(hash校验失败),9999  0005 用户名不允许注册  0003 用户名包含非法字符  0004用户名已被注册
    
    private String memo;//状态描述:登录成功，登录失败（用户名或密码错误）,登录失败（原因）
    
    public IsRegisteredIQResponse() {
    }
    
    @Override
    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append("isregisterediq").append(" xmlns=\"").append(
                "com:isharemessage:isregisterediq").append("\">");
        if (id != null) {
            buf.append("<id>").append(id).append("</id>");
        }
        if (apikey != null) {
            buf.append("<apikey>").append(apikey).append("</apikey>");
        }
        if (isregister != null) {
            buf.append("<isregister>").append(isregister).append("</isregister>");
        }
        if (verification != null) {
            buf.append("<verification>").append(verification).append("</verification>");
        }
        if(retcode!=null){
        	buf.append("<retcode>").append(retcode).append("</retcode>");
        }
        if(memo!=null){
        	buf.append("<memo>").append(memo).append("</memo>");
        }
        buf.append("</").append("isregisterediq").append(">");
        return buf.toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }
    public String getIsregister() {
        return isregister;
    }

    public void setIsregister(String isregister) {
        this.isregister = isregister;
    }
    public String getVerification() {
        return verification;
    }

    public void setVerification(String verification) {
        this.verification = verification;
    }
	public String getRetcode() {
		return retcode;
	}
	public void setRetcode(String retcode) {
		this.retcode = retcode;
	}

	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
}
