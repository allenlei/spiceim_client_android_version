package com.spice.im.register;

import org.jivesoftware.smack.packet.IQ;

public class RegisterAllIQ extends IQ{
	//用户注册：填写用户名、密码、确认密码、邮箱
    //elementName = registeralliq
	//namespace = com:isharemessage:registeralliq
    private String id;

    private String apikey;
    
//    private String uid;
    
    private String username;
    
    private String password;
    
    private String password2;
    
    private String email;
    
    private String name;//jchome_space表中name姓名
    private String avatar;//jchome_space表中avator值的值域：0默认未上传头像，1已上传头像
    private String note;//jchome_spacefield表中的note以及spacenote字段，代表用户更新的状态，可以让好友知道他在做什么
    private String sex;//jchome_spacefield表中sex 0 默认未知，1男，2女
	//比如1983.1.1或1983.12.12  即月日如果为个位数，前面不能补0
    private String birthyear;//1983
    private String birthmonth;//1或11
    private String birthday;//1或12
    
    private String uuid;
    
    private String hashcode;//apikey+uuid 使用登录成功后返回的sessionid作为密码3des运算
    
    public RegisterAllIQ(){}
    
    @Override
    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append("registeralliq").append(" xmlns=\"").append(
                "com:isharemessage:registeralliq").append("\">");
        if (id != null) {
            buf.append("<id>").append(id).append("</id>");
        }
        if (apikey != null) {
            buf.append("<apikey>").append(apikey).append("</apikey>");
        }
//        if (uid != null) {
//            buf.append("<uid>").append(uid).append("</uid>");
//        }
        if (username != null) {
            buf.append("<username>").append(username).append("</username>");
        }
        if (password != null) {
            buf.append("<password>").append(password).append("</password>");
        }
        if (password2 != null) {
            buf.append("<password2>").append(password2).append("</password2>");
        }
        if (email != null) {
            buf.append("<email>").append(email).append("</email>");
        }
        if (name != null) {
            buf.append("<name>").append(name).append("</name>");
        }
        if (avatar != null) {
            buf.append("<avatar>").append(avatar).append("</avatar>");
        }
        if (note != null) {
            buf.append("<note>").append(note).append("</note>");
        }
        if (sex != null) {
            buf.append("<sex>").append(sex).append("</sex>");
        }
        if (birthyear != null) {
            buf.append("<birthyear>").append(birthyear).append("</birthyear>");
        }
        if (birthmonth != null) {
            buf.append("<birthmonth>").append(birthmonth).append("</birthmonth>");
        }
        if (birthday != null) {
            buf.append("<birthday>").append(birthday).append("</birthday>");
        }
        if (uuid != null) {
            buf.append("<uuid>").append(uuid).append("</uuid>");
        }
        if (hashcode != null) {
            buf.append("<hashcode>").append(hashcode).append("</hashcode>");
        }
        buf.append("</").append("registeralliq").append(">");
        return buf.toString();
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

	public String getApikey() {
		return apikey;
	}
	public void setApikey(String apikey) {
		this.apikey = apikey;
	}
	
//	public String getUid() {
//		return uid;
//	}
//	public void setUid(String uid) {
//		this.uid = uid;
//	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getPassword2() {
		return password2;
	}
	public void setPassword2(String password2) {
		this.password2 = password2;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	
	public String getBirthyear() {
		return birthyear;
	}
	public void setBirthyear(String birthyear) {
		this.birthyear = birthyear;
	}
	public String getBirthmonth() {
		return birthmonth;
	}
	public void setBirthmonth(String birthmonth) {
		this.birthmonth = birthmonth;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getHashcode() {
		return hashcode;
	}
	public void setHashcode(String hashcode) {
		this.hashcode = hashcode;
	}
}
