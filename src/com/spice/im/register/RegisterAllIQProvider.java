package com.spice.im.register;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;


public class RegisterAllIQProvider implements IQProvider{
    public RegisterAllIQProvider() {
    }
    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {

    	RegisterAllIQ registerIQ = new RegisterAllIQ();
        for (boolean done = false; !done;) {
            int eventType = parser.next();
            if (eventType == 2) {
                if ("id".equals(parser.getName())) {
                	registerIQ.setId(parser.nextText());
                }
                if ("apikey".equals(parser.getName())) {
                	registerIQ.setApikey(parser.nextText());
                }
//                if ("uid".equals(parser.getName())) {
//                	searchIQ.setUid(parser.nextText());
//                }
                if ("username".equals(parser.getName())) {
                	registerIQ.setUsername(parser.nextText());
                }
                if ("password".equals(parser.getName())) {
                	registerIQ.setPassword(parser.nextText());
                }
                if ("password2".equals(parser.getName())) {
                	registerIQ.setPassword2(parser.nextText());
                }
                if ("email".equals(parser.getName())) {
                	registerIQ.setEmail(parser.nextText());
                }
                if ("name".equals(parser.getName())) {
                	registerIQ.setName(parser.nextText());
                }
                if ("avatar".equals(parser.getName())) {
                	registerIQ.setAvatar(parser.nextText());
                }
                if ("note".equals(parser.getName())) {
                	registerIQ.setNote(parser.nextText());
                }
                if ("sex".equals(parser.getName())) {
                	registerIQ.setSex(parser.nextText());
                }
                if ("birthyear".equals(parser.getName())) {
                	registerIQ.setBirthyear(parser.nextText());
                }
                if ("birthmonth".equals(parser.getName())) {
                	registerIQ.setBirthmonth(parser.nextText());
                }
                if ("birthday".equals(parser.getName())) {
                	registerIQ.setBirthday(parser.nextText());
                }
                if ("uuid".equals(parser.getName())) {
                	registerIQ.setUuid(parser.nextText());
                }
                if ("hashcode".equals(parser.getName())) {
                	registerIQ.setHashcode(parser.nextText());
                }
            } else if (eventType == 3
                    && "registeralliq".equals(parser.getName())) {
                done = true;
            }
        }

        return registerIQ;
    }
}
