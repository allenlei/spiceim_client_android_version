package com.spice.im.register;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;


public class RegisterIQResponseProvider implements IQProvider{
	public RegisterIQResponseProvider(){
		
	}
    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {

    	RegisterIQResponse mucInviteIQResponse = new RegisterIQResponse();
        for (boolean done = false; !done;) {
            int eventType = parser.next();
            if (eventType == 2) {
                if ("id".equals(parser.getName())) {
                	mucInviteIQResponse.setId(parser.nextText());
                }
                if ("apikey".equals(parser.getName())) {
                	mucInviteIQResponse.setApikey(parser.nextText());
                }
                if ("retcode".equals(parser.getName())) {
                	mucInviteIQResponse.setRetcode(parser.nextText());
                }
                if ("memo".equals(parser.getName())) {
                	mucInviteIQResponse.setMemo(parser.nextText());
                }
                if ("uid".equals(parser.getName())) {
                	mucInviteIQResponse.setUid(parser.nextText());
                }
            } else if (eventType == 3
                    && "registeriq".equals(parser.getName())) {
                done = true;
            }
        }

        return mucInviteIQResponse;
    }
}

