package com.spice.im.register;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;


public class RegisteredIQProvider implements IQProvider{
    public RegisteredIQProvider() {
    }
    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {

    	RegisterIQ registerIQ = new RegisterIQ();
        for (boolean done = false; !done;) {
            int eventType = parser.next();
            if (eventType == 2) {
                if ("id".equals(parser.getName())) {
                	registerIQ.setId(parser.nextText());
                }
                if ("apikey".equals(parser.getName())) {
                	registerIQ.setApikey(parser.nextText());
                }
//                if ("uid".equals(parser.getName())) {
//                	searchIQ.setUid(parser.nextText());
//                }
                if ("username".equals(parser.getName())) {
                	registerIQ.setUsername(parser.nextText());
                }
                if ("password".equals(parser.getName())) {
                	registerIQ.setPassword(parser.nextText());
                }
                if ("password2".equals(parser.getName())) {
                	registerIQ.setPassword2(parser.nextText());
                }
                if ("email".equals(parser.getName())) {
                	registerIQ.setEmail(parser.nextText());
                }
                if ("uuid".equals(parser.getName())) {
                	registerIQ.setUuid(parser.nextText());
                }
                if ("hashcode".equals(parser.getName())) {
                	registerIQ.setHashcode(parser.nextText());
                }
            } else if (eventType == 3
                    && "registeriq".equals(parser.getName())) {
                done = true;
            }
        }

        return registerIQ;
    }
}
