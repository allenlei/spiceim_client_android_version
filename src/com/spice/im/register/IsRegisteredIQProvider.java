package com.spice.im.register;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;


public class IsRegisteredIQProvider implements IQProvider{
    public IsRegisteredIQProvider() {
    }
    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {

    	IsRegisteredIQ searchIQ = new IsRegisteredIQ();
        for (boolean done = false; !done;) {
            int eventType = parser.next();
            if (eventType == 2) {
                if ("id".equals(parser.getName())) {
                	searchIQ.setId(parser.nextText());
                }
                if ("apikey".equals(parser.getName())) {
                	searchIQ.setApikey(parser.nextText());
                }
//                if ("uid".equals(parser.getName())) {
//                	searchIQ.setUid(parser.nextText());
//                }
                if ("username".equals(parser.getName())) {
                	searchIQ.setUsername(parser.nextText());
                }
                if ("uuid".equals(parser.getName())) {
                	searchIQ.setUuid(parser.nextText());
                }
                if ("hashcode".equals(parser.getName())) {
                	searchIQ.setHashcode(parser.nextText());
                }
            } else if (eventType == 3
                    && "isregisterediq".equals(parser.getName())) {
                done = true;
            }
        }

        return searchIQ;
    }
}
