package com.spice.im.gps;

import java.io.Serializable;
import java.util.Date;

import com.stb.core.chat.ContactGroup;

//import com.isharemessage.provider.plugin.ContactGroup;


//import org.jivesoftware.openfire.user.User;
//import org.jivesoftware.util.cache.CacheSizes;
//import org.jivesoftware.util.cache.Cacheable;

/** 
 * 用户gps信息
 * @author  程辉 
 * @version V1.0  创建时间：2013-6-15 下午4:01:22 
 */
public class GPS implements Serializable{//implements Cacheable
	private static final long serialVersionUID = 1L;
	private int id;
	private String user_id;
//	private User user;
	private ContactGroup cGroup;
	private String longitude;
	private String latitude;
	private String time;
	private String geolocation;
	private String distance;//距离
	
	private String gender;
	private String birthday;
	
	private String name;
	private String email;
	
	private String usersign;//20180102 added by allen
	private String avatarPath;//20180102 added by allen
	
//    public int getCachedSize() {
//        // Approximate the size of the object in bytes by calculating the size
//        // of each field.
//        int size = 0;
//        size += CacheSizes.sizeOfObject();              // overhead of object
//        size += CacheSizes.sizeOfLong();                // id
//        size += CacheSizes.sizeOfString(user_id);      // username
//        size += CacheSizes.sizeOfObject();          // name
//        size += CacheSizes.sizeOfString(longitude);
//        size += CacheSizes.sizeOfString(latitude);
//        size += CacheSizes.sizeOfString(time);
//        size += CacheSizes.sizeOfString(geolocation);
//        size += CacheSizes.sizeOfString(distance);
//        size += CacheSizes.sizeOfString(gender);
//        size += CacheSizes.sizeOfString(birthday);
//        size += CacheSizes.sizeOfString(usersign);
//        return size;
//    }
    
	//ofgps.user_id,ofgps.longitude,ofgps.latitude,ofgps.time,ofgps.geolocation,ofUser.name,ofUser.email,ofUser.creationDate,ofUser.modificationDate
	public GPS(String user_id,
			String longitude,
			String latitude,
			String time,
			String geolocation,
			String name,
			String email,
			String creationDate,
			String modificationDate,
			String gender,
			String birthday,
			String usersign,
			String avatarPath){//,String usersign
		this.user_id = user_id;
//		this.user = new User(user_id,name,email,new Date(Long.valueOf(creationDate)),new Date(Long.valueOf(modificationDate)));
		this.cGroup = new ContactGroup();
		this.cGroup.setType(0);
		this.cGroup.setUid(this.user_id);
		this.cGroup.setName(name);
		this.cGroup.setEmail(email);
		
		this.name = name;
		this.email = email;
		
		this.longitude = longitude;
		this.latitude = latitude;
//		this.time = new Date(Long.valueOf(time));
		this.time = time;
		this.geolocation = geolocation;
		this.gender = gender;
		this.birthday = birthday;
		this.usersign = usersign;
		this.avatarPath = avatarPath;
		
	}
	
	public GPS(String user_id,
			String longitude,
			String latitude,
			String time,
			String geolocation){
		this.user_id = user_id;
		this.longitude = longitude;
		this.latitude = latitude;
//		this.time = new Date(Long.valueOf(time));
		this.time = time;
		this.geolocation = geolocation;
		
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	
//	public User getUser() {
//		return user;
//	}
//	public void setUser(User user) {
//		this.user = user;
//	}
	public ContactGroup getContactGroup() {
		return cGroup;
	}
	public void setContactGroup(ContactGroup cGroup) {
		this.cGroup = cGroup;
	}
	
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getGeolocation() {
		return geolocation;
	}
	public void setGeolocation(String geolocation) {
		this.geolocation = geolocation;
	}
	public String getDistance() {
		return distance;
	}
	public void setDistance(String distance) {
		this.distance = distance;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getUsersign() {
		return usersign;
	}
	public void setUsersign(String usersign) {
		this.usersign = usersign;
	}
    public void setAvatarPath(String mAvatarPath) {
    	avatarPath = mAvatarPath;
    }
    public String getAvatarPath(){
    	return avatarPath;
    }
	public String getName() {  
		return name;  
	}  
	public void setName(String name) {  
		this.name = name;  
	} 
	public String getEmail() {  
		return email;  
	}  
	public void setEmail(String email) {  
		this.email = email;  
	} 
}
