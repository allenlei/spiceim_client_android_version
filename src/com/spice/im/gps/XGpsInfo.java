package com.spice.im.gps;

public class XGpsInfo { 
	public XGpsInfo(){}
	private String name;  
//	private String status;  
//	private String _name="";
	private String uid="";
	private String email="";
	private String distance="";
	private String time = "";//×î½ü¶¨Î»Ê±¼ä
	private String gender;
	private String birthday;
	private String usersign;//20180102 added by allen
	private String avatarPath;//20180102 added by allen
	
//	public String getStatus() {  
//		return status;  
//	}  
//	public void setStatus(String status) {  
//		this.status = status;  
//	}  
	public String getName() {  
		return name;  
	}  
	public void setName(String name) {  
		this.name = name;  
	} 
	public String getUid() {  
		return uid;  
	}  
	public void setUid(String uid) {  
		this.uid = uid;  
	} 
//	public String get_Name() {  
//		return _name;  
//	}  
//	public void set_Name(String _name) {  
//		this._name = _name;  
//	} 
	public String getEmail() {  
		return email;  
	}  
	public void setEmail(String email) {  
		this.email = email;  
	} 
	public String getDistance() {  
		return distance;  
	}  
	public void setDistance(String distance) {  
		this.distance = distance;  
	} 
	public String getTime() {  
		return time;  
	}  
	public void setTime(String time) {  
		this.time = time;  
	} 
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getUsersign() {
		return usersign;
	}
	public void setUsersign(String usersign) {
		this.usersign = usersign;
	}
    public void setAvatarPath(String mAvatarPath) {
    	avatarPath = mAvatarPath;
    }
    public String getAvatarPath(){
    	return avatarPath;
    }
}
