/*
 * Copyright (C) 2010 Moduad Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.spice.im.gps;

import org.jivesoftware.smack.packet.IQ;

/** 
 * This class represents a notifcatin IQ packet.
 *
 * @author Sehwan Noh (devnoh@gmail.com)
 */
public class GpsIQ extends IQ {

    private String id;

    private String apikey;

    private String username;

    private String longitude;

    private String latitude;
    
    private String time;
    
    private String geolocation;
    
    private String genderselect;
	private String ageselect;
	private String timeselect;
    
    private String radius;
    
    private String startIndex;
    
    private String numResults;

    private String hashcode;//apikey+username(uid)+time 
    public GpsIQ() {
    }

    @Override
    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append("gpsiq").append(" xmlns=\"").append(
                "com:stb:gpsiq").append("\">");
        if (id != null) {
            buf.append("<id>").append(id).append("</id>");
        }
        if (apikey != null) {
            buf.append("<apikey>").append(apikey).append("</apikey>");
        }
        if (username != null) {
            buf.append("<username>").append(username).append("</username>");
        }
        if (longitude != null) {
            buf.append("<longitude>").append(longitude).append("</longitude>");
        }
        if (latitude != null) {
            buf.append("<latitude>").append(latitude).append("</latitude>");
        }
        if (time != null) {
            buf.append("<time>").append(time).append("</time>");
        }
        if (geolocation != null) {
            buf.append("<geolocation>").append(geolocation).append("</geolocation>");
        }
        
        if (genderselect != null) {
            buf.append("<genderselect>").append(genderselect).append("</genderselect>");
        }
        if (ageselect != null) {
            buf.append("<ageselect>").append(ageselect).append("</ageselect>");
        }
        if (timeselect != null) {
            buf.append("<timeselect>").append(timeselect).append("</timeselect>");
        }
        
        if (radius != null) {
            buf.append("<radius>").append(radius).append("</radius>");
        }
        
        if (startIndex != null) {
            buf.append("<startIndex>").append(startIndex).append("</startIndex>");
        }
        if (numResults != null) {
            buf.append("<numResults>").append(numResults).append("</numResults>");
        }
        if (hashcode != null) {
            buf.append("<hashcode>").append(hashcode).append("</hashcode>");
        }
        buf.append("</").append("gpsiq").append(">");
        return buf.toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
    
    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
    
    public String getGeolocation() {
        return geolocation;
    }

    public void setGeolocation(String geolocation) {
        this.geolocation = geolocation;
    }
    
    public String getGenderselect() {
        return genderselect;
    }

    public void setGenderselect(String genderselect) {
        this.genderselect = genderselect;
    }
    
    public String getAgeselect() {
        return ageselect;
    }

    public void setAgeselect(String ageselect) {
        this.ageselect = ageselect;
    }
    
    public String getTimeselect() {
        return timeselect;
    }

    public void setTimeselect(String timeselect) {
        this.timeselect = timeselect;
    }
    
    public String getRadius() {
        return radius;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }
    
    
    public String getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(String startIndex) {
        this.startIndex = startIndex;
    }
    
    public String getNumResults() {
        return numResults;
    }

    public void setNumResults(String numResults) {
        this.numResults = numResults;
    }
	public String getHashcode() {
		return hashcode;
	}
	public void setHashcode(String hashcode) {
		this.hashcode = hashcode;
	}

}
