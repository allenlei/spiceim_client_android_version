/*
 * Copyright (C) 2010 Moduad Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.spice.im.gps;

import java.util.HashMap;


import org.jivesoftware.smack.packet.IQ;

import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;

/** 
 * This class parses incoming IQ packets to NotificationIQ objects.
 *
 * @author Sehwan Noh (devnoh@gmail.com)
 */
public class GpsIQResponseProvider implements IQProvider {
//	HashMap hm = new HashMap();
	XGpsInfo xgpsInfo = null;  
    public GpsIQResponseProvider() {
    }

    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {

    	GpsIQResponse gpsIQResponse = new GpsIQResponse();
        for (boolean done = false; !done;) {
            int eventType = parser.next();
            if (eventType == 2) {
                if ("id".equals(parser.getName())) {
                	gpsIQResponse.setId(parser.nextText());
                }
                if ("apikey".equals(parser.getName())) {
                	gpsIQResponse.setApikey(parser.nextText());
                }
                if ("totalnumber".equals(parser.getName())) {
                	gpsIQResponse.setTotalnumber(parser.nextText());
                }
                if ("cttotal".equals(parser.getName())) {
                	gpsIQResponse.setCttotal(parser.nextText());
                }
                if ("user".equals(parser.getName())) {
                	xgpsInfo = new XGpsInfo();  
                	//parser.getAttributeCount();
//                	xgpsInfo.setStatus(parser.getAttributeValue(0));
                	
//                	xgpsInfo.set_Name(parser.getAttributeValue(1)!=null?parser.getAttributeValue(1):"");
                	xgpsInfo.setUid(parser.getAttributeValue(0)!=null?parser.getAttributeValue(0):"");
//                	if(parser.getAttributeCount()==5){
                	xgpsInfo.setEmail(parser.getAttributeValue(1)!=null?parser.getAttributeValue(1):"");
                	xgpsInfo.setDistance(parser.getAttributeValue(2)!=null?parser.getAttributeValue(2):"");
                	xgpsInfo.setTime(parser.getAttributeValue(3)!=null?parser.getAttributeValue(3):"");
                	xgpsInfo.setGender(parser.getAttributeValue(4)!=null?parser.getAttributeValue(4):"");
                	xgpsInfo.setBirthday(parser.getAttributeValue(5)!=null?parser.getAttributeValue(5):"");
                	xgpsInfo.setUsersign(parser.getAttributeValue(6)!=null?parser.getAttributeValue(6):"");
                	xgpsInfo.setAvatarPath(parser.getAttributeValue(7)!=null?parser.getAttributeValue(7):"");
//                	}else
//                		xgpsInfo.setDistance(parser.getAttributeValue(3)!=null?parser.getAttributeValue(3):"");
                	
                	xgpsInfo.setName(parser.nextText());
                	
//                	advancedSearchIQResponse.users.add(parser.nextText());
//                	parser.getAttributeValue(0);
                	gpsIQResponse.users.add(xgpsInfo);
                }
                if ("retcode".equals(parser.getName())) {
                	gpsIQResponse.setRetcode(parser.nextText());
                }
                if ("memo".equals(parser.getName())) {
                	gpsIQResponse.setMemo(parser.nextText());
                }
            } else if (eventType == 3
                    && "gpsiq".equals(parser.getName())) {
                done = true;
            }
        }

        return gpsIQResponse;
    }

}
