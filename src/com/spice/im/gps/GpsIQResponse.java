/*
 * Copyright (C) 2010 Moduad Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.spice.im.gps;

import java.util.ArrayList;

import org.jivesoftware.smack.packet.IQ;

/** 
 * This class represents a notifcatin IQ packet.
 *
 * @author Sehwan Noh (devnoh@gmail.com)
 */
public class GpsIQResponse extends IQ {

    private String id;

    private String apikey;

    private String totalnumber;
    
    private String cttotal;

    public ArrayList users = new ArrayList();
    
    XGpsInfo gpsInfo = null;

    private String retcode;
    private String memo;
    
    public GpsIQResponse() {
    }

    @Override
    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append("gpsiq").append(" xmlns=\"").append(
                "com:stb:gpsiq").append("\">");
        if (id != null) {
            buf.append("<id>").append(id).append("</id>");
        }
        if (apikey != null) {
            buf.append("<apikey>").append(apikey).append("</apikey>");
        }
        if (totalnumber != null) {
            buf.append("<totalnumber>").append(totalnumber).append("</totalnumber>");
        }
        if (cttotal != null) {
            buf.append("<cttotal>").append(cttotal).append("</cttotal>");
        }
        if (users != null && users.size()!=0) {
        	
            buf.append("<users>");
            for(int i=0;i<users.size();i++){
            	gpsInfo = (XGpsInfo)users.get(i);
//            	buf.append("<user>").append(users.get(i)).append("</user>");
            	//<user status="100" "_name=" "uid=test" "email=null">
            	buf.append("<user uid=\""+gpsInfo.getUid()+"\" "+"email=\""+gpsInfo.getEmail()+"\" "+"distance=\""+gpsInfo.getDistance()+"\" "+"time=\""+gpsInfo.getTime()+"\" "+"gender=\""+gpsInfo.getGender()+"\" "+"birthday=\""+gpsInfo.getBirthday()+"\" "+"usersign=\""+gpsInfo.getUsersign()+"\""+"avatarPath=\""+gpsInfo.getAvatarPath()+"\""+">").append(gpsInfo.getName()).append("</user>");
            }
            buf.append("</users>");
        }
        if(retcode!=null){
        	buf.append("<retcode>").append(retcode).append("</retcode>");
        }
        if(memo!=null){
        	buf.append("<memo>").append(memo).append("</memo>");
        }
        buf.append("</").append("gpsiq").append(">");
        return buf.toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getTotalnumber() {
        return totalnumber;
    }

    public void setTotalnumber(String totalnumber) {
        this.totalnumber = totalnumber;
    }
    
    public String getCttotal() {
        return cttotal;
    }

    public void setCttotal(String cttotal) {
        this.cttotal = cttotal;
    }

    public ArrayList getUsers() {
        return users;
    }

    public void setUsers(ArrayList users) {
        this.users = users;
    }
    
	public String getRetcode() {
		return retcode;
	}
	public void setRetcode(String retcode) {
		this.retcode = retcode;
	}

	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}

}
