package com.spice.im.gps;

//package com.immomo.momo.android.popupwindow;

//import com.stb.isharemessage.IConnectionStatusCallback;

import com.spice.im.R;
import com.spice.im.ui.BasePopupWindow;
//import com.stb.isharemessage.IGpsFilterCallback;
//import com.stb.isharemessage.R;
//import com.stb.isharemessage.ui.AdvancedSearch;
//import com.stb.isharemessage.ui.ContactFrameActivity;
//import com.stb.isharemessage.ui.GpsSearchResPullRefListActivity;




import com.spice.im.ui.IGpsFilterCallback;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

//import com.immomo.momo.android.BasePopupWindow;
//import com.immomo.momo.android.activity.R;
//import com.immomo.momo.android.R;

/**
 * @fileName NearByPopupWindow.java
 * @package com.immomo.momo.android.popupwindow
 * @description 附近PopupWindow类
 * @author 任东卫
 * @email 86930007@qq.com
 * @version 1.0
 */
public class NearByPopupWindow extends BasePopupWindow {

	private LinearLayout mLayoutRoot;// 根布局
	private RadioGroup mRgGender;// 性别
	private RadioGroup mRgAge;//年龄
	private RadioGroup mRgTime;// 时间
	private Button mBtnSubmit;// 确认
	private Button mBtnCancel;// 取消

	public NearByPopupWindow(Context context,GpsSearchResPullRefListActivity mActivity) {
		super(LayoutInflater.from(context).inflate(
				R.layout.include_dialog_nearby_filter, null),
				LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT,mActivity);
		setAnimationStyle(R.style.Popup_Animation_PushDownUp);
	}

	@Override
	public void initViews() {
		mLayoutRoot = (LinearLayout) findViewById(R.id.dialog_nearby_layout_root);
		mRgGender = (RadioGroup) findViewById(R.id.dialog_nearby_rg_gender);
        mRgAge = (RadioGroup) findViewById(R.id.dialog_nearby_rg_age);
		mRgTime = (RadioGroup) findViewById(R.id.dialog_nearby_rg_time);
		mBtnSubmit = (Button) findViewById(R.id.dialog_nearby_btn_submit);
		mBtnCancel = (Button) findViewById(R.id.dialog_nearby_btn_cancel);
	}
    int GenderSelect = 0;//0全部 1男 2女
//    <item>不限(0)</item>
//    <item>16–22岁(1)</item>
//    <item>23–30岁(2)</item>
//    <item>31岁以上(3)</item>
    int AgeSelect = 0;
    
    int TimeSelect = 0;//0 不限,1 15分钟,2 60分钟,3 3天
	@Override
	public void initEvents() {
		mLayoutRoot.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
		mBtnSubmit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();
				if (mOnSubmitClickListener != null) {
					mOnSubmitClickListener.onClick();
				}
//				Intent intent =  new Intent(mActivity6, GpsSearchResPullRefListActivity.class);
//				intent.putExtra("GenderSelect", GenderSelect);
//		    	intent.putExtra("AgeSelect",AgeSelect);
//		    	intent.putExtra("TimeSelect",TimeSelect);
//		    	mActivity6.startActivityForResult(intent, 1);
				if (mGpsFilterCallback != null)
					mGpsFilterCallback.gpsFilterChanged(GenderSelect, AgeSelect, TimeSelect);
			}
		});
		mBtnCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
		mRgGender.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// 暂时不做任何操作
				switch(group.getCheckedRadioButtonId()){
					case R.id.dialog_nearby_rb_gender_all:
						mRgGender.check(R.id.dialog_nearby_rb_gender_all);
						GenderSelect = 0;
						break;
					case R.id.dialog_nearby_rb_gender_male:
						mRgGender.check(R.id.dialog_nearby_rb_gender_male);
						GenderSelect = 1;
						break;
					case R.id.dialog_nearby_rb_gender_female:
						mRgGender.check(R.id.dialog_nearby_rb_gender_female);
						GenderSelect = 2;
						break;						
				}
			}
		});
		mRgAge.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// 暂时不做任何操作
				switch(group.getCheckedRadioButtonId()){
					case R.id.dialog_nearby_rb_age_nolimitation:
						mRgAge.check(R.id.dialog_nearby_rb_age_nolimitation);
						AgeSelect = 0;
						break;
					case R.id.dialog_nearby_rb_age_sixteentwenty:
						mRgAge.check(R.id.dialog_nearby_rb_age_sixteentwenty);
						AgeSelect = 1;
						break;
					case R.id.dialog_nearby_rb_age_twentythreethirty:
						mRgAge.check(R.id.dialog_nearby_rb_age_twentythreethirty);
						AgeSelect = 2;
						break;	
					case R.id.dialog_nearby_rb_age_thirtyonefourty:
						mRgAge.check(R.id.dialog_nearby_rb_age_thirtyonefourty);
						AgeSelect = 3;
						break;	
				}				
			}
		});
		mRgTime.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// 暂时不做任何操作
				switch(group.getCheckedRadioButtonId()){
					case R.id.dialog_nearby_rb_time_fifteenminutes:
						mRgTime.check(R.id.dialog_nearby_rb_time_fifteenminutes);
						TimeSelect = 0;
						break;
					case R.id.dialog_nearby_rb_time_sixtyminutes:
						mRgTime.check(R.id.dialog_nearby_rb_time_sixtyminutes);
						TimeSelect = 1;
						break;
					case R.id.dialog_nearby_rb_time_oneday:
						mRgTime.check(R.id.dialog_nearby_rb_time_oneday);
						TimeSelect = 2;
						break;	
					case R.id.dialog_nearby_rb_time_twoday:
						mRgTime.check(R.id.dialog_nearby_rb_time_twoday);
						TimeSelect = 3;
						break;	
				}					
			}
		});
	}

	@Override
	public void init() {
		// 设置默认项
		mRgGender.check(R.id.dialog_nearby_rb_gender_all);
		mRgAge.check(R.id.dialog_nearby_rb_age_nolimitation);
		mRgTime.check(R.id.dialog_nearby_rb_time_fifteenminutes);
	}
	/**
	 * 注册注解面和聊天界面时连接状态变化回调
	 * 
	 * @param cb
	 */
	private IGpsFilterCallback mGpsFilterCallback;
	public void registerGpsFilterCallback(IGpsFilterCallback cb) {
		mGpsFilterCallback = cb;
	}

	public void unRegisterGpsFilterCallback() {
		mGpsFilterCallback = null;
	}
}

