package com.spice.im.ui;

public class DynamicsItem {
	public DynamicsItem(){}
	private int id;
	private String name;  
	private String status;  
	private String _name="";
	private String uid="";
	private String email="";
	private String distance="";
	private String time = "";//最近定位时间
	private String gender;
	private String birthday;
	private String usersign;
	private String username;
	private String memo; 
	private String pictures; 
	private String faner;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getStatus() {  
		return status;  
	}  
	public void setStatus(String status) {  
		this.status = status;  
	}  
	public String getName() {  
		return name;  
	}  
	public void setName(String name) {  
		this.name = name;  
	} 
	public String getUid() {  
		return uid;  
	}  
	public void setUid(String uid) {  
		this.uid = uid;  
	} 
	public String get_Name() {  
		return _name;  
	}  
	public void set_Name(String _name) {  
		this._name = _name;  
	} 
	public String getEmail() {  
		return email;  
	}  
	public void setEmail(String email) {  
		this.email = email;  
	} 
	public String getDistance() {  
		return distance;  
	}  
	public void setDistance(String distance) {  
		this.distance = distance;  
	} 
	public String getTime() {  
		return time;  
	}  
	public void setTime(String time) {  
		this.time = time;  
	} 
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getUsersign() {
		return usersign;
	}
	public void setUsersign(String usersign) {
		this.usersign = usersign;
	}
	public String getUsername() {  
		return username;  
	}  
	public void setUsername(String username) {  
		this.username = username;  
	} 
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public String getPictures() {
		return pictures;
	}
	public void setPictures(String pictures) {
		this.pictures = pictures;
	}
	public String getFaner() {
		return faner;
	}
	public void setFaner(String faner) {
		this.faner = faner;
	}
}
