package com.spice.im.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class DataSaveManager {
	/**
	 * 实现应用参数保存，String值
	 * 
	 * @param context 
	 * @param name 输入的name值
	 * @param value
	 * @throws Exception
	 */
	public static void setPrefenceString(Context context, String name, String value) throws Exception
	{
//		SharedPreferences preferences = context.getSharedPreferences(SHARE_PREFERENCES_NAME, Context.MODE_PRIVATE);
		SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(context);
		Editor editor = mSettings.edit();
		editor.putString(name, value);
		editor.commit();
	}
	/**
	 * 实现应用参数提取，String值
	 * 
	 * @param context
	 * @param name 输入的name值
	 * @return
	 * @throws Exception
	 */
	public static String getPreferenceString(Context context, String name) throws Exception
	{
//		SharedPreferences preferences = context.getSharedPreferences(SHARE_PREFERENCES_NAME, Context.MODE_PRIVATE);
		SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(context);
		String valueString = mSettings.getString(name, "");
		return valueString;
	}
	/**
	 * 实现应用参数保存，int值
	 * 
	 * @param context
	 * @param name 输入的name值
	 * @param value
	 * @throws Exception
	 */
	public static void setPrefenceInt(Context context, String name, int value) throws Exception
	{
//		SharedPreferences preferences = context.getSharedPreferences(SHARE_PREFERENCES_NAME, Context.MODE_PRIVATE);
		SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(context);
		Editor editor = mSettings.edit();
		editor.putInt(name, value);
		editor.commit();
	}

	/**
	 * 实现应用参数提取,int值
	 * 
	 * @param context
	 * @param name 输入的name值
	 * @return
	 * @throws Exception
	 */
	public static int getPreferenceInt(Context context, String name) throws Exception
	{
//		SharedPreferences preferences = context.getSharedPreferences(SHARE_PREFERENCES_NAME, Context.MODE_PRIVATE);
		SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(context);
		int value = mSettings.getInt(name, 0);
		return value;
	}

	/**
	 * 实现应用参数保存，boolean值
	 * 
	 * @param context
	 * @param name 输入的name值
	 * @param value
	 * @throws Exception
	 */
	public static void setPrefenceBoolean(Context context, String name, Boolean value) throws Exception
	{
//		SharedPreferences preferences = context.getSharedPreferences(SHARE_PREFERENCES_NAME, Context.MODE_PRIVATE);
		SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(context);
		Editor editor = mSettings.edit();
		editor.putBoolean(name, value);
		editor.commit();
	}

	/**
	 * 实现应用参数提取,boolean值
	 * 
	 * @param context
	 * @param name 输入的name值
	 * @return
	 * @throws Exception
	 */
	public static boolean getPreferenceBoolean(Context context, String name) throws Exception
	{
//		SharedPreferences preferences = context.getSharedPreferences(SHARE_PREFERENCES_NAME, Context.MODE_PRIVATE);
		SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(context);
		boolean value = mSettings.getBoolean(name, false);
		return value;
	}
}
