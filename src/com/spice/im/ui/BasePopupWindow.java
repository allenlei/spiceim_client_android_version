package com.spice.im.ui;



//import com.spice.im.attendance.AttendanceActivity;

import com.spice.im.attendance.AttendanceActivity2;
import com.spice.im.gps.GpsSearchResPullRefListActivity;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.PopupWindow;

public abstract class BasePopupWindow extends PopupWindow {

	protected View mContentView;
	public onSubmitClickListener mOnSubmitClickListener;
	
	public AttendanceActivity2 mActivity6 = null;
	public GpsSearchResPullRefListActivity mActivity5 = null;
	public Activity mActivity2;
	public String mContactname;

	public BasePopupWindow() {
		super();
	}

	public BasePopupWindow(Context context, AttributeSet attrs,
			int defStyleAttr, int defStyleRes) {
//		super(context, attrs, defStyleAttr, defStyleRes);
		super(context, attrs, defStyleAttr);
	}

	public BasePopupWindow(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public BasePopupWindow(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public BasePopupWindow(Context context) {
		super(context);
	}

	public BasePopupWindow(int width, int height) {
		super(width, height);
	}

	public BasePopupWindow(View contentView, int width, int height,
			boolean focusable) {
		super(contentView, width, height, focusable);
	}

	public BasePopupWindow(View contentView) {
		super(contentView);
	}

	@SuppressWarnings("deprecation")
	public BasePopupWindow(View contentView, int width, int height) {//,ManagermentActivity Activity
		super(contentView, width, height, true);
		mContentView = contentView;
//		mActivity = Activity;
		setBackgroundDrawable(new BitmapDrawable());
		initViews();
		initEvents();
		init();
	}
	@SuppressWarnings("deprecation")
	public BasePopupWindow(View contentView, int width, int height,GpsSearchResPullRefListActivity Activity) {//,ManagermentActivity Activity
		super(contentView, width, height, true);
		mContentView = contentView;
		mActivity5 = Activity;
		setBackgroundDrawable(new BitmapDrawable());
		initViews();
		initEvents();
		init();
	}
	
	@SuppressWarnings("deprecation")
	public BasePopupWindow(View contentView, int width, int height,AttendanceActivity2 Activity) {//,ManagermentActivity Activity
		super(contentView, width, height, true);
		mContentView = contentView;
		mActivity6 = Activity;
		setBackgroundDrawable(new BitmapDrawable());
		initViews();
		initEvents();
		init();
	}
	
	@SuppressWarnings("deprecation")
	public BasePopupWindow(View contentView, int width, int height,Activity activity1,String contactname) {
//		public BasePopupWindow(View contentView, int width, int height,ContactListWithPop Activity,String contactname) {
		super(contentView, width, height, true);
		mContentView = contentView;
		mActivity2 = activity1;
		mContactname = contactname;
		setBackgroundDrawable(new BitmapDrawable());
		initViews();
		initEvents();
		init();
	}
	
	public abstract void initViews();

	public abstract void initEvents();

	public abstract void init();

	public View findViewById(int id) {
		return mContentView.findViewById(id);
	}

	/**
	 * 显示在parent的上部并水平居中
	 * 
	 * @param parent
	 */
	public void showViewTopCenter(View parent) {
		showAtLocation(parent, Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
	}

	/**
	 * 显示在parent的中心
	 * 
	 * @param parent
	 */
	public void showViewCenter(View parent) {
		showAtLocation(parent, Gravity.CENTER, 0, 0);
	}

	/**
	 * 添加确认单击监听
	 * 
	 * @param l
	 */
	public void setOnSubmitClickListener(onSubmitClickListener l) {
		mOnSubmitClickListener = l;
	}

	public interface onSubmitClickListener {
		void onClick();
	}

}

