package com.spice.im.ui;

//package com.immomo.momo.android.popupwindow;

import java.io.File;



//import com.allen.ui.profile.ManagerFragment;
//import com.allen.ui.profile.ManagermentActivity;
//import com.allen.ui.profile.ProfileActivity;
//import com.stb.isharemessage.R;
//import com.stb.isharemessage.ui.AddDynamicsActivity;
//import com.stb.isharemessage.ui.profile.GroupProfileActivity;
//import com.stb.isharemessage.utils.PhotoUtils;







import com.spice.im.R;
import com.spice.im.group.GroupDetailsActivity;
import com.spice.im.preference.PreferenceActivity;
import com.spice.im.utils.PhotoUtils;
import com.stb.isharemessage.ui.feed.AddDynamicsActivity;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.RadioGroup.OnCheckedChangeListener;

//import com.immomo.momo.android.BasePopupWindow;
//import com.immomo.momo.android.activity.R;
//import com.immomo.momo.android.R;

/**
 * @fileName NearByPopupWindow.java
 * @package com.immomo.momo.android.popupwindow
 * @description 附近PopupWindow类
 * @author 任东卫
 * @email 86930007@qq.com
 * @version 1.0
 */
public class SelectPicPopupWindow extends BasePopupWindow {

	private LinearLayout mLayoutRoot;// 根布局
	private RadioGroup mSelectPic;// 上传照片
	private Button mBtnSubmit;// 确认
	private Button mBtnCancel;// 取消
	
	private String mTakePicturePath;
	public String getTakePicturePath() {
		return mTakePicturePath;
	}

////	private ManagermentActivity mActivity;
////	private ManagerFragment mManagerFragment;
//	public SelectPicPopupWindow(Context context,ManagermentActivity Activity) {
//		
//		super(LayoutInflater.from(context).inflate(
//				R.layout.include_dialog_selectpic_filter, null),
//				LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT,Activity);
////		mActivity = (ManagermentActivity)context;
////		mManagerFragment = managerFragment;
////		mActivity = Activity;
//		setAnimationStyle(R.style.Popup_Animation_PushDownUp);
//		
//	}

//	public SelectPicPopupWindow(Context context,String title,PreferenceActivity Activity) {
//		
//		super(LayoutInflater.from(context).inflate(
//				R.layout.include_dialog_selectpic_filter, null),
//				LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT,title,Activity);
////		mActivity = (ManagermentActivity)context;
////		mManagerFragment = managerFragment;
////		mActivity = Activity;
//		setAnimationStyle(R.style.Popup_Animation_PushDownUp);
//		
//	}
	public SelectPicPopupWindow(Context context,String str1,String str2,AddDynamicsActivity Activity) {
		
		super(LayoutInflater.from(context).inflate(
				R.layout.include_dialog_selectpic_filter, null),
				LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT,Activity,str1);
//		mActivity = (ManagermentActivity)context;
//		mManagerFragment = managerFragment;
//		mActivity = Activity;
		setAnimationStyle(R.style.Popup_Animation_PushDownUp);
		
	}
//	public SelectPicPopupWindow(Context context,String str1,String str2,String str3,GroupDetailsActivity Activity) {
//		
//		super(LayoutInflater.from(context).inflate(
//				R.layout.include_dialog_selectpic_filter, null),
//				LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT,str1,str2,str3,Activity);
////		mActivity = (ManagermentActivity)context;
////		mManagerFragment = managerFragment;
////		mActivity = Activity;
//		setAnimationStyle(R.style.Popup_Animation_PushDownUp);
//		
//	}
	@Override
	public void initViews() {
		mLayoutRoot = (LinearLayout) findViewById(R.id.dialog_nearby_layout_root);
		mSelectPic = (RadioGroup) findViewById(R.id.dialog_selectpic);

		
		mBtnSubmit = (Button) findViewById(R.id.dialog_selectpic_btn_submit);
		mBtnCancel = (Button) findViewById(R.id.dialog_selectpic_btn_cancel);
	}

	private int isSelect = 2;// 1 selectPhoto 0 takePicture
	@Override
	public void initEvents() {
		mLayoutRoot.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
		mBtnSubmit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();
				if (mOnSubmitClickListener != null) {
					mOnSubmitClickListener.onClick();
				}
				switch(isSelect){
					case 0:
						if(mActivity2!=null)
							mTakePicturePath = PhotoUtils.takePicture(mActivity2);
						break;
					case 1:
//						if(mActivity!=null)
//							PhotoUtils.selectPhoto(mActivity);
//						else 
						if(mActivity2!=null)
							PhotoUtils.selectPhoto(mActivity2);
						break;
					default:
						break;
				}
			}
		});
		mBtnCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
		mSelectPic.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// 暂时不做任何操作
				switch(group.getCheckedRadioButtonId()){
					case R.id.dialog_selectpic_photograph:
						mSelectPic.check(R.id.dialog_selectpic_photograph);
//						PhotoUtils.selectPhoto(mActivity);
						isSelect = 0;
//						mManagerFragment.doTakePhoto();
						break;
					case R.id.dialog_selectpic_selectlocalimg:
						mSelectPic.check(R.id.dialog_selectpic_selectlocalimg);
//						mTakePicturePath = PhotoUtils.takePicture(mActivity);
						isSelect = 1;
//						mManagerFragment.doPickPhotoFromGallery();
						break;
				}
			}
		});

	}

	@Override
	public void init() {
		// 设置默认项
//		mSelectPic.check(R.id.dialog_selectpic_photograph);
//		isSelect = 0;
	}

}

