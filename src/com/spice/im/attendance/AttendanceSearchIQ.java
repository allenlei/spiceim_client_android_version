package com.spice.im.attendance;

import org.jivesoftware.smack.packet.IQ;

public class AttendanceSearchIQ extends IQ{
    //elementName = attendancesearchiq
	//namespace = com:isharemessage:attendancesearchiq
    private String id;

    private String apikey;
    
    private String uid;
    
    private String begindate;
    
    private String enddate;
    
    private String uuid;
    
    private String hashcode;//apikey+uid+uuid 使用登录成功后返回的sessionid作为密码3des运算
    
    public AttendanceSearchIQ(){}
    
    @Override
    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append("attendancesearchiq").append(" xmlns=\"").append(
                "com:isharemessage:attendancesearchiq").append("\">");
        if (id != null) {
            buf.append("<id>").append(id).append("</id>");
        }
        if (apikey != null) {
            buf.append("<apikey>").append(apikey).append("</apikey>");
        }
        if (uid != null) {
            buf.append("<uid>").append(uid).append("</uid>");
        }
        if (begindate != null) {
            buf.append("<begindate>").append(begindate).append("</begindate>");
        }
        if (enddate != null) {
            buf.append("<enddate>").append(enddate).append("</enddate>");
        }
        if (uuid != null) {
            buf.append("<uuid>").append(uuid).append("</uuid>");
        }
        if (hashcode != null) {
            buf.append("<hashcode>").append(hashcode).append("</hashcode>");
        }
        buf.append("</").append("attendancesearchiq").append(">");
        return buf.toString();
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

	public String getApikey() {
		return apikey;
	}
	public void setApikey(String apikey) {
		this.apikey = apikey;
	}
	
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	
	public String getBegindate() {
		return begindate;
	}
	public void setBegindate(String begindate) {
		this.begindate = begindate;
	}
	
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getHashcode() {
		return hashcode;
	}
	public void setHashcode(String hashcode) {
		this.hashcode = hashcode;
	}
}
