package com.spice.im.attendance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.jivesoftware.smack.packet.IQ;

public class AttendanceSearchIQResponse extends IQ{
    private String id;

    private String apikey;

    private String totalnumcontact;

    public ArrayList aRecords = new ArrayList();
    
//    AttendanceRecord aRecord = null;
    
    public ArrayList aClassSections = new ArrayList();
    
    AttendanceClassSection aClassSection = null;
    
    AttendanceRecord2 aRecord2 = null;
    
    private String startdate;//本考勤规则起始日期
    private String enddate;//本考勤规则结束日期
    private String isweekend;//(休息日是否上班：周六班0，周日班1，周六日都班2，周末休息3),
    private String alternaterule;//（轮换规则：按天0，按周1，按半月2，按月3，按季度4，按半年5，按年轮换6，不轮换7）
    private String attendanceruleid;//考勤规则id
    private String attendanceshiftid;//考勤班次id （一天之内考勤的班段组合）

    private String retcode;//0000 success,0001 false（用户名或密码错误）,0002 false(hash校验失败),9999
    
    private String memo;//状态描述:登录成功，登录失败（用户名或密码错误）,登录失败（原因）
    
    public AttendanceSearchIQResponse() {
    }
    
    @Override
    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append("attendancesearchiq").append(" xmlns=\"").append(
                "com:isharemessage:attendancesearchiq").append("\">");
        if (id != null) {
            buf.append("<id>").append(id).append("</id>");
        }
        if (apikey != null) {
            buf.append("<apikey>").append(apikey).append("</apikey>");
        }
        if (totalnumcontact != null) {
            buf.append("<totalnumcontact>").append(totalnumcontact).append("</totalnumcontact>");
        }
        if (aRecords != null && aRecords.size()!=0) {
        	
            buf.append("<arecords>");
//            Iterator iter = aRecords.entrySet().iterator();
//			Map.Entry entry = null;
//			Object key = null;
//			Object val = null;
//			while (iter.hasNext()) {
//    			entry = (Map.Entry) iter.next();
//    			key = entry.getKey();
//    			val = entry.getValue();
//    			aRecord = (AttendanceRecord)val;
//	            	buf.append("<arecord id=\""+aRecord.getId()+"\" "
//	            			+"uid=\""+aRecord.getUid()+"\" "
//	            			+"mgpsaddr=\""+aRecord.getMgpsaddr()+"\" "
//	            			+"mlongitude=\""+aRecord.getMlongitude()+"\" "
//	            			+"mlatitude=\""+aRecord.getMlatitude()+"\" "
//	            			+"attendancepointid=\""+aRecord.getAttendancepointid()+"\" "
//	            			+"attendance_flag=\""+aRecord.getAttendance_flag()+"\""
//	            			+">")
//	            			.append(aRecord.getRemarks())
//	            			.append("</arecord>");
//			}
			for(int i=0;i<aRecords.size();i++){
//				aRecord = (AttendanceRecord)aRecords.get(i);
//            	buf.append("<arecord id=\""+aRecord.getId()+"\" "
//            			+"uid=\""+aRecord.getUid()+"\" "
//            			+"mgpsaddr=\""+aRecord.getMgpsaddr()+"\" "
//            			+"mlongitude=\""+aRecord.getMlongitude()+"\" "
//            			+"mlatitude=\""+aRecord.getMlatitude()+"\" "
//            			+"attendancepointid=\""+aRecord.getAttendancepointid()+"\" "
//            			+"attendance_flag=\""+aRecord.getAttendance_flag()+"\" "
//            			//新增加19个字段
//            			+"office_id=\""+aRecord.getOffice_id()+"\" "
//            			+"company_id=\""+aRecord.getCompany_id()+"\" "
//            	        +"create_by=\""+aRecord.getCreate_by()+"\" "
//            	        +"update_by=\""+aRecord.getUpdate_by()+"\" "
//            	        +"update_date=\""+aRecord.getUpdate_date()+"\" "
//            	        +"del_flag=\""+aRecord.getDel_flag()+"\" "
//            	        +"attendanceruleid=\""+aRecord.getAttendanceruleid()+"\" "
//            	        +"attendanceshiftid=\""+aRecord.getAttendanceshiftid()+"\" "
//            	        +"attendancesectionid=\""+aRecord.getAttendancesectionid()+"\" "
//            	        +"mtype=\""+aRecord.getMtype()+"\" "
//            	        +"mintime=\""+aRecord.getMintime()+"\" "
//            	        +"maxtime=\""+aRecord.getMaxtime()+"\" "
//            	        +"onoffdutyname=\""+aRecord.getOnoffdutyname()+"\" "
//            	        +"starttime=\""+aRecord.getStarttime()+"\" "
//            	        +"endtime=\""+aRecord.getEndtime()+"\" "
//            	        +"yearstr=\""+aRecord.getYearstr()+"\" "
//            	        +"monthdaystr=\""+aRecord.getMonthdaystr()+"\" "
//            	        +"classsectionids=\""+aRecord.getClasssectionids()+"\" "
//            	        +"classsectionidsNum=\""+aRecord.getClasssectionidsNum()+"\""
//            			+">")
//            			.append(aRecord.getRemarks())
//            			.append("</arecord>");
				
				
				
				aRecord2 = (AttendanceRecord2)aRecords.get(i);
            	buf.append("<arecord yearstr=\""+aRecord2.getYearstr()+"\" "
            			+"monthdaystr=\""+aRecord2.getMonthdaystr()+"\" "
            			+"uid=\""+aRecord2.getUid()+"\" "
            			+"id_gc=\""+aRecord2.getId_gc()+"\" "
            			+"create_date_gc=\""+aRecord2.getCreate_date_gc()+"\" "
//            			+"remarks_gc=\""+aRecord2.getRemarks_gc()+"\" "
            			+"remarkcode_gc=\""+aRecord2.getRemarkcode_gc()+"\" "
            	        +"attendanceruleid=\""+aRecord2.getAttendanceruleid()+"\" "
            	        +"attendanceshiftid=\""+aRecord2.getAttendanceshiftid()+"\" "
            	        +"attendancesectionid_gc=\""+aRecord2.getAttendancesectionid_gc()+"\" "
            	        +"mtype_gc=\""+aRecord2.getMtype_gc()+"\" "
            	        +"mintime_gc=\""+aRecord2.getMintime_gc()+"\" "
            	        +"maxtime_gc=\""+aRecord2.getMaxtime_gc()+"\" "
            	        +"onoffdutyname_gc=\""+aRecord2.getOnoffdutyname_gc()+"\" "
            	        +"starttime_gc=\""+aRecord2.getStarttime_gc()+"\" "
            	        +"endtime_gc=\""+aRecord2.getEndtime_gc()+"\""
            			+">")
            			.append(aRecord2.getRemarks_gc())
            			.append("</arecord>");
			}
            buf.append("</arecords>");
        }
        
        if (aClassSections != null && aClassSections.size()!=0) {
        	
            buf.append("<aclasssections>");
//            Iterator iter = aClassSections.entrySet().iterator();
//			Map.Entry entry = null;
//			Object key = null;
//			Object val = null;
//			while (iter.hasNext()) {
//    			entry = (Map.Entry) iter.next();
//    			key = entry.getKey();
//    			val = entry.getValue();
//    			aClassSection = (AttendanceClassSection)val;
//	            	buf.append("<aclasssection id=\""+aClassSection.getId()+"\" "
//	            			+"type=\""+aClassSection.getType()+"\" "
//	            			+"starttime=\""+aClassSection.getStarttime()+"\" "
//	            			+"starttime1=\""+aClassSection.getStarttime1()+"\" "
//	            			+"starttime2=\""+aClassSection.getStarttime2()+"\" "
//	            			+"endtime=\""+aClassSection.getEndtime()+"\" "
//	            			+"endtime1=\""+aClassSection.getEndtime1()+"\" "
//	            			+"endtime2=\""+aClassSection.getEndtime2()+"\""
//	            			+">")
//	            			.append(aClassSection.getOnoffdutyname())
//	            			.append("</aclasssection>");
//			}
			for(int j=0;j<aClassSections.size();j++){
    			aClassSection = (AttendanceClassSection)aClassSections.get(j);
            	buf.append("<aclasssection id=\""+aClassSection.getId()+"\" "
            			+"type=\""+aClassSection.getType()+"\" "
            			+"starttime=\""+aClassSection.getStarttime()+"\" "
            			+"starttime1=\""+aClassSection.getStarttime1()+"\" "
            			+"starttime2=\""+aClassSection.getStarttime2()+"\" "
            			+"endtime=\""+aClassSection.getEndtime()+"\" "
            			+"endtime1=\""+aClassSection.getEndtime1()+"\" "
            			+"endtime2=\""+aClassSection.getEndtime2()+"\""
            			+">")
            			.append(aClassSection.getOnoffdutyname())
            			.append("</aclasssection>");
			}
            buf.append("</aclasssections>");
        }
        
        if (attendanceruleid != null) {
            buf.append("<attendanceruleid>").append(attendanceruleid).append("</attendanceruleid>");
        }
        if (attendanceshiftid != null) {
            buf.append("<attendanceshiftid>").append(attendanceshiftid).append("</attendanceshiftid>");
        }
        if (startdate != null) {
            buf.append("<startdate>").append(startdate).append("</startdate>");
        }
        if (enddate != null) {
            buf.append("<enddate>").append(enddate).append("</enddate>");
        }
        if (isweekend != null) {
            buf.append("<isweekend>").append(isweekend).append("</isweekend>");
        }
        if (alternaterule != null) {
            buf.append("<alternaterule>").append(alternaterule).append("</alternaterule>");
        }
        
        if(retcode!=null){
        	buf.append("<retcode>").append(retcode).append("</retcode>");
        }
        if(memo!=null){
        	buf.append("<memo>").append(memo).append("</memo>");
        }
        buf.append("</").append("attendancesearchiq").append(">");
        return buf.toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getTotalnumcontact() {
        return totalnumcontact;
    }

    public void setTotalnumcontact(String totalnumcontact) {
        this.totalnumcontact = totalnumcontact;
    }

    public ArrayList getARecords() {
        return aRecords;
    }

    public void setARecords(ArrayList aRecords) {
        this.aRecords = aRecords;
    }
    
    public ArrayList getAClassSections() {
        return aClassSections;
    }

    public void setAClassSections(ArrayList aClassSections) {
        this.aClassSections = aClassSections;
    }
    
	public String getRetcode() {
		return retcode;
	}
	public void setRetcode(String retcode) {
		this.retcode = retcode;
	}

	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	
	public String getStartdate() {
		return startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	
	public String getEnddate() {
		return enddate;
	}

	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	
	public String getIsweekend() {
		return isweekend;
	}

	public void setIsweekend(String isweekend) {
		this.isweekend = isweekend;
	}
	
	public String getAlternaterule() {
		return alternaterule;
	}

	public void setAlternaterule(String alternaterule) {
		this.alternaterule = alternaterule;
	}
	
	public String getAttendanceruleid() {
		return attendanceruleid;
	}

	public void setAttendanceruleid(String attendanceruleid) {
		this.attendanceruleid = attendanceruleid;
	}
	
	public String getAttendanceshiftid() {
		return attendanceshiftid;
	}

	public void setAttendanceshiftid(String attendanceshiftid) {
		this.attendanceshiftid = attendanceshiftid;
	}
}
