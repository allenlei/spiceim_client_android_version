package com.spice.im.attendance;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;


public class AttendanceSearchIQProvider implements IQProvider{
    public AttendanceSearchIQProvider() {
    }
    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {

    	AttendanceSearchIQ attendanceSearchIQ = new AttendanceSearchIQ();
        for (boolean done = false; !done;) {
            int eventType = parser.next();
            if (eventType == 2) {
                if ("id".equals(parser.getName())) {
                	attendanceSearchIQ.setId(parser.nextText());
                }
                if ("apikey".equals(parser.getName())) {
                	attendanceSearchIQ.setApikey(parser.nextText());
                }
                if ("uid".equals(parser.getName())) {
                	attendanceSearchIQ.setUid(parser.nextText());
                }
                if ("begindate".equals(parser.getName())) {
                	attendanceSearchIQ.setBegindate(parser.nextText());
                }
                if ("enddate".equals(parser.getName())) {
                	attendanceSearchIQ.setEnddate(parser.nextText());
                }
                if ("uuid".equals(parser.getName())) {
                	attendanceSearchIQ.setUuid(parser.nextText());
                }
                if ("hashcode".equals(parser.getName())) {
                	attendanceSearchIQ.setHashcode(parser.nextText());
                }
            } else if (eventType == 3
                    && "attendancesearchiq".equals(parser.getName())) {
                done = true;
            }
        }

        return attendanceSearchIQ;
    }
}
