package com.spice.im.attendance;

import org.jivesoftware.smack.packet.IQ;

public class AttendanceIQ extends IQ{
    //elementName = attendanceiq
	//namespace = com:isharemessage:attendanceiq
    private String id;

    private String apikey;
    
    private String uid;
    
    private String username = "";
    
    private String mgpsaddr;
    
    private String mlongitude;
    
    private String mlatitude;
    
    private String mtype;//0 该班段上班考勤时间,1 该班段下班考勤时间
    private String time1;//格式 HH:mm:ss 允许开始打卡时间
    private String time2;//格式 HH:mm:ss 允许最后打卡时间
    private String attendanceruleid;//考勤规则id
    private String attendanceshiftid;//考勤班次id （一天之内考勤的班段组合）
    private String attendancesectionid;//考勤班段id
    
    private String startdate;//本考勤规则起始日期
    private String enddate;//本考勤规则结束日期
    private String isweekend;//(休息日是否上班：周六班0，周日班1，周六日都班2，周末休息3),
    private String alternaterule;//（轮换规则：按天0，按周1，按半月2，按月3，按季度4，按半年5，按年轮换6，不轮换7）
    
    private String uuid;
    
    private String hashcode;//apikey+uid+uuid 使用登录成功后返回的sessionid作为密码3des运算
    
    private String procinsid;//当为自由签时，代表拍照照片文件的名称 格式为 yyyymmddhhmmss.jpg
    
    public AttendanceIQ(){}
    
    @Override
    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append("attendanceiq").append(" xmlns=\"").append(
                "com:isharemessage:attendanceiq").append("\">");
        if (id != null) {
            buf.append("<id>").append(id).append("</id>");
        }
        if (apikey != null) {
            buf.append("<apikey>").append(apikey).append("</apikey>");
        }
        if (uid != null) {
            buf.append("<uid>").append(uid).append("</uid>");
        }
        if (username != null) {
            buf.append("<username>").append(username).append("</username>");
        }
        if (mgpsaddr != null) {
            buf.append("<mgpsaddr>").append(mgpsaddr).append("</mgpsaddr>");
        }
        if (mlongitude != null) {
            buf.append("<mlongitude>").append(mlongitude).append("</mlongitude>");
        }
        if (mlatitude != null) {
            buf.append("<mlatitude>").append(mlatitude).append("</mlatitude>");
        }
        if (mtype != null) {
            buf.append("<mtype>").append(mtype).append("</mtype>");
        }
        if (time1 != null) {
            buf.append("<time1>").append(time1).append("</time1>");
        }
        if (time2 != null) {
            buf.append("<time2>").append(time2).append("</time2>");
        }
        if (attendanceruleid != null) {
            buf.append("<attendanceruleid>").append(attendanceruleid).append("</attendanceruleid>");
        }
        if (attendanceshiftid != null) {
            buf.append("<attendanceshiftid>").append(attendanceshiftid).append("</attendanceshiftid>");
        }
        if (attendancesectionid != null) {
            buf.append("<attendancesectionid>").append(attendancesectionid).append("</attendancesectionid>");
        }
        if (startdate != null) {
            buf.append("<startdate>").append(startdate).append("</startdate>");
        }
        if (enddate != null) {
            buf.append("<enddate>").append(enddate).append("</enddate>");
        }
        if (isweekend != null) {
            buf.append("<isweekend>").append(isweekend).append("</isweekend>");
        }
        if (alternaterule != null) {
            buf.append("<alternaterule>").append(alternaterule).append("</alternaterule>");
        }
        if (procinsid != null) {
            buf.append("<procinsid>").append(procinsid).append("</procinsid>");
        }
        
        if (uuid != null) {
            buf.append("<uuid>").append(uuid).append("</uuid>");
        }
        if (hashcode != null) {
            buf.append("<hashcode>").append(hashcode).append("</hashcode>");
        }
        buf.append("</").append("attendanceiq").append(">");
        return buf.toString();
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

	public String getApikey() {
		return apikey;
	}
	public void setApikey(String apikey) {
		this.apikey = apikey;
	}
	
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getMgpsaddr() {
		return mgpsaddr;
	}
	public void setMgpsaddr(String mgpsaddr) {
		this.mgpsaddr = mgpsaddr;
	}
	
	public String getMlongitude() {
		return mlongitude;
	}
	public void setMlongitude(String mlongitude) {
		this.mlongitude = mlongitude;
	}
	
	public String getMlatitude() {
		return mlatitude;
	}
	public void setMlatitude(String mlatitude) {
		this.mlatitude = mlatitude;
	}
	
	
	public String getStartdate() {
		return startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	
	public String getEnddate() {
		return enddate;
	}

	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	
	public String getIsweekend() {
		return isweekend;
	}

	public void setIsweekend(String isweekend) {
		this.isweekend = isweekend;
	}
	
	public String getAlternaterule() {
		return alternaterule;
	}

	public void setAlternaterule(String alternaterule) {
		this.alternaterule = alternaterule;
	}
	
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getHashcode() {
		return hashcode;
	}
	public void setHashcode(String hashcode) {
		this.hashcode = hashcode;
	}
	
	public String getMtype() {
		return mtype;
	}

	public void setMtype(String mtype) {
		this.mtype = mtype;
	}
	
	public String getTime1() {
		return time1;
	}

	public void setTime1(String time1) {
		this.time1 = time1;
	}
	
	public String getTime2() {
		return time2;
	}

	public void setTime2(String time2) {
		this.time2 = time2;
	}
	
	public String getAttendanceruleid() {
		return attendanceruleid;
	}

	public void setAttendanceruleid(String attendanceruleid) {
		this.attendanceruleid = attendanceruleid;
	}
	
	public String getAttendanceshiftid() {
		return attendanceshiftid;
	}

	public void setAttendanceshiftid(String attendanceshiftid) {
		this.attendanceshiftid = attendanceshiftid;
	}
	
	public String getAttendancesectionid() {
		return attendancesectionid;
	}

	public void setAttendancesectionid(String attendancesectionid) {
		this.attendancesectionid = attendancesectionid;
	}
	
	public String getProcinsid() {
		return procinsid;
	}

	public void setProcinsid(String procinsid) {
		this.procinsid = procinsid;
	}
}
