package com.spice.im.attendance;

public class AttendanceClassSection {
	private String id;
	private String onoffdutyname;//班段名称
	private String type;//类型（如白班0、夜班1、上午班2、下午班3等）
	private String starttime;
	private String starttime1;//上班允许开始打卡时间
	private String starttime2;//上班允许最后打卡时间
	private String endtime;
	private String endtime1;//下班允许开始打卡时间
	private String endtime2;//下班允许最后打卡时间
	
	public void setId(String id){
		this.id = id;
	}
	public String getId(){
		return this.id;
	}
	
	public String getOnoffdutyname() {
		return onoffdutyname;
	}

	public void setOnoffdutyname(String onoffdutyname) {
		this.onoffdutyname = onoffdutyname;
	}
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public String getStarttime() {
		return starttime;
	}

	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}
	
	public String getEndtime() {
		return endtime;
	}

	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}
	
	public String getStarttime1() {
		return starttime1;
	}

	public void setStarttime1(String starttime1) {
		this.starttime1 = starttime1;
	}
	
	public String getStarttime2() {
		return starttime2;
	}

	public void setStarttime2(String starttime2) {
		this.starttime2 = starttime2;
	}
	
	public String getEndtime1() {
		return endtime1;
	}

	public void setEndtime1(String endtime1) {
		this.endtime1 = endtime1;
	}
	
	public String getEndtime2() {
		return endtime2;
	}

	public void setEndtime2(String endtime2) {
		this.endtime2 = endtime2;
	}
}
