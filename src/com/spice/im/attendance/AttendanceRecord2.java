package com.spice.im.attendance;

public class AttendanceRecord2 {
	//f.yearstr,
	//f.monthdaystr,
	//f.uid,
	//GROUP_CONCAT(f.id) AS id_gc,
	//GROUP_CONCAT(f.create_date) AS create_date_gc,
	//GROUP_CONCAT(f.remarks) AS remarks_gc,
	//GROUP_CONCAT(f.remarkcode) AS remarkcode_gc,
	//f.attendanceruleid,
	//f.attendanceshiftid,
	//GROUP_CONCAT(f.attendancesectionid) AS attendancesectionid_gc,
	//GROUP_CONCAT(f.mtype) AS mtype_gc,
	//GROUP_CONCAT(f.mintime) AS mintime_gc,
	//GROUP_CONCAT(f.maxtime) AS maxtime_gc,
	//GROUP_CONCAT(f.onoffdutyname) AS onoffdutyname_gc,
	//GROUP_CONCAT(f.starttime) AS starttime_gc,
	//GROUP_CONCAT(f.endtime) AS endtime_gc
	private String yearstr;
	public String getYearstr() {
		return yearstr;
	}

	public void setYearstr(String yearstr) {
		this.yearstr = yearstr;
	}
	private String monthdaystr;
	public String getMonthdaystr() {
		return monthdaystr;
	}

	public void setMonthdaystr(String monthdaystr) {
		this.monthdaystr = monthdaystr;
	}
	private String uid;
	public void setUid(String uid){
		this.uid = uid;
	}
	public String getUid(){
		return this.uid;
	}
	
	private String id_gc;
	public void setId_gc(String id_gc){
		this.id_gc = id_gc;
	}
	public String getId_gc(){
		return this.id_gc;
	}
	
	private String create_date_gc;//2017-10-26 22:56:58
	public void setCreate_date_gc(String create_date_gc){
		this.create_date_gc = create_date_gc;
	}
	public String getCreate_date_gc(){
		return this.create_date_gc;
	}
	
	private String remarks_gc;//"考勤成功:res:"+mname    或者    "考勤失败:res:"+mname
	//mname 考勤区域名称，或者是 自由签时为定位地址
	//res 0001 考勤失败，用户账户不存在;0005 未定义考勤区域，自由签成功；0003 定义了考勤区域，考勤成功；0004 定义了考勤区域，您不在考勤区域内考勤失败；9999 系统错误
	public void setRemarks_gc(String remarks_gc){
		this.remarks_gc = remarks_gc;
	}
	public String getRemarks_gc(){
		return this.remarks_gc;
	}
	
	private String remarkcode_gc;
	public void setRemarkcode_gc(String remarkcode_gc){
		this.remarkcode_gc = remarkcode_gc;
	}
	public String getRemarkcode_gc(){
		return this.remarkcode_gc;
	}
	
	private String attendanceruleid;
	private String attendanceshiftid;
	public String getAttendanceruleid() {
		return attendanceruleid;
	}

	public void setAttendanceruleid(String attendanceruleid) {
		this.attendanceruleid = attendanceruleid;
	}
	
	public String getAttendanceshiftid() {
		return attendanceshiftid;
	}

	public void setAttendanceshiftid(String attendanceshiftid) {
		this.attendanceshiftid = attendanceshiftid;
	}
	
	private String attendancesectionid_gc;
	public String getAttendancesectionid_gc() {
		return attendancesectionid_gc;
	}

	public void setAttendancesectionid_gc(String attendancesectionid_gc) {
		this.attendancesectionid_gc = attendancesectionid_gc;
	}
	
	
	
	private String mtype_gc;
	
	public String getMtype_gc() {
		return mtype_gc;
	}

	public void setMtype_gc(String mtype_gc) {
		this.mtype_gc = mtype_gc;
	}
	private String mintime_gc;
	public String getMintime_gc() {
		return mintime_gc;
	}

	public void setMintime_gc(String mintime_gc) {
		this.mintime_gc = mintime_gc;
	}
	private String maxtime_gc;
	public String getMaxtime_gc() {
		return maxtime_gc;
	}

	public void setMaxtime_gc(String maxtime_gc) {
		this.maxtime_gc = maxtime_gc;
	}
	private String onoffdutyname_gc;
	public String getOnoffdutyname_gc() {
		return onoffdutyname_gc;
	}
	public void setOnoffdutyname_gc(String onoffdutyname_gc) {
		this.onoffdutyname_gc = onoffdutyname_gc;
	}
	
	private String starttime_gc;
	public String getStarttime_gc() {
		return starttime_gc;
	}

	public void setStarttime_gc(String starttime_gc) {
		this.starttime_gc = starttime_gc;
	}
	private String endtime_gc;
	public String getEndtime_gc() {
		return endtime_gc;
	}

	public void setEndtime_gc(String endtime_gc) {
		this.endtime_gc = endtime_gc;
	}
	
	
	
	
	
	
	
	
	
	
//	private String mgpsaddr;
//	private String mlongitude;
//	private String mlatitude;
//	private String attendancepointid;
//	private String attendance_flag;//0考勤成功，1考勤失败
//
//	
//	
//	//20171113新增加字段start
//	private String office_id;
//	public String getOffice_id() {
//		return office_id;
//	}
//
//	public void setOffice_id(String office_id) {
//		this.office_id = office_id;
//	}
//	private String company_id;
//	public String getCompany_id() {
//		return company_id;
//	}
//
//	public void setCompany_id(String company_id) {
//		this.company_id = company_id;
//	}
//	private String create_by;
//	public String getCreate_by() {
//		return create_by;
//	}
//
//	public void setCreate_by(String create_by) {
//		this.create_by = create_by;
//	}
//	private String update_by;
//	public String getUpdate_by() {
//		return update_by;
//	}
//
//	public void setUpdate_by(String update_by) {
//		this.update_by = update_by;
//	}
//	private String update_date;
//	public String getUpdate_date() {
//		return update_date;
//	}
//
//	public void setUpdate_date(String update_date) {
//		this.update_date = update_date;
//	}
//	private String del_flag;
//	public String getDel_flag() {
//		return del_flag;
//	}
//
//	public void setDel_flag(String del_flag) {
//		this.del_flag = del_flag;
//	}
//
//	
//
//
//
//
//	
//
//	
//
//	
//
//	
//
//	private String classsectionids;
//	public String getClasssectionids(){
//		return classsectionids;
//	}
//	public void setClasssectionids(String classsectionids){
//		this.classsectionids = classsectionids;
//	}
//	private String classsectionidsNum;
//	public String getClasssectionidsNum(){
//		return classsectionidsNum;
//	}
//	public void setClasssectionidsNum(String classsectionidsNum){
//		this.classsectionidsNum = classsectionidsNum;
//	}
//	//20171113新增加字段end
//	
//
//	
//
//	
//	public void setMgpsaddr(String mgpsaddr){
//		this.mgpsaddr = mgpsaddr;
//	}
//	public String getMgpsaddr(){
//		return this.mgpsaddr;
//	}
//	
//	public void setMlongitude(String mlongitude){
//		this.mlongitude = mlongitude;
//	}
//	public String getMlongitude(){
//		return this.mlongitude;
//	}
//	
//	public void setMlatitude(String mlatitude){
//		this.mlatitude = mlatitude;
//	}
//	public String getMlatitude(){
//		return this.mlatitude;
//	}
//	
//	public void setAttendancepointid(String attendancepointid){
//		this.attendancepointid = attendancepointid;
//	}
//	public String getAttendancepointid(){
//		return this.attendancepointid;
//	}
//	
//	public void setAttendance_flag(String attendance_flag){
//		this.attendance_flag = attendance_flag;
//	}
//	public String getAttendance_flag(){
//		return this.attendance_flag;
//	}
//	

	

}
