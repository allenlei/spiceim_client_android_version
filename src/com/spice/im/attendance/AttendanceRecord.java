package com.spice.im.attendance;

public class AttendanceRecord {
	private String id;
	private String uid;
	private String mgpsaddr;
	private String mlongitude;
	private String mlatitude;
	private String attendancepointid;
	private String attendance_flag;//0考勤成功，1考勤失败
	private String remarks;//"考勤成功:res:"+mname    或者    "考勤失败:res:"+mname
	//mname 考勤区域名称，或者是 自由签时为定位地址
	//res 0001 考勤失败，用户账户不存在;0005 未定义考勤区域，自由签成功；0003 定义了考勤区域，考勤成功；0004 定义了考勤区域，您不在考勤区域内考勤失败；9999 系统错误
	private String create_date;//2017-10-26 22:56:58
	
	//20171113新增加字段start
	private String office_id;
	public String getOffice_id() {
		return office_id;
	}

	public void setOffice_id(String office_id) {
		this.office_id = office_id;
	}
	private String company_id;
	public String getCompany_id() {
		return company_id;
	}

	public void setCompany_id(String company_id) {
		this.company_id = company_id;
	}
	private String create_by;
	public String getCreate_by() {
		return create_by;
	}

	public void setCreate_by(String create_by) {
		this.create_by = create_by;
	}
	private String update_by;
	public String getUpdate_by() {
		return update_by;
	}

	public void setUpdate_by(String update_by) {
		this.update_by = update_by;
	}
	private String update_date;
	public String getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(String update_date) {
		this.update_date = update_date;
	}
	private String del_flag;
	public String getDel_flag() {
		return del_flag;
	}

	public void setDel_flag(String del_flag) {
		this.del_flag = del_flag;
	}
	private String attendanceruleid;
	private String attendanceshiftid;
	private String attendancesectionid;
	private String mtype;
	private String mintime;
	public String getMintime() {
		return mintime;
	}

	public void setMintime(String mintime) {
		this.mintime = mintime;
	}
	private String maxtime;
	public String getMaxtime() {
		return maxtime;
	}

	public void setMaxtime(String maxtime) {
		this.maxtime = maxtime;
	}
	private String onoffdutyname;
	public String getOnoffdutyname() {
		return onoffdutyname;
	}

	public void setOnoffdutyname(String onoffdutyname) {
		this.onoffdutyname = onoffdutyname;
	}
	private String starttime;
	public String getStarttime() {
		return starttime;
	}

	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}
	private String endtime;
	public String getEndtime() {
		return endtime;
	}

	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}
	
	public String getAttendanceruleid() {
		return attendanceruleid;
	}

	public void setAttendanceruleid(String attendanceruleid) {
		this.attendanceruleid = attendanceruleid;
	}
	
	public String getAttendanceshiftid() {
		return attendanceshiftid;
	}

	public void setAttendanceshiftid(String attendanceshiftid) {
		this.attendanceshiftid = attendanceshiftid;
	}
	
	public String getAttendancesectionid() {
		return attendancesectionid;
	}

	public void setAttendancesectionid(String attendancesectionid) {
		this.attendancesectionid = attendancesectionid;
	}
	
	public String getMtype() {
		return mtype;
	}

	public void setMtype(String mtype) {
		this.mtype = mtype;
	}
	
	private String yearstr;
	public String getYearstr() {
		return yearstr;
	}

	public void setYearstr(String yearstr) {
		this.yearstr = yearstr;
	}
	private String monthdaystr;
	public String getMonthdaystr() {
		return monthdaystr;
	}

	public void setMonthdaystr(String monthdaystr) {
		this.monthdaystr = monthdaystr;
	}
	private String classsectionids;
	public String getClasssectionids(){
		return classsectionids;
	}
	public void setClasssectionids(String classsectionids){
		this.classsectionids = classsectionids;
	}
	private String classsectionidsNum;
	public String getClasssectionidsNum(){
		return classsectionidsNum;
	}
	public void setClasssectionidsNum(String classsectionidsNum){
		this.classsectionidsNum = classsectionidsNum;
	}
	//20171113新增加字段end
	
	public void setId(String id){
		this.id = id;
	}
	public String getId(){
		return this.id;
	}
	
	public void setUid(String uid){
		this.uid = uid;
	}
	public String getUid(){
		return this.uid;
	}
	
	public void setMgpsaddr(String mgpsaddr){
		this.mgpsaddr = mgpsaddr;
	}
	public String getMgpsaddr(){
		return this.mgpsaddr;
	}
	
	public void setMlongitude(String mlongitude){
		this.mlongitude = mlongitude;
	}
	public String getMlongitude(){
		return this.mlongitude;
	}
	
	public void setMlatitude(String mlatitude){
		this.mlatitude = mlatitude;
	}
	public String getMlatitude(){
		return this.mlatitude;
	}
	
	public void setAttendancepointid(String attendancepointid){
		this.attendancepointid = attendancepointid;
	}
	public String getAttendancepointid(){
		return this.attendancepointid;
	}
	
	public void setAttendance_flag(String attendance_flag){
		this.attendance_flag = attendance_flag;
	}
	public String getAttendance_flag(){
		return this.attendance_flag;
	}
	
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	public String getRemarks(){
		return this.remarks;
	}
	
	public void setCreate_date(String create_date){
		this.create_date = create_date;
	}
	public String getCreate_date(){
		return this.create_date;
	}
}
