package com.spice.im.attendance;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;


public class AttendanceIQProvider implements IQProvider{
    public AttendanceIQProvider() {
    }
    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {

    	AttendanceIQ searchIQ = new AttendanceIQ();
        for (boolean done = false; !done;) {
            int eventType = parser.next();
            if (eventType == 2) {
                if ("id".equals(parser.getName())) {
                	searchIQ.setId(parser.nextText());
                }
                if ("apikey".equals(parser.getName())) {
                	searchIQ.setApikey(parser.nextText());
                }
                if ("uid".equals(parser.getName())) {
                	searchIQ.setUid(parser.nextText());
                }
                if ("username".equals(parser.getName())) {
                	searchIQ.setUsername(parser.nextText());
                }
                if ("mgpsaddr".equals(parser.getName())) {
                	searchIQ.setMgpsaddr(parser.nextText());
                }
                if ("mlongitude".equals(parser.getName())) {
                	searchIQ.setMlongitude(parser.nextText());
                }
                if ("mlatitude".equals(parser.getName())) {
                	searchIQ.setMlatitude(parser.nextText());
                }
                
                
                if ("mtype".equals(parser.getName())) {
                	searchIQ.setMtype(parser.nextText());
                }
                if ("time1".equals(parser.getName())) {
                	searchIQ.setTime1(parser.nextText());
                }
                if ("time2".equals(parser.getName())) {
                	searchIQ.setTime2(parser.nextText());
                }
                if ("attendanceruleid".equals(parser.getName())) {
                	searchIQ.setAttendanceruleid(parser.nextText());
                }
                if ("attendanceshiftid".equals(parser.getName())) {
                	searchIQ.setAttendanceshiftid(parser.nextText());
                }
                if ("attendancesectionid".equals(parser.getName())) {
                	searchIQ.setAttendancesectionid(parser.nextText());
                }
                if ("startdate".equals(parser.getName())) {
                	searchIQ.setStartdate(parser.nextText());
                }
                if ("enddate".equals(parser.getName())) {
                	searchIQ.setEnddate(parser.nextText());
                }
                if ("isweekend".equals(parser.getName())) {
                	searchIQ.setIsweekend(parser.nextText());
                }
                if ("alternaterule".equals(parser.getName())) {
                	searchIQ.setAlternaterule(parser.nextText());
                }
                if ("procinsid".equals(parser.getName())) {
                	searchIQ.setProcinsid(parser.nextText());
                }
                if ("uuid".equals(parser.getName())) {
                	searchIQ.setUuid(parser.nextText());
                }
                if ("hashcode".equals(parser.getName())) {
                	searchIQ.setHashcode(parser.nextText());
                }
            } else if (eventType == 3
                    && "attendanceiq".equals(parser.getName())) {
                done = true;
            }
        }

        return searchIQ;
    }
}
