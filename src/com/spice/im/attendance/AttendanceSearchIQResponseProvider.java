package com.spice.im.attendance;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;


public class AttendanceSearchIQResponseProvider implements IQProvider{
	AttendanceRecord2 aRecord2 = null;
	AttendanceClassSection aClassSection = null;
	public AttendanceSearchIQResponseProvider(){
		
	}
	String key = "",key1 = "";
    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {

    	AttendanceSearchIQResponse attendanceSearchIQResponse = new AttendanceSearchIQResponse();
        for (boolean done = false; !done;) {
            int eventType = parser.next();
            if (eventType == 2) {
                if ("id".equals(parser.getName())) {
                	attendanceSearchIQResponse.setId(parser.nextText());
                }
                if ("apikey".equals(parser.getName())) {
                	attendanceSearchIQResponse.setApikey(parser.nextText());
                }
                if ("totalnumcontact".equals(parser.getName())) {
                	attendanceSearchIQResponse.setTotalnumcontact(parser.nextText());
                }
                if ("arecord".equals(parser.getName())) {
//                	aRecord = new AttendanceRecord();
//                	aRecord.setId(parser.getAttributeValue(0)!=null?parser.getAttributeValue(0):"");
//                	aRecord.setUid(parser.getAttributeValue(1)!=null?parser.getAttributeValue(1):"");
//            		aRecord.setMgpsaddr(parser.getAttributeValue(2)!=null?parser.getAttributeValue(2):"");
//            		aRecord.setMlongitude(parser.getAttributeValue(3)!=null?parser.getAttributeValue(3):"");
//            		aRecord.setMlatitude(parser.getAttributeValue(4)!=null?parser.getAttributeValue(4):"");
//            		aRecord.setAttendancepointid(parser.getAttributeValue(5)!=null?parser.getAttributeValue(5):"");
//            		aRecord.setAttendance_flag(parser.getAttributeValue(6)!=null?parser.getAttributeValue(6):"");
//            		
//            		//新增加19个字段
//					aRecord.setOffice_id(parser.getAttributeValue(7)!=null?parser.getAttributeValue(7):"");
//					aRecord.setCompany_id(parser.getAttributeValue(8)!=null?parser.getAttributeValue(8):"");
//					aRecord.setCreate_by(parser.getAttributeValue(9)!=null?parser.getAttributeValue(9):"");
//					aRecord.setUpdate_by(parser.getAttributeValue(10)!=null?parser.getAttributeValue(10):"");
//					aRecord.setUpdate_date(parser.getAttributeValue(11)!=null?parser.getAttributeValue(11):"");
//					aRecord.setDel_flag(parser.getAttributeValue(12)!=null?parser.getAttributeValue(12):"");
//					aRecord.setAttendanceruleid(parser.getAttributeValue(13)!=null?parser.getAttributeValue(13):"");
//					aRecord.setAttendanceshiftid(parser.getAttributeValue(14)!=null?parser.getAttributeValue(14):"");
//					aRecord.setAttendancesectionid(parser.getAttributeValue(15)!=null?parser.getAttributeValue(15):"");
//					aRecord.setMtype(parser.getAttributeValue(16)!=null?parser.getAttributeValue(16):"");
//					aRecord.setMintime(parser.getAttributeValue(17)!=null?parser.getAttributeValue(17):"");
//					aRecord.setMaxtime(parser.getAttributeValue(18)!=null?parser.getAttributeValue(18):"");
//					aRecord.setOnoffdutyname(parser.getAttributeValue(19)!=null?parser.getAttributeValue(19):"");
//					aRecord.setStarttime(parser.getAttributeValue(20)!=null?parser.getAttributeValue(20):"");
//					aRecord.setEndtime(parser.getAttributeValue(21)!=null?parser.getAttributeValue(21):"");
//					aRecord.setYearstr(parser.getAttributeValue(22)!=null?parser.getAttributeValue(22):"");
//					aRecord.setMonthdaystr(parser.getAttributeValue(23)!=null?parser.getAttributeValue(23):"");
//					aRecord.setClasssectionids(parser.getAttributeValue(24)!=null?parser.getAttributeValue(24):"");
//					aRecord.setClasssectionidsNum(parser.getAttributeValue(25)!=null?parser.getAttributeValue(25):"");
//            		
//            		key = (parser.getAttributeValue(0)!=null?parser.getAttributeValue(0):"");
//            		aRecord.setRemarks(parser.nextText());
//
////            		attendanceSearchIQResponse.aRecords.put(key,aRecord);
//            		attendanceSearchIQResponse.aRecords.add(aRecord);
                	
                	
                	
                	aRecord2 = new AttendanceRecord2();
                	aRecord2.setYearstr(parser.getAttributeValue(0)!=null?parser.getAttributeValue(0):"");
					aRecord2.setMonthdaystr(parser.getAttributeValue(1)!=null?parser.getAttributeValue(1):"");
					
                	aRecord2.setUid(parser.getAttributeValue(2)!=null?parser.getAttributeValue(2):"");
                	
            		aRecord2.setId_gc(parser.getAttributeValue(3)!=null?parser.getAttributeValue(3):"");
            		aRecord2.setCreate_date_gc(parser.getAttributeValue(4)!=null?parser.getAttributeValue(4):"");
            		aRecord2.setRemarkcode_gc(parser.getAttributeValue(5)!=null?parser.getAttributeValue(5):"");
            		
            	
					aRecord2.setAttendanceruleid(parser.getAttributeValue(6)!=null?parser.getAttributeValue(6):"");
					aRecord2.setAttendanceshiftid(parser.getAttributeValue(7)!=null?parser.getAttributeValue(7):"");
					
					aRecord2.setAttendancesectionid_gc(parser.getAttributeValue(8)!=null?parser.getAttributeValue(8):"");
					
					aRecord2.setMtype_gc(parser.getAttributeValue(9)!=null?parser.getAttributeValue(9):"");
					
					aRecord2.setMintime_gc(parser.getAttributeValue(10)!=null?parser.getAttributeValue(10):"");
					aRecord2.setMaxtime_gc(parser.getAttributeValue(11)!=null?parser.getAttributeValue(11):"");
					aRecord2.setOnoffdutyname_gc(parser.getAttributeValue(12)!=null?parser.getAttributeValue(12):"");
					aRecord2.setStarttime_gc(parser.getAttributeValue(13)!=null?parser.getAttributeValue(13):"");
					aRecord2.setEndtime_gc(parser.getAttributeValue(14)!=null?parser.getAttributeValue(14):"");
					
            		
            		key = (parser.getAttributeValue(3)!=null?parser.getAttributeValue(3):"");
            		aRecord2.setRemarks_gc(parser.nextText());

//            		attendanceSearchIQResponse.aRecords.put(key,aRecord);
            		attendanceSearchIQResponse.aRecords.add(aRecord2);
                }
                if ("aclasssection".equals(parser.getName())) {
                	aClassSection = new AttendanceClassSection();
                	aClassSection.setId(parser.getAttributeValue(0)!=null?parser.getAttributeValue(0):"");
                	aClassSection.setType(parser.getAttributeValue(1)!=null?parser.getAttributeValue(1):"");
                	aClassSection.setStarttime(parser.getAttributeValue(2)!=null?parser.getAttributeValue(2):"");
                	aClassSection.setStarttime1(parser.getAttributeValue(3)!=null?parser.getAttributeValue(3):"");
                	aClassSection.setStarttime2(parser.getAttributeValue(4)!=null?parser.getAttributeValue(4):"");
                	aClassSection.setEndtime(parser.getAttributeValue(5)!=null?parser.getAttributeValue(5):"");
                	aClassSection.setEndtime1(parser.getAttributeValue(6)!=null?parser.getAttributeValue(6):"");
                	aClassSection.setEndtime2(parser.getAttributeValue(7)!=null?parser.getAttributeValue(7):"");
            		key1 = (parser.getAttributeValue(0)!=null?parser.getAttributeValue(0):"");
            		aClassSection.setOnoffdutyname(parser.nextText());

//            		attendanceSearchIQResponse.aClassSections.put(key1,aClassSection);
            		attendanceSearchIQResponse.aClassSections.add(aClassSection);
                }
                if ("startdate".equals(parser.getName())) {
                	attendanceSearchIQResponse.setStartdate(parser.nextText());
                }
                if ("enddate".equals(parser.getName())) {
                	attendanceSearchIQResponse.setEnddate(parser.nextText());
                }
                if ("isweekend".equals(parser.getName())) {
                	attendanceSearchIQResponse.setIsweekend(parser.nextText());
                }
                if ("alternaterule".equals(parser.getName())) {
                	attendanceSearchIQResponse.setAlternaterule(parser.nextText());
                }
                if ("attendanceruleid".equals(parser.getName())) {
                	attendanceSearchIQResponse.setAttendanceruleid(parser.nextText());
                }
                if ("attendanceshiftid".equals(parser.getName())) {
                	attendanceSearchIQResponse.setAttendanceshiftid(parser.nextText());
                }
                if ("retcode".equals(parser.getName())) {
                	attendanceSearchIQResponse.setRetcode(parser.nextText());
                }
                if ("memo".equals(parser.getName())) {
                	attendanceSearchIQResponse.setMemo(parser.nextText());
                }
            } else if (eventType == 3
                    && "attendancesearchiq".equals(parser.getName())) {
                done = true;
            }
        }

        return attendanceSearchIQResponse;
    }
}

