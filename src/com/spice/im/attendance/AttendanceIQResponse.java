package com.spice.im.attendance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.jivesoftware.smack.packet.IQ;

public class AttendanceIQResponse extends IQ{
    private String id;

    private String apikey;

    private String retcode;//0002 false(hash校验失败),0001 考勤失败，用户账户不存在,0003 定义了考勤区域，考勤成功,0004 定义了考勤区域，考勤失败 ,0005 未定义考勤区域，自由签, 9999 其他错误
    
    private String memo;//状态描述:登录成功，登录失败（用户名或密码错误）,登录失败（原因）
    
//    private String sessionid;//登录成功后分配的会话密钥
    
    private String uid;
    
//    private String username;
//    private String name;
//    private String avatarexists;//true false
//    private String avatarpath;
//    private String avatar;//注意avator值的值域：0默认未上传头像，1已上传头像
    
    public AttendanceIQResponse() {
    }
    
    @Override
    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append("attendanceiq").append(" xmlns=\"").append(
                "com:isharemessage:attendanceiq").append("\">");
        if (id != null) {
            buf.append("<id>").append(id).append("</id>");
        }
        if (apikey != null) {
            buf.append("<apikey>").append(apikey).append("</apikey>");
        }
        if(retcode!=null){
        	buf.append("<retcode>").append(retcode).append("</retcode>");
        }
        if(memo!=null){
        	buf.append("<memo>").append(memo).append("</memo>");
        }
//        if(sessionid!=null){
//        	buf.append("<sessionid>").append(sessionid).append("</sessionid>");
//        }
        if(uid!=null){
        	buf.append("<uid>").append(uid).append("</uid>");
        }
//        if(username!=null){
//        	buf.append("<username>").append(username).append("</username>");
//        }
//        if(name!=null){
//        	buf.append("<name>").append(name).append("</name>");
//        }
//        if(avatarexists!=null){
//        	buf.append("<avatarexists>").append(avatarexists).append("</avatarexists>");
//        }
//        if(avatarpath!=null){
//        	buf.append("<avatarpath>").append(avatarpath).append("</avatarpath>");
//        }
//        if(avatar!=null){
//        	buf.append("<avatar>").append(avatar).append("</avatar>");
//        }
        buf.append("</").append("attendanceiq").append(">");
        return buf.toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

	public String getRetcode() {
		return retcode;
	}
	public void setRetcode(String retcode) {
		this.retcode = retcode;
	}

	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
//	public String getSessionid() {
//		return sessionid;
//	}
//	public void setSessionid(String sessionid) {
//		this.sessionid = sessionid;
//	}
    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
//    
//    public String getUsername() {
//        return username;
//    }
//
//    public void setUsername(String username) {
//        this.username = username;
//    }
//    
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String username) {
//        this.username = username;
//    }
//    
//    public String getAvatarexists() {
//        return avatarexists;
//    }
//
//    public void setAvatarexists(String avatarexists) {
//        this.avatarexists = avatarexists;
//    }
//    
//    public String getAvatarpath() {
//        return avatarpath;
//    }
//
//    public void setAvatarpath(String avatarpath) {
//        this.avatarpath = avatarpath;
//    }
//    
//    public String getAvatar() {
//        return avatar;
//    }
//
//    public void setAvatar(String avatar) {
//        this.avatar = avatar;
//    }
}
