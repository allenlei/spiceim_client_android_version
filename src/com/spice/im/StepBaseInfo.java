package com.spice.im;

//package com.immomo.momo.android.activity.register;

import java.util.UUID;


//import com.stb.isharemessage.R;

import android.content.Context;
import android.content.DialogInterface;


import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

//import com.immomo.momo.android.BaseDialog;
//import com.immomo.momo.android.R;
//import com.immomo.momo.android.activity.R;

public class StepBaseInfo extends RegisterStep implements TextWatcher,
		OnCheckedChangeListener {

	private EditText mEtName;
	private EditText mEtUserSign;
	private RadioGroup mRgGender;
	private RadioButton mRbMale;
	private RadioButton mRbFemale;

	private boolean mIsChange = true;
	private boolean mIsGenderAlert;
	private BaseDialog mBaseDialog;
	
	private String IMEI = "";//deviceId
	private String simSerialNumber = "";//UIM/SIM iccid号码
	private String androidId = "";//
	private UUID deviceUuid = null;
	private String uniqueIuniqueId = "";
	private TelephonyManager tm = null;

	public StepBaseInfo(RegisterActivity activity, View contentRootView) {
		super(activity, contentRootView);
		try{
			tm = (TelephonyManager) mActivity.getSystemService(Context.TELEPHONY_SERVICE);
			IMEI = tm.getDeviceId();
			simSerialNumber=tm.getSimSerialNumber();
			androidId =android.provider.Settings.Secure.getString(mActivity.getContentResolver(),android.provider.Settings.Secure.ANDROID_ID);
			deviceUuid =new UUID(androidId.hashCode(), ((long)IMEI.hashCode() << 32) |simSerialNumber.hashCode());
			uniqueIuniqueId = deviceUuid.toString();
		}catch(Exception e){
			e.printStackTrace();
			IMEI = "";
			simSerialNumber = "";
			androidId = "";
			uniqueIuniqueId = "";
		}
	}

	@Override
	public void initViews() {
		mEtName = (EditText) findViewById(R.id.reg_baseinfo_et_name);
		mEtUserSign = (EditText) findViewById(R.id.reg_baseinfo_et_usersign);
		mRgGender = (RadioGroup) findViewById(R.id.reg_baseinfo_rg_gender);
		mRbMale = (RadioButton) findViewById(R.id.reg_baseinfo_rb_male);
		mRbFemale = (RadioButton) findViewById(R.id.reg_baseinfo_rb_female);
	}

	@Override
	public void initEvents() {
		mEtName.addTextChangedListener(this);
		mEtUserSign.addTextChangedListener(this);
		mRgGender.setOnCheckedChangeListener(this);
	}
    String name = null;
    String gender = null;//默认0未知，1男，2女
    String usersign = null;
	@Override
	public void doNext() {
//		IMEI = "";
//		simSerialNumber = "";
//		androidId = "";
//		uniqueIuniqueId = "";
		mActivity.attributes.put("imei", IMEI);
		mActivity.attributes.put("simserialnumber", simSerialNumber);
		mActivity.attributes.put("androidid", androidId);
		mActivity.attributes.put("uniqueiuniqueid", uniqueIuniqueId);
		
		mActivity.attributes.put("name", name);
//		mActivity.attributes.put("email", "");
		mActivity.attributes.put("sex", gender);
		mActivity.attributes.put("note", usersign);
		
		mOnNextActionListener.next();
	}

	@Override
	public boolean validate() {
		if (isNull(mEtName)) {
			showCustomToast("请输入用户名");
			mEtName.requestFocus();
			return false;
		}else{
			name = mEtName.getText().toString().trim();
		}
		if (isNull(mEtUserSign)) {
			showCustomToast("请输入个性签名");
			mEtUserSign.requestFocus();
			return false;
		}else{
			usersign = mEtUserSign.getText().toString().trim();
		}
		if (mRgGender.getCheckedRadioButtonId() < 0) {
			showCustomToast("请选择性别");
			return false;
		}
		return true;
	}

	@Override
	public boolean isChange() {
		return mIsChange;
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		mIsChange = true;
		if (!mIsGenderAlert) {
			mIsGenderAlert = true;
			mBaseDialog = BaseDialog.getDialog(mContext, "提示", "注册成功后性别将不可更改",
					"确认", new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
			mBaseDialog.show();
		}
		switch (checkedId) {
		case R.id.reg_baseinfo_rb_male:
			mRbMale.setChecked(true);
			gender = "1";//默认0未知，1男male，2女female
			break;

		case R.id.reg_baseinfo_rb_female:
			mRbFemale.setChecked(true);
			gender = "2";//默认0未知，1男，2女
			break;
		}
	}

	@Override
	public void afterTextChanged(Editable s) {

	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		mIsChange = true;
	}

}

