package com.spice.im;

//package com.immomo.momo.android.view;

import java.util.regex.Matcher;

import java.util.regex.Pattern;

import com.spice.im.ui.HandyTextView;



//import com.immomo.momo.android.BaseApplication;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import android.util.AttributeSet;

public class EmoticonsTextView extends HandyTextView {

	public EmoticonsTextView(Context context) {
		super(context);
	}

	public EmoticonsTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public EmoticonsTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public void setText(CharSequence text, BufferType type) {
		if (!TextUtils.isEmpty(text)) {
			super.setText(replace(text), type);
		} else {
			super.setText(text, type);
		}
	}
	private StringBuilder patternString;
	private String s;
	private Pattern buildPattern() {
		patternString = new StringBuilder(
				SpiceApplication.mEmoticons.size() * 3);
		patternString.append('(');
		for (int i = 0; i < SpiceApplication.mEmoticons.size(); i++) {
			s = SpiceApplication.mEmoticons.get(i);
			patternString.append(Pattern.quote(s));
			patternString.append('|');
		}
		patternString.replace(patternString.length() - 1,
				patternString.length(), ")");
		return Pattern.compile(patternString.toString());
	}
	private SpannableStringBuilder builder;
	private Pattern pattern;
	private Matcher matcher;
	private Bitmap bitmap;
	private ImageSpan span;
	private int id;
	private CharSequence replace(CharSequence text) {
		try {
			builder = new SpannableStringBuilder(text);
			pattern = buildPattern();
			matcher = pattern.matcher(text);
			while (matcher.find()) {
				if (SpiceApplication.mEmoticonsId.containsKey(matcher.group())) {
					id = SpiceApplication.mEmoticonsId.get(matcher.group());
					bitmap = BitmapFactory.decodeResource(
							getResources(), id);
					if (bitmap != null) {
						span = new ImageSpan(getContext(), bitmap);
						builder.setSpan(span, matcher.start(), matcher.end(),
								Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
					}
				}
			}
			return builder;
		} catch (Exception e) {
			return text;
		}finally{
//			if(bitmap!=null && !bitmap.isRecycled()){
//				bitmap.recycle();
//				bitmap = null;
//			}
//			if(builder!=null){
//				builder.clearSpans();
//				builder.clear();
//				builder = null;
//			}
		}
	}
}

