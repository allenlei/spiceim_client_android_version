package com.spice.im.chat;

//package com.immomo.momo.android.adapter;

import java.io.ByteArrayOutputStream;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.spice.im.ui.PhotoView;
import com.spice.im.utils.ImageFetcher;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;

//import com.immomo.momo.android.BaseApplication;
//import com.immomo.momo.android.activity.ImageBrowserActivity;
//import com.immomo.momo.android.view.photoview.PhotoView;

public class ImageBrowserAdapter extends PagerAdapter {

//	private BaseApplication mApplication;
	private List<String> mPhotos = new ArrayList<String>();
	private String mType;
	private Context mContext;
	private ImageFetcher mImageFetcher;
//	public ImageBrowserAdapter(BaseApplication application,
//			List<String> photos, String type) {
	public ImageBrowserAdapter(Context context,
			List<String> photos, String type) {		
//		mApplication = application;
		mContext = context;
		if (photos != null) {
			mPhotos = photos;
		}
		mType = type;
		mImageFetcher = new ImageFetcher(mContext, 240);//77
	}

	@Override
	public int getCount() {
		if (mPhotos.size() > 1) {
			return Integer.MAX_VALUE;
		}
		return mPhotos.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == object;
	}

	@Override
	public View instantiateItem(ViewGroup container, int position) {
		PhotoView photoView = new PhotoView(container.getContext());
		Bitmap bitmap = null;
		if (ImageBrowserActivity.TYPE_ALBUM.equals(mType)) {
//			bitmap = mApplication.getPhotoOriginal(mPhotos.get(position
//					% mPhotos.size()));
//			bitmap = getHttpImage("http://10.0.2.2:8080/"+mPhotos.get(position % mPhotos.size()));
			bitmap = mImageFetcher.processBitmap(mPhotos.get(position % mPhotos.size()));
		} else if (ImageBrowserActivity.TYPE_PHOTO.equals(mType)) {
//			if(mPhotos.get(position).startsWith("http://")){
//				bitmap = getHttpImage(mPhotos.get(position));
//				mImageFetcher = new ImageFetcher(mContext, 77);
				bitmap = mImageFetcher.processBitmap(mPhotos.get(position));
//			}else
//				bitmap = BitmapFactory.decodeFile(mPhotos.get(position));
		}
		if(bitmap!=null)
		photoView.setImageBitmap(bitmap);
		container.addView(photoView, LayoutParams.FILL_PARENT,
				LayoutParams.FILL_PARENT);
		return photoView;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View) object);
	}
	
	public Bitmap getHttpImage(String htmlpath){
		try{
			byte[] imagearray = null;
			URL url = new URL(htmlpath);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setConnectTimeout(5000);
			conn.setReadTimeout(5000);
			// conn.setRequestMethod("GET");
			conn.connect();
			if (conn.getResponseCode() == 200) {
			byte[] buffer = new byte[1024];
			ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
			int len = 0;
			InputStream inputStream = conn.getInputStream();
			while ((len = inputStream.read(buffer)) != -1) {
			arrayOutputStream.write(buffer, 0, buffer.length);
			}
			imagearray = arrayOutputStream.toByteArray();
			} 
			return BitmapFactory.decodeByteArray(imagearray, 0, imagearray.length);
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
}
