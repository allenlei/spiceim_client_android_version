package com.spice.im.chat.file;

import java.io.BufferedInputStream;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import android.os.Handler;
import android.os.Message;


public class FileOperation {
	public static final int LOG_SIZE = 30;
	long max;
	int length;
	Handler handler;
	public FileOperation(){
		
	}
	public FileOperation(Handler hand,String path){
		handler=hand;
		File file=new File(path);
		if(file.exists()){
			if(file.isFile()){
				this.max=file.length();
			}else{
				this.max=Tools.getDirectorySize(file);
			}
		}
		length=0;
	}
	public static void newFile(String path) {
		File f = new File(path);
		try {
			if (!f.exists()) {
				f.createNewFile();
			}
		} catch (IOException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}

	public void newFolder(String folderPath) {
		File f = new File(folderPath);
		if (!f.exists()) {
			f.mkdirs();
		}
	}

	public void newFileAndFolder(String folderPath, String fileName) {
		File f = new File(folderPath);
		if (!f.exists()) {
			f.mkdirs();
		}
		File file = new File(folderPath + "/" + fileName);
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
			}
		}
	}

	public String getFileType(String path) {// 获取后缀并转成大写
		String all[] = path.split("\\.");
		return all[all.length - 1].toUpperCase();
	}

	/*
	 * public boolean deleteFile(String path){//删除文件 File f=new File(path);
	 * if(f.exists()){ if(f.delete()){System.out .println("ttttttttttttttttS");
	 * return true;} } return false; }
	 */

	public boolean deleteFile(File file) {
		try {
			if(file.exists()){
				if (file.isFile()) {
					file.delete();
					return true;
				}
				if (file.isDirectory()) {
					File[] childFiles = file.listFiles();
					if (childFiles == null || childFiles.length == 0) {
						file.delete();
						return true;
					}
					for (int i = 0; i < childFiles.length; i++) {
						deleteFile(childFiles[i]);
					}
					file.delete();
				}
			}
			return true;
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
			return false;
		}
	}

	public void writeLogFile(String log, String path) {// <sdcard>/<app
														// name>/log
		FileWriter fw = null;
		try {
			String[] oldLog = readLogFile(path);
			for (int i=0;i<oldLog.length &&i < LOG_SIZE-1; i++) {
				log = log + "?" + oldLog[i];
			}
			fw = new FileWriter(path, false);
			// 写入文件中
			fw.write(log);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} finally {
			try {
				fw.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public String[] readLogFile(String path) {// <sdcard>/<app name>/log
		FileReader fr;
		char a[] = new char[1];
		String s = "";
		try {
			fr = new FileReader(path);
			while (fr.read(a) != -1) {
				s = s + String.valueOf(a);
			}
			fr.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return s.split("[?]");// 注意
	}

	public boolean moveFile(String oldPath, String newPath) {// 移动文件或者目录
		try {

			File src = new File(oldPath);
			if (src.isFile()) {// 文件
				File dest = new File(newPath);
				src.renameTo(dest);
			} else {// 目录
				File dest = new File(newPath);
				File newFile = new File(dest.getAbsoluteFile() + "");
				src.renameTo(newFile);
			}
			return true;
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
			return false;
		}
	}
	//oldPath传递全部的   newPath传路径就醒了  不需要文件名称
	public void copyFile(String oldPath,String newPath){
		File oldFile=new File(oldPath);
		BufferedInputStream bis=null;
		BufferedOutputStream bos=null;
		if(oldFile.exists()){
			try {
				File newFloder=new File(newPath);
				if(!newFloder.exists()){
					newFloder.mkdirs();
				}
				File newFile=new File(newPath+"/"+oldFile.getName());
				if(!newFile.exists()){
					newFile.createNewFile();
				}
				bis=new BufferedInputStream(new FileInputStream(oldFile));
				bos=new BufferedOutputStream(new FileOutputStream(newFile));
				int length=0;
				byte buffer[]=new byte[2048]; 
				while((length=bis.read(buffer))!=-1){
					bos.write(buffer, 0, length);
					this.length=this.length+length;
					Message msg=Message.obtain();
					msg.what=0x111;
					msg.arg1=(int)((this.length/(float)this.max)*100);
					handler.sendMessage(msg);
				}
				bos.flush();//从缓存写入文件中
				bis.close();
				bos.close();
			} catch (Exception e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
			}
		}
	}
	
	public boolean copyFloder(String oldPath,String newPath){
		File oldFile=new File(oldPath);
		try {
			if(oldFile.exists()){
				if(oldFile.isDirectory()){
					File allSon[]=oldFile.listFiles();
					if(allSon==null||allSon.length==0){//空文件夹
						File newFloder=new File(newPath+"/"+oldFile.getName());
						if(!newFloder.exists()){
							newFloder.mkdirs();
						}
					}else{
						for(int i=0;i<allSon.length;i++){
							copyFloder(allSon[i].getPath(),newPath+"/"+oldFile.getName());
						}
					}
				}else if(oldFile.isFile()){
					copyFile(oldPath,newPath);
				}
			}
			return true;
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
			return false;
		}
	}
	
}
