package com.spice.im.chat.file;

import java.io.File;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.util.Log;

public class Tools {
	public String CountDate(long date) {
		Date result = new Date(date);
		// SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日 HH点mm分ss秒");
		return df.format(result);
	}

	public static String getFileName(String url) {
		try {
			URL u=new URL(url);
			String result=u.getPath();
			if (result.contains("?")) {
				return result.substring(result.lastIndexOf('/') + 1,
						result.lastIndexOf('?'));
			} else
				return result.substring(result.lastIndexOf('/') + 1, result.length());
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
			return null;
		}
	}

	/*
	 * 
	 * 穿入文件大大小 和下载的线程数目
	 */
	public static int[] getBlock(int size, int num) {
		int result[] = new int[num + 1];
		int residue = size % num;

		int block = size / num;
		result[0] = 0;
		for (int i = 0; i < num; i++) {
			result[i + 1] = result[i] + block;
		}
		result[num] = result[num] + residue;
		return result;
	}

	public static int getProportion(String size, int down) {
		int result = (int) ((float) down / Float.valueOf(size) * 100);
		return result;
	}

	/*
	 * public static long getFileSize(File f){ FileInputStream fis = null;
	 * if(f.exists()){ try { fis = new FileInputStream(f); return
	 * fis.available(); } catch (Exception e) { // TODO 自动生成的 catch 块
	 * e.printStackTrace(); } finally{ try { fis.close(); } catch (Exception e)
	 * { // TODO 自动生成的 catch 块 e.printStackTrace(); } } } return 0; }
	 */
	public static long getDirectorySize(File f) {
		long size = 0;
		try {
			if (f.exists()) {
				File flist[] = f.listFiles();
				for (int i = 0; i < flist.length; i++) {
					if (flist[i].isDirectory()) {
						size = size + getDirectorySize(flist[i]);
					} else {
						size = size + flist[i].length();
					}
				}
			}
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		return size;
	}

	// String.format("%.2f", Float.valueOf(size)/1024/1024);
	public static String FormetFileSize(long fileS) {// 转换文件大小
		DecimalFormat df = new DecimalFormat("#.##");// #代表数字
		String fileSizeString = "";
		if (fileS < 1024) {
			fileSizeString = df.format((double) fileS) + "B";
		} else if (fileS < 1048576) {
			fileSizeString = df.format((double) fileS / 1024) + "KB";
		} else if (fileS < 1073741824) {
			fileSizeString = df.format((double) fileS / 1048576) + "MB";
		} else {
			fileSizeString = df.format((double) fileS / 1073741824) + "GB";
		}
		return fileSizeString;
	}

}
