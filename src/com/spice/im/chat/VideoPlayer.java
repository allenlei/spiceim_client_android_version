package com.spice.im.chat;



import com.spice.im.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;
/**
 * 视频播放组件
 * @author Administrator
 *
 */
public class VideoPlayer extends Activity implements OnTouchListener{
	
	//private 
	MediaController mController;
	VideoView viv;
	int progress=0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	requestWindowFeature(Window.FEATURE_NO_TITLE);
	this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
			WindowManager.LayoutParams.FLAG_FULLSCREEN);
	setContentView(R.layout.video_player);
	viv=(VideoView)findViewById(R.id.videoView);
	mController=new MediaController(this);
	viv.setMediaController(mController);
	String videopath=getIntent().getStringExtra("path");
//	Toast.makeText(this, "视频"+videopath, Toast.LENGTH_SHORT).show(); 
	if (videopath!=null) 
	{
		viv.setVideoPath(videopath);
	}
	viv.requestFocus();
	viv.start();
	}
    @Override
    protected void onPause() 
    {
    	// TODO Auto-generated method stub
    	super.onPause();
    	progress=viv.getCurrentPosition();
    }
    @Override
    protected void onResume() 
    {
    	// TODO Auto-generated method stub
    	super.onResume();
    	viv.seekTo(progress);
    	viv.start();
    }
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		return false;
		
	} 
}
