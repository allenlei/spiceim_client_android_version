package com.spice.im.chat;

import java.text.DecimalFormat;
import java.util.ArrayList;




import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jivesoftware.smack.util.StringUtils;






//import com.byl.qrobot.util.ExpressionUtil;
import com.dodola.model.DuitangInfo;
import com.dodola.model.DuitangInfoAdapter;
import com.dodowaterfall.widget.FlowView;
//import com.example.android.bitmapfun.util.AsyncTask;
import com.beem.push.utils.AsyncTask;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.spice.im.EmoticonsTextView;
import com.spice.im.FlippingLoadingDialog;
import com.spice.im.R;
import com.spice.im.SimpleListDialog;
import com.spice.im.SimpleListDialogAdapter;
import com.spice.im.SpiceApplication;
import com.spice.im.SimpleListDialog.onSimpleListItemClickListener;
import com.spice.im.chat.ChatPullRefListActivity.StaggeredAdapter;
import com.spice.im.chat.MessageText.MESSAGE_TYPE;
import com.spice.im.ui.HandyTextView;
import com.spice.im.utils.ImageFetcher;
import com.spiceim.db.TContactGroup;
import com.spiceim.db.TContactGroupAdapter;
import com.stb.isharemessage.service.XmppConnectionAdapter;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.text.ClipboardManager;
import android.text.SpannableString;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.LinearLayout;
import android.widget.Toast;

public class TextMessageItem implements OnLongClickListener{
	private static final String TAG = "TextMessageItem > DeleteMessage";
	private MessageText mMsgText;
	protected Context mContext;
	private MESSAGE_TYPE messageType;
	protected LayoutInflater mInflater;
	protected LinearLayout mLayoutMessageContainer;
	protected View mRootView;
	protected int mBackground;
//	private HandyTextView mEtvContent;
	private EmoticonsTextView mEtvContent;
	
	private LinearLayout mLayoutLeftContainer;
	private FlowView mIvPhotoView;
	private HandyTextView message_tv_timestamp;//消息发送/接收时间
	private HandyTextView message_tv_nickname;
	private String headimg = "",myheadimg = "";
//	private ImageFetcher mImageFetcher;
	private HandyTextView message_tv_status_audio = null;//消息发送状态
	private ChatPullRefListActivity mActivity = null;
	private String username = "",myusername = "";
	
	private int mPosition;
	private StaggeredAdapter mStAdapter = null;
	
	private String fromAccount;
	private String toAccount;
	
	
	private DuitangInfo duitangInfo = null;
	private DuitangInfoAdapter duitangInfoAdapter = null;//用户信息存入本地数据库
	private TContactGroup tg = null;
	private SimpleListDialog mDialog = null;
	private MessageAdapter messageAdapter = null;//删除本地数据库用户聊天记录
	private TContactGroupAdapter tContactGroupAdapter = null;
	public TextMessageItem(Context context,
			MessageText msgText,
			int position,
			StaggeredAdapter stAdapter,
			String _fromAccount,
			String _toAccount,
			String _headimg,
			String _myheadimg,
			ChatPullRefListActivity activity,
			String _username,
			String _myusername){
//		super(bareJid, name, message, isError,date);
		mMsgText = msgText;
		mContext = context;
		mPosition = position;
		mStAdapter = stAdapter;
		fromAccount = _fromAccount;
		toAccount = _toAccount;
		headimg = _headimg;
		myheadimg = _myheadimg;
		mActivity = activity;
		username = _username;
		myusername = _myusername;
		
		duitangInfoAdapter = DuitangInfoAdapter.getInstance(mContext);
		tContactGroupAdapter = TContactGroupAdapter.getInstance(mContext);
		
		messageType = mMsgText.getMessageType();
		
//		mMsgText.getBareJid();
//		mMsgText.getMTo();
//		Log.e("※※※※※20141020※※※※※", "※※消息TextMessageItem=>mMsgText.getBareJid()="
//				+mMsgText.getBareJid()
//				+";mMsgText.getMTo()="
//				+mMsgText.getMTo()
//				+";fromAccount="
//				+fromAccount
//				+";toAccount="
//				+toAccount
//				+";messageType="
//				+messageType);
//		initNameImg();
		
		//20121224 start
		messageAdapter = MessageAdapter.getInstance(mContext);
		mLoadingDialog = new FlippingLoadingDialog(mContext, "请求提交中");
		String[] codes = new String[] { "复制","删除","转发" };//分享,翻译
		mDialog = new SimpleListDialog(context);
		mDialog.setTitle("提示");
		mDialog.setTitleLineVisibility(View.GONE);
		mDialog.setAdapter(new SimpleListDialogAdapter(context, codes));
		mDialog.setOnSimpleListItemClickListener(new OnReplyDialogItemClickListener(
				));
		//20141224 end
		
		mInflater = LayoutInflater.from(context);
		switch (messageType) {
		case RECEIVER:
//			mRootView = mInflater.inflate(R.layout.specialchatting_item_to_picture,
//					null);
////			mBackground = R.drawable.bg_message_box_send;
			//message_send_template.xml
			mRootView = mInflater.inflate(R.layout.message_send_template,
					null);
			mBackground = R.drawable.bg_message_box_send;
			break;

		case SEND:
//			mRootView = mInflater.inflate(R.layout.specialchatting_item_from_picture,
//					null);
////			mBackground = R.drawable.bg_message_box_receive;
			//message_receive_template.xml
			mRootView = mInflater.inflate(R.layout.message_receive_template,
					null);
			mBackground = R.drawable.bg_message_box_receive;
			message_tv_status_audio = (HandyTextView)mRootView.findViewById(R.id.message_tv_status_audio);
			break;
		case conference:
//			//message_send_template.xml
//			mRootView = mInflater.inflate(R.layout.message_send_template,
//					null);
//			mBackground = R.drawable.bg_message_box_send;
			if(mMsgText.getBareJid().indexOf("@1")==-1){//自己发给群
				mRootView = mInflater.inflate(R.layout.message_receive_template,
						null);
				mBackground = R.drawable.bg_message_box_receive;
				message_tv_status_audio = (HandyTextView)mRootView.findViewById(R.id.message_tv_status_audio);
			}else{
				String key = StringUtils.parseResource(mMsgText.getBareJid());
				if(key!=null && key.startsWith("/"))
					key = key.substring(1);//"/18901203590"
				String key2 = StringUtils.parseName(fromAccount);
				if(key.equals(key2)){//自己发给群
					mRootView = mInflater.inflate(R.layout.message_receive_template,
							null);
					mBackground = R.drawable.bg_message_box_receive;
					message_tv_status_audio = (HandyTextView)mRootView.findViewById(R.id.message_tv_status_audio);
				}else{
					//message_send_template.xml
					mRootView = mInflater.inflate(R.layout.message_send_template,
							null);
					mBackground = R.drawable.bg_message_box_send;
					message_tv_status_audio = (HandyTextView)mRootView.findViewById(R.id.message_tv_status_audio);
				}
			}
			break;
		}

//		if (mRootView != null) {
		mLayoutMessageContainer = (LinearLayout) mRootView
		.findViewById(R.id.message_layout_messagecontainer);
		mLayoutMessageContainer.setBackgroundResource(mBackground);
		
		mLayoutLeftContainer = (LinearLayout) mRootView
		.findViewById(R.id.message_layout_leftcontainer);
		mIvPhotoView = (FlowView) mRootView.findViewById(R.id.message_iv_userphoto);
		
		message_tv_timestamp = (HandyTextView)mRootView.findViewById(R.id.message_tv_timestamp);
		message_tv_nickname = (HandyTextView)mRootView.findViewById(R.id.message_tv_nickname);
		
		
		onInitViews();
//		}
	}

	protected void onInitViews() {
		View view = mInflater.inflate(R.layout.message_text, null);
		mLayoutMessageContainer.addView(view);
		mEtvContent = (EmoticonsTextView) view
				.findViewById(R.id.message_etv_msgtext);
//		String zhengze = "f0[0-9]{2}|f10[0-7]";											//正则表达式，用来判断消息内是否有表情
//		try {
//			SpannableString spannableString = ExpressionUtil.getExpressionString(mContext, mMsgText.getMessage(), zhengze);
////			holder.contentView.setText(spannableString);
//			mEtvContent.setText(spannableString);
//		} catch (NumberFormatException e) {
//			e.printStackTrace();
//		} catch (SecurityException e) {
//			e.printStackTrace();
//		} catch (IllegalArgumentException e) {
//			e.printStackTrace();
//		}
		if(mMsgText.getIsOffline()==2){
//			mRootView.findViewById(R.id.isOfflineMsg).setVisibility(View.VISIBLE);
			
		    if(message_tv_status_audio!=null){
		    	message_tv_status_audio.setBackgroundResource(R.drawable.bg_msgbox_state_failure2);
		    	message_tv_status_audio.setText("发送失败");
		    }
		}else if(mMsgText.getIsOffline()==1){
//			mRootView.findViewById(R.id.isOfflineMsg).setVisibility(View.GONE);
		    if(message_tv_status_audio!=null){
		    	message_tv_status_audio.setBackgroundResource(R.drawable.bg_msgbox_state_read2);
		    	message_tv_status_audio.setText("送达");
		    }
		}
		else if(mMsgText.getIsOffline()==0){
//			mRootView.findViewById(R.id.isOfflineMsg).setVisibility(View.GONE);
		    if(message_tv_status_audio!=null){
		    	message_tv_status_audio.setBackgroundResource(R.drawable.bg_msgbox_state_read2);
		    	message_tv_status_audio.setText("发送中");
		    }
		}
//		mEtvContent.setText(mMsgText.getMessage());
		mEtvContent.setText(ExpressionUtil.prase(mContext, mEtvContent, mMsgText.getMessage()));
		mEtvContent.setOnLongClickListener(this);
		mLayoutMessageContainer.setOnLongClickListener(this);
		
		
//        mImageFetcher = new ImageFetcher(mContext, 77);
//        mImageFetcher.setUListype(0);
//        mImageFetcher.setLoadingImage(R.drawable.empty_photo3);
//        mImageFetcher.setExitTasksEarly(false);
        
	}
	String path = "";
	protected void onFillMessage() {
		
		mLayoutLeftContainer.setVisibility(View.VISIBLE);
////		mIvPhotoView.setImageBitmap();
////		mIvPhotoView.setImageResource(R.drawable.head);
//		switch (messageType) {
//			case RECEIVER:
//				mImageFetcher.loadImage(headimg, mIvPhotoView);
//				break;
//			case SEND:
//				mImageFetcher.loadImage(myheadimg, mIvPhotoView);
//				break;
//		
//		}
//		
//		message_tv_timestamp.setText(mMsgText.getTimestamp().toLocaleString());
		
		switch (messageType) {
		case RECEIVER:
//			mImageFetcher.loadImage(headimg, mIvPhotoView);
//			message_tv_nickname.setText(username);
			if(mMsgText.getBareJid().indexOf("@1")==-1){
//        		try{
//        			path = "data/avatar/"+avatar_file(Integer.parseInt(StringUtils.parseName(mMsgText.getBareJid())),"middle","");
//        		}catch(Exception e){
//        			e.printStackTrace();
//        		}
//        		if(tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(mMsgText.getBareJid()))!=null){
//        			path = tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(mMsgText.getBareJid())).getAvatarPath();
//					if(tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(mMsgText.getBareJid())).getName()!=null && tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(mMsgText.getBareJid())).getName().length()!=0)
//						message_tv_nickname.setText(tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(mMsgText.getBareJid())).getName());
//					else
//						message_tv_nickname.setText(tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(mMsgText.getBareJid())).getUsername());
//        		}else{
//        			path = "";
//        			message_tv_nickname.setText(mMsgText.getBareJid());
//        		}
				tg = tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(mMsgText.getBareJid()));
				if(tg!=null){
					path = tg.getAvatarPath();
					if(tg.getName()!=null && tg.getName().length()!=0)
						message_tv_nickname.setText(tg.getName());
					else
						message_tv_nickname.setText(tg.getUsername());
				}else{
					path = "";
					message_tv_nickname.setText(mMsgText.getBareJid());
				}
        		
        	}else{
				String key = StringUtils.parseResource(mMsgText.getBareJid());
				if(key!=null && key.startsWith("/"))
					key = key.substring(1);//"/18901203590"
				
//        		try{
//        			path = "data/avatar/"+avatar_file(Integer.parseInt(key),"middle","");
//        		}catch(Exception e){
//        			e.printStackTrace();
//        		}
////				message_tv_nickname.setText("uid为"+key);
////				message_tv_nickname.setText(tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress("2@0")).getName());
//        		if(tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(key+"@0"))!=null){
//        			path = tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(key+"@0")).getAvatarPath();
//					if(tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(key+"@0")).getName()!=null && tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(key+"@0")).getName().length()!=0)
//						message_tv_nickname.setText(tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(key+"@0")).getName());//bug 需要登录成功后将自己的信息存入本地数据库
//					else
//						message_tv_nickname.setText(tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(key+"@0")).getUsername());
//        		}else{
//        			path = "";
//        			message_tv_nickname.setText(key+"@0");
//        		}
				
				
				tg = tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(key+"@0"));
				if(tg!=null){
					path = tg.getAvatarPath();
					if(tg.getName()!=null && tg.getName().length()!=0)
						message_tv_nickname.setText(tg.getName());
					else
						message_tv_nickname.setText(tg.getUsername());
				}else{
					path = "";
					message_tv_nickname.setText(StringUtils.parseBareAddress(key+"@0"));//"空"+
				}
			}
			Log.e("================2017 TextMessageItem ================", "TextMessageItem===receiver==="+XmppConnectionAdapter.downloadPrefix.replace("/javacenterhome/","/")+path);
//			mImageFetcher.loadImage(XmppConnectionAdapter.downloadPrefix+path, mIvPhotoView);
//			mImageFetcher.loadImage(XmppConnectionAdapter.downloadPrefix.replace("/javacenterhome/","/")+path, mIvPhotoView);
			if(path!=null
    	    		&& path.length()!=0
    	    		&& !path.equalsIgnoreCase("null"))
//    	    	ImageLoader.getInstance().displayImage(XmppConnectionAdapter.downloadPrefix.replace("/javacenterhome/","/")+path, mIvPhotoView,SpiceApplication.getInstance().options);
			ImageLoader.getInstance().displayImage(XmppConnectionAdapter.downloadPrefix+path, mIvPhotoView,SpiceApplication.getInstance().options);
    	    else
    	    	mIvPhotoView.setImageResource(R.drawable.empty_photo4);
			break;
		case SEND:
//			mImageFetcher.loadImage(myheadimg, mIvPhotoView);
//			message_tv_nickname.setText(myusername);
			if(mMsgText.getBareJid().indexOf("@1")==-1){
//				try{
//        			path = "data/avatar/"+avatar_file(Integer.parseInt(StringUtils.parseName(mMsgText.getBareJid())),"middle","");
//        		}catch(Exception e){
//        			e.printStackTrace();
//        		}
//				if(tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(mMsgText.getBareJid()))!=null){
//					path = tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(mMsgText.getBareJid())).getAvatarPath();
//					if(tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(mMsgText.getBareJid())).getName()!=null && tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(mMsgText.getBareJid())).getName().length()!=0)
//						message_tv_nickname.setText(tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(mMsgText.getBareJid())).getName());
//					else
//						message_tv_nickname.setText(tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(mMsgText.getBareJid())).getUsername());
//				}else{
//					path = "";
//					message_tv_nickname.setText(mMsgText.getBareJid());
//				}
				
				tg = tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(mMsgText.getBareJid()));
				if(tg!=null){
					path = tg.getAvatarPath();
//					Toast toast = Toast.makeText(mContext, "我的tg="+tg.toXML()+"头像avatar="+tg.getAvatar()+";avatarpath="+tg.getAvatarPath(), Toast.LENGTH_LONG);
//		    	    toast.show();
					if(tg.getName()!=null && tg.getName().length()!=0)
						message_tv_nickname.setText(tg.getName());
					else
						message_tv_nickname.setText(tg.getUsername());
				}else{
					path = "";
					message_tv_nickname.setText(StringUtils.parseBareAddress(mMsgText.getBareJid()));//"空"+
				}
			}else{
				String key = StringUtils.parseResource(mMsgText.getBareJid());
				if(key!=null && key.startsWith("/"))
					key = key.substring(1);//"/18901203590"
//				try{
//        			path = "data/avatar/"+avatar_file(Integer.parseInt(key),"middle","");
//        		}catch(Exception e){
//        			e.printStackTrace();
//        		}
////				message_tv_nickname.setText("uid为"+key);
////				message_tv_nickname.setText(tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress("2@0")).getName());
//				if(tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(key+"@0"))!=null){
//					path = tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(key+"@0")).getAvatarPath();
//					if(tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(key+"@0")).getName()!=null && tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(key+"@0")).getName().length()!=0)
//						message_tv_nickname.setText(tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(key+"@0")).getName());//bug 需要登录成功后将自己的信息存入本地数据库
//					else
//						message_tv_nickname.setText(tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(key+"@0")).getUsername());
//				}else{
//					path = "";
//					message_tv_nickname.setText(key+"@0");
//				}
				
				tg = tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(key+"@0"));
				if(tg!=null){
					path = tg.getAvatarPath();
					if(tg.getName()!=null && tg.getName().length()!=0)
						message_tv_nickname.setText(tg.getName());
					else
						message_tv_nickname.setText(tg.getUsername());
				}else{
					path = "";
					message_tv_nickname.setText(StringUtils.parseBareAddress(key+"@0"));//"空"+
				}
			}
			Log.e("================2017 TextMessageItem ================", "TextMessageItem===send==="+XmppConnectionAdapter.downloadPrefix.replace("/javacenterhome/","/")+path);
//			mImageFetcher.loadImage(XmppConnectionAdapter.downloadPrefix+path, mIvPhotoView);
//			mImageFetcher.loadImage(XmppConnectionAdapter.downloadPrefix.replace("/javacenterhome/","/")+path, mIvPhotoView);
			if(path!=null
    	    		&& path.length()!=0
    	    		&& !path.equalsIgnoreCase("null"))
//    	    	ImageLoader.getInstance().displayImage(XmppConnectionAdapter.downloadPrefix.replace("/javacenterhome/","/")+path, mIvPhotoView,SpiceApplication.getInstance().options);
			ImageLoader.getInstance().displayImage(XmppConnectionAdapter.downloadPrefix+path, mIvPhotoView,SpiceApplication.getInstance().options);
    	    else
    	    	mIvPhotoView.setImageResource(R.drawable.empty_photo4);
			break;
		case conference:
			Log.e("※※※※※2014080508050805※※※※※", "※※fromAccount="+fromAccount+";toAccount="+toAccount);
			if(mMsgText.getBareJid().indexOf("@1")==-1){//自己发给群
////				mImageFetcher.loadImage(myheadimg, mIvPhotoView);
////				message_tv_nickname.setText(myusername);
//				try{
//        			path = "data/avatar/"+avatar_file(Integer.parseInt(StringUtils.parseName(mMsgText.getBareJid())),"middle","");
//        		}catch(Exception e){
//        			e.printStackTrace();
//        		}
//				if(tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(mMsgText.getBareJid()))!=null){
//					path = tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(mMsgText.getBareJid())).getAvatarPath();
//					if(tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(mMsgText.getBareJid())).getName()!=null)
//						message_tv_nickname.setText(tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(mMsgText.getBareJid())).getName());
//					else
//						message_tv_nickname.setText(tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(mMsgText.getBareJid())).getUsername());
//				}else{
//					path = "";
//					message_tv_nickname.setText(mMsgText.getBareJid());
//				}
				
				tg = tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(mMsgText.getBareJid()));
				if(tg!=null){
					path = tg.getAvatarPath();
//					Toast toast = Toast.makeText(mContext, "我的tg="+tg.toXML()+"头像avatar="+tg.getAvatar()+";avatarpath="+tg.getAvatarPath(), Toast.LENGTH_LONG);
//		    	    toast.show();
					if(tg.getName()!=null && tg.getName().length()!=0)
						message_tv_nickname.setText(tg.getName());
					else
						message_tv_nickname.setText(tg.getUsername());
				}else{
					path = "";
					message_tv_nickname.setText(StringUtils.parseBareAddress(mMsgText.getBareJid()));//"空"+
				}
			}else{
				String key = StringUtils.parseResource(mMsgText.getBareJid());
				if(key!=null && key.startsWith("/"))
					key = key.substring(1);//"/18901203590"
//				try{
//        			path = "data/avatar/"+avatar_file(Integer.parseInt(key),"middle","");
//        		}catch(Exception e){
//        			e.printStackTrace();
//        		}
				String key2 = StringUtils.parseName(fromAccount);
				if(key.equals(key2)){//自己发给群
////					mImageFetcher.loadImage(myheadimg, mIvPhotoView);
////					message_tv_nickname.setText(myusername);
////					message_tv_nickname.setText("uid为"+key);
//					if(tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(key+"@0"))!=null){
//						path = tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(key+"@0")).getAvatarPath();
//						if(tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(key+"@0")).getName()!=null && tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(key+"@0")).getName().length()!=0)
//							message_tv_nickname.setText(tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(key+"@0")).getName());//bug 需要登录成功后将自己的信息存入本地数据库
//						else
//							message_tv_nickname.setText(tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(key+"@0")).getUsername());
//					}else{
//						path = "";
//						message_tv_nickname.setText(key+"@0");
//					}
					
					tg = tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(key+"@0"));
					if(tg!=null){
						path = tg.getAvatarPath();
						if(tg.getName()!=null && tg.getName().length()!=0)
							message_tv_nickname.setText(tg.getName());
						else
							message_tv_nickname.setText(tg.getUsername());
					}else{
						path = "";
						message_tv_nickname.setText(StringUtils.parseBareAddress(key+"@0"));//"空"+
					}
				}else{
////					Contact contact = mActivity.mListContact.get(key);
////					String url = "";
////					if(contact!=null){
////						message_tv_nickname.setText(contact.getName());
////			            if(contact.getAvatarId()!=null 
////			            		&& contact.getAvatarId().length()!=0 
////			            		&& !contact.getAvatarId().equalsIgnoreCase("null")
////			            		&& (
////			            				contact.getAvatarId().toLowerCase().endsWith(".jpg")
////			            				|| contact.getAvatarId().toLowerCase().endsWith(".png")
////			            				|| contact.getAvatarId().toLowerCase().endsWith(".bmp")
////			            				|| contact.getAvatarId().toLowerCase().endsWith(".gif")
////			            			))
////			            	url = XmppConnectionAdapter.downloadPrefix+StringUtils.parseName(contact.getJID())+"/"+"thumbnail_"+contact.getAvatarId();
////			            if(url!=null && url.length()!=0)
////							mImageFetcher.loadImage(url, mIvPhotoView);
////					}else{
////						mImageFetcher.loadImage(headimg, mIvPhotoView);
////						message_tv_nickname.setText(username);
//						if(tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(key+"@0"))!=null){
//							path = tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(key+"@0")).getAvatarPath();
//							if(tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(key+"@0")).getName()!=null
//									&& tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(key+"@0")).getName().length()!=0
//									&& !tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(key+"@0")).getName().equalsIgnoreCase("null"))
//								message_tv_nickname.setText(tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(key+"@0")).getName());
//							else
//								message_tv_nickname.setText(tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(key+"@0")).getUsername());
//						}else{
//							path = "";
//							message_tv_nickname.setText(key+"@0");
//						}
////						message_tv_nickname.setText(key);
////					}
////					if(url!=null && url.length()!=0)
////						mImageFetcher.loadImage(url, mIvPhotoView);
					
					
					tg = tContactGroupAdapter.getTContactGroup(StringUtils.parseBareAddress(key+"@0"));
					if(tg!=null){
						path = tg.getAvatarPath();
						if(tg.getName()!=null && tg.getName().length()!=0 && !tg.getName().equalsIgnoreCase("null"))
							message_tv_nickname.setText(tg.getName());
						else
							message_tv_nickname.setText(tg.getUsername());
					}else{
						path = "";
						message_tv_nickname.setText(StringUtils.parseBareAddress(key+"@0"));//"空"+
					}
				}
			}
			Log.e("================2017 TextMessageItem ================", "TextMessageItem===conference==="+XmppConnectionAdapter.downloadPrefix.replace("/javacenterhome/","/")+path);
//			mImageFetcher.loadImage(XmppConnectionAdapter.downloadPrefix+path, mIvPhotoView);
//			mImageFetcher.loadImage(XmppConnectionAdapter.downloadPrefix.replace("/javacenterhome/","/")+path, mIvPhotoView);	
			if(path!=null
    	    		&& path.length()!=0
    	    		&& !path.equalsIgnoreCase("null"))
//    	    	ImageLoader.getInstance().displayImage(XmppConnectionAdapter.downloadPrefix.replace("/javacenterhome/","/")+path, mIvPhotoView,SpiceApplication.getInstance().options);
			ImageLoader.getInstance().displayImage(XmppConnectionAdapter.downloadPrefix+path, mIvPhotoView,SpiceApplication.getInstance().options);
    	    else
    	    	mIvPhotoView.setImageResource(R.drawable.empty_photo4);
			break;
	
		}
	
		message_tv_timestamp.setText(mMsgText.getTimestamp().toLocaleString());
	}
	public View getRootView() {
		return mRootView;
	}
	@Override
	public boolean onLongClick(View v) {
//		System.out.println("长按");
//		return true;
		if(mDialog!=null)
			mDialog.show();
		return true;
	}
	
	public void initNameImg(){
		switch (messageType) {
			case RECEIVER:
				if(headimg==null
						|| headimg.equalsIgnoreCase("null")
						|| headimg.length()==0
						|| username==null
						|| username.equalsIgnoreCase("null")
						|| username.length()==0){
					duitangInfo = duitangInfoAdapter.getDuitangInfo(StringUtils.parseName(mMsgText.getBareJid()));//toAccount
					if(duitangInfo!=null){
						headimg = duitangInfo.getIsrc();
						username = duitangInfo.getName();
					}else{
						headimg = "";
						username = StringUtils.parseName(mMsgText.getBareJid());
					}
				}
//				if(myheadimg==null
//						|| myheadimg.equalsIgnoreCase("null")
//						|| myheadimg.length()==0
//						|| myusername==null
//						|| myusername.equalsIgnoreCase("null")
//						|| myusername.length()==0){
//					duitangInfo = duitangInfoAdapter.getDuitangInfo(StringUtils.parseName(fromAccount));
//					if(duitangInfo!=null){
//						myheadimg = duitangInfo.getIsrc();
//						myusername = duitangInfo.getName();
//					}else{
//						myheadimg = "";
//						myusername = StringUtils.parseName(fromAccount);
//					}
//				}
				break;
			case SEND:
//				if(headimg==null
//						|| headimg.equalsIgnoreCase("null")
//						|| headimg.length()==0
//						|| username==null
//						|| username.equalsIgnoreCase("null")
//						|| username.length()==0){
//					duitangInfo = duitangInfoAdapter.getDuitangInfo(StringUtils.parseName(fromAccount));
//					if(duitangInfo!=null){
//						headimg = duitangInfo.getIsrc();
//						username = duitangInfo.getName();
//					}else{
//						headimg = "";
//						username = StringUtils.parseName(fromAccount);
//					}
//				}
				if(myheadimg==null
						|| myheadimg.equalsIgnoreCase("null")
						|| myheadimg.length()==0
						|| myusername==null
						|| myusername.equalsIgnoreCase("null")
						|| myusername.length()==0){
					if(mMsgText.getBareJid().equalsIgnoreCase("Me"))
						duitangInfo = duitangInfoAdapter.getDuitangInfo(StringUtils.parseName(fromAccount));
					else
						duitangInfo = duitangInfoAdapter.getDuitangInfo(StringUtils.parseName(mMsgText.getBareJid()));
					if(duitangInfo!=null){
						myheadimg = duitangInfo.getIsrc();
						myusername = duitangInfo.getName();
					}else{
						myheadimg = "";
						if(mMsgText.getBareJid().equalsIgnoreCase("Me"))
							myusername = StringUtils.parseName(fromAccount);
						else
							myusername = StringUtils.parseName(mMsgText.getBareJid());//toAccount
					}
				}
				break;
			case conference:
				if(mMsgText.getBareJid().indexOf("@conference.")==-1){//自己发给群
					if(myheadimg==null
							|| myheadimg.equalsIgnoreCase("null")
							|| myheadimg.length()==0
							|| myusername==null
							|| myusername.equalsIgnoreCase("null")
							|| myusername.length()==0){
						if(mMsgText.getBareJid().equalsIgnoreCase("Me"))
							duitangInfo = duitangInfoAdapter.getDuitangInfo(StringUtils.parseName(fromAccount));
						else
							duitangInfo = duitangInfoAdapter.getDuitangInfo(StringUtils.parseName(mMsgText.getBareJid()));
						if(duitangInfo!=null){
							myheadimg = duitangInfo.getIsrc();
							myusername = duitangInfo.getName();
						}else{
							myheadimg = "";
							if(mMsgText.getBareJid().equalsIgnoreCase("Me"))
								myusername = StringUtils.parseName(fromAccount);
							else
								myusername = StringUtils.parseName(mMsgText.getBareJid());
						}
					}
				}else{
					String key = StringUtils.parseResource(mMsgText.getBareJid());
					if(key!=null && key.startsWith("/"))
						key = key.substring(1);//"/18901203590"
					String key2 = StringUtils.parseName(fromAccount);
					if(key.equals(key2)){//自己发给群
						if(myheadimg==null
								|| myheadimg.equalsIgnoreCase("null")
								|| myheadimg.length()==0
								|| myusername==null
								|| myusername.equalsIgnoreCase("null")
								|| myusername.length()==0){
							duitangInfo = duitangInfoAdapter.getDuitangInfo(key);
							if(duitangInfo!=null){
								myheadimg = duitangInfo.getIsrc();
								myusername = duitangInfo.getName();
							}else{
								myheadimg = "";
								myusername = key;
							}
						}
					}else{
						//message_send_template.xml
						if(headimg==null
								|| headimg.equalsIgnoreCase("null")
								|| headimg.length()==0
								|| username==null
								|| username.equalsIgnoreCase("null")
								|| username.length()==0){
							duitangInfo = duitangInfoAdapter.getDuitangInfo(key);
							if(duitangInfo!=null){
								headimg = duitangInfo.getIsrc();
								username = duitangInfo.getName();
							}else{
								headimg = "";
								username = key;
							}
						}
					}
				}
				
//				Log.e("※※※※※20141020※※※※※", "※※消息TextMessageItem=>mMsgText.getBareJid()="
//						+mMsgText.getBareJid()
//						+";mMsgText.getMTo()="
//						+mMsgText.getMTo()
//						+";key="
//						+StringUtils.parseResource(mMsgText.getBareJid())
//						+";key2="
//						+StringUtils.parseName(fromAccount));
				break;
			default:
				break;
		
		}
	}
	
	
	private class OnReplyDialogItemClickListener implements
	onSimpleListItemClickListener {
		
		public OnReplyDialogItemClickListener() {
		}
		@Override
		public void onItemClick(int position) {
			// "复制","删除","转发" };//分享,翻译
			switch (position) {
				case 0:
					copy(mMsgText.getMessage());
					break;
				case 1:
					startDelete();
					break;
				case 2:
//					ClipboardManager m = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
//					m.setText(mMsgText.getMessage());
//		        	Intent intent = new Intent(mContext, BulkMsgContactListPullRefListActivity.class);//BulkMsgContactListPullRefListActivity
//		        	intent.putExtra("myheadimg", myheadimg);
//		        	intent.putExtra("myusername", myusername);
//		        	
//		        	intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//		        	mContext.startActivity(intent);
//		        	if(mActivity != null)
//		        		((ChatPullRefListActivity)mContext).finish();
//		        	else if(mActivity_bulkmsg !=null)
//		        		((BulkMsgChatPullRefListActivity)mContext).finish();
					break;
			}
		}
	}
	private void copy(String text) {
		ClipboardManager m = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
		m.setText(text);
		showCustomToast("已成功复制文本:"+text);
	}
	/** 显示自定义Toast提示(来自String) **/
	protected void showCustomToast(String text) {
		View toastRoot = LayoutInflater.from(mContext).inflate(
				R.layout.common_toast, null);
		((HandyTextView) toastRoot.findViewById(R.id.toast_text)).setText(text);
		Toast toast = new Toast(mContext);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.setDuration(Toast.LENGTH_SHORT);
		toast.setView(toastRoot);
		toast.show();
	}
	
	private static ExecutorService LIMITED_TASK_EXECUTOR;
    static {
      LIMITED_TASK_EXECUTOR = (ExecutorService) Executors.newFixedThreadPool(7); 
    }; 
	protected List<AsyncTask<Void, Void, Boolean>> mAsyncTasks = new ArrayList<AsyncTask<Void, Void, Boolean>>();
	protected FlippingLoadingDialog mLoadingDialog;
	protected void putAsyncTask(AsyncTask<Void, Void, Boolean> asyncTask) {
//		mAsyncTasks.add(asyncTask.execute());
		mAsyncTasks.add(asyncTask.executeOnExecutor(LIMITED_TASK_EXECUTOR, null));
	}

	protected void clearAsyncTask() {
		Iterator<AsyncTask<Void, Void, Boolean>> iterator = mAsyncTasks
				.iterator();
		while (iterator.hasNext()) {
			AsyncTask<Void, Void, Boolean> asyncTask = iterator.next();
			if (asyncTask != null && !asyncTask.isCancelled()) {
				asyncTask.cancel(true);
			}
		}
		mAsyncTasks.clear();
	}

	protected void showLoadingDialog(String text) {
		if (text != null) {
			mLoadingDialog.setText(text);
		}
		mLoadingDialog.show();
	}

	protected void dismissLoadingDialog() {
		if (mLoadingDialog.isShowing()) {
			mLoadingDialog.dismiss();
		}
	}
	private void startDelete() {
		putAsyncTask(new AsyncTask<Void, Void, Boolean>() {

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				showLoadingDialog("正在删除,请稍候...");
				Log.e("MMMMMMMMMMMM20141224DeleteMessageMMMMMMMMMMMM", "++++++++++++++20141224DeleteMessage="+mMsgText.getId());
			}

			@Override
			protected Boolean doInBackground(Void... params) {
				try {
				    messageAdapter.deleteMessage(mMsgText.getId());
				} catch (Exception e) {
				    Log.e(TAG, e.getMessage());
				}
				return true;
			}

			@Override
			protected void onPostExecute(Boolean result) {
				super.onPostExecute(result);
				dismissLoadingDialog();
				if (!result) {
					showCustomToast("删除失败...");
				}else{
					showCustomToast("删除成功...");
					mHandler.sendEmptyMessage(3);
				}
				
				
			}

		});
	}
	Handler mHandler = new Handler() {

		@Override
		public void handleMessage(android.os.Message msg) {
			super.handleMessage(msg);
			switch (msg.what) {
				case 3:
					if(mStAdapter!=null){
						mStAdapter.deleteMsg(mPosition);
						mStAdapter.notifyDataSetChanged();
					}
					break;
			}
		}
	};
	
	
	
	public String sprintf(String format, double number) {
		return new DecimalFormat(format).format(number);
	}
	//sizeType :big,middle,small
	//type : real 或 ""
	public String avatar_file(int uid, String sizeType, String type) {
		String var = "avatarfile_" + uid + "_" + sizeType + "_" + type;
		String avatarPath = "";
			uid = Math.abs(uid);
			String struid = sprintf("000000000", uid);
			String dir1 = struid.substring(0, 3);
			String dir2 = struid.substring(3, 5);
			String dir3 = struid.substring(5, 7);
			StringBuffer avater = new StringBuffer();
			avater.append(dir1 + "/" + dir2 + "/" + dir3 + "/" + struid.substring(struid.length() - 2));
			avater.append("real".equals(type) ? "_real_avatar_" : "_avatar_");
			avater.append(sizeType + ".jpg");
			avatarPath = avater.toString();

		return avatarPath;
	}
	
}
