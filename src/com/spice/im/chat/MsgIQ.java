package com.spice.im.chat;

import org.jivesoftware.smack.packet.IQ;

public class MsgIQ extends IQ{
    //elementName = msgiq
	//namespace = com:isharemessage:msgiq
    private String id;
    private String apikey;
    private String partnerid;
    
	private String fromUser;//发送者
	private String fromUserUid;
	private String fromUserUuid;
	private int msgType1 = 0;//发送消息类型：0个人消息，1群组消息
	private String toUser;//接收者:可以是个人用户也可以是群组
	private String toUserUid;
	private String toUserUuid;
	private int msgType2 = 0;//信息类型：0文本消息,1图片,2语音,3位置,4音乐,5新闻
	private String subject;//信息标题
	private String content;//信息内容
	private String url;//图片或语音链接
	private String date;//时间
	private String jsoninfo;//
	private String bak1;//扩展1 //音乐是否正在播放 0未播放 1正在播放
	private String bak2;//扩展2
	private String bak3;//扩展3
	private String bak4;//扩展4
	private String bak5;//扩展5
	private String bak6;//扩展6
	
	private String hash;//partnerid+touserUuid+fromUserUuid 取sha-1摘要
	
    @Override
    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append("msgiq").append(" xmlns=\"").append(
                "com:isharemessage:msgiq").append("\">");
        if (id != null) {
            buf.append("<id>").append(id).append("</id>");
        }
        if (apikey != null) {
            buf.append("<apikey>").append(apikey).append("</apikey>");
        }
        if (partnerid != null) {
            buf.append("<partnerid>").append(partnerid).append("</partnerid>");
        }
        if (fromUser != null) {
            buf.append("<fromUser>").append(fromUser).append("</fromUser>");
        }
        if (fromUserUid != null) {
            buf.append("<fromUserUid>").append(fromUserUid).append("</fromUserUid>");
        }
        if (fromUserUuid != null) {
            buf.append("<fromUserUuid>").append(fromUserUuid).append("</fromUserUuid>");
        }
        
        buf.append("<msgType1>").append(msgType1).append("</msgType1>");
        if (toUser != null) {
            buf.append("<toUser>").append(toUser).append("</toUser>");
        }
        if (toUserUid != null) {
            buf.append("<toUserUid>").append(toUserUid).append("</toUserUid>");
        }
        if (toUserUuid != null) {
            buf.append("<toUserUuid>").append(toUserUuid).append("</toUserUuid>");
        }
        buf.append("<msgType2>").append(msgType2).append("</msgType2>");
        if (subject != null) {
            buf.append("<subject>").append(subject).append("</subject>");
        }
        if (content != null) {
            buf.append("<content>").append(content).append("</content>");
        }
        if (url != null) {
            buf.append("<url>").append(url).append("</url>");
        }
        if (date != null) {
            buf.append("<date>").append(date).append("</date>");
        }
        if (jsoninfo != null) {
            buf.append("<jsoninfo>").append(jsoninfo).append("</jsoninfo>");
        }
        if (bak1 != null) {
            buf.append("<bak1>").append(bak1).append("</bak1>");
        }
        if (bak2 != null) {
            buf.append("<bak2>").append(bak2).append("</bak2>");
        }
        if (bak3 != null) {
            buf.append("<bak3>").append(bak3).append("</bak3>");
        }
        if (bak4 != null) {
            buf.append("<bak4>").append(bak4).append("</bak4>");
        }
        if (bak5 != null) {
            buf.append("<bak5>").append(bak5).append("</bak5>");
        }
        if (bak6 != null) {
            buf.append("<bak6>").append(bak6).append("</bak6>");
        }
        if (hash != null) {
            buf.append("<hash>").append(hash).append("</hash>");
        }
        buf.append("</").append("msgiq").append(">");
        return buf.toString();
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

	public String getApikey() {
		return apikey;
	}
	public void setApikey(String apikey) {
		this.apikey = apikey;
	}
	public String getPartnerid() {
		return partnerid;
	}
	public void setPartnerid(String partnerid) {
		this.partnerid = partnerid;
	}
	public String getFromUser() {
		return fromUser;
	}
	public void setFromUser(String fromUser) {
		this.fromUser = fromUser;
	}
	public String getFromUserUid() {
		return fromUserUid;
	}
	public void setFromUserUid(String fromUserUid) {
		this.fromUserUid = fromUserUid;
	}
	public String getFromUserUuid() {
		return fromUserUuid;
	}
	public void setFromUserUuid(String fromUserUuid) {
		this.fromUserUuid = fromUserUuid;
	}
	public int getMsgType1() {
		return msgType1;
	}
	public void setMsgType1(int msgType1) {
		this.msgType1 = msgType1;
	}
	public String getToUser() {
		return toUser;
	}
	public void setToUser(String toUser) {
		this.toUser = toUser;
	}
	public String getToUserUid() {
		return toUserUid;
	}
	public void setToUserUid(String toUserUid) {
		this.toUserUid = toUserUid;
	}
	public String getToUserUuid() {
		return toUserUuid;
	}
	public void setToUserUuid(String toUserUuid) {
		this.toUserUuid = toUserUuid;
	}
	public int getMsgType2() {
		return msgType2;
	}
	public void setMsgType2(int msgType2) {
		this.msgType2 = msgType2;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getJsoninfo() {
		return jsoninfo;
	}
	public void setJsoninfo(String jsoninfo) {
		this.jsoninfo = jsoninfo;
	}
	public String getBak1() {
		return bak1;
	}
	public void setBak1(String bak1) {
		this.bak1 = bak1;
	}
	public String getBak2() {
		return bak2;
	}
	public void setBak2(String bak2) {
		this.bak2 = bak2;
	}
	public String getBak3() {
		return bak3;
	}
	public void setBak3(String bak3) {
		this.bak3 = bak3;
	}
	public String getBak4() {
		return bak4;
	}
	public void setBak4(String bak4) {
		this.bak4 = bak4;
	}
	public String getBak5() {
		return bak5;
	}
	public void setBak5(String bak5) {
		this.bak5 = bak5;
	}
	public String getBak6() {
		return bak6;
	}
	public void setBak6(String bak6) {
		this.bak6 = bak6;
	}
	
	public String getHash() {
		return hash;
	}
	public void setHash(String hash) {
		this.hash = hash;
	}
	
}
