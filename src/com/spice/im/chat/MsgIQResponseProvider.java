package com.spice.im.chat;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;

public class MsgIQResponseProvider  implements IQProvider {
//	HashMap parameters = new HashMap();
    public MsgIQResponseProvider() {
    }

    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {

//    	ThreePartPushIQ threePartPushIQ = new ThreePartPushIQ();
    	MsgIQResponse msgIQResponse = new MsgIQResponse();
        for (boolean done = false; !done;) {
            int eventType = parser.next();
            if (eventType == 2) {
                if ("id".equals(parser.getName())) {
                	msgIQResponse.setId(parser.nextText());
//                	threePartPushIQResponse.setId(threePartPushIQ.getId());
                }
                if ("retcode".equals(parser.getName())) {
                	msgIQResponse.setRetcode(parser.nextText());
                }
                if ("memo".equals(parser.getName())) {
                	msgIQResponse.setMemo(parser.nextText());
                }
            } else if (eventType == 3
                    && "msgiq".equals(parser.getName())) {//elementName = bindingpartneriq
            	
                done = true;
            }
        }
        
        return msgIQResponse;
        		
    }
}
