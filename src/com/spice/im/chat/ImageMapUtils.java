package com.spice.im.chat;


import java.io.InputStream;


import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

//import com.example.android.bitmapfun.util.AsyncTask;
import com.beem.push.utils.AsyncTask;
import com.spice.im.utils.PhotoUtils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

public class ImageMapUtils {
	public static ImageMapUtils mImageMapUtils;
//	private Executor mExecutor;
	private OnMapDownloadStateListener mOnMapDownloadStateListener;
	private Bitmap mMapImage;
	
	private static Context mContext;

	private ImageMapUtils(Context context) {
		mContext = context;
//		mExecutor = Executors.newFixedThreadPool(5);
	}

	public static ImageMapUtils create(Context context) {
		if (mImageMapUtils == null) {
			mContext = context;
			mImageMapUtils = new ImageMapUtils(context);
		}
		return mImageMapUtils;
	}
    private String path = null;
	public void display(final String uri) {
		mMapImage = null;
		if (mOnMapDownloadStateListener != null) {
			mOnMapDownloadStateListener.onStart();
		}
//		mExecutor.execute(new Runnable() {
//
//			@Override
//			public void run() {
//				InputStream is = null;
//				try {
//					URL url = new URL(uri);
//					HttpURLConnection connection = (HttpURLConnection) url
//							.openConnection();
//					is = connection.getInputStream();
//					mMapImage = BitmapFactory.decodeStream(is);
//					String path = PhotoUtils.savePhotoToSDCard(mContext,mMapImage);
//					Log.e("※※※※※20141013path※※※※※", "※※path="+path);
//					if (mOnMapDownloadStateListener != null) {
//						mOnMapDownloadStateListener.onFinish(path);
//					}
//				} catch (Exception e) {
//					if (mOnMapDownloadStateListener != null) {
//						mOnMapDownloadStateListener.onFinish(null);
//					}
//				}
//			}
//		});
		
		putAsyncTask(new AsyncTask<Void, Void, Boolean>() {

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
			}

			@Override
			protected Boolean doInBackground(Void... params) {
				InputStream is = null;
				URL url = null;
				HttpURLConnection connection = null;
				try {
					url = new URL(uri);
					connection = (HttpURLConnection) url
							.openConnection();
					is = connection.getInputStream();
					mMapImage = BitmapFactory.decodeStream(is);
					path = PhotoUtils.savePhotoToSDCard(mContext,mMapImage);
					Log.e("※※※※※20141013path※※※※※", "※※path="+path);
					return true;
				} catch (Exception e) {
					return false;
				} finally{
					if(is!=null){
						try{
							is.close();
							is = null;
						}catch(Exception e){}
					}
					if(connection!=null){
						try{
						connection.disconnect();
						connection = null;
						}catch(Exception e){}
					}
					if(url!=null)
						url = null;
				}
			}

			@Override
			protected void onPostExecute(Boolean result) {
				super.onPostExecute(result);
				if (result){ 
					if (mOnMapDownloadStateListener != null) {
						mOnMapDownloadStateListener.onFinish(path);
					}
				}else{
					if (mOnMapDownloadStateListener != null) {
						mOnMapDownloadStateListener.onFinish(null);
					}
				}
			}

		});
	}

	public void display(double longitude, double latitude) {
		String uri = getMapUrl(longitude, latitude);
		Log.e("※※※※※20141013uri※※※※※", "※※uri="+uri);
		display(uri);
	}

	public String getMapUrl(double longitude, double latitude) {
		return "http://api.map.baidu.com/staticimage?center="+longitude+","+latitude+"&width=256&height=128&zoom=13";
	}

	public void setOnMapDownloadStateListener(
			OnMapDownloadStateListener listener) {
		mOnMapDownloadStateListener = listener;
	}

	public interface OnMapDownloadStateListener {
		void onStart();

		void onFinish(String path);
	}
	
    private static ExecutorService LIMITED_TASK_EXECUTOR;  
      
    static { 
        LIMITED_TASK_EXECUTOR = (ExecutorService) Executors.newFixedThreadPool(7);  
    }; 
	protected List<AsyncTask<Void, Void, Boolean>> mAsyncTasks = new ArrayList<AsyncTask<Void, Void, Boolean>>();	
	protected void putAsyncTask(AsyncTask<Void, Void, Boolean> asyncTask) {
		mAsyncTasks.add(asyncTask.executeOnExecutor(LIMITED_TASK_EXECUTOR, null));//LIMITED_TASK_EXECUTOR  FULL_TASK_EXECUTOR
//		mAsyncTasks.add(asyncTask.execute());
	}
	protected void clearAsyncTask() {
		Iterator<AsyncTask<Void, Void, Boolean>> iterator = mAsyncTasks.iterator();
		while (iterator.hasNext()) {
			AsyncTask<Void, Void, Boolean> asyncTask = iterator.next();
			if (asyncTask != null && !asyncTask.isCancelled()) {
				asyncTask.cancel(true);
			}
		}
		mAsyncTasks.clear();
	}
//	@Override
	public void onCancel() {
		clearAsyncTask();
	}
}

