package com.spice.im.chat;

import java.util.ArrayList;
import java.util.Date;


//import com.stb.isharemessage.service.Message;

public class MessageText implements Comparable<MessageText>{
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
	private boolean isUpload;
	public void setIsUpload(boolean _isUpload){
		isUpload = _isUpload;
	}
	public boolean getIsUpload(){
		return isUpload;
	}
	private boolean isDownload;
	public void setIsDownload(boolean _isDownload){
		isDownload = _isDownload;
	}
	public boolean getIsDownload(){
		return isDownload;
	}
    private MESSAGE_TYPE messageType;
//    private CONTENT_TYPE contentType;
	private String mBareJid;
	private String mName;
	private String mMessage;
	private boolean mIsError;
	private Date mTimestamp;
//	private boolean isOffline = false;
//	public void setIsOffline(boolean _isOffline){
//		isOffline = _isOffline;
//	}
//	public boolean getIsOffline(){
//		return isOffline;
//	}
	
	private int mIsOffline = 0;//0 other发送中,1 false notoffline发送成功, 2true 发送失败isoffline
    public int getIsOffline() {
    	return mIsOffline;
    }
    public void setIsOffline(int isOffline) {
    	mIsOffline = isOffline;
    }
	
	private String mTo;
	public void setMTo(String To){
		mTo = To;
	}
	public String getMTo(){
		return mTo;
	}

	/**
	 * Constructor.
	 * @param bareJid A String containing the bare JID of the message's author.
	 * @param name A String containing the name of the message's author.
	 * @param message A String containing the message.
	 */
	public MessageText(final MESSAGE_TYPE xmessageType,final String bareJid, final String name, final String message) {
		messageType = xmessageType;
	    mBareJid = bareJid;
	    mName = name;
	    mMessage = message;
	    mIsError = false;
	}

	/**
	 * Constructor.
	 * @param bareJid A String containing the bare JID of the message's author.
	 * @param name A String containing the name of the message's author.
	 * @param message A String containing the message.
	 * @param isError if the message is an error message.
	 */
	public MessageText(final MESSAGE_TYPE xmessageType,final String bareJid, final String name, final String message, final boolean isError) {
		messageType = xmessageType;
		mBareJid = bareJid;
	    mName = name;
	    mMessage = message;
	    mIsError = isError;
	}

	/**
	 * Constructor.
	 * @param bareJid A String containing the bare JID of the message's author.
	 * @param name A String containing the name of the message's author.
	 * @param message A String containing the message.
	 * @param isError if the message is an error message.
	 * @param date the time of the message.
	 */
	//,final CONTENT_TYPE xcontentType
	public MessageText(final MESSAGE_TYPE xmessageType,final String bareJid, final String name, final String message, final boolean isError,
	    final Date date) {
		messageType = xmessageType;
//		contentType = xcontentType;
	    mBareJid = bareJid;
	    mName = name;
	    mMessage = message;
	    mIsError = isError;
	    mTimestamp = date;
	}
	public MessageText(final MESSAGE_TYPE xmessageType,final String bareJid, final String name, final String message, final boolean isError,
		    final Date date,final int _isOffline) {
			messageType = xmessageType;
//			contentType = xcontentType;
		    mBareJid = bareJid;
		    mName = name;
		    mMessage = message;
		    mIsError = isError;
		    mTimestamp = date;
		    mIsOffline = _isOffline;
		}

	public MESSAGE_TYPE getMessageType(){
		return messageType;
	}
	public void setMessageType(MESSAGE_TYPE xmessageType){
		messageType = xmessageType;
	}
//	public CONTENT_TYPE getContentType(){
//		return contentType;
//	}
//	public void setContentType(CONTENT_TYPE xcontentType){
//		contentType = xcontentType;
//	}
	/**
	 * JID attribute accessor.
	 * @return A String containing the bare JID of the message's author.
	 */
	public String getBareJid() {
	    return mBareJid;
	}

	/**
	 * Name attribute accessor.
	 * @return A String containing the name of the message's author.
	 */
	public String getName() {
	    return mName;
	}

	/**
	 * Message attribute accessor.
	 * @return A String containing the message.
	 */
	public String getMessage() {
	    return mMessage;
	}

	/**
	 * JID attribute mutator.
	 * @param bareJid A String containing the author's bare JID of the message.
	 */
	@SuppressWarnings("unused")
	public void setBareJid(String bareJid) {
	    mBareJid = bareJid;
	}

	/**
	 * Name attribute mutator.
	 * @param name A String containing the author's name of the message.
	 */
	@SuppressWarnings("unused")
	public void setName(String name) {
	    mName = name;
	}

	/**
	 * Message attribute mutator.
	 * @param message A String containing a message.
	 */
	public void setMessage(String message) {
	    mMessage = message;
	}

	/**
	 * Get the message type.
	 * @return true if the message is an error message.
	 */
	public boolean isError() {
	    return mIsError;
	}

	/**
	 * Set the Date of the message.
	 * @param date date of the message.
	 */
	public void setTimestamp(Date date) {
	    mTimestamp = date;
	}

	/**
	 * Get the Date of the message.
	 * @return if it is a delayed message get the date the message was sended.
	 */
	public Date getTimestamp() {
	    return mTimestamp;
	}
	/////////type
	public enum MESSAGE_TYPE {
		RECEIVER, SEND , conference;
	}
	////////contenttype
	public enum CONTENT_TYPE {
		TEXT, IMAGE, MAP, VOICE,VIDEO,FILE;
	}
	@Override
	public int compareTo(MessageText oth) {
		if (null == this.getTimestamp() || null == oth.getTimestamp()) {
			return 0;
		}
		Date time1 = null;
		Date time2 = null;
		time1 = this.getTimestamp();
		time2 = oth.getTimestamp();
		return time1.compareTo(time2);
	}
	
	private ArrayList groupMsgRecipient;//群发消息接收人
	public void setGroupMsgRecipient(ArrayList mGroupMsgRecipient){
		this.groupMsgRecipient = mGroupMsgRecipient;
	}
	public ArrayList getGroupMsgRecipient(){
		return this.groupMsgRecipient;
	}
}
