package com.spice.im.chat;

import org.jivesoftware.smack.util.StringUtils;


import android.content.Context;

import com.dodola.model.UnReadMsgNumber;
import com.lidroid.xutils.DbUtils;
import com.lidroid.xutils.db.sqlite.WhereBuilder;
import com.lidroid.xutils.exception.DbException;
import com.stb.core.chat.Message;
public class MessageAdapter {
	private static final String TAG = "MessageAdapter";
	private static MessageAdapter instance = null;
	public static MessageAdapter getInstance(Context context){
		if(instance==null)
			instance = new MessageAdapter(context);
		return instance;
	}
	public MessageAdapter(Context context){
		mContext = context;
		initialDB();	
	}
	
    private Context mContext;
    public void setContext(Context context){
    	mContext = context;
    }
    public Context getContext(){
    	return mContext;
    }
    private DbUtils db;
    public void initialDB(){
    	db = DbUtils.create(mContext);
        db.configAllowTransaction(true);
        db.configDebug(true);
    }
    public static String sqliteEscape(String keyWord){  
        keyWord = keyWord.replace("/", "//");  
        keyWord = keyWord.replace("'", "''");  
        keyWord = keyWord.replace("[", "/[");  
        keyWord = keyWord.replace("]", "/]");  
        keyWord = keyWord.replace("%", "/%");  
        keyWord = keyWord.replace("&","/&");  
        keyWord = keyWord.replace("_", "/_");  
        keyWord = keyWord.replace("(", "/(");  
        keyWord = keyWord.replace(")", "/)");  
        return keyWord;  
    } 
    
    public static String reverseSqliteEscape(String keyWord){  
        keyWord = keyWord.replace("//", "/");  
        keyWord = keyWord.replace("''", "'");  
        keyWord = keyWord.replace("/[", "[");  
        keyWord = keyWord.replace("/]", "]");  
        keyWord = keyWord.replace("/%", "%");  
        keyWord = keyWord.replace("/&","&");  
        keyWord = keyWord.replace("/_", "_");  
        keyWord = keyWord.replace("/(", "(");  
        keyWord = keyWord.replace("/)", ")");  
        return keyWord;  
    } 
    public void deleteMessages(String jid,String CurrentUid){//jid 为 StringUtils.parseBareAddress
    	//12-27 15:35:02.980: D/DbUtils.debugSql(L:779)(1137): SELECT count(id) as count FROM Message WHERE mfrom = '43@1//26' AND mto = '26@0' OR (mto = '26@0' AND mfrom LIKE '43@1%') OR (mfrom = '43@1//26' AND mto = '43@1') OR (mto = '43@1' AND mfrom LIKE '43@1%') LIMIT 1 OFFSET 0
    	try{
    		if(jid.indexOf("@1")==-1)//conference.
    			db.delete(Message.class, WhereBuilder.b("mTo", "=", jid).or("mFrom", "=", jid));
    		else{
    			db.delete(Message.class, WhereBuilder.b("mTo", "=", CurrentUid+"@0").or("mFrom", "=", jid+"//"+CurrentUid));
    			db.delete(Message.class, WhereBuilder.b("mTo", "=", CurrentUid+"@0").or("mFrom", "LIKE", jid+"%"));
    			db.delete(Message.class, WhereBuilder.b("mTo", "=", jid).or("mFrom", "=", jid+"//"+CurrentUid));
    			db.delete(Message.class, WhereBuilder.b("mTo", "=", jid).or("mFrom", "LIKE", jid+"%"));
    		}
//    		db.delete(UnReadMsgNumber.class, WhereBuilder.b("key", "=", StringUtils.parseName(jid)));
    		db.delete(UnReadMsgNumber.class, WhereBuilder.b("key", "=", jid));
    	}catch (DbException e) {
            e.printStackTrace();
            
        } catch (Exception e){
        	e.printStackTrace();
        }
    }
    public void deleteMessage(int id){
    	try{
    		db.delete(Message.class, WhereBuilder.b("id","=",id));
    	}catch (DbException e) {
            e.printStackTrace();
            
        } catch (Exception e){
        	e.printStackTrace();
        }
    }
}
