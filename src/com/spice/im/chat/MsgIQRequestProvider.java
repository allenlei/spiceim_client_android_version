package com.spice.im.chat;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;

public class MsgIQRequestProvider implements IQProvider{
    public MsgIQRequestProvider() {
    }

    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {

    	MsgIQ msgIQ = new MsgIQ();
        for (boolean done = false; !done;) {
            int eventType = parser.next();
            if (eventType == 2) {
                if ("id".equals(parser.getName())) {
                	msgIQ.setId(parser.nextText());
                }
                if ("apikey".equals(parser.getName())) {
                	msgIQ.setApikey(parser.nextText());
                }
                if ("partnerid".equals(parser.getName())) {
                	msgIQ.setPartnerid(parser.nextText());
                }
                if ("fromUser".equals(parser.getName())) {
                	msgIQ.setFromUser(parser.nextText());
                }
                if ("fromUserUid".equals(parser.getName())) {
                	msgIQ.setFromUserUid(parser.nextText());
                }
                if ("fromUserUuid".equals(parser.getName())) {
                	msgIQ.setFromUserUuid(parser.nextText());
                }
                
                if ("msgType1".equals(parser.getName())) {
                	msgIQ.setMsgType1(Integer.parseInt(parser.nextText()));
                }
                if ("toUser".equals(parser.getName())) {
                	msgIQ.setToUser(parser.nextText());
                }
                if ("toUserUid".equals(parser.getName())) {
                	msgIQ.setToUserUid(parser.nextText());
                }
                if ("toUserUuid".equals(parser.getName())) {
                	msgIQ.setToUserUuid(parser.nextText());
                }
                if ("msgType2".equals(parser.getName())) {
                	msgIQ.setMsgType2(Integer.parseInt(parser.nextText()));
                }
                if ("subject".equals(parser.getName())) {
                	msgIQ.setSubject(parser.nextText());
                }
                if ("content".equals(parser.getName())) {
                	msgIQ.setContent(parser.nextText());
                }
                if ("url".equals(parser.getName())) {
                	msgIQ.setUrl(parser.nextText());
                }
                if ("date".equals(parser.getName())) {
                	msgIQ.setDate(parser.nextText());
                }
                if ("jsoninfo".equals(parser.getName())) {
                	msgIQ.setJsoninfo(parser.nextText());
                }
                if ("bak1".equals(parser.getName())) {
                	msgIQ.setBak1(parser.nextText());
                }
                if ("bak2".equals(parser.getName())) {
                	msgIQ.setBak2(parser.nextText());
                }
                if ("bak3".equals(parser.getName())) {
                	msgIQ.setBak3(parser.nextText());
                }
                if ("bak4".equals(parser.getName())) {
                	msgIQ.setBak4(parser.nextText());
                }
                if ("bak5".equals(parser.getName())) {
                	msgIQ.setBak5(parser.nextText());
                }
                if ("bak6".equals(parser.getName())) {
                	msgIQ.setBak6(parser.nextText());
                }
                if ("hash".equals(parser.getName())) {
                	msgIQ.setHash(parser.nextText());
                }
            } else if (eventType == 3
                    && "msgiq".equals(parser.getName())) {//elementName = bindingpartneriq  	
                done = true;
            }
        }
        
        return msgIQ;
        		
    }
}
