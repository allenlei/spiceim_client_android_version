package com.spice.im.chat;

import org.jivesoftware.smack.packet.IQ;

public class MsgIQResponse extends IQ{
	
    private String id;

    private String retcode;//0000 success,0001 false,0002 false(hash校验失败),0003 对方不在线，已发送离线消息,9999
    
    private String memo;//状态描述:成功，失败，。。。
    
    public MsgIQResponse() {
    }

    @Override
    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append("msgiq").append(" xmlns=\"").append(
                "com:isharemessage:msgiq").append("\">");
        if (id != null) {
            buf.append("<id>").append(id).append("</id>");
        }
        if(retcode!=null){
        	buf.append("<retcode>").append(retcode).append("</retcode>");
        }
        if(memo!=null){
        	buf.append("<memo>").append(memo).append("</memo>");
        }
        buf.append("</").append("msgiq").append(">");
        return buf.toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
	public String getRetcode() {
		return retcode;
	}
	public void setRetcode(String retcode) {
		this.retcode = retcode;
	}

	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
}
