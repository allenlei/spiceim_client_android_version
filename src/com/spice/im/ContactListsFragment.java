package com.spice.im;

import java.lang.reflect.Field;

import android.annotation.SuppressLint;
import android.app.Activity;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.spice.im.ContactFrameActivity.ContactGroupListOnClick;
import com.spice.im.ContactFrameActivity.CustomListAdapter;
import com.spice.im.ContactFrameActivity.MyOnRefreshListener2;
@SuppressLint("ValidFragment")
public class ContactListsFragment extends BaseFragment{
	public PullToRefreshListView ptrlv;
	private CustomListAdapter mAdapter;
	private OnPtrlvListener mListener;
	public void setAdapter(CustomListAdapter adapter){
		mAdapter = adapter;
	}
	private ContactGroupListOnClick mOnContactClick;
	public void setOnContactClick(ContactGroupListOnClick onContactClick){
		mOnContactClick = onContactClick;
	}
    private MyOnRefreshListener2 mMyOnRefreshListener2;
	public void setMyOnRefreshListener2(MyOnRefreshListener2 myOnRefreshListener2){
		mMyOnRefreshListener2 = myOnRefreshListener2;
	}
	public ContactListsFragment() {
		super();
	}

	public ContactListsFragment(Activity activity,
			Context context) {
		
		super(activity, context);
		Log.e("MMMMMMMMMMMM20141031ContactFrameActivity(init111)MMMMMMMMMMMM", "++++++++++++++20141031ContactFrameActivity(init111)");
//		mAdapter = adapter;
//		mOnContactClick = onContactClick;
//		mMyOnRefreshListener2 = myOnRefreshListener2;
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.e("MMMMMMMMMMMM20141031ContactFrameActivity(init222)MMMMMMMMMMMM", "++++++++++++++20141031ContactFrameActivity(init222)");
		mView = inflater.inflate(R.layout.fragment_contactlists, container,
				false);
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	protected void initViews() {
		Log.e("MMMMMMMMMMMM20141031ContactFrameActivity(init333)MMMMMMMMMMMM", "++++++++++++++20141031ContactFrameActivity(init333)");
		ptrlv = (PullToRefreshListView) findViewById(R.id.ptrlvHeadLineNews);
	}
//	@Override
//	protected void initEvents() {
//		
//	}
//	@Override
	protected void initEvents() {
		Log.e("MMMMMMMMMMMM20141031ContactFrameActivity(init666)MMMMMMMMMMMM", "++++++++++++++20141031ContactFrameActivity(init666)");
//		ptrlv.setOnItemClickListener(this);
//		ptrlv.setOnRefreshListener(this);
//		ptrlv.setOnCancelListener(this);
        ptrlv.setOnItemClickListener(mOnContactClick);
        ptrlv.setMode(Mode.BOTH);
        ptrlv.setOnRefreshListener(mMyOnRefreshListener2);
        ptrlv.setAdapter(mAdapter);
        
        mListener.onPtrlvSet(ptrlv);
	}

	@Override
	protected void init() {
//		getPeoples();
	}
	
    public interface OnPtrlvListener {
        public void onPtrlvSet(PullToRefreshListView ptrlv);
    }
    
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnPtrlvListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnPtrlvListener");
        }
    }
    
    //解决恶意切换导致的java.lang.IllegalStateException: No activity
    //解决方法重写onDetach()
    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager =
                    Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
