package com.spring4jchome.encrypt.utils;
import javax.crypto.spec.IvParameterSpec;

//import org.apache.commons.codec.binary.Hex;

public class DES3DES {
	public static byte[] biv = "12345678".getBytes();
	public static IvParameterSpec iv = new IvParameterSpec(biv);
	public static String padding = "PKCS5Padding";
	//16位或24位key 用3DES
	public static String enc_3des_cbc(String info, String key, String padding,IvParameterSpec iv) throws Exception {
		//System.out.println("src byte[]="+info.getBytes());
		byte[] infob = info.getBytes("UTF-8");
		
		System.out.print("src byte[]=");
		for(int j=0;j<infob.length;j++){
			System.out.print(infob[j]);
		}
		
		char[] HEX_CHAR = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
        StringBuffer sb = new StringBuffer(2 * infob.length);
        for (int i = 0; i < infob.length; i++) {
            sb.append(HEX_CHAR[(infob[i] >>> 4) & 0X0F]);
            sb.append(HEX_CHAR[infob[i] & 0X0F]);
        }
        System.out.println("src hex="+sb.toString());
        
        
		System.out.println("");
		byte[] bencrypt = Encrypt.enc_3des_cbc(Encrypt.padding(info.getBytes("UTF-8")), key.getBytes(), padding,iv);
		System.out.print("encrypt byte[]=");
		for(int i=0;i<bencrypt.length;i++){
			System.out.print(bencrypt[i]);
		}
		
		return Encrypt.b2hex(bencrypt);
	}
	public static String dec_3des_cbc(String info, String key, String padding,IvParameterSpec iv) throws Exception {
		byte[] bdecrypt = Encrypt.dec_3des_cbc(Encrypt.hex2b(info), key.getBytes(), padding,iv);
		return new String(Encrypt.unpadding(bdecrypt));
	}
	//8位key用DES
	public static String enc_des(String info, String key) throws Exception {
		byte[] bencrypt = Encrypt.enc_des(Encrypt.padding(info.getBytes("UTF-8")), key.getBytes());
		return Encrypt.b2hex(bencrypt);
	}
	 public static String dec_des(String info, String key) throws Exception {
		 byte[] bdecrypt = Encrypt.dec_des(Encrypt.hex2b(info), key.getBytes());
		 return new String(Encrypt.unpadding(bdecrypt));
	 }
	
	public static void main(String args[]){
		try{
			String keysrc16 = "wserrtyuiop12fer";
			String keysrc24 = "wserrtyuiop12fercsffswqh";
			//String src = new String("<?xml version='1.0' encoding='GBK'?><K><ID>001</ID><RCD>0000</RCD><RM>商品银行列表获取成功.</RM><BIFO><BID>01000000</BID><BTP>1</BTP><BNM>北京银联</BNM><FAMT>20000</FAMT><URL>http://pay</URL><RURL>http://code</RURL></BIFO><PID>N3QxYXw2cTJnfDRtN3R8</PID></K>".getBytes("GBK"),"UTF-8");
			String src = "中文测试2009";
			System.out.println(src);
			src = new String(src.getBytes("ISO8859_1"),"UTF8");
			src = GB2U.gb2utf(src);
			String encrypt_src = DES3DES.enc_3des_cbc(src, keysrc24, DES3DES.padding, DES3DES.iv);
			System.out.println("encrypt_src="+encrypt_src);
			String decrypt_src = DES3DES.dec_3des_cbc(encrypt_src, keysrc24, DES3DES.padding, DES3DES.iv);
			System.out.println("decrypt_src="+decrypt_src);
			
			
			String keysrc8 = "1212323a";
			encrypt_src = DES3DES.enc_des(src, keysrc8);
			System.out.println("encrypt_src="+encrypt_src);
			decrypt_src = DES3DES.dec_des(encrypt_src, keysrc8);
			System.out.println("decrypt_src="+decrypt_src);
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
