package com.spring4jchome.encrypt.utils;

public final class GB2U {

	public GB2U() {
		// Default constructor
	}

	public String byte2hex(byte abyte0[]) {
		String s = "";
		String s1 = "";
		for (int i = 0; i < abyte0.length; i++) {
			String s2 = Integer.toHexString(abyte0[i] & 0xff);
			if (s2.length() == 1)
				s = s + "0" + s2;
			else
				s = s + s2;
		}

		return s;
	}

	public String gb2mms(String s) {
		byte abyte0[] = new byte[s.length() * 2 + 2];
		byte abyte1[] = s.getBytes();
		abyte0[0] = (byte) Integer.parseInt("FF", 16);
		abyte0[1] = (byte) Integer.parseInt("FE", 16);
		for (int i = 0; i < s.length() * 2; i++)
			abyte0[i + 2] = abyte1[i];

		return new String(abyte0);
	}

	public static String gb2u(String s) {
		try {
			String sUnicode = new String(s.getBytes("8859_1"), "gb2312");
			s = "";
			for (int i = 0; i < sUnicode.length(); i++) {
				if (sUnicode.charAt(i) < 255) {
					s = s + sUnicode.charAt(i);
				} else {
					String sHex = Integer.toHexString(sUnicode.charAt(i))
							.toUpperCase();
					if (sHex.length() < 4)
						sHex = "0000".substring(0, 4 - sHex.length()) + sHex;
					s = s + "&#x" + sHex + ";";
				}
			}
		} catch (Exception exception) {
		}
		return s;
	}
	public static String gb2eu(String s) {
		try {
			//String sUnicode = new String(s.getBytes("gb2312"), "iso-8859-1");
			String sUnicode = s;
			s = "";
			for (int i = 0; i < sUnicode.length(); i++) {
				if (sUnicode.charAt(i) < 255) {
					s = s + sUnicode.charAt(i);
				} else {
					String sHex = Integer.toHexString(sUnicode.charAt(i))
							.toUpperCase();
					if (sHex.length() < 4)
						sHex = "0000".substring(0, 4 - sHex.length()) + sHex;
					s = s + "&#x" + sHex + ";";
				}
			}
		} catch (Exception exception) {
		}
		return s;
	}
	public static String gb2utf(String s) {
		try {
			String sUnicode = s;
			s = "";
			for (int i = 0; i < sUnicode.length(); i++) {
				if (sUnicode.charAt(i) < 255) {
					s = s + sUnicode.charAt(i);
				} else {
					String sHex = Integer.toHexString(sUnicode.charAt(i))
							.toUpperCase();
					if (sHex.length() < 4)
						sHex = "0000".substring(0, 4 - sHex.length()) + sHex;
					s = s + "&#x" + sHex + ";";
				}
			}
		} catch (Exception exception) {
		}
		return s;
	}
	public String gb2umms(String s) {
		try {
			s = new String(s.getBytes("8859_1"), "gb2312");
			char ac[] = s.toCharArray();
			s = "";
			for (int i = 0; i < ac.length; i++)
				s = s + "\\u" + Integer.toHexString(ac[i]).toUpperCase();

		} catch (Exception exception) {
		}
		return s;
	}

	public String hex2string(String s) {
		byte abyte0[] = new byte[s.length() / 2];
		for (int i = 0; i < s.length() / 4; i++) {
			abyte0[i * 2] = (byte) Integer.parseInt(s.substring(i * 4,
					i * 4 + 2), 16);
			abyte0[i * 2 + 1] = (byte) Integer.parseInt(s.substring(i * 4 + 2,
					i * 4 + 4), 16);
		}

		return new String(abyte0);
	}

	public static String iso2u(String s) {
		try {
			char ac[] = s.toCharArray();
			s = "";
			for (int i = 0; i < ac.length; i++)
				s = s + "&#x" + Integer.toHexString(ac[i]).toUpperCase() + ";";

		} catch (Exception exception) {

		}
		return s;
	}

	public String iso2umms(String s) {
		try {
			char ac[] = s.toCharArray();
			s = "";
			for (int i = 0; i < ac.length; i++)
				s = s + "\\u" + Integer.toHexString(ac[i]).toUpperCase();

		} catch (Exception exception) {

		}
		return s;
	}

	public static void main(String args[]) {
		/*
		if (args.length != 1)
			System.out.println("java com.tom.util.GB2U chinese");
		else {
			System.out.println(GB2U.gb2u(args[0]));
		}*/
		System.out.println(GB2U.gb2u("中国移动手机钱包"));
	}

	/**
	 * 
	 * 
	 * @return java.lang.String
	 * @param ascstr
	 *            java.lang.String
	 */
	public static String asc2u(String ascstr) {
		return ascstr.replaceAll("---", "&#x");
	}

	/**
	 *
	 * 
	 * @return java.lang.String
	 * @param utf8str
	 *            java.lang.String
	 */
	public static String u2asc(String utf8str) {
		return utf8str.replaceAll("&#x", "---");
	}
	
		/**
	 * ASCII to GBK
	 *
	 * 1024k data need 300ms
	 */
	public static String r_gb2u(String src)
	{
		StringBuffer result = new StringBuffer();     
		int len = src.length();
		for (int i = 0; i < len; i++)
		{
			if (src.charAt(i) == '&')
			{
				if (++i >= len) break;
				
				if (src.charAt(i) == '#')
				{
					if (++i >= len) break;
					
					if (src.charAt(i) == 'x')
					{						
						if ((i + 5) < len && src.charAt(i + 5) == ';') {
							String part = src.substring(i + 1, i + 5);
							int nByte1 = 0, nByte2 = 0;
        					nByte1 = Integer.decode("#" + part.substring(0, 2)).byteValue();
        					nByte1 &= 0x000000FF;
        					nByte1 <<= 8;
        					nByte2 = Integer.decode("#" + part.substring(2, 4)).byteValue();
        					nByte2 &= 0x000000FF;
        					
        					char aChar = (char)(nByte1 + nByte2);
        					System.out.println("GB2U b:" + Integer.toHexString((int)aChar));
        					System.out.println("GB2U c:" + aChar);
        					result.append(aChar);
        					i += 5;
						} else {
							int end = Math.min(i + 4, len);
							result.append("&#x");
							result.append(src.substring(i + 1, end));
							i += 4;
						}
					} else {
						result.append("&#").append(src.charAt(i));
					}
				} else {
					result.append('&').append(src.charAt(i));
				}
			} else {
				result.append(src.charAt(i));
			}
		}
		return result.toString();
	}

}
