package com.spring4jchome.encrypt.utils;

import java.security.*;
import java.security.spec.*;

import javax.crypto.*;
import javax.crypto.spec.*;

public class Encrypt {
    public static final int ENCRYPT_MODE = 1;
    public static final int DECRYPT_MODE = 2;
    private static final int DES_SAND = 0x12345678;

    public static byte[] hex2b(byte b[], int offset, int len)
    {
        byte d[] = new byte[len];
        for(int i = 0; i < len * 2; i++)
        {
            int shift = i % 2 != 1 ? 4 : 0;
            d[i >> 1] |= Character.digit((char)b[offset + i], 16) << shift;
        }

        return d;
    }

    public static byte[] hex2b(String s)
    {
        return hex2b(s.getBytes(), 0, s.length() >> 1);
    }
    
    public static String b2hex(byte[] bs) {
        char[] HEX_CHAR = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
        StringBuffer sb = new StringBuffer(2 * bs.length);
        for (int i = 0; i < bs.length; i++) {
            sb.append(HEX_CHAR[(bs[i] >>> 4) & 0X0F]);
            sb.append(HEX_CHAR[bs[i] & 0X0F]);
        }
        System.out.println("encrypt hex="+sb.toString());
        return sb.toString();
    }

    /*
     * 整数->网络序
     */
    private static void int2net(int i, byte[] buf4, int offset) {
        buf4[offset + 0] = (byte) (i >>> 24 & 0xff); //网络顺序
        buf4[offset + 1] = (byte) (i >>> 16 & 0xff);
        buf4[offset + 2] = (byte) (i >>> 8 & 0xff);
        buf4[offset + 3] = (byte) (i & 0xff);
    }

    /*
     * 网络序->整数
     */
    private static int net2int(byte[] buf, int offset) {
        int i = (buf[offset + 0] & 0xff) << 24 | (buf[offset + 1] & 0xff) << 16 | (buf[offset + 2] & 0xff) << 8 | (buf[offset + 3] & 0xff); //网络顺序
        return i;
    }

    /**
     * 消息摘要
     * 
     * @param binfo
     * @param alg
     * @return @throws
     *         NoSuchAlgorithmException
     */
    public static byte[] digest(byte[] binfo, String alg) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance(alg);
        md.update(binfo);
        return md.digest();
    }

    public static String digest(String info, String alg) throws NoSuchAlgorithmException {
        return b2hex(Encrypt.digest(info.getBytes(), alg));
    }

    /**
     * 消息摘要 - MD2
     * 
     * @param binfo
     * @return
     */
    public static byte[] md2(byte[] binfo) {
        try {
            return digest(binfo, "MD2");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return new byte[0];
        }
    }

    public static String md2(String info) {
        return b2hex(md2(info.getBytes()));
    }

    /**
     * 消息摘要 - MD5
     * 
     * @param binfo
     * @return
     */
    public static byte[] md5(byte[] binfo) {
        try {
            return digest(binfo, "MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return new byte[0];
        }
    }

    public static String md5(String info) {
        return b2hex(md5(info.getBytes()));
    }

    /**
     * 消息摘要 - SHA-1
     * 
     * @param binfo
     * @return
     */
    public static byte[] sha_1(byte[] binfo) {
        try {
            return digest(binfo, "SHA-1");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return new byte[0];
        }
    }

    public static String sha_1(String info) {
        return b2hex(sha_1(info.getBytes()));
    }
    public static byte[] cipher(byte[] binfo, byte[] bkey, int mode, String transformation,IvParameterSpec iv ) throws Exception {
        String algs[] = transformation.split("/");
        SecretKey key = new SecretKeySpec(bkey, algs[0]);
        Cipher c1 = Cipher.getInstance(transformation);
        if(mode==1)
        	c1.init(Cipher.ENCRYPT_MODE, key,iv);
        else
        	c1.init(Cipher.DECRYPT_MODE,key,iv);
        return c1.doFinal(binfo);
    }
    
    /**
     * 对称加密算法
     * 
     * @param binfo
     * @param bkey
     * @param mode
     * @param alg
     * @return @throws
     *         Exception
     */
    public static byte[] cipher(byte[] binfo, byte[] bkey, int mode, String transformation) throws Exception {
        String algs[] = transformation.split("/");
        SecretKey key = new SecretKeySpec(bkey, algs[0]);
        Cipher c1 = Cipher.getInstance(transformation);
        c1.init(mode, key);
        return c1.doFinal(binfo);
    }

    public static String cipher(String info, byte[] bkey, int mode, String transformation) throws Exception {
        return b2hex(cipher(info.getBytes(), bkey, mode, transformation));
    }

    /**
     * 对称加密算法 - DES加密
     * 
     * @param binfo
     * @param key
     * @return @throws
     *         Exception
     */
    public static byte[] enc_des(byte[] binfo, byte[] b8key) throws Exception {
        return cipher(binfo, b8key, ENCRYPT_MODE, "DES/ECB/NoPadding");
    }

    public static String enc_des(String info, byte[] b8key) throws Exception {
        return b2hex(enc_des(info.getBytes(), b8key));
    }

    public static byte[] enc_des(byte[] binfo, byte[] b8key, String padding) throws Exception {
        return cipher(binfo, b8key, ENCRYPT_MODE, "DES/ECB/" + padding);
    }

    /**
     * 对称加密算法 - DES解密
     * 
     * @param binfo
     * @param key
     * @return @throws
     *         Exception
     */
    public static byte[] dec_des(byte[] binfo, byte[] b8key) throws Exception {
        return cipher(binfo, b8key, DECRYPT_MODE, "DES/ECB/NoPadding");
    }

    public static String dec_des(String info, byte[] b8key) throws Exception {
        return b2hex(dec_des(info.getBytes(), b8key));
    }

    public static byte[] dec_des(byte[] binfo, byte[] b8key, String padding) throws Exception {
        return cipher(binfo, b8key, DECRYPT_MODE, "DES/ECB/" + padding);
    }

    /**
     * 对称加密算法 - DESede(Triple DES)加密
     * 
     * @param binfo
     * @param key
     * @return @throws
     *         Exception
     */
    public static byte[] enc_3des(byte[] binfo, byte[] bkey) throws Exception {
        return enc_3des(binfo, bkey, "NoPadding");
    }
    
    public static byte[] enc_3des(byte[] binfo, byte[] bkey, String padding) throws Exception {
        byte[] b24key;
        if (bkey.length == 16) {
            b24key = new byte[24];
            System.arraycopy(bkey, 0, b24key, 0, 16);
            System.arraycopy(bkey, 0, b24key, 16, 8);
        } else if (bkey.length == 24) {
            b24key = bkey;
        } else {
            throw new InvalidKeyException("Invalid key length: " + bkey.length + " bytes");
        }
        return cipher(binfo, b24key, ENCRYPT_MODE, "DESede/ECB/" + padding);
        //return 
    }
    
    public static byte[] enc_3des_cbc(byte[] binfo, byte[] bkey, String padding,IvParameterSpec iv) throws Exception {
    	
    	//test start
    	char[] HEX_CHAR = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
        StringBuffer sb = new StringBuffer(2 * binfo.length);
        for (int i = 0; i < binfo.length; i++) {
            sb.append(HEX_CHAR[(binfo[i] >>> 4) & 0X0F]);
            sb.append(HEX_CHAR[binfo[i] & 0X0F]);
        }
        System.out.println("enc_3des_cbc_src hex="+sb.toString());
    	//test end
        byte[] b24key;
        if (bkey.length == 16) {
            b24key = new byte[24];
            System.arraycopy(bkey, 0, b24key, 0, 16);
            System.arraycopy(bkey, 0, b24key, 16, 8);
        } else if (bkey.length == 24) {
            b24key = bkey;
        } else {
            throw new InvalidKeyException("Invalid key length: " + bkey.length + " bytes");
        }
        return cipher(binfo, b24key, ENCRYPT_MODE, "DESede/CBC/" + padding,iv);
        //return 
    }

    public static String enc_3des(String info, byte[] b16key) throws Exception {
        return b2hex(enc_3des(info.getBytes(), b16key));
    }

    /**
     * 对称加密算法 - DESede(Triple DES)解密
     * 
     * @param binfo
     * @param key
     * @return @throws
     *         Exception
     */
    public static byte[] dec_3des(byte[] binfo, byte[] bkey) throws Exception {
        return dec_3des(binfo, bkey, "NoPadding");
    }
    
    public static byte[] dec_3des(byte[] binfo, byte[] bkey, String padding) throws Exception {
        byte[] b24key;
        if (bkey.length == 16) {
            b24key = new byte[24];
            System.arraycopy(bkey, 0, b24key, 0, 16);
            System.arraycopy(bkey, 0, b24key, 16, 8);
        } else if (bkey.length == 24) {
            b24key = bkey;
        } else {
            throw new InvalidKeyException("Invalid key length: " + bkey.length + " bytes");
        }
        return cipher(binfo, b24key, DECRYPT_MODE, "DESede/ECB/" + padding);
    }
    
    public static byte[] dec_3des_cbc(byte[] binfo, byte[] bkey, String padding,IvParameterSpec iv) throws Exception {
        byte[] b24key;
        if (bkey.length == 16) {
            b24key = new byte[24];
            System.arraycopy(bkey, 0, b24key, 0, 16);
            System.arraycopy(bkey, 0, b24key, 16, 8);
        } else if (bkey.length == 24) {
            b24key = bkey;
        } else {
            throw new InvalidKeyException("Invalid key length: " + bkey.length + " bytes");
        }
        return cipher(binfo, b24key, DECRYPT_MODE, "DESede/CBC/" + padding,iv);
    }

    public static String dec_3des(String info, byte[] b16key) throws Exception {
        return b2hex(dec_3des(info.getBytes(), b16key));
    }

    /**
     * DSA数字签名
     * 
     * @param binfo
     * @param prikey
     * @return
     */
    public static byte[] sign(byte[] binfo, byte[] bprikey) throws Exception {
        PKCS8EncodedKeySpec priPKCS8 = new PKCS8EncodedKeySpec(bprikey);
        KeyFactory keyFactory = KeyFactory.getInstance("DSA");
        PrivateKey priKey = keyFactory.generatePrivate(priPKCS8);
        Signature signet = Signature.getInstance("DSA");
        signet.initSign(priKey);
        signet.update(binfo);
        return signet.sign();
    }

    /**
     * DSA数字验签
     * 
     * @param binfo
     * @param sign
     * @param bpubkey
     * @return @throws
     *         Exception
     */
    public static boolean verify(byte[] binfo, byte[] sign, byte[] bpubkey) throws Exception {
        X509EncodedKeySpec bobPubKeySpec = new X509EncodedKeySpec(bpubkey);
        KeyFactory keyFactory = KeyFactory.getInstance("DSA");
        PublicKey pubKey = keyFactory.generatePublic(bobPubKeySpec);
        Signature signet = Signature.getInstance("DSA");
        signet.initVerify(pubKey);
        signet.update(binfo);
        return signet.verify(sign);
    }

    /**
     * 确保8的倍数
     * @param src
     * @return
     */
    public static byte[] padding(byte[] src) {
        byte[] pad;
        int mod = src.length % 8;
        if (mod == 0) {
            pad = new byte[4 + 4 + src.length];
        } else {
            pad = new byte[4 + 4 + src.length + 8 - mod];
        }
        int2net(DES_SAND, pad, 0);
        int2net(src.length, pad, 4);
        System.arraycopy(src, 0, pad, 8, src.length);
        return pad;
    }

    /**
     * padding的反操作
     * @param buf
     * @return
     */
    public static byte[] unpadding(byte[] buf) {
        int sand = net2int(buf, 0);
        if (sand != DES_SAND)
            return null;
        int len = net2int(buf, 4);
        byte[] unpad = new byte[len];
        System.arraycopy(buf, 8, unpad, 0, unpad.length);
        return unpad;
    }

    public static void main(String args[]) throws Exception {
        byte[] bb = enc_des("123456".getBytes(), "20071219".getBytes(), "PKCS5Padding");
        byte[] src = dec_des(bb, "20071219".getBytes(), "PKCS5Padding");
        System.out.println("src = " + new String(src));
        
        String arg = "dsa";
        enc_3des("123456789".getBytes(),"000000000000000000000000".getBytes());
        long t1 = System.currentTimeMillis();
        if (arg.equals("3des")) {
            KeyGenerator keygen = KeyGenerator.getInstance("DESede");
            SecretKey deskey = keygen.generateKey();
            byte[] desEncode = deskey.getEncoded();
            System.out.println("3des key = " + b2hex(desEncode));
        } else if (arg.equals("des")) {
            KeyGenerator keygen = KeyGenerator.getInstance("DES");
            SecretKey deskey = keygen.generateKey();
            byte[] desEncode = deskey.getEncoded();
            System.out.println("des key = " + b2hex(desEncode));
        } else if (arg.equals("dsa")) {
            KeyPairGenerator keygen = KeyPairGenerator.getInstance("DSA");
            KeyPair keys = keygen.generateKeyPair();
            
            long t2 = System.currentTimeMillis();
            System.out.println("generateKeyPair use " + (t2 - t1) + " ms");
            
            PublicKey pubkey = keys.getPublic();
            byte[] pubEncode = pubkey.getEncoded();
            System.out.println("dsa public key = " + b2hex(pubEncode));
            PrivateKey prikey = keys.getPrivate();
            byte[] priEncode = prikey.getEncoded();           
            System.out.println("dsa private key = " + b2hex(priEncode));
            
            long t3 = System.currentTimeMillis();
            System.out.println("get keys use " + (t3 - t2) + " ms");
            
            byte[] info = "大家好".getBytes();
            byte[] bsign = sign(info, priEncode);
            long t4 = System.currentTimeMillis();
            System.out.println("dsa sign = " + b2hex(bsign) + "; use " + (t4 - t3) + " ms");

            sign(info, priEncode);
            long t41 = System.currentTimeMillis();
            System.out.println("sign 2 use " + (t41 - t4) + " ms");

            boolean v = verify(info, bsign, pubEncode);
            long t5 = System.currentTimeMillis();
            System.out.println("dsa verify = " + v + "; use " + (t5 - t4) + " ms");

            verify(info, bsign, pubEncode);
            long t51 = System.currentTimeMillis();
            System.out.println("verify 2 use " + (t51 - t5) + " ms");
        } else if (arg.equals("rsa")) {
            KeyPairGenerator keygen = KeyPairGenerator.getInstance("RSA");
            KeyPair keys = keygen.generateKeyPair();
            PublicKey pubkey = keys.getPublic();
            byte[] pubEncode = pubkey.getEncoded();
            System.out.println("rsa public key = " + b2hex(pubEncode));
            PrivateKey prikey = keys.getPrivate();
            byte[] priEncode = prikey.getEncoded();
            System.out.println("rsa private key = " + b2hex(priEncode));
        }
        System.out.println("use " + (System.currentTimeMillis() - t1) + " ms");
    }
}

