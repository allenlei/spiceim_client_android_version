package com.spring4jchome.encrypt.utils;

public class Test {
	public static void main(String args[]){
		try{
			String salt = "AHLHiq";
			byte[] bkey = salt.getBytes();
			byte[] b24key = new byte[24];
			System.arraycopy(bkey, 0, b24key, 0, 6);
            System.arraycopy(bkey, 0, b24key, 6, 6);
            System.arraycopy(bkey, 0, b24key, 12, 6);
            System.arraycopy(bkey, 0, b24key, 18, 6);
            System.out.println(new String(b24key));
            System.out.println(Base64URLEncodeTool.base64UrlEncode(b24key));
            String rec = "0000AHLHiq";
            System.out.println(rec.substring(4));
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
