package com.spring4jchome.encrypt.utils;

import java.net.URLDecoder;
import java.net.URLEncoder;
//import org.summercool.hsf.test.phone.Base64;
public class Base64URLEncodeTool {
	/**
	 * 去掉字符串的换行符号
	 * base64编码3-DES的数据时，得到的字符串有换行符号
	 * ，一定要去掉，否则uni-wise平台解析票根不会成功， 
	 * 提示“sp验证失败”。在开发的过程中，因为这个问题让我束手无策，
	 * 一个朋友告诉我可以问联通要一段加密后 的文字，然后去和自己生成的字符串比较，
	 * 这是个不错的调试方法。我最后比较发现我生成的字符串唯一不同的 是多了换行。
	 * 我用c#语言也写了票根请求程序，没有发现这个问题。 
	 * 
	     */

	 private static String filter(String str)
	 {
		  String output = null;
		  StringBuffer sb = new StringBuffer();
		  for(int i = 0; i < str.length(); i++)
		  {
		  int asc = str.charAt(i);
		  if(asc != 10 && asc != 13)
		  sb.append(str.subSequence(i, i + 1));
		  }
		  output = new String(sb);
		  return output;
	 }
	 /**
	  * 对字符串进行URLDecoder.encode(strEncoding)编码
	  * @param String src 要进行编码的字符串
	  * 
	  * @return  String 进行编码后的字符串
	  */
	 
	 public static String getURLEncode(String src)
	 {
		  String requestValue="";
		  try{
		  
		  requestValue = URLEncoder.encode(src);
		  }
		  catch(Exception e){
		    e.printStackTrace();
		  }
		  
		  return requestValue;
	 }
	 /**
	  * 对字符串进行URLDecoder.decode(strEncoding)解码
	  * @param String src 要进行解码的字符串
	  * 
	  * @return  String 进行解码后的字符串
	  */
	 
	 public static String getURLDecoderdecode(String src)
	 {   
		  String requestValue="";
		  try{
		  
		  requestValue = URLDecoder.decode(src);
		  }
		  catch(Exception e){
		    e.printStackTrace();
		  }
		  
		  return requestValue;
	 }
	 
	 public static String base64UrlEncode(byte[] raw) throws Exception {
//			BASE64Encoder encoder = new BASE64Encoder();
//			String base64data = encoder.encode(raw);
//			base64data = filter(base64data);
		    String base64data = Base64.encodeToString(raw, Base64.NO_WRAP);
			base64data = getURLEncode(base64data);
			return base64data;
	 }
     public static String decodeBase64URLUserInfo(String userInfo) throws Exception {
			userInfo = getURLDecoderdecode(userInfo);
//			BASE64Decoder decoder = new BASE64Decoder();
			byte[] raw = Base64.decode(userInfo, Base64.NO_WRAP);
//			byte[] raw = decoder.decodeBuffer(userInfo);
			return new String(raw);
	 }
	 
}
