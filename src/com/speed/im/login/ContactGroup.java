package com.speed.im.login;

public class ContactGroup {
	private int type;//0 contact,1 group
	//个人帐户
	private String uid;//帐户对应id号
	private String uuid;//终端id号
	private String username;//登录用户名
	private String name;//真实姓名
	private int sex;//0 默认未知，1男，2女
	private String blood;//A,B,O,AB
	private int marry;//婚姻状态：0默认未知，1单身，2已婚
	private String avatar;//注意avator值的值域：0默认未上传头像，1已上传头像
	private String avatarPath;
	private String email;
	private String mobile;
	private String qq;
	private String birthyear;
	private String birthmonth;
	private String birthday;
	private String birthprovince;//家乡省份
	private String birthcity;//家乡城市
	private String resideprovince;//居住省份
	private String residecity;//居住城市
	private String note;//代表用户更新的状态，可以让好友知道他在做什么
	
	public void setType(int mType){
		type = mType;
	}
	public int getType(){
		return type;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String mUid) {
		uid = mUid;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String mUuid) {
		uuid = mUuid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String mUsername) {
		username = mUsername;
	}
	public String getName() {
		return name;
	}
	public void setName(String mName) {
		name = mName;
	}
	public int getSex() {
		return sex;
	}
	public void setSex(int mSex) {
		sex = mSex;
	}
	public String getBlood() {
		return blood;
	}
	public void setBlood(String mBlood) {
		blood = mBlood;
	}
	public int getMarry() {
		return marry;
	}
	public void setMarry(int mMarry) {
		marry = mMarry;
	}
    public void setAvatar(String mAvatar) {
    	avatar = mAvatar;
    }
    public String getAvatar(){
    	return avatar;
    }
    public void setAvatarPath(String mAvatarPath) {
    	avatarPath = mAvatarPath;
    }
    public String getAvatarPath(){
    	return avatarPath;
    }
    public void setEmail(String mEmail) {
    	email = mEmail;
    }
    public String getEmail(){
    	return email;
    }
    public void setMobile(String mMobile) {
    	mobile = mMobile;
    }
    public String getMobile(){
    	return mobile;
    }
    public void setQq(String mQq) {
    	qq = mQq;
    }
    public String getQq(){
    	return qq;
    }
    public void setBirthyear(String mBirthyear) {
    	birthyear = mBirthyear;
    }
    public String getBirthyear(){
    	return birthyear;
    }
    public void setBirthmonth(String mBirthmonth) {
    	birthmonth = mBirthmonth;
    }
    public String getBirthmonth(){
    	return birthmonth;
    }
    public void setBirthday(String mBirthday) {
    	birthday = mBirthday;
    }
    public String getBirthday(){
    	return birthday;
    }
    public void setBirthprovince(String mBirthprovince) {
    	birthprovince = mBirthprovince;
    }
    public String getBirthprovince(){
    	return birthprovince;
    }
    public void setBirthcity(String mBirthcity) {
    	birthcity = mBirthcity;
    }
    public String getBirthcity(){
    	return birthcity;
    }
    public void setResideprovince(String mResideprovince) {
    	resideprovince = mResideprovince;
    }
    public String getResideprovince(){
    	return resideprovince;
    }
    public void setResidecity(String mResidecity) {
    	residecity = mResidecity;
    }
    public String getResidecity(){
    	return residecity;
    }
    public void setNote(String mNote) {
    	note = mNote;
    }
    public String getNote(){
    	return note;
    }
    
    
	//群组帐户
	private String tagid;//群组id号
	private String tagname;//群组名称
	private String fieldid;//群组所属类别id号
	private String title;//群组所属类别名称
	private String membernum;//群组成员数量
	private String threadnum;//群组在线成员数量
	private String pic;//群组头像pic
	
	private String tid;//群组最新话题id号
	private String subject;//群组最新话题标题
	private String tuid;//群组最新话题作者用户uid
	private String tuuid;//群组最新话题作者用户uuid
	private String tusername;//群组最新话题作者用户username
	private String lastauthorid;//群组最新话题最后更新用户id
	private String lastauthor;//群组最新话题最后更新用户名
	private String viewnum;//群组最新话题被阅读次数
	private String replynum;//群组最新话题被回应（回复）次数
	private String dateline;//群组最新话题发布时间
	private String lastpost;//群组最新话题最后更新时间
	
	public String getTagid() {
		return tagid;
	}
	public void setTagid(String mTagid) {
		tagid = mTagid;
	}
	public String getTagname() {
		return tagname;
	}
	public void setTagname(String mTagname) {
		tagname = mTagname;
	}
	public String getFieldid() {
		return fieldid;
	}
	public void setFieldid(String mFieldid) {
		fieldid = mFieldid;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String mTitle) {
		title = mTitle;
	}
	public String getMembernum() {
		return membernum;
	}
	public void setMembernum(String mMembernum) {
		membernum = mMembernum;
	}
	public String getThreadnum() {
		return threadnum;
	}
	public void setThreadnum(String mThreadnum) {
		threadnum = mThreadnum;
	}
	public String getPic() {
		return pic;
	}
	public void setPic(String mPic) {
		pic = mPic;
	}
	public String getTid() {
		return tid;
	}
	public void setTid(String mTid) {
		tid = mTid;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String mSubject) {
		subject = mSubject;
	}
	
	public String getTuid() {
		return tuid;
	}
	public void setTuid(String mTuid) {
		tuid = mTuid;
	}
	public String getTuuid() {
		return tuuid;
	}
	public void setTuuid(String mTuuid) {
		tuuid = mTuuid;
	}
	public String getTusername() {
		return tusername;
	}
	public void setTusername(String mTusername) {
		tusername = mTusername;
	}
	public String getLastauthorid() {
		return lastauthorid;
	}
	public void setLastauthorid(String mLastauthorid) {
		lastauthorid = mLastauthorid;
	}
	public String getLastauthor() {
		return lastauthor;
	}
	public void setLastauthor(String mLastauthor) {
		lastauthor = mLastauthor;
	}
	public String getViewnum() {
		return viewnum;
	}
	public void setViewnum(String mViewnum) {
		viewnum = mViewnum;
	}
	public String getReplynum() {
		return replynum;
	}
	public void setReplynum(String mReplynum) {
		replynum = mReplynum;
	}
	public String getDateline() {
		return dateline;
	}
	public void setDateline(String mDateline) {
		dateline = mDateline;
	}
	public String getLastpost() {
		return lastpost;
	}
	public void setLastpost(String mLastpost) {
		lastpost = mLastpost;
	}
}
