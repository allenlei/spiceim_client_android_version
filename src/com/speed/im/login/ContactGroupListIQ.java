package com.speed.im.login;

import org.jivesoftware.smack.packet.IQ;

public class ContactGroupListIQ extends IQ{
    //elementName = contactgrouplistiq
	//namespace = com:isharemessage:contactgrouplistiq
    private String id;

    private String apikey;
    
    private String uid;
    
    private String page;// = 0;
	
    private String groupstr = "1";//0,1
	
    private String searchkey = "";
    
    private String view = "me";//view 值域{"me", "hot", "recommend", "manage"},对应我参与的群组、热门群组、推荐群组、我管理的群组等
	
    private String fieldid;// = 0;//0包含所有类型群组，大于0则对应具体类型群组
    
    private String orderby = "threadnum";//orderby 值域{"threadnum", "postnum", "membernum"},对应群组列表排序：按在线人数，提交话题数量，群组成员数量等
    
    private String uuid;
    
    private String hashcode;//apikey+view+orderby+uuid 使用登录成功后返回的sessionid作为密码做3des运算
    
    public ContactGroupListIQ(){}
    
    @Override
    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append("contactgrouplistiq").append(" xmlns=\"").append(
                "com:isharemessage:contactgrouplistiq").append("\">");
        if (id != null) {
            buf.append("<id>").append(id).append("</id>");
        }
        if (apikey != null) {
            buf.append("<apikey>").append(apikey).append("</apikey>");
        }
        if (uid != null) {
            buf.append("<uid>").append(uid).append("</uid>");
        }
        if (page != null) {
            buf.append("<page>").append(page).append("</page>");
        }
        if (groupstr != null) {
            buf.append("<groupstr>").append(groupstr).append("</groupstr>");
        }
        if (searchkey != null) {
            buf.append("<searchkey>").append(searchkey).append("</searchkey>");
        }
        if (view != null) {
            buf.append("<view>").append(view).append("</view>");
        }
        if (fieldid != null) {
            buf.append("<fieldid>").append(fieldid).append("</fieldid>");
        }
        if (orderby != null) {
            buf.append("<orderby>").append(orderby).append("</orderby>");
        }
        if (uuid != null) {
            buf.append("<uuid>").append(uuid).append("</uuid>");
        }
        if (hashcode != null) {
            buf.append("<hashcode>").append(hashcode).append("</hashcode>");
        }
        buf.append("</").append("contactgrouplistiq").append(">");
        return buf.toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

	public String getApikey() {
		return apikey;
	}
	public void setApikey(String apikey) {
		this.apikey = apikey;
	}
	
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
    
	public String getGroupstr() {
		return groupstr;
	}
	public void setGroupstr(String groupstr) {
		this.groupstr = groupstr;
	}
    
	public String getSearchkey() {
		return searchkey;
	}
	public void setSearchkey(String searchkey) {
		this.searchkey = searchkey;
	}
    
	public String getView() {
		return view;
	}
	public void setView(String view) {
		this.view = view;
	}
	
	public String getFieldid() {
		return fieldid;
	}
	public void setFieldid(String fieldid) {
		this.fieldid = fieldid;
	}
	
	public String getOrderby() {
		return orderby;
	}
	public void setOrderby(String orderby) {
		this.orderby = orderby;
	}
	
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getHashcode() {
		return hashcode;
	}
	public void setHashcode(String hashcode) {
		this.hashcode = hashcode;
	}
}
