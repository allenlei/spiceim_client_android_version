//package com.speed.im.login;
//
//import org.jivesoftware.smack.packet.IQ;
//
//public class LoginIQ extends IQ{
//    //elementName = loginiq
//	//namespace = com:isharemessage:loginiq
//    private String id;
//
//    private String apikey;
//    
//    private String username;
//    
//    private String password;
//    
//    private String uuid;
//    
//    private String hash;//apikey+username+uuid ȡsha-1ժҪ
//    
//    public LoginIQ() {
//    }
//
//    @Override
//    public String getChildElementXML() {
//        StringBuilder buf = new StringBuilder();
//        buf.append("<").append("loginiq").append(" xmlns=\"").append(
//                "com:isharemessage:loginiq").append("\">");
//        if (id != null) {
//            buf.append("<id>").append(id).append("</id>");
//        }
//        if (apikey != null) {
//            buf.append("<apikey>").append(apikey).append("</apikey>");
//        }
//        if (username != null) {
//            buf.append("<username>").append(username).append("</username>");
//        }
//        if (password != null) {
//            buf.append("<password>").append(password).append("</password>");
//        }
//        if (uuid != null) {
//            buf.append("<uuid>").append(uuid).append("</uuid>");
//        }
//        if (hash != null) {
//            buf.append("<hash>").append(hash).append("</hash>");
//        }
//        buf.append("</").append("loginiq").append(">");
//        return buf.toString();
//    }
//
//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//	public String getApikey() {
//		return apikey;
//	}
//	public void setApikey(String apikey) {
//		this.apikey = apikey;
//	}
//	
//	public String getUsername() {
//		return username;
//	}
//	public void setUsername(String username) {
//		this.username = username;
//	}
//	
//	public String getPassword() {
//		return password;
//	}
//	public void setPassword(String password) {
//		this.password = password;
//	}
//	
//	public String getUuid() {
//		return uuid;
//	}
//	public void setUuid(String uuid) {
//		this.uuid = uuid;
//	}
//	public String getHash() {
//		return hash;
//	}
//	public void setHash(String hash) {
//		this.hash = hash;
//	}
//	
//}
