package com.speed.im.login;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;


public class ContactGroupListIQResponseProvider implements IQProvider{
	ContactGroup cGroup = null;
	int mType = 0;//0 contact,1 group
	public ContactGroupListIQResponseProvider(){
		
	}
    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {

    	ContactGroupListIQResponse cGroupListIQResponse = new ContactGroupListIQResponse();
        for (boolean done = false; !done;) {
            int eventType = parser.next();
            if (eventType == 2) {
                if ("id".equals(parser.getName())) {
                	cGroupListIQResponse.setId(parser.nextText());
                }
                if ("apikey".equals(parser.getName())) {
                	cGroupListIQResponse.setApikey(parser.nextText());
                }
                if ("totalnumcontact".equals(parser.getName())) {
                	cGroupListIQResponse.setTotalnumcontact(parser.nextText());
                }
                if ("totalnumgroup".equals(parser.getName())) {
                	cGroupListIQResponse.setTotalnumgroup(parser.nextText());
                }
                if ("cgroup".equals(parser.getName())) {
                	cGroup = new ContactGroup();  
                	//parser.getAttributeCount();
                	mType = Integer.parseInt(parser.getAttributeValue(0));
                	cGroup.setType(mType);
                	switch(mType){
	                	case 0:
	                		cGroup.setUid(parser.getAttributeValue(1)!=null?parser.getAttributeValue(1):"");
	                		cGroup.setUuid(parser.getAttributeValue(2)!=null?parser.getAttributeValue(2):"");
	                		cGroup.setUsername(parser.getAttributeValue(3)!=null?parser.getAttributeValue(3):"");
	                		cGroup.setName(parser.getAttributeValue(4)!=null?parser.getAttributeValue(4):"");
	                		cGroup.setSex(parser.getAttributeValue(5)!=null?Integer.parseInt(parser.getAttributeValue(5)):0);
	                		cGroup.setBlood(parser.getAttributeValue(6)!=null?parser.getAttributeValue(6):"");
	                		cGroup.setMarry(parser.getAttributeValue(7)!=null?Integer.parseInt(parser.getAttributeValue(7)):0);
	                		cGroup.setAvatar(parser.getAttributeValue(8)!=null?parser.getAttributeValue(8):"");
	                		cGroup.setAvatarPath(parser.getAttributeValue(9)!=null?parser.getAttributeValue(9):"");
	                		cGroup.setEmail(parser.getAttributeValue(10)!=null?parser.getAttributeValue(10):"");
	                		cGroup.setMobile(parser.getAttributeValue(11)!=null?parser.getAttributeValue(11):"");
	                		cGroup.setQq(parser.getAttributeValue(12)!=null?parser.getAttributeValue(12):"");
	                		cGroup.setBirthyear(parser.getAttributeValue(13)!=null?parser.getAttributeValue(13):"");
	                		cGroup.setBirthmonth(parser.getAttributeValue(14)!=null?parser.getAttributeValue(14):"");
	                		cGroup.setBirthday(parser.getAttributeValue(15)!=null?parser.getAttributeValue(15):"");
	                		cGroup.setBirthprovince(parser.getAttributeValue(16)!=null?parser.getAttributeValue(16):"");
	                		cGroup.setBirthcity(parser.getAttributeValue(17)!=null?parser.getAttributeValue(17):"");
	                		cGroup.setResideprovince(parser.getAttributeValue(18)!=null?parser.getAttributeValue(18):"");
	                		cGroup.setResidecity(parser.getAttributeValue(19)!=null?parser.getAttributeValue(19):"");
	                		cGroup.setNote(parser.getAttributeValue(20)!=null?parser.getAttributeValue(20):"");
	                		cGroup.setUsername(parser.nextText());
	                		break;
	                	case 1:
	                		cGroup.setTagid(parser.getAttributeValue(1)!=null?parser.getAttributeValue(1):"");
	                		cGroup.setTagname(parser.getAttributeValue(2)!=null?parser.getAttributeValue(2):"");
	                		cGroup.setFieldid(parser.getAttributeValue(3)!=null?parser.getAttributeValue(3):"");
	                		cGroup.setTitle(parser.getAttributeValue(4)!=null?parser.getAttributeValue(4):"");
	                		cGroup.setMembernum(parser.getAttributeValue(5)!=null?parser.getAttributeValue(5):"");
	                		cGroup.setThreadnum(parser.getAttributeValue(6)!=null?parser.getAttributeValue(6):"");
	                		cGroup.setPic(parser.getAttributeValue(7)!=null?parser.getAttributeValue(7):"");
	                		cGroup.setTid(parser.getAttributeValue(8)!=null?parser.getAttributeValue(8):"");
	                		cGroup.setSubject(parser.getAttributeValue(9)!=null?parser.getAttributeValue(9):"");
	                		cGroup.setTuid(parser.getAttributeValue(10)!=null?parser.getAttributeValue(10):"");
	                		cGroup.setTuuid(parser.getAttributeValue(11)!=null?parser.getAttributeValue(11):"");
	                		cGroup.setTusername(parser.getAttributeValue(12)!=null?parser.getAttributeValue(12):"");
	                		cGroup.setLastauthorid(parser.getAttributeValue(13)!=null?parser.getAttributeValue(13):"");
	                		cGroup.setLastauthor(parser.getAttributeValue(14)!=null?parser.getAttributeValue(14):"");
	                		cGroup.setViewnum(parser.getAttributeValue(15)!=null?parser.getAttributeValue(15):"");
	                		cGroup.setReplynum(parser.getAttributeValue(16)!=null?parser.getAttributeValue(16):"");
	                		cGroup.setDateline(parser.getAttributeValue(17)!=null?parser.getAttributeValue(17):"");
	                		cGroup.setLastpost(parser.getAttributeValue(18)!=null?parser.getAttributeValue(18):"");
	                		cGroup.setTagname(parser.nextText());
	                		break;
                	}
                	cGroupListIQResponse.cGroups.add(cGroup);
                }
                if ("retcode".equals(parser.getName())) {
                	cGroupListIQResponse.setRetcode(parser.nextText());
                }
                if ("memo".equals(parser.getName())) {
                	cGroupListIQResponse.setMemo(parser.nextText());
                }
            } else if (eventType == 3
                    && "contactgrouplistiq".equals(parser.getName())) {
                done = true;
            }
        }

        return cGroupListIQResponse;
    }
}
