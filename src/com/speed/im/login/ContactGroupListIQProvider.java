package com.speed.im.login;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;


public class ContactGroupListIQProvider implements IQProvider{
    public ContactGroupListIQProvider() {
    }
    @Override
    public IQ parseIQ(XmlPullParser parser) throws Exception {

    	ContactGroupListIQ contactGroupListIQ = new ContactGroupListIQ();
        for (boolean done = false; !done;) {
            int eventType = parser.next();
            if (eventType == 2) {
                if ("id".equals(parser.getName())) {
                	contactGroupListIQ.setId(parser.nextText());
                }
                if ("apikey".equals(parser.getName())) {
                	contactGroupListIQ.setApikey(parser.nextText());
                }
                if ("uid".equals(parser.getName())) {
                	contactGroupListIQ.setUid(parser.nextText());
                }
                if ("page".equals(parser.getName())) {
                	contactGroupListIQ.setPage(parser.nextText());
                }
                if ("groupstr".equals(parser.getName())) {
                	contactGroupListIQ.setGroupstr(parser.nextText());
                }
                if ("searchkey".equals(parser.getName())) {
                	contactGroupListIQ.setSearchkey(parser.nextText());
                }
                if ("view".equals(parser.getName())) {
                	contactGroupListIQ.setView(parser.nextText());
                }
                if ("fieldid".equals(parser.getName())) {
                	contactGroupListIQ.setFieldid(parser.nextText());
                }
                if ("orderby".equals(parser.getName())) {
                	contactGroupListIQ.setOrderby(parser.nextText());
                }
                if ("uuid".equals(parser.getName())) {
                	contactGroupListIQ.setUuid(parser.nextText());
                }
                if ("hashcode".equals(parser.getName())) {
                	contactGroupListIQ.setHashcode(parser.nextText());
                }
            } else if (eventType == 3
                    && "contactgrouplistiq".equals(parser.getName())) {
                done = true;
            }
        }

        return contactGroupListIQ;
    }
}
