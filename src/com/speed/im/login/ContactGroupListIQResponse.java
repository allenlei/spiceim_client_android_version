package com.speed.im.login;

import java.util.ArrayList;

import org.jivesoftware.smack.packet.IQ;

public class ContactGroupListIQResponse extends IQ{
    private String id;

    private String apikey;

    private String totalnumcontact;
    
    private String totalnumgroup;

    public ArrayList cGroups = new ArrayList();
    
    ContactGroup cGroup = null;

    private String retcode;//0000 success,0001 false（用户名或密码错误）,0002 false(hash校验失败),9999
    
    private String memo;//状态描述:登录成功，登录失败（用户名或密码错误）,登录失败（原因）
    
    public ContactGroupListIQResponse() {
    }
    
    @Override
    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append("contactgrouplistiq").append(" xmlns=\"").append(
                "com:isharemessage:contactgrouplistiq").append("\">");
        if (id != null) {
            buf.append("<id>").append(id).append("</id>");
        }
        if (apikey != null) {
            buf.append("<apikey>").append(apikey).append("</apikey>");
        }
        if (totalnumcontact != null) {
            buf.append("<totalnumcontact>").append(totalnumcontact).append("</totalnumcontact>");
        }
        if (totalnumgroup != null) {
            buf.append("<totalnumgroup>").append(totalnumgroup).append("</totalnumgroup>");
        }
        if (cGroups != null && cGroups.size()!=0) {
        	
            buf.append("<cgroups>");
            for(int i=0;i<cGroups.size();i++){
            	cGroup = (ContactGroup)cGroups.get(i);
            	if(cGroup.getType()==0)
	            	buf.append("<cgroup type=\""+cGroup.getType()+"\" "
	            			+"uid=\""+cGroup.getUid()+"\" "
	            			+"uuid=\""+cGroup.getUuid()+"\" "
	            			+"username=\""+cGroup.getUsername()+"\" "
	            			+"name=\""+cGroup.getName()+"\" "
	            			+"sex=\""+cGroup.getSex()+"\" "
	            			+"blood=\""+cGroup.getBlood()+"\" "
	            			+"marry=\""+cGroup.getMarry()+"\" "
	            			+"avatar=\""+cGroup.getAvatar()+"\" "
	            			+"avatarPath=\""+cGroup.getAvatarPath()+"\" "
	            			+"email=\""+cGroup.getEmail()+"\" "
	            			+"mobile=\""+cGroup.getMobile()+"\" "
	            			+"qq=\""+cGroup.getQq()+"\" "
	            			+"birthyear=\""+cGroup.getBirthyear()+"\" "
	            			+"birthmonth=\""+cGroup.getBirthmonth()+"\" "
	            			+"birthday=\""+cGroup.getBirthday()+"\" "
	            			+"birthprovince=\""+cGroup.getBirthprovince()+"\" "
	            			+"birthcity=\""+cGroup.getBirthcity()+"\" "
	            			+"resideprovince=\""+cGroup.getResideprovince()+"\" "
	            			+"residecity=\""+cGroup.getResidecity()+"\" "
	            			+"note=\""+cGroup.getNote()+"\""
	            			+">")
	            			.append(cGroup.getUsername())
	            			.append("</cgroup>");
            	else if(cGroup.getType()==1)
	            	buf.append("<cgroup type=\""+cGroup.getType()+"\" "
	            			+"tagid=\""+cGroup.getTagid()+"\" "
	            			+"tagname=\""+cGroup.getTagname()+"\" "
	            			+"fieldid=\""+cGroup.getFieldid()+"\" "
	            			+"title=\""+cGroup.getTitle()+"\" "
	            			+"membernum=\""+cGroup.getMembernum()+"\" "
	            			+"threadnum=\""+cGroup.getThreadnum()+"\" "
	            			+"pic=\""+cGroup.getPic()+"\" "
	            			+"tid=\""+cGroup.getTid()+"\" "
	            			+"subject=\""+cGroup.getSubject()+"\" "
	            			+"tuid=\""+cGroup.getTuid()+"\" "
	            			+"tuuid=\""+cGroup.getTuuid()+"\" "
	            			+"tusername=\""+cGroup.getTusername()+"\" "
	            			+"lastauthorid=\""+cGroup.getLastauthorid()+"\" "
	            			+"lastauthor=\""+cGroup.getLastauthor()+"\" "
	            			+"viewnum=\""+cGroup.getViewnum()+"\" "
	            			+"replynum=\""+cGroup.getReplynum()+"\" "
	            			+"dateline=\""+cGroup.getDateline()+"\" "
	            			+"lastpost=\""+cGroup.getLastpost()+"\""
	            			+">")
	            			.append(cGroup.getTagname())
	            			.append("</cgroup>");
            }
            buf.append("</cgroups>");
        }
        if(retcode!=null){
        	buf.append("<retcode>").append(retcode).append("</retcode>");
        }
        if(memo!=null){
        	buf.append("<memo>").append(memo).append("</memo>");
        }
        buf.append("</").append("contactgrouplistiq").append(">");
        return buf.toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getTotalnumcontact() {
        return totalnumcontact;
    }

    public void setTotalnumcontact(String totalnumcontact) {
        this.totalnumcontact = totalnumcontact;
    }
    
    public String getTotalnumgroup() {
        return totalnumgroup;
    }

    public void setTotalnumgroup(String totalnumgroup) {
        this.totalnumgroup = totalnumgroup;
    }

    public ArrayList getCGroups() {
        return cGroups;
    }

    public void setCGroups(ArrayList cGroups) {
        this.cGroups = cGroups;
    }
	public String getRetcode() {
		return retcode;
	}
	public void setRetcode(String retcode) {
		this.retcode = retcode;
	}

	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
}
