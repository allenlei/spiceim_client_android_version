//package com.speed.im.login;
//
//import org.jivesoftware.smack.packet.IQ;
//import org.jivesoftware.smack.provider.IQProvider;
//import org.xmlpull.v1.XmlPullParser;
//
//public class LoginIQRequestProvider implements IQProvider{
//    public LoginIQRequestProvider() {
//    }
//
//    @Override
//    public IQ parseIQ(XmlPullParser parser) throws Exception {
//
//    	LoginIQ loginIQ = new LoginIQ();
//        for (boolean done = false; !done;) {
//            int eventType = parser.next();
//            if (eventType == 2) {
//                if ("id".equals(parser.getName())) {
//                	loginIQ.setId(parser.nextText());
//                }
//                if ("apikey".equals(parser.getName())) {
//                	loginIQ.setApikey(parser.nextText());
//                }
//                if ("username".equals(parser.getName())) {
//                	loginIQ.setUsername(parser.nextText());
//                }
//                if ("password".equals(parser.getName())) {
//                	loginIQ.setPassword(parser.nextText());
//                }
//                if ("uuid".equals(parser.getName())) {
//                	loginIQ.setUuid(parser.nextText());
//                }
//                if ("hash".equals(parser.getName())) {
//                	loginIQ.setHash(parser.nextText());
//                }
//            } else if (eventType == 3
//                    && "loginiq".equals(parser.getName())) {//elementName = loginiq  	
//                done = true;
//            }
//        }
//        
//        return loginIQ;
//        		
//    }
//}
