//package com.speed.im.login;
//
//import org.jivesoftware.smack.packet.IQ;
//
//public class LoginIQResponse extends IQ{
//    //elementName = loginiq
//	//namespace = com:isharemessage:loginiq
//    private String id;
//    
//    private String uuid;
//
//    private String retcode;//0000 success,0001 false（用户名或密码错误）,0002 false(hash校验失败),9999
//    
//    private String memo;//状态描述:登录成功，登录失败（用户名或密码错误）,登录失败（原因）
//    
//    private String sessionid;//登录成功后分配的会话密钥
//    
//    private String uid;
//
//    public LoginIQResponse() {
//    }
//
//    @Override
//    public String getChildElementXML() {
//        StringBuilder buf = new StringBuilder();
//        buf.append("<").append("loginiq").append(" xmlns=\"").append(
//                "com:isharemessage:loginiq").append("\">");
//        if (id != null) {
//            buf.append("<id>").append(id).append("</id>");
//        }
//        if (uuid != null) {
//            buf.append("<uuid>").append(uuid).append("</uuid>");
//        }
//        if(retcode!=null){
//        	buf.append("<retcode>").append(retcode).append("</retcode>");
//        }
//        if(memo!=null){
//        	buf.append("<memo>").append(memo).append("</memo>");
//        }
//        if(sessionid!=null){
//        	buf.append("<sessionid>").append(sessionid).append("</sessionid>");
//        }
//        if(uid!=null){
//        	buf.append("<uid>").append(uid).append("</uid>");
//        }
//        buf.append("</").append("loginiq").append(">");
//        return buf.toString();
//    }
//
//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//    public String getUuid() {
//        return uuid;
//    }
//
//    public void setUuid(String uuid) {
//        this.uuid = uuid;
//    }
//	public String getRetcode() {
//		return retcode;
//	}
//	public void setRetcode(String retcode) {
//		this.retcode = retcode;
//	}
//
//	public String getMemo() {
//		return memo;
//	}
//	public void setMemo(String memo) {
//		this.memo = memo;
//	}
//	
//	public String getSessionid() {
//		return sessionid;
//	}
//	public void setSessionid(String sessionid) {
//		this.sessionid = sessionid;
//	}
//    public String getUid() {
//        return uid;
//    }
//
//    public void setUid(String uid) {
//        this.uid = uid;
//    }
//}
